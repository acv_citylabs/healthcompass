var db = require('../models/entry');
var dbUser = require('../models/user');
var dbMeal = require('../models/meal')
var audit = require('../audit-log');
var tokenManager = require('../token_manager');
var jsonwebtoken = require('jsonwebtoken');


//Retrieve last entry
exports.last = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.originalUrl.indexOf("/entries") === 4) {
            //The actor is the patient
            if (req.params.type !== undefined) {
                last(actorID, actorID, { type: req.params.type, subType: req.params.subtype }, function(err, entry) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        return res.json(entry);
                    }
                });
            } else {
                audit.logEvent(actorID, 'Entries', 'Last', '', '', 'failed',
                    'The user could not retrieve the entry because one or more params of the request was not defined');
                return res.sendStatus(400);
            }
        } else {
            //The actor is the doctor
            if (!req.forbidden) {
                if (req.params.type !== undefined && req.params.username !== undefined) {
                    dbUser.userModel.findOne({
                            username: req.params.username
                        }, { _id: 1 })
                        .exec(function(err, user) {
                            if (err) {
                                console.log(err);
                                audit.logEvent('[mongodb]', 'Entries', 'Last', '', '', 'failed', 'Mongodb attempted to retrieve a user');
                                res.status(500).send(err);
                            } else {
                                last(actorID, user._id, { type: req.params.type, subType: req.params.subtype }, function(err, entry) {
                                    if (err) {
                                        res.status(500).send(err);
                                    } else {
                                        return res.json(entry);
                                    }
                                });
                            }
                        });
                } else {
                    audit.logEvent(actorID, 'Entries', 'Last', '', '', 'failed',
                        'The user could not retrieve the entry because one or more params of the request was not defined');
                    return res.sendStatus(400);
                }
            } else {
                return res.sendStatus(403);
            }
        }
    } else {
        audit.logEvent('[anonymous]', 'Entries', 'Last', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
}

// Retrieve entries
exports.list = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.originalUrl.indexOf("/entries") === 4) {
            //The actor is the patient
            if (req.params.type !== undefined) {
                list(actorID, actorID, { type: req.params.type, subType: req.params.subtype }, function(err, entries) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        return res.json(entries);
                    }
                });
            } else {
                audit.logEvent(actorID, 'Entries', 'List', '', '', 'failed',
                    'The user could not retrieve entries because one or more params of the request was not defined');
                return res.sendStatus(400);
            }
        } else {
            //The actor is the doctor
            if (!req.forbidden) {
                if (req.params.type !== undefined && req.params.username !== undefined) {
                    dbUser.userModel.findOne({
                            username: req.params.username
                        }, { _id: 1 })
                        .exec(function(err, user) {
                            if (err) {
                                console.log(err);
                                audit.logEvent('[mongodb]', 'Entries', 'List', '', '', 'failed', 'Mongodb attempted to retrieve a user');
                                res.status(500).send(err);
                            } else {
                                list(actorID, user._id, { type: req.params.type, subType: req.params.subtype }, function(err, entries) {
                                    if (err) {
                                        res.status(500).send(err);
                                    } else {
                                        return res.json(entries);
                                    }
                                });
                            }
                        });
                } else {
                    audit.logEvent(actorID, 'Entries', 'List', '', '', 'failed',
                        'The user could not retrieve entries because one or more params of the request was not defined');
                    return res.sendStatus(400);
                }
            } else {
                return res.sendStatus(403);
            }
        }
    } else {
        audit.logEvent('[anonymous]', 'Entries', 'List', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
}

var list = function(actorID, patientID, config, callback) {
    var query = {
        userID: patientID,
        type: config.type,
        isSkipped: false
    };

    if (config.subType !== undefined && config.subType !== 'undefined') {
        query.subType = config.subType;
    }

    if(config.type === 'newMeals') {
        delete query.type;
        dbMeal.mealModel.find(query).limit(10)
        .sort({ "datetime": -1 })
        .exec(function(err, entries){
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Entries', 'List', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                callback(null, entries);
            }
        });
    } else {
        db.entryModel.find(query)
        .limit(10)
        .sort({ "datetimeAcquisition": -1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Entries', 'List', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                callback(null, entries);
            }
        });
    }
}

var last = function(actorID, patientID, config, callback) {
    var query = {
        userID: patientID,
        type: config.type,
        isSkipped: false
    };

    if (config.subType !== undefined && config.subType !== 'undefined') {
        query.subType = config.subType;
    }

    db.entryModel.findOne(query)
        .sort({ "datetimeAcquisition": -1 })
        .exec(function(err, entry) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Entries', 'Last', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                callback(null, entry);
            }
        });
}

// Add a new entry
exports.create = function(req, res) {
    // console.log("In entries.js - create");
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        var skipped = req.body.skipped || '';
        var value = req.body.value || '';
        var datetimeAcquisition = req.body.datetimeAcquisition || '';
        var comments = req.body.comments || '';
        var subType = req.body.subType || '';
        var values = req.body.values || '';

        var theEntry = {
            userID: actorID,
            type: req.body.type
        }

        if (subType !== '') {
            theEntry.subType = subType;
        }
        if (value !== '') {
            theEntry.value = value;
            // console.log("value :",value,"\nsubType :", subType, "\ntype :", req.body.type, "\nvalues :",values);

        }
        if (values !== '') {
            theEntry.values = values;
        }
        if (datetimeAcquisition !== '') {
            theEntry.datetimeAcquisition = datetimeAcquisition;
        }
        if (comments !== '') {
            theEntry.comments = comments;
        }
        if (skipped !== '') {
            theEntry.isSkipped = skipped;
        }

        if (theEntry.type === 'mobility') {
            db.entryModel.findOne({
                userID: actorID,
                type: 'mobility',
                datetimeAcquisition: {
                    $gt: new Date(theEntry.datetimeAcquisition).setHours(0, 0, 0, 0),
                    $lt: new Date(theEntry.datetimeAcquisition).setHours(23, 59, 59, 999)
                }
            }, {
                _id: 0
            }, function(err, entry) {
                if (entry) {
                    return res.json({ duplicate: true });
                } else {
                    db.entryModel.create(theEntry, function(err, entries) {
                        if (err) {
                            audit.logEvent('[mongodb]', 'Entries', 'Create', '', '', 'failed', 'Mongodb attempted to create an entry');
                            console.log(err);
                            return res.status(500).send(err);
                        } else {
                            //console.log("entries.js - entry.id", entries._id);
                            return res.sendStatus(200);
                        }
                    });
                }
            });
        } else {
            db.entryModel.create(theEntry, function(err, entries) {
                if (err) {
                    audit.logEvent('[mongodb]', 'Entries', 'Create', '', '', 'failed', 'Mongodb attempted to create an entry');
                    console.log(err);
                    return res.status(500).send(err);
                } else {
                    // console.log("entries.js - entry.id", entries._id);
                    return res.status(200).json({
                        inserted: true,
                        entry_id: entries._id
                    });
                }
            });
        }
    } else {
        audit.logEvent('[anonymous]', 'Entries', 'Create', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
}

// Delete an entry
exports.delete = function(req, res) {
    // console.log("In delete entry,");
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.params.id !== undefined) {
            // console.log("entries.js - Object id, to delete : ", req.params.id);
            db.entryModel.remove({ _id: req.params.id }, function(err) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'Entries', 'Delete', '', '', 'failed', 'Mongodb attempted to delete an entry');
                    return res.status(500).send(err);
                } else {
                    return res.status(200).json({
                        entry_id: req.params.id,
                        removed: true,
                    });
                }
            });
        } else {
            audit.logEvent(actorID, 'Entries', 'Delete', '', '', 'failed',
                'The user could not delete the entry because one or more params of the request was not defined');
            return res.sendStatus(400);
        }
    } else {
        audit.logEvent('[anonymous]', 'Entries', 'Delete', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
}

exports.entriesFilter = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.params.type !== undefined) {
            entriesFilter(actorID, { type: req.params.type, subType: req.params.subtype, from: req.params.from, to: req.params.to }, function(err, entries) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        return res.json(entries);
                    }
                });
        } else {
            audit.logEvent(actorID, 'Entries', 'Delete', '', '', 'failed',
                'The user could not delete the entry because one or more params of the request was not defined');
            return res.sendStatus(400);
        }
    } else {
        audit.logEvent('[anonymous]', 'Entries', 'Filter', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
}

var entriesFilter = function(actorID, config, callback) {
    var query = {
        userID: actorID,
        type: {
            $in: JSON.parse(config.type)
        },
        isSkipped: false,
        datetimeAcquisition: {
            $gt: new Date(config.from),
            $lt: new Date(config.to)
        }
    };

    if (config.subType !== undefined && config.subType !== 'undefined') {
        query.subType = config.subType;
    }

    db.entryModel.find(query)
        .sort({ "datetimeAcquisition": -1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Entries', 'Filter', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                callback(null, entries);
            }
        });
}
