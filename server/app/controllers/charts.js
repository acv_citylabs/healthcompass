var dbEntry = require('../models/entry');
var dbUser = require('../models/user');
var audit = require('../audit-log');
var tokenManager = require('../token_manager');
var jsonwebtoken = require('jsonwebtoken');
var cardsList = require('../dashboard/cards.json');
var _ = require('underscore');
var stats = require("stats-lite")
var roundTo = require('round-to');


// Build a chart with a specified patient id, a type of entry and a date range
exports.build = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var user = jsonwebtoken.decode(token);
        var actorID = user.id;
        if (req.originalUrl.indexOf("/charts") === 4) {
            //The actor is the patientvar 
            var patientCondition = user.condition;
            if (req.params.type !== undefined && req.params.from !== undefined && req.params.to !== undefined) {
                if (req.params.type === 'glycaemia') {
                    standardDay(actorID, actorID, { type: req.params.type, from: req.params.from, to: req.params.to }, function(err, chart) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            return res.json(chart);
                        }
                    });
                } else if (req.params.type === 'sport') {
                    sport(actorID, actorID, { from: req.params.from, to: req.params.to }, function(err, chart) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            return res.json(chart);
                        }
                    });
                } else if (req.params.type === 'meal') {
                    meals(actorID, actorID, patientCondition, { from: req.params.from, to: req.params.to, type: req.params.type }, function(err, chart) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            return res.json(chart);
                        }
                    });
                } else if (req.params.type === 'morning' || req.params.type === 'snack10' || req.params.type === 'midday' || req.params.type === 'snack4' || req.params.type === 'evening' || req.params.type === 'bedtime') {
                    insulin(actorID, actorID, { from: req.params.from, to: req.params.to, type: req.params.type }, function(err, chart) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            return res.json(chart);
                        }
                    });
                } else if (req.params.type === 'mobility') {
                    mobility(actorID, actorID, { from: req.params.from, to: req.params.to }, function(err, chart) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            return res.json(chart);
                        }
                    });
                } else if (req.params.type === 'symptoms') {
                    symptoms(actorID, actorID, { from: req.params.from, to: req.params.to }, function(err, chart) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            return res.json(chart);
                        }
                    });
                } else if (req.params.type === 'bloodpressure') {
                    bloodpressure(actorID, actorID, { from: req.params.from, to: req.params.to }, function(err, chart) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            return res.json(chart);
                        }
                    });
                } else if (req.params.type === 'heartrate') {
                    heartrate(actorID, actorID, { from: req.params.from, to: req.params.to }, function(err, chart) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            return res.json(chart);
                        }
                    });
                } else if (req.params.type === 'salt' || req.params.type === 'liquid') {
                    saltOrLiquid(actorID, actorID, req.params.type, { from: req.params.from, to: req.params.to }, function(err, chart) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            return res.json(chart);
                        }
                    });

                } else if(req.params.type === 'newGly') {
                    newGlycaemia(actorID, actorID, {type: req.params.type, from: req.params.from, to: req.params.to}, function(err, chart) {
                        if(err) {
                            res.status(500).send(err);
                        } else {
                            return res.json(chart);
                        }
                    });
                } else {
                    classic(actorID, actorID, { type: req.params.type, from: req.params.from, to: req.params.to }, function(err, chart) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            return res.json(chart);
                        }
                    });
                }
            } else {
                audit.logEvent(actorID, 'Charts', 'Build', '', '', 'failed',
                    'The user could not build a chart because one or more params of the request was not defined');
                return res.sendStatus(400);
            }
        } else {
            //The actor is the doctor
            if (!req.forbidden) {
                if (req.params.username !== undefined && req.params.type !== undefined && req.params.from !== undefined && req.params.to !== undefined) {
                    dbUser.userModel.findOne({
                            username: req.params.username
                        }, { _id: 1 })
                        .exec(function(err, user) {
                            if (err) {
                                console.log(err);
                                audit.logEvent('[mongodb]', 'Charts', 'Build', '', '', 'failed', 'Mongodb attempted to retrieve a user');
                                res.status(500).send(err);
                            } else {
                                if (req.params.type === 'glycaemia') {
                                    standardDay(actorID, user._id, { type: req.params.type, from: req.params.from, to: req.params.to}, function(err, chart) {
                                        if (err) {
                                            res.status(500).send(err);
                                        } else {
                                            return res.json(chart);
                                        }
                                    });
                                } else if(req.params.type === 'newGly') {
                                    newGlycaemia(actorID, user._id, {type: req.params.type, from: req.params.from, to: req.params.to, matrixconfig: req.params.matrixconfig, dateconfig: req.params.dateconfig}, function(err, chart) {
                                        if(err) {
                                            res.status(500).send(err);
                                        } else {
                                            return res.json(chart);
                                        }
                                    });
                                }
                                else {
                                    classic(actorID, user._id, { type: req.params.type, from: req.params.from, to: req.params.to }, function(err, chart) {
                                        if (err) {
                                            res.status(500).send(err);
                                        } else {
                                            return res.json(chart);
                                        }
                                    });
                                }
                            }
                        });
                } else {
                    audit.logEvent(actorID, 'Charts', 'Build', '', '', 'failed',
                        'The user could not build a chart because one or more params of the request was not defined');
                    return res.sendStatus(400);
                }
            } else {
                return res.sendStatus(403);
            }
        }
    } else {
        audit.logEvent('[anonymous]', 'Charts', 'Build', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
}


//Build a classic chart
var classic = function(actorID, patientID, config, callback) {
    dbEntry.entryModel.find({
            userID: patientID,
            type: config.type,
            datetimeAcquisition: {
                $gt: new Date(config.from),
                $lt: new Date(config.to).setHours(23, 59, 59, 999)
            }
        })
        .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Charts', 'Build classic chart', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                var chart = [];
                entries.forEach(function(item) {
                    if (item.value) {
                        chart.push([new Date(item.datetimeAcquisition).getTime(), parseFloat(item.value)]);
                    }
                });
                callback(null, chart);
            }
        });
}

//Build a glycaemia chart (standard day)
var standardDay = function(actorID, patientID, config, callback) {
    // Define the chart data structure
    var result = {
        series: [],
        mean: 0, 
        max: 0,
        min: 0
    };
    var nb = 0,
        total = 0,
        min = 500,
        max = 0;

    function myLoop(d) {
        if (d <= new Date(config.to)) {
            classic(actorID, patientID, {
                type: config.type,
                from: new Date(d).setHours(0, 0, 0, 0),
                to: new Date(d).setHours(23, 59, 59, 999)
            }, function(err, data) {
                if (err) {
                    callback(err);
                } else {
                    if (data.length > 0) {
                        var tooltips = [];
                        var chart = [];
                        for (var i = 0; i < data.length; i++) {
                            // Calculate mean 
                            total = total + data[i][1];

                            if (data[i][1]>max){
                                max=data[i][1];
                            }
                            if (data[i][1]<min){
                                min=data[i][1]
                            }
                            nb++;

                            // Build the new chart
                            chart.push({
                                x: new Date(2015, 0, 1, new Date(data[i][0]).getHours(), new Date(data[i][0]).getMinutes(), 0, 0).getTime(),
                                y: data[i][1],
                                origdate: new Date(data[i][0])
                            })
                        }
                        // Add this new series to the main result structure
                        result.series.push({
                            data: chart,
                            color: '#D32F2F',
                            fillOpacity: 0.2,
                            marker: { symbol: 'circle' }
                        });
                    }
                    myLoop(new Date(d.setDate(d.getDate() + 1)));
                }
            });
        } else {
            if (nb > 0) {
                result.mean = total / nb;
                result.min = min;
                result.max = max;
                //console.log("Mean,min,max :",result.mean,result.min,result.max);
            }
            callback(null, result);
        }
    }
    myLoop(new Date(config.from));
}

//Build a new glycaemia chart
var newGlycaemia = function (actorID, patientID, config, callback) {

    var matrixconfig = config.matrixconfig;
    var dateconfig = config.dateconfig;
    var multipleDates = [];
    var result;
    var queryResult;
    var dailyGlucose;
    var seriesData;
    var from, to;

    if(dateconfig == 'datepick') {
        //console.log('DATE PICK');
        var datesSelected = config.from;
        var current;
        var MyDateString;
        var datepickMax = 0;
        var datepickmin = new Date();

        var datesSplit = datesSelected.split(",").map(function (val) {
          current = new Date(val);
          if(current > datepickMax) { datepickMax = current; }
          if(current < datepickmin) { datepickmin = current; }
          MyDateString = current.getFullYear() + '-'
             + ('0' + (current.getMonth()+1)).slice(-2) + '-'
             + ('0' + current.getDate()).slice(-2);
          multipleDates.push({timePart: '' + MyDateString + ''});
        });
        // console.log('MIN', datepickmin);
        // console.log('MAX:',datepickMax);
        
        queryResult = dbEntry.entryModel.aggregate([
            {
                $project: {
                    timePart: {
                        $dateToString: {
                            format: '%Y-%m-%d',
                            date: '$datetimeAcquisition'
                        } 
                    },
                    date: 1,
                    value: 1,
                    datetimeAcquisition: 1,
                    type: 1,
                    userID: 1                    
                }
            },{
                $match: {
                    $and: [
                        {
                            $or: multipleDates
                        },
                        {
                            type: 'glycaemia',
                            userID: patientID.toString()
                        }
                    ]
                }
            },{
                $project: {
                    date: 1,
                    value: 1,
                    datetimeAcquisition: 1,
                    type: 1
                }
            }
        ]);
    } else  {
        //console.log('DATE RANGE');
        if(dateconfig == 'dailyGlucose') {
            //custom 1 day view
            //we need to get all values, but separate them. 1day goes to the agp, the rest goes to daily glucose
            from = new Date(new Date(new Date(config.from).setDate(new Date(config.from).getDate() - 14)).setHours(1,0,0,0));
            to = new Date(new Date(config.to).setHours(24, 59, 59, 999));
        } else {
            //normal 1 day view
            from = new Date(new Date(config.from).setHours(1, 0, 0, 0));
            to = new Date(new Date(config.to).setHours(24, 59, 59, 999));
        }        
        // console.log('FROM:',from);
        // console.log('TO:',to);

        if ((Math.floor((to - from) / 86400000)) > 31) {
            from = new Date(from.getFullYear(), from.getMonth(), 1);
        }

        queryResult = dbEntry.entryModel.find({
                userID: patientID,
                type: 'glycaemia',
                datetimeAcquisition: {
                    $gt: from,
                    $lt: to
                },
                isSkipped: false
            });
    }    
    //query for specific dates, when values for multiple, non-continuous dates have to be found
    // Retrieve glycaemia entries from db
        queryResult
        .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if(err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Charts', 'Build new glycaemia', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                //console.log(entries, '==============');

                if(dateconfig == 'dailyGlucose') {
                    seriesData = calculateSeriesData(entries, new Date(new Date(to).setHours(-24,0,0,0)).getTime()); 
                    //console.log('TO:', new Date(new Date(new Date(to).setHours(-24,0,0,0)).getTime()));
                } else if(dateconfig == 'datepick') {
                    seriesData = calculateSeriesData(entries, datepickmin);
                } else {
                    //console.log('FROM NOW:', from);
                    seriesData = calculateSeriesData(entries, from); 
                }

                //if default views are used, 'to' is always today. In case of a datepicker, the 'to' can be freely selected by the user, thus, making it different than the today's date
                if(dateconfig !== 'datepick') {
                    dailyGlucose = calculateDailyGlucose(entries, config.to);
                } else if(dateconfig == 'datepick') {
                    dailyGlucose = calculateDailyGlucose(entries, datepickMax);
                }

                //console.log(seriesData);
                result = {
                    series: [{
                         name: 'Median',
                         data: [],
                         connectNulls: true,
                         zIndex: 1,
                         type: 'spline',
                         color: '#003399',
                         marker: {
                             fillColor: '#6699aa',
                             lineWidth: 0.5,
                             lineColor: 'blue',
                             enabled: false,
                             symbol: 'circle'
                            }
                        },{
                             name: '25th to 75th percentile',
                             data: [],
                             showInLegend :true,
                             type: 'arearange',
                             lineWidth: 0,
                             color: '#6699ff',
                             fillOpacity: 0.70,
                             zIndex: 0,
                             marker: {
                                 enabled: false,
                                 symbol: 'circle'
                             }
                         },{
                             name: '10th to 90th percentile',
                             data: [],
                             showInLegend :true,
                             type: 'arearange',
                             lineWidth: 0,
                             color: '#b3ccff',//'rgba(66,215,244,0.4)',
                             fillOpacity: 0.5,
                             zIndex: 0,
                             marker: {
                                fillColor: 'rgba(66,215,244,0.4)',
                                lineWidth: 2,
                                lineColor: 'rgba(0, 66, 204,0.4)',
                                enabled: false,
                                symbol: 'circle'
                             }
                         }],
                    mealTimes: [],
                    eventTimes: [],
                    dailyGlucose: []
                };

                if(seriesData[0].length > 0) {
                    result.dailyGlucose = dailyGlucose;
                    result.series[0].data = seriesData[0];
                    result.series[1].data = seriesData[2];
                    result.series[2].data = seriesData[1];
                
                    if(matrixconfig === 'events') {
                        //find event times 
                        to = new Date(new Date(config.to)); //set the 'to' to midnight of the current day
                        var queryEvents = {
                            userID: patientID,
                            $or: [
                                { 
                                    type: { $in: ['insulin','meal']}
                                },
                                {
                                    type: 'activity',
                                    subType: 'sport',
                                    "values.type": 'intensity',
                                    "values.value": '5',
                                }
                            ], 
                            datetimeAcquisition: {
                                $gt: from,
                                $lt: to
                            },
                            isSkipped: false
                        };
                        if(dateconfig == 'dailyGlucose') {  
                            queryEvents.datetimeAcquisition['$gt'] = new Date(new Date(from).setDate(new Date(from).getDate() + 14)).setHours(1,0,0,0);
                            //console.log('EVENTS BOUNDARY', new Date(new Date(new Date(from).setDate(new Date(from).getDate() + 14)).setHours(1,0,0,0))); 
                        }
                        
                      dbEntry.entryModel.find(queryEvents, {datetimeAcquisition: 1 , type: 1, subType: 1, value: 1, _id: 0})
                        .sort({ "datetimeAcquisition": 1 })
                        .exec(function(err, entries) {
                            if(err) {
                                console.log(err);
                                audit.logEvent('[mongodb]', 'Charts', 'Find event times', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                                callback(err);
                            } else {
                                entries.forEach(function (entry) {
                                    //console.log(entry);
                                    result.eventTimes.push({ name: entry.subType, type: entry.type, data: new Date(entry.datetimeAcquisition).getHours(), value: entry.value});
                                });
                                //console.log('NUMBER OF EVENTS', result.eventTimes.length);
                                callback(null, result);
                            }
                        });
                    } else {
                      //find meal and sleep times
                      dbEntry.entryModel.find({
                            userID: patientID,
                            type: 'meal',
                            datetimeAcquisition: {
                                $gt: from,
                                $lt: to
                            },
                            isSkipped: false
                        },{datetimeAcquisition: 1 , _id: 0})
                        .sort({ "datetimeAcquisition": 1 })
                        .exec(function(err, entries) {
                            if(err) {
                                console.log(err);
                                audit.logEvent('[mongodb]', 'Charts', 'Find meal and sleep times', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                                callback(err);
                            } else {
                                //console.log('Entries: ', entries);
                                var breakfast = [], lunch = [], dinner = [], sleep = [];
                                //take the entries between 7 and 10 for breakfast, 10 and 15 for lunch and 15 to 20 for dinner
                                entries.forEach(function (entry) {
                                    if(new Date(entry.datetimeAcquisition).getHours() >= new Date(new Date().setHours(7, 0, 0)).getHours() && new Date(entry.datetimeAcquisition).getHours() <= new Date(new Date().setHours(10, 0, 0)).getHours()) {
                                        breakfast.push(new Date(entry.datetimeAcquisition).getHours());
                                    } else if (new Date(entry.datetimeAcquisition).getHours() >= new Date(new Date().setHours(12, 0, 0)).getHours() && new Date(entry.datetimeAcquisition).getHours() <= new Date(new Date().setHours(15, 0, 0)).getHours()) {
                                        lunch.push(new Date(entry.datetimeAcquisition).getHours());
                                    } else if(new Date(entry.datetimeAcquisition).getHours() >= new Date(new Date().setHours(15, 0, 0)).getHours() && new Date(entry.datetimeAcquisition).getHours() <= new Date(new Date().setHours(20, 0, 0)).getHours()) {
                                        dinner.push(new Date(entry.datetimeAcquisition).getHours());
                                    }
                                });

                                var medianBreakfast = Math.round(stats.median(breakfast));
                                var medianLunch = Math.round(stats.median(lunch));
                                var medianDinner = Math.round(stats.median(dinner));
                                var medianSleep = 0;

                                if(isNaN(medianBreakfast)) {
                                    medianBreakfast = 8;
                                }

                                if(isNaN(medianLunch)) {
                                    medianLunch = 12;
                                }
                                
                                if(isNaN(medianDinner)) {
                                    medianDinner = 18;
                                }

                                if(!isNaN(medianDinner)) {
                                    medianSleep = medianDinner + 3;
                                } else {
                                    medianSleep = 22;
                                }
                                
                                //console.log(medianBreakfast + ' ' + medianLunch + ' ' + medianDinner + ' ' + medianSleep);

                                result.mealTimes[0] = {name:'breakfast', data: medianBreakfast};
                                result.mealTimes[1] = {name:'lunch', data: medianLunch};
                                result.mealTimes[2] = {name:'dinner', data: medianDinner};
                                result.mealTimes[3] = {name:'sleep', data: medianSleep};

                                callback(null, result);
                            }
                        });

                    }
                } else {
                    callback(null, result);
                }
            }
        }); //END find meal and sleep times

    
    //calculate the series data for the AGP
    function calculateSeriesData(entries, boundmax) {
        /*Array seriesData
          [0] - median values, 50th percentile
          [1] - 10th to 90th percentiles
          [2] - 25th to 75th percentiles
        */
        //console.log('boundmax:', new Date(boundmax));
        //console.log('CALCULATE SERIES');
        var seriesData = [[],[],[]];

        if(entries.length > 0) {
                var hours = {
                    0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6:[], 7: [], 8:[], 9:[], 10: [], 11: [], 12: [], 13: [], 14: [], 15: [],
                    16: [], 17: [], 18: [], 19:[], 20:[], 21: [], 22: [], 23: [], 24: []
                };
                entries.forEach(function (entry) {
                    if(new Date(entry.datetimeAcquisition).getTime() > boundmax) {
                        var position = new Date(entry.datetimeAcquisition).getHours();
                        hours[position].push(entry.value);
                        //console.log(entry.value);
                    }                    
                });

                var i = 0;
                for(var key in hours) {
                        hours[key].sort(function(a, b){return a-b}); //sort each array (sort as string)
                        var med;
                        if(i === 24) {
                            if(!isNaN(stats.median(hours[0]))) { med = roundTo(stats.median(hours[0]), 2) } else { med = null; }
                            var median = [new Date(new Date().setDate(3)).setHours(i, 0, 0, 0), med];
                            var percentile1090 = [new Date(new Date().setDate(3)).setHours(i, 0, 0, 0), roundTo(stats.percentile(hours[0], 0.10), 2), roundTo(stats.percentile(hours[0], 0.90), 2)]; 
                            var percentile2575 = [new Date(new Date().setDate(3)).setHours(i, 0, 0, 0), roundTo(stats.percentile(hours[0], 0.25), 2), roundTo(stats.percentile(hours[0], 0.75), 2)];
                        } else {
                            if(!isNaN(stats.median(hours[key]))) { med = roundTo(stats.median(hours[key]), 2) } else { med = null; }
                            var median = [new Date(new Date().setDate(3)).setHours(i, 0, 0, 0), med];
                            var percentile1090 = [new Date(new Date().setDate(3)).setHours(i, 0, 0, 0), roundTo(stats.percentile(hours[key], 0.10), 2), roundTo(stats.percentile(hours[key], 0.90), 2)]; 
                            var percentile2575 = [new Date(new Date().setDate(3)).setHours(i, 0, 0, 0), roundTo(stats.percentile(hours[key], 0.25),2), roundTo(stats.percentile(hours[key], 0.75), 2)];
                        }
                        seriesData[0].push(median);
                        seriesData[1].push(percentile1090);
                        seriesData[2].push(percentile2575);
                    i++;
                }
            }
        return seriesData;
    };

    //separate entries for the daily glucose charts
    function calculateDailyGlucose(entries, datemax) {
        //datemax = config.to OR datepickmax
        //lowerTimeBound = a time two weeks before the config.to (e.g. past two weeks)
        //today = upper time bound

        var dates = [];
        var today;
        var lowerTimeBound = new Date(new Date(new Date(new Date(datemax).setDate(new Date(datemax).getDate() - 14)).setHours(0,0,0,0))).getTime();
        //console.log('TIMEBOUND', new Date(lowerTimeBound));

        if(dateconfig == 'daterange' || dateconfig == 'dailyGlucose') {
            today = new Date(new Date(new Date(new Date(datemax).setDate(new Date(datemax).getDate())).setHours(0,0,0,0))).getTime();
            //console.log('dailyGlcuose');
        } else {
            today = new Date(new Date(new Date().setDate(new Date().getDate())).setHours(0,0,0,0)).getTime(); //TODAY SHOULD REALLY BE TODAY UNLESS DATERANGE OR DATE PICKER ARE USED
            //console.log('something else');
        }        
        //console.log('TODAY:', new Date(today), today);
        //console.log('lowerTimeBound:', lowerTimeBound);
        //console.log('TWO WEEKS', twoWeeksAgo);

        entries.forEach(function (entry) {
            if(new Date(entry.datetimeAcquisition).getTime() >= lowerTimeBound && new Date(entry.datetimeAcquisition).getTime() <= today) {
                dates.push([entry.value, new Date(entry.datetimeAcquisition).getTime()]);
            }            
        });
        return dates;
    };
};

// Build a weight chart
var weight = function(actorID, patientID, config, callback) {
    classic(actorID, patientID, { type: 'weight', from: config.from, to: config.to }, function(err, chart) {
        if (err) {
            callback(err);
        } else {
            var average = 0;
            for (var i = 0; i < chart.length; i++) {
                average += chart[i][1];
            }
            callback(null, [average / chart.length]);
        }
    });
}

//Build a blood pressure chart
var bloodpressure = function(actorID, patientID, config, callback) {
    dbEntry.entryModel.find({
            userID: patientID,
            type: 'bloodpressure',
            datetimeAcquisition: {
                $gt: new Date(config.from),
                $lt: new Date(config.to).setHours(23, 59, 59, 999)
            },
            isSkipped: false
        })
        .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Charts', 'Build blood pressure', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                var result = {
                    series: [{
                        name: 'Systolic',
                        data: [],
                        color: '#6200ea',
                        pointPlacement: 'on'
                    }, {
                        name: 'Diastolic',
                        data: [],
                        color: '#00BCD4',
                        pointPlacement: 'on'
                    }]

                };
                for (var i = 0; i < entries.length; ++i) {
                    result.series[0].data.push([new Date(entries[i].datetimeAcquisition).getTime(), Number(entries[i].values[0].value)]);
                    result.series[1].data.push([new Date(entries[i].datetimeAcquisition).getTime(), Number(entries[i].values[1].value)]);
                }
                callback(null, result);
            }
        });
}


//Build a heart rate chart
var heartrate = function(actorID, patientID, config, callback) {
    dbEntry.entryModel.find({
            userID: patientID,
            type: 'heartrate',
            datetimeAcquisition: {
                $gt: new Date(config.from),
                $lt: new Date(config.to).setHours(23, 59, 59, 999)
            },
            isSkipped: false
        })
        .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Charts', 'Build heart rate', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                var result = {
                    series: [{
                            name: 'After exercise',
                            data: [],
                            color: '#ff8a80',
                            pointPlacement: 'on'
                        }, {
                            name: 'At rest',
                            data: [],
                            color: '#b388ff',
                            pointPlacement: 'on'
                        }
                        /*,{
                                            name: 'Global',
                                            data: [],
                                            color: '#212121',
                                            pointPlacement: 'on'
                                        }*/
                    ]
                };
                for (var i = 0; i < entries.length; ++i) {
                    //result.series[2].data.push([new Date(entries[i].datetimeAcquisition).getTime(), Number(entries[i].value)]);
                    if (entries[i].subType == 'atRest')
                        result.series[1].data.push([new Date(entries[i].datetimeAcquisition).getTime(), Number(entries[i].value)]);
                    else
                        result.series[0].data.push([new Date(entries[i].datetimeAcquisition).getTime(), Number(entries[i].value)]);
                }
                callback(null, result);
            }
        });
}

//Build a steps chart
var steps = function(actorID, patientID, config, callback) {
    classic(actorID, patientID, { type: 'steps', from: config.from, to: config.to }, function(err, chart) {
        if (err) {
            callback(err);
        } else {
            var average = 0;
            for (var i = 0; i < chart.length; i++) {
                average += chart[i][1];
            }
            callback(null, [average / chart.length]);
        }
    });
}

// Build a sport chart
var sport = function(actorID, patientID, config, callback) {
    var from = new Date(new Date(config.from).setHours(0, 0, 0, 0));
    var to = new Date(new Date(config.to).setHours(23, 59, 59, 999));
    if ((Math.floor((to - from) / 86400000)) > 31) {
        from = new Date(from.getFullYear(), from.getMonth(), 1);
    }

    // Retrieve sport entries
    dbEntry.entryModel.find({
            userID: patientID,
            type: 'activity',
            subType: 'sport',
            datetimeAcquisition: {
                $gt: from,
                $lt: to
            },
            isSkipped: false
        })
        .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Charts', 'Build sport', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                // Define the chart data structure
                var result = {
                    categories: [],
                    series: [
                        /*{
                            name: 'Very light',
                            data: [],
                            color: '#'
                        },*/
                        {
                            name: 'Light',
                            data: [],
                            color: '#E1BEE7'
                        }, {
                            name: 'Moderate',
                            data: [],
                            color: '#BA68C8'
                        }, {
                            name: 'Vigorous',
                            data: [],
                            color: '#9C27B0'
                        }
                        /*{
                                            name: 'Very vigorous',
                                            data: [],
                                            color: '#9C27B0'
                                        }*/
                    ]
                };

                // If there is any entry
                if (entries.length > 0) {
                    var chart = [];
                    // The actor asked for a view per WEEK
                    if ((Math.floor((to - from) / 86400000)) <= 31) {
                        var weeks = [];
                        // Loop over entries array and build result array 
                        for (i = 0; i < entries.length; i++) {
                            // If init OR If the datetime week number of curent entry is the same as the last treated entry
                            if ((i === 0) || (new Date(entries[i].datetimeAcquisition).getWeekNumber() ===
                                    new Date(weeks[weeks.length - 1].datetime).getWeekNumber())) {
                                weeks.push({
                                    datetime: new Date(entries[i].datetimeAcquisition).getTime(),
                                    value: parseFloat(entries[i].value),
                                    intensity: entries[i].values[0].value
                                });
                            } else {
                                chart.push({
                                    rangeOfWeek: getDateRangeOfWeek(
                                        new Date(weeks[weeks.length - 1].datetime).getWeekNumber(),
                                        new Date(weeks[weeks.length - 1].datetime).getFullYear()
                                    ),
                                    data: weeks
                                });

                                // Restart with a new weeks array
                                weeks = [];
                                weeks.push({
                                    datetime: new Date(entries[i].datetimeAcquisition).getTime(),
                                    value: parseFloat(entries[i].value),
                                    intensity: entries[i].values[0].value
                                });
                            }

                            // For the last entry
                            if (i === entries.length - 1) {
                                chart.push({
                                    rangeOfWeek: getDateRangeOfWeek(
                                        new Date(weeks[weeks.length - 1].datetime).getWeekNumber(),
                                        new Date(weeks[weeks.length - 1].datetime).getFullYear()
                                    ),
                                    data: weeks
                                });
                            }
                        }

                        // Found the week number max
                        var max;
                        if (new Date(config.to).getWeekNumber() === 1) {
                            max = weeksInYear(new Date(config.from).getFullYear()) + 2;
                        } else {
                            max = new Date(config.to).getWeekNumber() + 1;
                        }

                        // Build chart categories
                        for (i = new Date(config.from).getWeekNumber() + 1; i < max; i++) {

                            result.categories.push(getDateRangeOfWeek(i, new Date(config.from).getFullYear()));
                            for (j = 0; j < result.series.length; j++) {
                                result.series[j].data.push(0);
                            }
                        }

                        // Loop over categories
                        for (i = 0; i < result.categories.length; i++) {
                            for (j = 0; j < chart.length; j++) {
                                if (result.categories[i] === chart[j].rangeOfWeek) {
                                    for (k = 0; k < chart[j].data.length; k++) {
                                        var value = chart[j].data[k].value;
                                        switch (chart[j].data[k].intensity) {
                                            case '1':
                                                result.series[0].data[i] += value;
                                                break;
                                            case '3':
                                                result.series[1].data[i] += value;
                                                break;
                                            case '5':
                                                result.series[2].data[i] += value;
                                                break;
                                                /*case '4':
                                                    result.series[3].data[i] += value;
                                                    break;
                                                case '5':
                                                    result.series[4].data[i] += value;
                                                    break;*/
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        // The actor asked for a view per MONTH
                        var months = [];
                        for (i = 0; i < entries.length; i++) {
                            if ((i === 0) || (new Date(entries[i].datetimeAcquisition).getMonth() + 1 ===
                                    new Date(months[months.length - 1][0]).getMonth() + 1)) {
                                months.push({
                                    datetime: new Date(entries[i].datetimeAcquisition).getTime(),
                                    value: parseFloat(entries[i].value),
                                    intensity: entries[i].values[0].value
                                });
                            } else {
                                chart.push({
                                    monthNumber: new Date(months[months.length - 1].datetime).getMonth() + 1,
                                    data: months
                                });

                                // Restart with a new months array
                                months = [];
                                months.push({
                                    datetime: new Date(entries[i].datetimeAcquisition).getTime(),
                                    value: parseFloat(entries[i].value),
                                    intensity: entries[i].values[0].value
                                });
                            }

                            if (i === entries.length - 1) {
                                chart.push({
                                    monthNumber: new Date(months[months.length - 1].datetime).getMonth() + 1,
                                    data: months
                                });
                            }
                        };

                        // Build chart categories
                        for (i = new Date(new Date(config.from).setMonth(new Date(config.from).getMonth() + 1)); i <= new Date(new Date(config.to).setMonth(new Date(config.to).getMonth() + 1)); i = new Date(new Date(i).setMonth(i.getMonth() + 1))) {
                            result.categories.push(i.getMonth() + 1);
                            for (j = 0; j < result.series.length; j++) {
                                result.series[j].data.push(0);
                            }
                        }

                        // Loop over categories
                        for (i = 0; i < result.categories.length; i++) {
                            for (j = 0; j < chart.length; j++) {
                                if (result.categories[i] === chart[j].monthNumber) {
                                    for (k = 0; k < chart[j].data.length; k++) {
                                        var value = chart[j].data[k].value;
                                        switch (chart[j].data[k].intensity) {
                                            case '1':
                                                result.series[0].data[i] += value;
                                                break;
                                            case '3':
                                                result.series[1].data[i] += value;
                                                break;
                                            case '5':
                                                result.series[2].data[i] += value;
                                                break;
                                                /* case '4':
                                                     result.series[3].data[i] += value;
                                                     break;
                                                 case '5':
                                                     result.series[4].data[i] += value;
                                                     break; */
                                        }
                                    }
                                }
                            }
                        }

                        // Calculate mean value
                        for (i = 0; i < result.series.length; i++) {
                            for (j = 0; j < result.series[i].data.length; j++) {
                                result.series[i].data[j] = result.series[i].data[j] / 4;
                            }
                        }
                    }
                }
                callback(null, result);
            }
        });
}

//Build a salt or liquid chart
var saltOrLiquid = function(actorID, patientID, type, config, callback) {
    dbEntry.entryModel.find({
            userID: patientID,
            type: type,
            datetimeAcquisition: {
                $gt: new Date(config.from),
                $lt: new Date(config.to).setHours(23, 59, 59, 999)
            },
            isSkipped: false
        })
        .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Charts', 'Build ' + type + '', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                var color = '#00bfa5';
                if (type == 'salt') {
                    color = '#607d8b';
                }
                var result = {
                    series: [{
                        name: type,
                        data: [],
                        color: color,
                        pointPlacement: 'on'
                    }]
                };

                if (entries.length > 0) {
                    var day = entries[0].datetimeAcquisition.getDate();
                    var month = entries[0].datetimeAcquisition.getMonth();
                    var year = entries[0].datetimeAcquisition.getFullYear();
                    var id = 0;

                    result.series[0].data.push([new Date(entries[0].datetimeAcquisition).getTime(), Number(entries[0].value)]);

                    for (var i = 1; i < entries.length; i++) {
                        if (entries[i].datetimeAcquisition.getFullYear() == year && entries[i].datetimeAcquisition.getMonth() == month && entries[i].datetimeAcquisition.getDate() == day) {

                            result.series[0].data[id][1] = result.series[0].data[id][1] + Number(entries[i].value);

                        } else {
                            result.series[0].data.push([new Date(entries[i].datetimeAcquisition).getTime(), Number(entries[i].value)]);
                            day = entries[i].datetimeAcquisition.getDate();
                            month = entries[i].datetimeAcquisition.getMonth();
                            year = entries[i].datetimeAcquisition.getFullYear();
                            id = id + 1;
                        }

                    }
                }
                callback(null, result);
            }
        });
};


// Build a meal chart
var meals = function(actorID, patientID, condition, config, callback) {
    dbEntry.entryModel.find({
            userID: patientID,
            type: 'meal',
            datetimeAcquisition: {
                $gt: new Date(config.from),
                $lt: new Date(config.to).setHours(23, 59, 59, 999)
            }
        }, {
            'values': 1,
            'datetimeAcquisition': 1,
            _id: 0
        })
        .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Charts', 'Build meals', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                var result = {
                    categories: ['Morning', 'Midday', 'Evening'],
                    series: []
                };
                if (condition.indexOf('d1') > -1 || condition.indexOf('d2') > -1) {
                    result.series.push({
                        name: 'Fats',
                        data: [0, 0, 0],
                        color: '#B2EBF2',
                        stack: 'calorie',
                        score: 1
                    });
                    result.series.push({
                        name: 'Fast sugars',
                        data: [0, 0, 0],
                        color: '#00BCD4',
                        stack: 'calorie',
                        score: 1
                    });
                    result.series.push({
                        name: 'Slow sugars',
                        data: [0, 0, 0],
                        color: '#00838F',
                        stack: 'calorie',
                        score: 2
                    });
                }
                /*
                if(condition.indexOf('hf') > -1){
                    result.series.push({
                        name: 'Salt',
                        data: [0,0,0],
                        color: '#bdbdbd',
                        stack: 'salt',
                        score: 1
                    });
                }
                */
                if (entries.length > 0) {
                    var totalMorning = 0,
                        totalMidday = 0,
                        totalEvening = 0;
                    var from, to;
                    for (i = 0; i < entries.length; i++) {
                        var isMorning = false;
                        var isMidday = false;
                        var isEvening = false;
                        if (entries[i].datetimeAcquisition >= new Date(entries[i].datetimeAcquisition).setHours(0, 0, 0) && entries[i].datetimeAcquisition < new Date(entries[i].datetimeAcquisition).setHours(11, 0, 0)) {
                            isMorning = true;
                            totalMorning++;
                        } else if (entries[i].datetimeAcquisition >= new Date(entries[i].datetimeAcquisition).setHours(11, 0, 0) && entries[i].datetimeAcquisition < new Date(entries[i].datetimeAcquisition).setHours(15, 0, 0)) {
                            isMidday = true;
                            totalMidday++;
                        } else if (entries[i].datetimeAcquisition >= new Date(entries[i].datetimeAcquisition).setHours(15, 0, 0) && entries[i].datetimeAcquisition < new Date(entries[i].datetimeAcquisition).setHours(23, 59, 59)) {
                            isEvening = true;
                            totalEvening++;
                        }
                        for (j = 0; j < entries[i].values.length; j++) {
                            var k = -1;
                            if ((condition.indexOf('d1') > -1 || condition.indexOf('d2') > -1) && entries[i].values[j].type === 'slow') {
                                k = 2;
                            } else if ((condition.indexOf('d1') > -1 || condition.indexOf('d2') > -1) && entries[i].values[j].type === 'fast') {
                                k = 1;
                            } else if ((condition.indexOf('d1') > -1 || condition.indexOf('d2') > -1) && entries[i].values[j].type === 'fats') {
                                k = 0;
                            }
                            /*else if (condition.indexOf('hf') > -1 && entries[i].values[j].type === 'salt'){
                                                       if(condition.indexOf('d1') > -1 || condition.indexOf('d2') > -1)
                                                        k = 3;  
                                                       else k = 0;
                                                   }*/
                            if (k > -1 && k < result.series.length) {
                                if (isMorning)
                                    result.series[k].data[0] += isNaN(parseInt(entries[i].values[j].value)) ? 0 : parseInt(entries[i].values[j].value);
                                else if (isMidday)
                                    result.series[k].data[1] += isNaN(parseInt(entries[i].values[j].value)) ? 0 : parseInt(entries[i].values[j].value);
                                else if (isEvening)
                                    result.series[k].data[2] += isNaN(parseInt(entries[i].values[j].value)) ? 0 : parseInt(entries[i].values[j].value);
                            }
                        }
                    }

                    //Mean morning
                    for (i = 0; i < result.series.length; i++) {
                        result.series[i].data[0] = (result.series[i].data[0] / totalMorning) * result.series[i].score;
                    }

                    //Mean midday
                    for (i = 0; i < result.series.length; i++) {
                        result.series[i].data[1] = (result.series[i].data[1] / totalMidday) * result.series[i].score;
                    }

                    //Mean evening
                    for (i = 0; i < result.series.length; i++) {
                        // NB : the bug was here... wrong index ;-)
                        result.series[i].data[2] = (result.series[i].data[2] / totalEvening) * result.series[i].score;
                    }
                }
                callback(null, result);
            }
        });
}

var symptoms = function(actorID, patientID, config, callback) {
    var symptomsTypes = ['dyspnea', 'sleep', 'swellings', 'palpitations', 'dizziness', 'fatigue'];

    dbEntry.entryModel.find({
            userID: patientID,
            type: 'symptoms',
            datetimeAcquisition: {
                $gt: new Date(config.from),
                $lt: new Date(config.to).setHours(23, 59, 59, 999)
            },
            isSkipped: false
        })
        .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Charts', 'Build symptoms', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {


                var result = {
                    series: [{
                        name: 'Dyspnea',
                        data: [],
                        color: '#00695c',
                        pointPlacement: 'on'
                    }, {
                        name: 'Sleep',
                        data: [],
                        color: '#00bfa5',
                        pointPlacement: 'on'
                    }, {
                        name: 'Swellings',
                        data: [],
                        color: '#01579b',
                        pointPlacement: 'on'
                    }, {
                        name: 'Palpitations',
                        data: [],
                        color: '#9e9e9e',
                        pointPlacement: 'on'
                    }, {
                        name: 'Dizziness',
                        data: [],
                        color: '#795548',
                        pointPlacement: 'on'
                    }, {
                        name: 'Fatigue',
                        data: [],
                        color: '#aa00ff',
                        pointPlacement: 'on'
                    }]

                };
                for (var i = 0; i < entries.length; ++i) {

                    if (!entries[i].isSkipped && entries[i].values) {
                        for (var k = 0; k < symptomsTypes.length; ++k) {
                            result.series[k].data.push([new Date(entries[i].datetimeAcquisition).getTime(), Number(entries[i].values[k].value)]);
                        }
                    }
                }

                /*
            var result = {
                series : [{
                    name: 'Symptoms',
                    data: [0, 0, 0, 0, 0, 0],
                    color: '#8d6e63'
                }],
                categories: symptomsTypes
            };
             if(entries.length > 0){
                 // Compute the mean for each type
                 var totals = [0, 0, 0, 0, 0, 0];
                 var from, to;
                 // sum
                 for(var i = 0; i < entries.length; i++){
                     if(!entries[i].isSkipped && entries[i].values){
                        
                             for(var k = 0; k < symptomsTypes.length; ++k){                          
                                 result.series[0].data[k] += Number(entries[i].values[k].value);
                                 totals[k]++;
                             }
                     }
                 }
                 // mean
                 for(var i = 0; i < result.series[0].data.length; ++i){
                     if(totals[i] > 0){
                         result.series[0].data[i] = result.series[0].data[i] / totals[i];
                         
                         if(result.series[0].data[i] > 0.5 && result.series[0].data[i] <= 1.5)
                             result.series[0].data[i] = { y: result.series[0].data[i]
                                                        , color: '#ffc400'
                                                        , img: './img/notWell.png'};
                         else if(result.series[0].data[i] > 1.5)
                             result.series[0].data[i] = { y: result.series[0].data[i]
                                                        , color: '#d50000'
                                                        , img: './img/bad.png'};
                         else
                             result.series[0].data[i] = { y: result.series[0].data[i]
                                                        , color: '#76ff03'
                                                        , img: './img/good.png'};
                        
                     }
                 }
             }       
             */
                callback(null, result);
            }
        });
};

// Build an insulin chart
var insulin = function(actorID, patientID, config, callback) {
    dbEntry.entryModel.find({
            userID: patientID,
            type: 'insulin',
            datetimeAcquisition: {
                $gt: new Date(config.from).setHours(0, 0, 0, 0),
                $lt: new Date(config.to).setHours(23, 59, 59, 999)
            }
        }, {
            _id: 0
        })
        .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Charts', 'Build insulin', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                var timeslot = _.findWhere(_.findWhere(cardsList, { name: 'insulin' }).subitems, { name: config.type }).params.timeslot;
                var result = {
                    categories: [],
                    series: [{
                        name: 'long',
                        title: 'Long',
                        data: [0, 0, 0, 0, 0, 0, 0],
                        color: '#1565C0'
                    }, {
                        name: 'intermediate',
                        title: 'Intermediate',
                        data: [0, 0, 0, 0, 0, 0, 0],
                        color: '#1E88E5'
                    }, {
                        name: 'mixed',
                        title: 'Mixed',
                        data: [0, 0, 0, 0, 0, 0, 0],
                        color: '#E0E0E0'
                    }, {
                        name: 'rapid',
                        title: 'Rapid',
                        data: [0, 0, 0, 0, 0, 0, 0],
                        color: '#42A5F5'
                    }, {
                        name: 'short',
                        title: 'Short',
                        data: [0, 0, 0, 0, 0, 0, 0],
                        color: '#90CAF9'
                    }],
                    glycaemias: [0, 0, 0, 0, 0, 0, 0]
                };

                function myLoopJ(j) {
                    if (j >= 0) {
                        result.categories[j] = new Date(new Date(config.to).setDate(new Date(config.to).getDate() - j)).getDay();

                        function myLoopI(i) {
                            if (i < entries.length) {
                                if (entries[i].datetimeAcquisition >= new Date(entries[i].datetimeAcquisition).setHours(
                                        timeslot.from[0],
                                        timeslot.from[1],
                                        timeslot.from[2],
                                        timeslot.from[3]
                                    ) && entries[i].datetimeAcquisition < new Date(entries[i].datetimeAcquisition).setHours(
                                        timeslot.to[0],
                                        timeslot.to[1],
                                        timeslot.to[2],
                                        timeslot.to[3]
                                    ) && !entries[i].isSkipped) {
                                    if (entries[i].datetimeAcquisition >= new Date(new Date(config.to).setDate(new Date(config.to).getDate() - j)).setHours(0, 0, 0, 0) && entries[i].datetimeAcquisition < new Date(new Date(config.to).setDate(new Date(config.to).getDate() - j)).setHours(23, 59, 59, 999)) {
                                        var value = isNaN(parseInt(entries[i].value)) ? 0 : parseInt(entries[i].value);
                                        switch (entries[i].subType) {
                                            case 'short':
                                                result.series[4].data[j] += value;
                                                break;
                                            case 'rapid':
                                                result.series[3].data[j] += value;
                                                break;
                                            case 'mixed':
                                                result.series[2].data[j] += value;
                                                break;
                                            case 'intermediate':
                                                result.series[1].data[j] += value;
                                                break;
                                            case 'long':
                                                result.series[0].data[j] += value;
                                                break;
                                        }

                                        // Should be set depending on mealtimes...    
                                        dbEntry.entryModel.findOne({
                                            userID: patientID,
                                            type: 'glycaemia',
                                            datetimeAcquisition: {
                                                $gt: new Date(new Date(config.to).setDate(new Date(config.to).getDate() - j)).setHours(
                                                    timeslot.from[0],
                                                    timeslot.from[1],
                                                    timeslot.from[2],
                                                    timeslot.from[3]),
                                                $lt: new Date(new Date(config.to).setDate(new Date(config.to).getDate() - j)).setHours(
                                                    timeslot.to[0],
                                                    timeslot.to[1],
                                                    timeslot.to[2],
                                                    timeslot.to[3])
                                            }
                                        }).exec(function(err, entry) {
                                            if (err) {
                                                console.log(err);
                                                audit.logEvent('[mongodb]', 'Charts', 'Build insulin', '', '', 'failed',
                                                    'Mongodb attempted to retrieve glycaemia entries');
                                                callback(err);
                                            } else {
                                                if (entry) {
                                                    result.glycaemias[j] = parseInt(entry.value);
                                                }
                                                myLoopI(i + 1);
                                            }
                                        });
                                    } else {
                                        myLoopI(i + 1);
                                    }
                                } else {
                                    myLoopI(i + 1);
                                }
                            } else {
                                myLoopJ(j - 1);
                            }
                        }
                        myLoopI(0);
                    } else {
                        callback(null, result);
                    }
                }
                myLoopJ(6);
            }
        });
}

// Build a mobility chart
var mobility = function(actorID, patientID, config, callback) {
    dbEntry.entryModel.find({
            userID: patientID,
            type: 'mobility',
            datetimeAcquisition: {
                $gt: new Date(config.from),
                $lt: new Date(config.to).setHours(23, 59, 59, 999)
            },
            isSkipped: false
        }, {
            'values': 1,
            _id: 0
        })
        .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Charts', 'Build mobility', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                var result = [];
                if (entries.length > 0) {
                    result = [
                        ['Motor vehicle', 0],
                        ['Public transports', 0],
                        ['Bike', 0],
                        ['Walking', 0]
                    ];
                    for (i = 0; i < entries.length; i++) {
                        for (j = 0; j < entries[i].values.length; j++) {
                            if (entries[i].values[j].type === 'motor') {
                                result[0][1] += parseFloat(entries[i].values[j].value);
                            } else if (entries[i].values[j].type === 'public') {
                                result[1][1] += parseFloat(entries[i].values[j].value);
                            } else if (entries[i].values[j].type === 'bike') {
                                result[2][1] += parseFloat(entries[i].values[j].value);
                            } else {
                                result[3][1] += parseFloat(entries[i].values[j].value);
                            }
                        }
                    }
                    for (i = 0; i < result.length; i++) {
                        result[i][1] = result[i][1] / entries.length;
                    }
                }
                callback(null, result);
            }
        });
}


Date.prototype.getWeekNumber = function() {
    var d = new Date(+this);
    d.setHours(0, 0, 0);
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    return Math.ceil((((d - new Date(d.getFullYear(), 0, 1)) / 8.64e7) + 1) / 7);
};

function getWeekNumber(d) {
    // Copy date so don't modify original
    d = new Date(+d);
    d.setHours(0, 0, 0);
    // Set to nearest Thursday: current date + 4 - current day number
    // Make Sunday's day number 7
    d.setDate(d.getDate() + 4 - (d.getDay() || 7));
    // Get first day of year
    var yearStart = new Date(d.getFullYear(), 0, 1);
    // Calculate full weeks to nearest Thursday
    var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7)
    // Return array of year and week number
    return [d.getFullYear(), weekNo];
}

function weeksInYear(year) {
    var d = new Date(year, 11, 31);
    var week = getWeekNumber(d)[1];
    return week == 1 ? getWeekNumber(d.setDate(24))[1] : week;
}

function getDateRangeOfWeek(w, y) {
    var simple = new Date(y, 0, 1 + (w - 1) * 7);
    var dow = simple.getDay();
    var ISOweekStart = simple;
    var ISOweekEnd = new Date();
    if (dow <= 4) {
        ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    } else {
        ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    }
    ISOweekEnd.setDate(ISOweekStart.getDate() + 6);
    return ((ISOweekStart.getDate()) + '-' + ((ISOweekEnd.getDate()) + '/' + (ISOweekStart.getMonth() + 1)));
}