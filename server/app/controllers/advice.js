exports.getAdvice = function(req, res) {
	var displayedAdvices = [];
	var childProcess = require('child_process'),
	spawn = childProcess.spawn;
	
	var python = ""
	if (req.params.choice == 0) python = spawn( 'python',['-u', 'app/python/advice.py', req.params.userID]);
	else python = spawn( 'python',['-u', 'app/python/advice.py', req.params.userID, req.params.choice]);
	
	var chunk = '';
	var flag = false;
	python.stdout.on('data', function(data){chunk += data;});
	python.stderr.on('data', function(data){chunk += data; flag = true});
	python.stdout.on('close', function( ){ 
		if (flag) console.log(chunk);
		return res.json(chunk);
	});
};


