var mongoose        = require('mongoose');
var db              = require('../models/entry');
var dbUser          = require('../models/user');
var audit           = require('../audit-log');
var tokenManager    = require('../token_manager');
var jsonwebtoken    = require('jsonwebtoken');



// Update entries
exports.settechentry = function(req, res) {

    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
		entries = JSON.parse(req.params.array);
		for	(i = 0; i < entries.length; i++) {
			db.entryModel.create({
				userID : actorID,
				type : entries[i].type,
				value : entries[i].value,
				datetime : entries[i].datetime
			}, function(err, temp) {
            if (err){console.log(err);}
            else{
					console.log("Entree creee (techentry)");
				}
			});
        }
		
		entries = JSON.parse(req.params.array2);
		for	(i = 0; i < entries.length; i++) {
			db.entryModel.remove({
				_id : entries[i]._id
			}, function(err, temp) {
            if (err){console.log(err);}
            else{
					console.log("Entree effacee (techentry)");
				}
			});
        }
		return res.json(null);
    }
    else{
		console.log("oups")
        return res.send(401); 
    }
}

// Retrieve entries with a specified patient 
exports.gettechentry = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        var actorID = jsonwebtoken.decode(token).id;
		
        if(req.params.username !== undefined){
            dbUser.userModel.findOne({
                username : req.params.username
            }, {_id:1})
            .exec(function(err, user) {
                if (err){
                    return res.send(err);
                }
                else{
                    db.entryModel.find({
                        userID : user._id,
                    })
                    .exec(function(err, entries) {
                        if (err){
                            return res.send(err);
                        }
                        else{
                            return res.json(entries);
                        }
                    });
                }
            });
        }
        else{
            return res.send(400);
        }
    }
    else{
        return res.send(401); 
    }
}
