var db = require('../models/entry');
var dbUser = require('../models/user');
var audit = require('../audit-log');
var tokenManager = require('../token_manager');
var jsonwebtoken = require('jsonwebtoken');
var stats = require('stats-lite');
var roundTo = require('round-to');
var _ = require('underscore');

var entryType = "glycaemia";



/**
 * Compute the estimated glycated hemoglobin value over the
 * past 120 days of glycaemia entries.
 */
exports.computeValue = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.originalUrl.indexOf("/hemoglobin") === 4) {
            //The actor is the patient
            var patientCondition = jsonwebtoken.decode(token).condition;
            computeValue(actorID, actorID, patientCondition, function(err, entries) {
                if (err) {
                    res.status(500).send(err);
                } else {
                    return res.json(entries);
                }
            });
        } else {
            //The actor is the doctor
            if (!req.forbidden) {
                if (req.params.username !== undefined) {
                    dbUser.userModel.findOne({
                            username: req.params.username
                        }, { _id: 1 })
                        .exec(function(err, user) {
                            if (err) {
                                console.log(err);
                                audit.logEvent('[mongodb]', 'Hemoglobin', 'ComputeValue', '', '', 'failed', 'Mongodb attempted to retrieve a user');
                                res.status(500).send(err);
                            } else {
                                computeValue(actorID, user._id, user.condition, function(err, entries) {
                                    if (err) {
                                        res.status(500).send(err);
                                    } else {
                                        return res.json(entries);
                                    }
                                });
                            }
                        });
                } else {
                    audit.logEvent(actorID, 'Hemoglobin', 'ComputeValue', '', '', 'failed',
                        'The user could not retrieve entries because one or more params of the request was not defined');
                    return res.sendStatus(400);
                }
            } else {
                return res.sendStatus(403);
            }
        }
    } else {
        audit.logEvent('[anonymous]', 'Hemoglobin', 'ComputeValue', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
}


/**
 * Take all the glycaemia entries from the past 120 days.<br>
 * Compute the mean value from those entries.<br>
 * Estimate the glycated hemoglobin from this mean and send it back.<br>
 * Formula used : eAG(mg/dl)= (28.7*HbA1c)-46.7<br>
 * Reversed to have: HbA1c = (eAG + 46.7) / 28.7
 */
var computeValue = function(actorID, patientID, patientCondition, callback) {

    var query = {
        userID: patientID,
        type: entryType
    };

    // 120 days ago =>
    var threeMonthsAgo = new Date();
    threeMonthsAgo.setDate(threeMonthsAgo.getDate() - 120);

    db.entryModel.find(query)
        //.where('datetimeAcquisition').gt(threeMonthsAgo)
        .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Hemoglobin', 'ComputeValue', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                var len = entries.length;
                var glycHemo = 0;
                var divider = 0;
                var dateFirstEntry = new Date();
                var isValueGood = false;

                if (len > 0) {
                    // Compute the mean.
                    // Sum
                    var sum = 0.0;

                    var foundFirst = false;
                    for (var i = 0; i < len; ++i) {
                        if (entries[i].value !== undefined && entries[i].value !== 'undefined') {
                            sum += parseFloat(entries[i].value);
                            ++divider;
                            if (!foundFirst) {
                                dateFirstEntry = entries[i].datetimeAcquisition;
                                foundFirst = true;
                            }
                        }
                    }
                    // Divide
                    if (divider > 0) {
                        var mean = sum / divider;
                        // Estimate the glycated hemoglobin.
                        glycHemo = (mean + 46.7) / 28.7;
                    }
                    // Is the value good ?
                    if (patientCondition.indexOf("d1") > -1) {
                        if (glycHemo < 8.0)
                            isValueGood = true;
                    }
                    if (patientCondition.indexOf("d2") > -1) {
                        if (glycHemo < 6.5 && glycHemo > 6.0)
                            isValueGood = true;
                    }

                }
                // Send back the estimated glycated hemoglobin value.
                callback(null, { value: glycHemo, nbEntries: divider, start: dateFirstEntry, isGood: isValueGood, condition: patientCondition });
            }
        });
}




/**
 * Compute the estimated glycated hemoglobin value over the
 * past 120 days of glycaemia entries.
 */
exports.getGlyStats = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        //console.log(req.originalUrl); 

        var timebound = req.params.timebound || '';
        var hourlowerb = req.params.hourlowerb || '';
        var hourupperb = req.params.hourupperb || '';
        var lowerb = req.params.lowerb || '';
        var upperb = req.params.upperb || '';

        // == The actor is the patient

        if (req.originalUrl.indexOf("/glystats") === 4) {
            if (timebound === '' || hourlowerb === '' || hourupperb === '' || lowerb === '' || upperb === '') {
                console.log('glycaemia.js - getGlyStats : Missing parameters. Received params : ', req.params);
                return res.sendStatus(400);
            } else {
                getGlyStats(actorID, actorID, timebound, hourlowerb, hourupperb, lowerb, upperb, function(err, entries) {
                    if (err) {
                        res.status(500).send(err);
                    } else {
                        return res.json(entries);
                    }
                });
            }
        }

        // == The actor is the doctor
        else if(req.originalUrl.indexOf("/doctorglystats") === 4){
            if (!req.forbidden) {
                if (timebound === '' || hourlowerb === '' || hourupperb === '' || lowerb === '' || upperb === '') {
                    console.log('glycaemia.js - getGlyStats : Missing parameters. Received params : ', req.params);
                    return res.sendStatus(400);
                } else {
                    if (req.params.username !== undefined) {
                        dbUser.userModel.findOne({
                                username: req.params.username
                            }, { _id: 1 })
                            .exec(function(err, user) {
                                if (err) {
                                    console.log('ERR : glycaemia.js - getGlyStats ', err);
                                    res.status(500).send(err);
                                } else {
                                    if(user) {
                                        getGlyStats(actorID, user._id, timebound, hourlowerb, hourupperb, lowerb, upperb, function(err, entries) {
                                            if (err) {
                                                console.log('ERR : glycaemia.js - getGlyStats ', err);
                                                res.status(500).send(err);
                                            } else {
                                                return res.json(entries);
                                            }
                                        });
                                    } else {
                                        return res.sendStatus(400);
                                    }
                                }
                            });
                    } else {
                        return res.sendStatus(400);
                    }
                }
            } else {
                return res.sendStatus(403);
            }
        } else {
            return res.sendStatus(400);
        }
    } else {
        return res.sendStatus(401);
    }
}


/**
 * Computes Glycaemia statistics within a specific range and timeframe.
 * Days range = Today-timebound
 * Hours range = hourupperb - hourlowerb 
 * Glycaemia range = upperb - lowerb
 */
var getGlyStats = function(actorID, patientID, timebound, hourlowerb, hourupperb, lowerb, upperb, callback) {

    var daysframe = new Date();
    daysframe.setDate(daysframe.getDate() - timebound);

    var query = { //put boundaries in the query
        userID: patientID,
        type: entryType
    };

    var data = [];

    db.entryModel.find(query)
        .where('datetimeAcquisition').gt(daysframe)

        // .where('value').gt(lowerb).lt(upperb)     
        // .sort({ "datetimeAcquisition": 1 })
        .exec(function(err, entries) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Hemoglobin', 'GlyStats', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                var len = entries.length;
                //console.log("glycaemia.js - Length :", len);
                var cond0;
                var cond1;
                var hour_entry;
                var val;
                var len_inframe = 0;
                var len_hyper = 0;
                var len_hypo = 0;
                var glymin = 0;
                var glymax = 0;

                if (len > 0) {
                    var initMinMax = _.find(entries, function(val){ return val.value != undefined && val.value !== 'undefined'; });
                    glymin = glymax = initMinMax.value;
                    for (var i = 0; i < len; ++i) {
                        hour_entry = Number(entries[i].datetimeAcquisition.getHours());
                        val = Number(entries[i].value);
                        cond0 = (val !== undefined && val !== 'undefined');
                        cond1 = (hour_entry < hourupperb && hour_entry > hourlowerb);

                        // Defined and within specific timeframe
                        if (cond0 && cond1) {
                            len_inframe++
                            // Update min and max
                            if (val > glymax) {
                                glymax = val;
                            }
                            if (val < glymin) {
                                glymin = val;
                            }

                            // Above upper target - hyper
                            if (val > upperb) {
                                len_hyper++;
                            }

                            // Below lower target - hypo
                            if (val < lowerb) {
                                len_hypo++;
                            }

                            // Within glycaemia targets
                            if (val > lowerb && val < upperb) {
                                data.push(val);
                            }

                        }
                    }

                }
                
                /* Compute stats */
                var glymean = Number((stats.mean(data)).toFixed(2));
                var hba1c = (glymean + 46.7)/28.7;
                var glystdev = Number((stats.stdev(data)).toFixed(2));
                var glyvar = Number((glystdev / glymean).toFixed(2));
                var hypoperc = Number(((len_hypo / len_inframe) * 100).toFixed(2));
                var hyperperc = Number(((len_hyper / len_inframe) * 100).toFixed(2));
                var readperday = Number((len / timebound).toFixed(2));

                // Send back the stats
                callback(null, {
                    length: len_inframe,
                    glymin: glymin,
                    glymax: glymax,
                    glymean: glymean,
                    glystdev: glystdev,
                    glyvar: glyvar,
                    hypoperc: hypoperc,
                    hyperperc: hyperperc,
                    readperday: readperday,
                    hba1c: hba1c,
                    risk: 0
                });
            }
        });
}