var db = require('../models/entry');
var obj = require('../models/objective');
var tokenManager = require('../token_manager');
var audit = require('../audit-log');
var jsonwebtoken = require('jsonwebtoken');
var FitbitApiClient = require('fitbit-node');
var async = require('async');
var session = require('express-session');
var Withings = require('withings-oauth2').Withings;


/* ================================================= POPULATE MODE =========================================================*/

var localwork = true;
var populateMode = true;

/* NB : before launching the populate function, clean DB and type in the shell :

 > mongo 
 > use healthcompass
 > db.entries.remove( { type: { $in: [ "glycaemia","weight","steps","activity","meal","insulin"]} ,userID: '58f62fd02dfcb50049a3453b'} )
 > Click on "Sync with Withings" to launch the populate fct


FOR THE WEB VERSION - JOHN DOE
 1) Locally, switch popoulate mode to TRUE, then git push to Remote.
 2) Go to UCL server. 
    - sudo git pull
    - sudo /opt/restart_egle 
 3) (mongo) db.entries.remove( { type: { $in: [ "glycaemia","weight","steps","activity","meal","insulin"]} ,userID: '58f9fabf6ddd54be070d24fb'} )
 4) Click on "Sync Nokia"

 Back to localhost
 5) Switch populate mode to off
 6) git push (populate mode switched to FALSE)
 7) sudo /opt/restart_egle 



 where  ID John Doe (web) = '58f9fabf6ddd54be070d24fb'  and ID John Test (local) = '58f62fd02dfcb50049a3453b'

/* ================================================= POPULATE MODE =========================================================*/




Date.prototype.addDays = function(days) {
    var dat = new Date(this.valueOf());
    dat.setDate(dat.getDate() + days);
    return dat;
}

Date.prototype.addHours = function(h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}


Date.prototype.addRdmHour = function() {
    var val = randomVal(-0.5, 0.5);
    this.setTime(this.getTime() + (val * 60 * 60 * 1000));
    return this;
}


var _D90 = new Date().addDays(-90);
var _D85 = new Date().addDays(-85);
var _D80 = new Date().addDays(-80);
var _D75 = new Date().addDays(-75);
var _D60 = new Date().addDays(-60);
var _D45 = new Date().addDays(-45);
var _D30 = new Date().addDays(-30);
var _D15 = new Date().addDays(-15);
var _D7 = new Date().addDays(-7);
var _DD = new Date();
var _DD30 = new Date().addDays(30);

/* =================================================== */


/* SETTINGS : https://oauth.withings.com/partner/dashboard */
/* Note : if not working anymore, try with john.doe.euromedlab@gmail.com */

if (localwork) {
    var _CONSUMER_KEY = '1973f27b231c9ce3bd8c7fd781712670305bc525c33e7cedb62d3f4f5cb2';
    var _CONSUMER_SECRET = '1df170773865e42d0525184e0874e42108ed0bdc8f3f29407e4db17e1';
    var _CALLBACK_URL = 'https://localhost:3001/api/users/withingscallback';

} else {
    // ==== Web-test Withings registered app ==== 
    var _CONSUMER_KEY = '8920b021320ec8003570f83c87bb85454bb512b91dce326dec1c529239fe27';
    var _CONSUMER_SECRET = '192171793b173d99e31cbfd2ebe09576c4544b3146f38c7ccd40c431';
    var _CALLBACK_URL = 'https://beta.egle.be/api/users/withingscallback';
}


/* 
    - Redirects the user to the Withings authorization page
    - Request access to the Withings user's data
*/

exports.withingsAccess = function(req, res) {
    /* ========================= POPULATE MODE =========================== **/
    var egletoken = tokenManager.getToken(req.headers);
    var actorId = jsonwebtoken.decode(egletoken).id; /**/

    /**/
    if (populateMode) {
        console.log("Populate mode is ON");
        if (localwork) {
            // john@test.com / azerty
            // To be customized
            var actorId = '58f62fd02dfcb50049a3453b';
        } else {
            // John Doe
            var actorId = '58f9fabf6ddd54be070d24fb';
        }
        dbpopulateGly(actorId);
        dbpopulateWeight(actorId);
        dbpopulateSteps(actorId);
        dbpopulateInsulin(actorId);
        dbpopulateMeal(actorId);
        dbpopulateSport(actorId);
        return res.redirect('/');
        console.log("DB should be populated for John Doe.");

    }
    /* =================================================================== */

    console.log('\n==== In withingsAccess - withings.js =====\n');


    var optionsA = {
        consumerKey: _CONSUMER_KEY,
        consumerSecret: _CONSUMER_SECRET,
        callbackUrl: _CALLBACK_URL
    };
    var clientA = new Withings(optionsA);
    // STEP 1 : GET OAUTH REQUEST TOKEN
    clientA.getRequestToken(function(err, rtoken, rtokenSecret) {
        if (err) {
            // Throw error
            console.log("Withings access error - withings.js");
            return;
        }

        req.session.oauth = {
            requestToken: rtoken,
            requestTokenSecret: rtokenSecret
        };

        req.session.egletoken = egletoken;

        var auth_url = clientA.authorizeUrl(rtoken, rtokenSecret);
        var data = {
            url: auth_url
        };

        console.log("TOKEN :", rtoken, "\nTOKEN SECRET :", rtokenSecret);
        console.log("AUTH :", auth_url);



        // STEP 2 : END-USER AUTHORIZATION (...)
        return res.status(200).json(data);

    });
};

// On return from authorization
exports.withingsCallback = function(req, res) {
    console.log('\n==== Get Withings callback =====\n');

    var verifier = req.query.oauth_verifier;
    var oauthSettings = req.session.oauth;
    //var egle_token = tokenManager.getToken(req.headers);
    var egle_token = req.session.egletoken;
    console.log("egle token :", egle_token);


    var optionsB = {
        consumerKey: _CONSUMER_KEY,
        consumerSecret: _CONSUMER_SECRET,
        //callbackUrl: _CALLBACK_URL,
        userID: req.query.userid
    };

    // STEP 3 : GET ACCESS TOKEN
    console.log("\ngetAccessToken\n");
    var clientB = new Withings(optionsB);
    clientB.getAccessToken(oauthSettings.requestToken, oauthSettings.requestTokenSecret, verifier,
        function(err, token, secret) {
            if (err) {
                console.log("Withings - getAccessToken");
                return;
            }

            var optionsC = {
                consumerKey: _CONSUMER_KEY,
                consumerSecret: _CONSUMER_SECRET,
                accessToken: token,
                accessTokenSecret: secret,
                userID: req.query.userid
            };

            var clientC = new Withings(optionsC);
            var today = new Date();
            var today_1yearpast = new Date(new Date().setFullYear(new Date().getFullYear() - 1));


            // STEP 4 : ACCESS USER DATA
            clientC.getWeightMeasures(today_1yearpast, today, function(err, data) {
                if (err) {
                    res.send(err);
                }
                if (data) {
                    console.log("Weight : ",data[0]);
                    var time = data[0].date;
                    var dateAcquis = timeConverter(time);
                    var unit = data[0].measures[0].unit;
                    var fact = Math.pow(10, unit);

                    var weight = data[0].measures[0].value * fact;
                    console.log("WEIGHT INIT: ", weight);
                } else {
                    var weight = 75;
                    var dateAcquis = new Date();
                    console.log("WEIGHT DEFAULT", weight);

                }

                //console.log("\n\n WITHINGS DATA \n\n: ", data.measures[measures.length].value);

                /* ========================= WRITE IN DB ========================== */

                /* WRITE IN DB */
                console.log('>> WRITE THE ENTRY IN DATABASE ');
                var d = new Date();
                var actorId = jsonwebtoken.decode(egle_token).id;

                var theEntry = {
                    userID: actorId,
                    type: '',
                    subType: '',
                    value: '',
                    values: '',
                    datetimeAcquisition: dateAcquis,
                    comments: 'Withings',
                    datetime: d,
                    isSkipped: false,
                    isValidated: false
                };


                theEntry.type = 'weight';
                /* if (actorId == '58f9fabf6ddd54be070d24fb') {
                     theEntry.value = randomWeight(-0.5, 0.5, weight);
                 } else {
                     theEntry.value = weight;
                 }*/
                theEntry.value = weight;
                console.log("Weight :", theEntry.value);


                // Add new entry
                db.entryModel.create(theEntry, function(err, entries) {
                    if (err) {
                        audit.logEvent('[mongodb]', 'Entries', 'Create', '', '', 'failed', 'Mongodb attempted to create an entry');
                        return res.send(err);

                    } else {
                        console.log("> Entry is added in DB.");
                        var string = encodeURIComponent('weightid');
                        return res.redirect('/?paramiot=' + string);
                    }
                });
            });

            /*   // Last Diastolic Blood Pressure (mmHg)
               clientC.getMeasures(9, today_1yearpast, today, function(err, data) {
                   if (err) {
                       res.send(err);
                   }
                   console.log("Last DIASTOLIC BLOOD PRESSURE (mmHg) : ", data.body.measuregrps[0].measures[0].value); //.measures.value);

               });


               // Last Systolic Blood Pressure (mmHg)
               clientC.getMeasures(10, today_1yearpast, today, function(err, data) {
                   if (err) {
                       res.send(err);
                   }
                   console.log("Last SYSTOLIC BLOOD PRESSURE (mmHg) : ", data.body.measuregrps[0].measures[0].value); //.measures.value);

               });


               // Last Pulse Wave Velocity (mmHg)
               clientC.getMeasures(91, today_1yearpast, today, function(err, data) {
                   if (err) {
                       res.send(err);
                   }
                   console.log("Last PULSE WAVE Velocity : ", data.body.measuregrps);

               });*/



        }
    );
}

/* ============================================== POPULATE DATABASE ====================================================== */

var subsArrayNoise = function(my_array, random_basis) {
    var len = my_array.length;
    var array_noise = Array.from({ length: len }, () => Math.floor(Math.random() * random_basis));
    var valuesB = my_array.map((a, i) => a - array_noise[i]);
    return valuesB;
}

var dbpopulateGly = function(actorId) {
    var stdev_val = 30;
    var stdev_hour = 90;
    var hours = [8, 10.5, 13.5, 16.5, 18.5, 20.5, 22, 1, 3, 5.5];
    var values = [65, 90, 130, 95, 180, 220, 190, 160, 120, 80];

    // for loop does not work in asynchronous mode
    //dbpopulate('glycaemia', values[0], '', actorId, d, dd, hours[0]);
    dbpopulate('glycaemia', '', values[0], '', actorId, _D60, _DD, hours[0], stdev_val, stdev_hour);
    dbpopulate('glycaemia', '', values[1], '', actorId, _D60, _DD, hours[1], stdev_val, stdev_hour);
    dbpopulate('glycaemia', '', values[2], '', actorId, _D60, _DD, hours[2], stdev_val, stdev_hour);
    dbpopulate('glycaemia', '', values[3], '', actorId, _D60, _DD, hours[3], stdev_val, stdev_hour);
    dbpopulate('glycaemia', '', values[4], '', actorId, _D60, _DD, hours[4], stdev_val, stdev_hour);
    dbpopulate('glycaemia', '', values[5], '', actorId, _D60, _DD, hours[5], stdev_val, stdev_hour);
    dbpopulate('glycaemia', '', values[6], '', actorId, _D60, _DD, hours[6], stdev_val, stdev_hour);
    dbpopulate('glycaemia', '', values[7], '', actorId, _D60, _DD, hours[7], stdev_val, stdev_hour);
    dbpopulate('glycaemia', '', values[8], '', actorId, _D60, _DD, hours[8], stdev_val, stdev_hour);
    dbpopulate('glycaemia', '', values[9], '', actorId, _D60, _DD, hours[9], stdev_val, stdev_hour);


}


var dbpopulateWeight = function(actorId) {
    // NB : we need to populate for 3 months
    var stdev_val = 0.4;
    var stdev_hour = 45;
    var hour = 9;
    var values = [84, 82.3, 80.8, 79.1, 77.5, 75.5, 74, 73]
    // for loop does not work in asynchronous mode
    dbpopulate('weight', '', values[0], '', actorId, _D90, _D85, hour, stdev_val, stdev_hour);
    dbpopulate('weight', '', values[1], '', actorId, _D85, _D80, hour, stdev_val, stdev_hour);
    dbpopulate('weight', '', values[2], '', actorId, _D80, _D75, hour, stdev_val, stdev_hour);

    dbpopulate('weight', '', values[3], '', actorId, _D75, _D60, hour, stdev_val, stdev_hour);
    dbpopulate('weight', '', values[4], '', actorId, _D60, _D45, hour, stdev_val, stdev_hour);
    dbpopulate('weight', '', values[5], '', actorId, _D45, _D30, hour, stdev_val, stdev_hour);
    dbpopulate('weight', '', values[6], '', actorId, _D30, _D15, hour, stdev_val, stdev_hour);
    dbpopulate('weight', '', values[7], '', actorId, _D15, _DD, hour, stdev_val, stdev_hour);
}


var randomWeight = function(min, max, weight) {
    return (Math.random() * (max - min + 1) + min + weight).toFixed(1);
}


var dbpopulateSteps = function(actorId) {
    var stdev_val = 500;
    var stdev_hour = 45;
    var hour = 9;
    values = [4000, 4500, 4800, 5500, 6600, 7500];
    dbpopulate('steps', '', values[0], '', actorId, _D90, _D75, hour, stdev_val, stdev_hour);
    dbpopulate('steps', '', values[1], '', actorId, _D75, _D60, hour, stdev_val, stdev_hour);
    dbpopulate('steps', '', values[2], '', actorId, _D60, _D45, hour, stdev_val, stdev_hour);
    dbpopulate('steps', '', values[3], '', actorId, _D45, _D30, hour, stdev_val, stdev_hour);
    dbpopulate('steps', '', values[4], '', actorId, _D30, _D15, hour, stdev_val, stdev_hour);
    dbpopulate('steps', '', values[5], '', actorId, _D15, _DD, hour, stdev_val, stdev_hour);
}


var dbpopulateInsulin = function(actorId) {
    var stdev_val = 1;
    var stdev_hour = 35;
    var refval = 3;
    // for loop does not work in asynchronous mode
    // dbpopulate('weight', value, '', actorId, d, dd, hour, stdev_val, stdev_hour);
    dbpopulate('insulin', '', refval, 'rapid', actorId, _D7, _DD30, 8, stdev_val, stdev_hour);
    dbpopulate('insulin', '', refval + 1, 'rapid', actorId, _D7, _DD30, 12, stdev_val, stdev_hour);
    dbpopulate('insulin', '', refval, 'rapid', actorId, _D7, _DD30, 19, stdev_val, stdev_hour);
    dbpopulate('insulin', '', refval - 1, 'rapid', actorId, _D7, _DD30, 23, stdev_val, stdev_hour);
}


var dbpopulateSport = function(actorId) {
    var stdev_val = 0.1;
    var stdev_hour = 45;
    var hour = 9;

    values_1 = [{ type: 'intensity', value: 1 }];
    values_2 = [{ type: 'intensity', value: 3 }];
    values_3 = [{ type: 'intensity', value: 5 }];
    // for loop does not work in asynchronous mode
    // dbpopulate('weight', value, '', actorId, d, dd, hour, stdev_val, stdev_hour);
    dbpopulate('activity', values_1, 0.5, 'sport', actorId, _D90, _D60, hour, stdev_val, stdev_hour);
    dbpopulate('activity', values_2, 0.3, 'sport', actorId, _D90, _D60, hour, stdev_val, stdev_hour);
    dbpopulate('activity', values_3, 0.2, 'sport', actorId, _D90, _D60, hour, stdev_val, stdev_hour);

    dbpopulate('activity', values_1, 0.7, 'sport', actorId, _D60, _D30, hour, stdev_val, stdev_hour);
    dbpopulate('activity', values_2, 0.4, 'sport', actorId, _D60, _D30, hour, stdev_val, stdev_hour);
    dbpopulate('activity', values_3, 0.3, 'sport', actorId, _D60, _D30, hour, stdev_val, stdev_hour);

    dbpopulate('activity', values_1, 0.8, 'sport', actorId, _D30, _D15, hour, stdev_val, stdev_hour);
    dbpopulate('activity', values_2, 0.5, 'sport', actorId, _D30, _D15, hour, stdev_val, stdev_hour);
    dbpopulate('activity', values_3, 0.4, 'sport', actorId, _D30, _D15, hour, stdev_val, stdev_hour);

    dbpopulate('activity', values_1, 1.1, 'sport', actorId, _D15, _DD, hour, stdev_val, stdev_hour);
    dbpopulate('activity', values_2, 0.7, 'sport', actorId, _D15, _DD, hour, stdev_val, stdev_hour);
    dbpopulate('activity', values_3, 0.5, 'sport', actorId, _D15, _DD, hour, stdev_val, stdev_hour);
};

var dbpopulateMeal = function(actorId) {
    var stdev_val = 3;
    var stdev_hour = 45;
    // Score given to the meal depending on nutrients
    var value = 13;

    var values_1 = [{ type: 'slow', value: randomVal(4, 5) },
        { type: 'fast', value: randomVal(1, 5) },
        { type: 'fats', value: randomVal(1, 3) }
    ];

    var values_2 = [{ type: 'slow', value: randomVal(2, 4) },
        { type: 'fast', value: randomVal(3, 5) },
        { type: 'fats', value: randomVal(2, 3) }
    ];

    var values_3 = [{ type: 'slow', value: randomVal(1, 3) },
        { type: 'fast', value: randomVal(2, 4) },
        { type: 'fats', value: randomVal(3, 5) }
    ];

    dbpopulate('meal', values_1, value + randomVal(0, 2), '', actorId, _D7, _DD30, 8, stdev_val, stdev_hour);
    dbpopulate('meal', values_2, value + randomVal(0, 3), '', actorId, _D7, _DD30, 13, stdev_val, stdev_hour);
    dbpopulate('meal', values_2, value + randomVal(0, 3), '', actorId, _D7, _DD30, 16, stdev_val, stdev_hour);
    dbpopulate('meal', values_3, value + randomVal(0, 4), '', actorId, _D7, _DD30, 19, stdev_val, stdev_hour);
};


var dbpopulate = function(type, values_init, value_init, subType, actorId, datestart, datestop, hour, stdev_val, stdev_hour) {
    console.log("In dbpopulate :",type);
    var dateArray = getDatesFixedHour(datestart, datestop, hour, stdev_hour);
    var len = dateArray.length;
    var valuesArray = getRdmValues(value_init, len, stdev_val);
    //console.log(dateArray, valuesArray);


    for (i = 0; i < len; i++) {
        var theEntry = {
            userID: actorId,
            type: type,
            subType: subType,
            value: valuesArray[i].toString(),
            values: values_init,
            datetimeAcquisition: dateArray[i],
            comments: 'populate',
            datetime: dateArray[i],
            isSkipped: false,
            isValidated: false
        };

        //if (type == 'activity') { console.log(theEntry); }

        // Add new entry
        db.entryModel.create(theEntry, function(err, entries) {
            if (err) {
                audit.logEvent('[mongodb]', 'Entries', 'Create', '', '', 'failed', 'Mongodb attempted to create an entry');
                console.log("Populate err :",err);


            } else {
                console.log("> Populate : Entry is added in DB.");
                //return -1;
            }
        });


    }
};






/* Fonctions auxiliaires */
function getDates(startDate, stopDate, frequency) {
    var dateArray = new Array();
    var currentDate = startDate;
    if (frequency < 0 || frequency > 24 || !frequency) {
        return -1;
    } else {
        while (currentDate <= stopDate) {
            dateArray.push(new Date(currentDate.addRdmHour()));
            currentDate = currentDate.addHours(Math.round(24 / frequency));
        }
        return dateArray;
    }
}


function getDatesFixedHour(startDate, stopDate, hour, stdev_hour) {
    var dateArray = new Array();
    var currentDate = startDate;
    var counter = 0;

    if (hour < 0 || hour > 24 || !hour) {
        return -1;
    } else {
        while (currentDate <= stopDate) {
            counter += 1;
            currentDate.setHours(hour + randomVal(0, 1));
            currentDate.setMinutes(randomVal(-stdev_hour, stdev_hour));
            dateArray.push(currentDate);
            currentDate = startDate.addDays(counter);
        }
        return dateArray;
    }
}


function getRdmValues(value, size, stdev) {
    var valuesArray = new Array();
    var counter = 0;

    while (counter <= size) {
        var val = randomVal(-1, 1) * stdev + value;
        valuesArray.push(val.toFixed(1));
        counter += 1;
    }

    return valuesArray;
}



function timeConverter(UNIX_timestamp) {
    var a = new Date(UNIX_timestamp * 1000);
    var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
}

function randomVal(min, max) {
    return Math.random() * (max - min) + min;
};