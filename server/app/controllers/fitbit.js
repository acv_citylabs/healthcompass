var localwork = false;
var db = require('../models/entry');
var obj = require('../models/objective');
var tokenManager = require('../token_manager');
var audit = require('../audit-log');
var jsonwebtoken = require('jsonwebtoken');
var FitbitApiClient = require('fitbit-node');
var async = require('async');


if (localwork) {
    /* ==== Local-test Fitbit registered app  ===   ("MY_CLIENT_ID", "MY_CLIENT_SECRET");*/
    var client = new FitbitApiClient("2285HK", "7833f5b83fbccac0e78999a46e42a08a");
    var callback_url = 'https://localhost:3001/api/users/fitbitcallback';
} else {
	// ==== Web-test Fitbit registered app ==== 
    var client = new FitbitApiClient("228DG5", "b32f888544e0e688566602a083e324ae");
    var callback_url = 'https://beta.egle.be/api/users/fitbitcallback';
}


/* 
	- Redirects the user to the Fitbit authorization page
	- Request access to the Fitbit user's data
*/
exports.fitbitAccess = function(req, res) {
    console.log('\n==== In fitbitAccess - fitbit.js =====\n');
    //tokenManager.verifyToken(req, res, function(){
    var type = req.params.type;
    var token = tokenManager.getToken(req.headers);
    var state = token + type; // token
    // var scope = 'activity heartrate location nutrition profile settings sleep social weight';
    var scope = 'activity heartrate profile weight';
    var prompt = ''; //'consent';

    if (state != null) {
        auth_url = client.getAuthorizeUrl(scope, callback_url, prompt, state);
        //console.log('\n auth_url : \n' + auth_url + '\n');

        var data = {
            url: auth_url
        };
        return res.status(200).json(data);

    } else {
        audit.logEvent('[anonymous]', 'Fitbit', 'Access', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
    //})
};


exports.fitbitCallback = function(req, res) {
    console.log('\n==== Get fitbit callback - fitbit.js =====\n');

    var params = req.query.state;
    var egle_token = params.substring(0, params.length - 1);
    var type_sync = params[params.length - 1];
    var actorId = jsonwebtoken.decode(egle_token).id;
    var objective_steps;

    getEgleObj(actorId, 'steps', function(err, objective) {
        if (err) {
            callback(err);
        } else {
            // console.log("Objective :",objective);
            objective_steps = objective;
        }
    });

    // TODO : Refresh token if needed (?) 

    // exchange the authorizationtion code we just received for an access token
    client.getAccessToken(req.query.code, callback_url).then(function(result) {
        // use the access token to fetch the user's profile information
        var weight, nbsteps, min_acti_light, min_acti_fair, min_acti_heavy, steps_goal;

        /* ====  GET WEIGHT ===== */
        client.get("/profile.json", result.access_token).then(function(results) {
            weight = results[0].user.weight;
        });

        /* ==== SYNC OBJECTIVES ====' */
        console.log('>> SYNC OBJECTIVES');
        /* Necessary because the app regurarly sends motivating notifications to the Fitbit band */
        client.get("/activities/goals/daily.json", result.access_token).then(function(results) {
            steps_goal = results[0].goals.steps;
            console.log("> Same objectives ?", objective_steps == steps_goal);
        });

        if (!objective_steps == steps_goal) {
            var data = {
                "steps": objective_steps,
                "distance": objective_steps / 1000,
                "floors": objective_steps / 500
            };
            client.post("/activities/goals/daily.json", result.access_token, data).then(function(results) {
                console.log("Objectives are now the same");
            });
        }


        /* GET ACTIVITIES */
        console.log('>> GET DATA');
        client.get("/activities/date/today.json", result.access_token).then(function(resultsAct) {
            nbsteps = resultsAct[0].summary.steps.toString();
            min_acti_light = resultsAct[0].summary.lightlyActiveMinutes.toString();
            min_acti_fair = resultsAct[0].summary.fairlyActiveMinutes.toString();
            min_acti_heavy = resultsAct[0].summary.veryActiveMinutes.toString();

            console.log("\n-------------------------------------------------------")
            console.log("Weight [kg] :", weight);
            console.log("Number of steps today :", nbsteps);
            console.log("High intensity activity [min] :", min_acti_heavy);
            console.log("Mid intensity activity [min] :", min_acti_fair);
            console.log("Low intensity activity [min] :", min_acti_light);
            console.log("Distance run today :" + resultsAct[0].summary.distances[1].distance);
            console.log("Resting heart rate today :" + resultsAct[0].summary.restingHeartRate);
            console.log("Number of minutes today where 98<BPM<137 :" + resultsAct[0].summary.heartRateZones[1].minutes);
            console.log("Number of minutes today where BPM>166 :" + resultsAct[0].summary.heartRateZones[2].minutes);
            console.log("-------------------------------------------------------\n")

            /* WRITE IN DB */
            console.log('>> WRITE THE ENTRY IN DATABASE ');
            var d = new Date();
            var actorId = jsonwebtoken.decode(egle_token).id;

            var theEntry = {
                userID: actorId,
                type: '',
                subType: '',
                value: '',
                values: '',
                datetimeAcquisition: d,
                comments: 'Fitbit',
                datetime: d,
                isSkipped: false,
                isValidated: false
            };


            switch (type_sync) {
                case '1':
                    theEntry.type = 'steps';
                    theEntry.value = nbsteps;
                    syncFitbitDB(actorId, theEntry, function(err, ok) {
                        if (err) {
                            return res.status(500).send(err);
                        } else {
                            // In case of success, redirect to homepage.
                            return res.redirect('/#');
                        }
                    });
                    break;

                case '2':
                    theEntry.type = 'activity';
                    theEntry.subType = 'sport';

                    async.series([
                        function(callback) {
                            theEntry.comments = 'Fitbit-sport-low';
                            theEntry.value = Number((min_acti_light / 60).toFixed(2));
                            theEntry.values = [{ type: 'intensity', value: 1, }];
                            syncFitbitDBb(actorId, theEntry);
                            callback(null, 1);
                        },
                        function(callback) {
                            theEntry.comments = 'Fitbit-sport-mid';
                            theEntry.value = Number((min_acti_fair / 60).toFixed(2));
                            theEntry.values = [{ type: 'intensity', value: 3, }];
                            syncFitbitDBb(actorId, theEntry);
                            callback(null, 2);
                        },
                        function(callback) {
                            theEntry.comments = 'Fitbit-sport-high';
                            theEntry.value = Number((min_acti_heavy / 60).toFixed(2));
                            theEntry.values = [{ type: 'intensity', value: 5, }];
                            syncFitbitDBb(actorId, theEntry);
                            callback(null, 3);
                        }
                    ], function(error, results) {
                        console.log(results);
                        if (error) {
                            return res.status(500).send(err);
                        } else {
                           return res.redirect('/#');
                        }

                    });
                    break;
                default:
                    console.log("Erreur fitbit.js - switch sync");

            }

        });
    });
}




var getEgleObj = function(patientID, type_entry, callback) {

    var query = {
        userID: patientID,
        select: type_entry
    };
    // console.log(query);

    obj.objectiveModel.findOne(query)
        .exec(function(err, objective) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Entries', 'Last', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {

                /*NB : null == undefined is true, but null === undefined is false. 
                Thus the code below, as is, will catch both undefined and null. */
                // Si il n'existe pas d'objectif Eglé, par défaut on le renvoie à 3000 steps

                if (objective == null) {
                    callback(null, 3000);
                } else {
                    callback(null, objective.values[0].value);
                }
            }
        });
}



var lastfitbitID = function(patientID, type_entry, filter_comment, callback) {
    // console.log("LAST :",type_entry,filter_comment);
    var yesterday = new Date();
    yesterday.setDate(yesterday.getDate() - 1);

    var query = {
        userID: patientID,
        type: type_entry,
        comments: filter_comment,
        datetimeAcquisition: {
            $gt: yesterday.setHours(23, 59, 59, 999),
            $lt: new Date().setHours(23, 59, 59, 999)
        }
    };

    db.entryModel.findOne(query)
        .sort({ "datetimeAcquisition": -1 })
        .exec(function(err, entry) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'Entries', 'Last', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                callback(err);
            } else {
                callback(null, entry);
            }
        });
}

var syncFitbitDB = function(patientID, theEntry, callback) {
    // Removes last Fitbit entry (if exists) before adding the updated one
    lastfitbitID(patientID, theEntry.type, theEntry.comments, function(err, entry) {
        if (err) {
            callback(err);
        } else {
            if (entry) {
                db.entryModel.remove({ _id: entry._id }, function(err) {
                    if (err) {
                        console.log(err);
                        audit.logEvent('[mongodb]', 'Entries', 'Delete', '', '', 'failed', 'Mongodb attempted to delete an entry');
                        callback(err);
                    } else {
                        console.log("> Previous Fitbit entry successfully removed.")
                    }
                });
            } else {
                console.log("> No Fitbit entry currently in DB.");
            }
            // Add new entry
            db.entryModel.create(theEntry, function(err, entries) {
                if (err) {
                    audit.logEvent('[mongodb]', 'Entries', 'Create', '', '', 'failed', 'Mongodb attempted to create an entry');
                    console.log(err);
                    callback(err);
                } else {
                    console.log("> Entry is added in DB.");
                    callback(null, "OK");
                }
            });
        }
    });
}


var syncFitbitDBb = function(patientID, theEntry) {
    // Removes last Fitbit entry (if exists) before adding the updated one
    async.series([
        function(callback) {
            lastfitbitID(patientID, theEntry.type, theEntry.comments, function(err, entry) {
                if (err) {
                    console.log(err);
                } else {
                    if (entry) {
                        db.entryModel.remove({ _id: entry._id }, function(err) {
                            if (err) {
                                console.log(err);
                                audit.logEvent('[mongodb]', 'Entries', 'Delete', '', '', 'failed', 'Mongodb attempted to delete an entry');
                                callback(err);
                            } else {
                                console.log("> Previous Fitbit entry successfully removed.")
                                    //callback(null,1);
                            }
                        });
                    } else {
                        console.log("> No Fitbit entry currently in DB.");
                    }
                }
            });
            callback(null, 1);
        },
        function(callback) {
            db.entryModel.create(theEntry, function(err, entries) {
                if (err) {
                    audit.logEvent('[mongodb]', 'Entries', 'Create', '', '', 'failed', 'Mongodb attempted to create an entry');
                    console.log(err);
                    callback(err);
                } else {
                    console.log("> Entry is added in DB.");
                    callback(null, 2);
                }
            });
            //callback(null,2);
        },

    ], function(error, results) {
        if (error) {
            return res.status(500).send(err);
        } else {
            // console.log("Order of execution :",results);
        }

    });
}










/* lastfitbitID(patientID, theEntry.type, theEntry.comments, function(err, entry){
                 if (err){
                 	 // callback(err);
                 } else {
                 	if(entry){
                 		db.entryModel.remove({_id : entry._id}, function(err){
               			 	if (err) {
                   		 	 console.log(err);
                  	    	 audit.logEvent('[mongodb]', 'Entries', 'Delete', '', '', 'failed', 'Mongodb attempted to delete an entry');
                  			// callback(err);                 	
              	  			} else {
                  			  console.log("Previous Fitbit entry successfully removed.")
                			}
            			});
            		} else {
                     	console.log("No Fitbit entry currently in DB.");
            		}
            		// Add new entry
                    db.entryModel.create(theEntry, function(err, entries) {
			                if (err){
			                    audit.logEvent('[mongodb]', 'Entries', 'Create', '', '', 'failed', 'Mongodb attempted to create an entry');
			                    console.log(err);
			                  //  callaback(err);
			                } else {
			                	console.log("Entry is added in DB.");
			                  //	callback(null,"OK");
			                }
	           		});
                 }
             });
}*/
