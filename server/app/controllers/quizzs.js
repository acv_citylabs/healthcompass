var dbUser          = require('../models/user');
var tokenManager    = require('../token_manager');
var jsonwebtoken    = require('jsonwebtoken');
var audit           = require('../audit-log');
var fs              = require('fs');
var _               = require('underscore');


// Retrieve quizzs
exports.list = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        var quizzsLib = fs.readFileSync("./quizzs/quizzs.json").toString();
        var quizzs = JSON.parse(quizzsLib);
        dbUser.userModel.findOne({
            _id: actorID
        }, {
            _id: 1, condition: 1, "achievements.quizzs": 1
        }).exec(function (err, user) {
            if (err) {
                audit.logEvent('[mongodb]', 'quizzs', 'List', '', '', 'failed', 'Mongodb attempted to retrieve a quizz');
                return res.status(500).send(err);
                console.log(err);
            } else {
                var quizzsToReturn = _.filter(quizzs, function (quizz){
                    var conditionOk = false;
                    for(var i = 0; i < quizz.condition.length; ++i){
                        if(user.condition.indexOf(quizz.condition[i]) > -1)
                            conditionOk = true;
                    }
                    return conditionOk;
                });
                for (i=0; i<quizzsToReturn.length; i++) {
                    if (user.achievements.quizzs.indexOf(quizzsToReturn[i].id) > -1) {
                        quizzsToReturn[i].bookmarked = true;
                    } else {
                        quizzsToReturn[i].bookmarked = false;
                    }
                }
                return res.json(quizzsToReturn);
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'quizzs', 'List', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
};

//Read a quizz
exports.read = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        if(req.params.id == undefined){
            audit.logEvent(actorID, 'quizzs', 'Read', '', '', 'failed', 
                           'The user could not read the quizz because one or more params of the request was not defined');
            return res.sendStatus(400);
        }
        var quizzsLib = fs.readFileSync("./quizzs/quizzs.json").toString();
        var quizzs = JSON.parse(quizzsLib);
        dbUser.userModel.findOne({
            _id: actorID
        }, {
            _id: 1, condition: 1, "achievements.quizzs": 1
        }).exec(function (err, user) {
            if (err) {
                console.log(err);
                audit.logEvent('[mongodb]', 'quizzs', 'Read', '', '', 'failed', 'Mongodb attempted to retrieve a quizz');
                return res.status(500).send(err);
            } else {
                //console.log(quizzs);
                var quizzToReturn = _.findWhere(quizzs, {id: req.params.id});
               // console.log(quizzToReturn);
                for(var i = 0; i < quizzToReturn.condition.length; ++i){
                    if(user.condition.indexOf(quizzToReturn.condition[i]) > -1){
                        if (user.achievements.quizzs.indexOf(quizzToReturn.id) > -1) {
                            quizzToReturn.bookmarked = true;
                        } else {
                            quizzToReturn.bookmarked = false;
                        }
                        return res.json(quizzToReturn);
                    }
                }
                
                return res.status(403).send(err);
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'quizzs', 'Read', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
};

// Mark a quizz as achieved
exports.bookmark = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token !== null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.body.id == undefined) {
            audit.logEvent(actorID, 'quizzs', 'Bookmark', '', '', 'failed',
                           'The user could not bookmark or unbookmark the quizz because one or more params of the request was not defined');
            return res.send(400);
        } else {
            dbUser.userModel.findOne({
                _id: actorID
            }).exec(function (err, user) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'quizzs', 'Bookmark', '', '', 'failed', 'Mongodb attempted to modify a user');
                    return res.status(500).send(err);
                } else {
                    var bookmarked;
                    var found = user.achievements.quizzs.indexOf(req.body.id);
                    if (found > -1) {
                        user.achievements.quizzs.splice(found, 1);
                        bookmarked = false;
                    } else {
                        user.achievements.quizzs.push(req.body.id);
                        bookmarked = true;
                    }
                    user.save(function (err) {
                        if (err) {
                            console.log(err);
                            audit.logEvent('[mongodb]', 'quizzs', 'Bookmark', "username", user.username, 'failed',
                                           "Mongodb attempted to save the modified user");
                            return res.status(500).send(err);
                        }
                        res.json({bookmarked: bookmarked});
                    });
                }
            });
        }
    } else {
        audit.logEvent('[anonymous]', 'quizzs', 'Bookmark', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
};