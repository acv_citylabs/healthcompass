var db = require('../models/objective');
var dbUser = require('../models/user');
var audit = require('../audit-log');
var tokenManager = require('../token_manager');
var jsonwebtoken = require('jsonwebtoken');

// Update objectives
exports.setObjectives = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    // console.log("objectives.js : req", req);
    if (req.params.username !== undefined) {
        dbUser.userModel.findOne({
                username: req.params.username
            }, { _id: 1 })
            .exec(function(err, user) {
                if (err) return res.status(500).send(err);
                objectives = JSON.parse(req.params.array);
                for (i = 0; i < objectives.length; i++) {
                    db.objectiveModel.create({
                        userID: user._id,
                        select: objectives[i].select,
                        type: objectives[i].type,
                        startValue: objectives[i].startValue,
                        value: objectives[i].value,
                        startDate: objectives[i].startDate,
                        endDate: objectives[i].endDate
                    }, function(err, temp) {
                        if (err) { console.log(err); } else {
                            console.log("Objective created");
                        }
                    });
                }

                objectives = JSON.parse(req.params.array2);
                for (i = 0; i < objectives.length; i++) {
                    db.objectiveModel.remove({
                        _id: objectives[i]._id
                    }, function(err, temp) {
                        if (err) { console.log(err); } else {
                            console.log("Objective deleted");
                        }
                    });
                }

                return res.json(null);
            });
    } else return res.send(400);

}

// Retrieve objectives for a specified patient 
exports.getObjectives = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null){
        //var actorID = jsonwebtoken.decode(token).id;
        //console.log(req.params.username);

        if (req.params.username !== undefined) {
            dbUser.userModel.findOne({
                    username: req.params.username
                }, { _id: 1 })
                .exec(function(err, user) {
                    if (err) {
                        console.log(err);
                        return res.status(500).send(err);
                    } else if(user){
                        db.objectiveModel.find({
                                userID: user._id,
                            })
                            .exec(function(err, objectives) {
                                if (err) {
                                    console.log(err);
                                    return res.status(500).send(err);
                                } else {
                                    return res.json(objectives);
                                }
                            });
                    } else {
                        return res.status(500);
                    }
                });
        } else return res.send(400);
    }
    else {
        return res.send(401); 
    }
}

// For the patient : retrieve objective with a specified metric 
exports.getObjective = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if (token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        if (req.params.select !== undefined) {
            db.objectiveModel.findOne({
                    userID: actorID,
                    select: req.params.select
                })
                .exec(function(err, objective) {
                    if (err) {
                        console.log(err);
                        audit.logEvent('[mongodb]', 'Objectives', 'Read', '', '', 'failed', 'Mongodb attempted to retrieve an objective');
                        return res.status(500).send(err);
                    } else {
                        return res.json(objective);
                    };
                });
        } else {
            audit.logEvent(actorID, 'Objectives', 'Read', '', '', 'failed',
                'The user could not retrieve an objective because one or more params of the request was not defined');
            return res.sendStatus(400);
        };
    } else {
        audit.logEvent('[anonymous]', 'Objectives', 'Read', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
};

// Update objectives
exports.setObjective = function(req, res) {
    var token = tokenManager.getToken(req.headers);

    if (token != null) {
        if (req.body.objective !== undefined) {
            var objective = req.body.objective;
            var decoded = jsonwebtoken.decode(token);
            objective.userID = decoded.id;
            db.objectiveModel.update({
                userID: objective.userID,
                select: objective.select
            }, objective, {
                upsert: true
            }).exec(function(err, ret) {
                if (err) {
                    console.log(err);
                } else {
                	return res.status(200).json({
                        user_id: objective.userID,
                        updated: true,
                    });
                };
            });
        } else {
            return res.send(400)
        }
    } else {
        audit.logEvent('[anonymous]', 'Objectives ctrl', 'setObjective', '', '', 'failed', 'The user was not authenticated');
        return res.sendStatus(401);
    }
}
exports.setObjectiveForUser = function(req, res) {
    if (req.params.name == null) return res.send(401);
    dbUser.userModel.findOne({ username: req.params.name }).exec(function(err, user) {
        if (req.params.array !== undefined) {
            objective = JSON.parse(req.params.array);
            db.objectiveModel.update({
                userID: user._id,
                select: objective.select
            }, {
                userID: user._id,
                select: objective.select,
                type: objective.type,
                //startValue: objective.startValue,
                values: objective.values,
                //endDate: objective.endDate,
                //startDate: objective.startDate
            }, {
                upsert: true
            }).exec(function(err, ret) {
                if (err) {
                    console.log(err);
                } else {
                    return res.json(null);
                }                
            });
        } else return res.send(400);
    });
}

getSplineInt = function(p, t, n) {
    var _pts = [],
        results = [],
        x, y, t1x, t2x, t1y, t2y, c1, c2, c3, c4, st, t, i;
    _pts = p.slice(0); // slice needed ?
    var tension = (t != null && t != "undefined") ? t : 0.4;
    var numOfSegments = (n != null && n != "undefined") ? n : 16;

    //_pts = pts.slice(0);
    // Duplicate beginning and end
    _pts.unshift(p[1]);
    _pts.unshift(p[0]);
    _pts.push(p[p.length - 2]);
    _pts.push(p[p.length - 1]);

    for (i = 2; i < (_pts.length - 4); i += 2) {
        // loop goes through each segment between the 2 pts + 1e point before and after
        for (t = 0; t <= numOfSegments; t++) {
            // Tension vectors
            t1x = (_pts[i + 2] - _pts[i - 2]) * tension;
            t2x = (_pts[i + 4] - _pts[i]) * tension;
            t1y = (_pts[i + 3] - _pts[i - 1]) * tension;
            t2y = (_pts[i + 5] - _pts[i + 1]) * tension;

            // Step
            st = t / numOfSegments;

            // Cardinals
            c1 = 2 * Math.pow(st, 3) - 3 * Math.pow(st, 2) + 1;
            c2 = -(2 * Math.pow(st, 3)) + 3 * Math.pow(st, 2);
            c3 = Math.pow(st, 3) - 2 *
                Math.pow(st, 2) + st;
            c4 = Math.pow(st, 3) - Math.pow(st, 2);

            // Spline points with common control vectors
            x = c1 * _pts[i] + c2 * _pts[i + 2] + c3 * t1x + c4 * t2x;
            y = c1 * _pts[i + 1] + c2 * _pts[i + 3] + c3 * t1y + c4 * t2y;

            results.push(x);
            results.push(y);
        }
    }
    return results;
}

exports.getSpline = function(req, res) {
    if (req.params.pts == null) return res.send(400);
    return res.json(getSplineInt(req.params.pts.split(','), req.params.tension, req.params.numOfSegments));
}

// Patient and doctor
exports.objAtDate = function(req, res) {
    var userID = null;
    // No username -> Patient side. We need its session userID.
    if (req.params.username == null || req.params.username == "undefined") {
        var token = tokenManager.getToken(req.headers);
        if (token == null) return res.send(401);
        userID = jsonwebtoken.decode(token).id;
    } else {
        // Doctor side. Test async, might have a problem here.
        dbUser.userModel.findOne({ username: req.params.username }).exec(function(err, user) {
            userID = user._id;
        });
    }
    if (req.params.type == null) return res.send(400);
    if (req.params.date == null) return res.send(400);
    db.objectiveModel.findOne({ userID: userID, select: req.params.type }).exec(function(err, obj) {
        return res.json(objAtDateInt(obj, Date.parse(req.params.date)));
    });
}

exports.objAtDateInt = function(obj, date) {
    var spline = getSplineInt(obj.points, 0.5, 16);
    var x = 100 * (date - obj.startDate) / (obj.endDate - obj.startDate);
    for (var i = 0; i < (spline.length); i += 2) {
        if (spline[i] > x) {
            var basis = Math.min(obj.startValue, obj.value);
            var relative = (spline[i + 1] / 100) * Math.abs(obj.value - obj.startValue);
            var total = (basis + relative);
            return total;
        }
    }
}