var dbMeal = require('../models/mealProduct');
var dbMeals = require('../models/meal');
var dbUser = require('../models/user');
var audit = require('../audit-log');
var tokenManager = require('../token_manager');
var jsonwebtoken = require('jsonwebtoken');
var fs = require('fs');
var csv = require("fast-csv");
var _ = require('underscore');
var formidable = require('formidable');
var util = require('util');

var create = function () {
	console.log('MEALS.JS - CREATE MEALS DB START');
    var name = "P";
    var counterName = 1;
    var dailyGoalKcal = 2500;

    var theEntry = {
        productID: "",
        translation: [{
            language: "",
            name: "",
            foodGroup: "",
            foodSubGroup: "",
            foodSubGroupType: "",
        }],
        serving: "",
        values: [],
        grade: 0,
        datetimeAcquisition: new Date(),
        datetime: new Date(),
        comments: ""
    }

    var nutrients = {
        carbohydrates: 0.55, //percentage
        totalFat: 0.27, //percentage
        saturatedFat: 0.06, //percentage
        proteins: 0.18, //percetage
        cholesterol: 150, //grams
        fiber: 30, //grams
        sodium: 1500, //miligrams
        potassium: 4700, //miligrams
        calcium: 1250, //miligrams
        magnesium: 500, //miligrams
        sugarKcal: 4,
        fiberKcal: 2,
        fatKcal: 9,
        proteinsKcal: 4,
        carbsKcal: 4,
        sodiumKcal: 0,
        calciumKcal: 0,
    };

    var nutrientSum = 0;
    var nutrientCounter = 0;

    var localwork = true;
    var stream;

    if(localwork) {
        stream = fs.createReadStream("/home/luka/Desktop/Food_ENG_FR.csv", {encoding: 'UTF-8'});
    } else {
        stream = fs.createReadStream("/opt/egle/server/app/databases/Food_ENG_FR.csv", {encoding: 'UTF-8'});
    }

    csv
     .fromStream(stream, {headers : true})
     .on("data", function(data){
         console.log(data);
         theEntry.productID = name + counterName;
         theEntry.serving = "100g",
         theEntry.values = []

         theEntry.translation.push({
            language: "english",
            name: data.alim_nom_eng,
            foodGroup: data.alim_grp_nom_eng,
            foodSubGroup: data.alim_ssgrp_nom_eng,
            foodSubGroupType: data.alim_ssssgrp_nom_eng
         });

         theEntry.translation.push({
            language: "french",
            name: data.alim_nom_fr,
            foodGroup: data.alim_grp_nom_fr,
            foodSubGroup: data.alim_ssgrp_nom_fr,
            foodSubGroupType: data.alim_ssssgrp_nom_fr
         });

         theEntry.values.push(
            {type: "kcal", value: data["Energy, Regulation EU No 1169/2011 (kcal/100g)"]},
            {type: "protein", value: data["Protein (g/100g)"]},
            {type: "carbs", value: data["Carbohydrate (g/100g)"]},
            {type: "fats", value: data["Fat (g/100g)"]},
            {type: "sugar", value: data["Sugars (g/100g)"]},
            {type: "fibres", value: data["Fibres (g/100g)"]},
            {type: "alcohol", value: data["Alcohol (g/100g)"]},
            {type: "cholesterol", value: data["Cholesterol (mg/100g)"]},
            {type: "calcium", value: data["Calcium (mg/100g)"]},
            {type: "sodium", value: data["Sodium (mg/100g)"]},
            {type: "saturated fat", value: data["FA saturated (g/100g)"]},
            {type: "potassium", value: data["Potassium (mg/100g)"]},
            {type: "magnesium", value: data["Magnesium (mg/100g)"]}
        );

         //calculate the product grade
         //----------------------------------------------------------------------
         // grade = kcal(foodproduct)/dailyGoalKcal X recommendedNutritionValue
         // calculate the number of kcals = 
         theEntry.values.forEach(function(product) {
            switch(product.type) {
                case 'protein':
                    nutrientSum += parseFloat((product.value).replace(',', '.'))*4/dailyGoalKcal*dailyGoalKcal*nutrients.proteins;
                    console.log(parseFloat((product.value).replace(',', '.'))*4/dailyGoalKcal);
                    console.log(dailyGoalKcal*nutrients.proteins);
                    console.log(parseFloat((product.value).replace(',', '.'))*4/dailyGoalKcal*dailyGoalKcal*nutrients.proteins);
                    console.log('--------------------------- 1 ---------------------------------');
                    nutrientCounter++;
                    break;
                case 'carbs':
                    nutrientSum += parseFloat((product.value).replace(',', '.'))*4/dailyGoalKcal*dailyGoalKcal*nutrients.carbohydrates;
                    console.log(parseFloat((product.value).replace(',', '.'))*4/dailyGoalKcal);
                    console.log(dailyGoalKcal*nutrients.carbohydrates);
                    console.log(parseFloat((product.value).replace(',', '.'))*4/dailyGoalKcal*dailyGoalKcal*nutrients.carbohydrates);
                    console.log('--------------------------- 2 --------------------------------');
                    nutrientCounter++;
                    break;
                case 'fats':
                    nutrientSum += parseFloat((product.value).replace(',', '.'))*9/dailyGoalKcal*dailyGoalKcal*nutrients.totalFat;
                    console.log(parseFloat((product.value).replace(',', '.'))*9/dailyGoalKcal);
                    console.log(dailyGoalKcal*nutrients.totalFat);
                    console.log(parseFloat((product.value).replace(',', '.'))*9/dailyGoalKcal*dailyGoalKcal*nutrients.totalFat);
                    console.log('------------------------------ 3 -----------------------------');
                    nutrientCounter++;
                    break;
                case 'fibres':
                    nutrientSum += parseFloat((product.value).replace(',', '.'))*2/dailyGoalKcal*60;
                    console.log(parseFloat((product.value).replace(',', '.'))*2/dailyGoalKcal);
                    console.log(60);
                    console.log(parseFloat((product.value).replace(',', '.'))*2/dailyGoalKcal*60);
                    console.log('------------------------------ 4 -----------------------------');
                    nutrientCounter++;
                    break;
                case 'saturated fat':
                    nutrientSum += parseFloat((product.value).replace(',', '.'))*9/dailyGoalKcal*dailyGoalKcal*nutrients.saturatedFat;
                    console.log(parseFloat((product.value).replace(',', '.'))*9/dailyGoalKcal);
                    console.log(dailyGoalKcal*nutrients.saturatedFat);
                    console.log(parseFloat((product.value).replace(',', '.'))*9/dailyGoalKcal*dailyGoalKcal*nutrients.saturatedFat);
                    console.log('------------------------------ 5 -----------------------------');
                    nutrientCounter++;
                    break;
            }
         });

         //console.log(nutrientSum);
         //console.log(nutrientCounter);
         theEntry.grade = Number(nutrientSum/nutrientCounter).toFixed(0);
         //theEntry.grade.toFixed(0);
         //----------------------------------------------------------------------
        nutrientCounter = 0;
        nutrientSum = 0;
        //console.log(theEntry);

        dbMeal.mealProductModel.create(theEntry, function(err, entries) {
            if (err) {
               audit.logEvent('[mongodb]', 'Entries', 'Create', '', '', 'failed', 'Mongodb attempted to create an entry');
               console.log(err);
           } else {
               console.log("entries.js - entry.id", entries);
           }
       });
        counterName++;
        theEntry.translation = [];
        theEntry.values = [];
    }); //END CSV PARSING
        //dbMeal.mealProductModel.ensureIndex({name: "text"});
        // console.log('INDEXES', dbMeal.mealProductModel.getIndexes());

        //dbMeal.mealProductModel.index({"name": "text"});
        //  })
        //  .on("end", function(){
        //      console.log("done");
        //  });

        // dbMeal.mealProductModel.find().exec(function (err, entries) {
        //     console.log('39: NASAO:', entries);
        // });
    console.log('MEALS.JS - CREATE MEALS DB END');
}

//create();

function uploadImage(file, actorID, old, callback) {
    //console.log('UPLOAD STARTED');
    if(file.size < 5242880){
        var name = file.name;
        var filename = name.substr(0, name.lastIndexOf("."));
        var fileExtension = name.substr(name.lastIndexOf("."), name.length - 1);
        //var tmpFile = "./tmp/" + keyGenerator.generateKey() + fileExtension;

        if (!fs.existsSync("./public/photosUpload/")) {
            fs.mkdirSync("./public/photosUpload/", function(err) {
                if(err) {
                    console.log(err);
                    callback(err);
                }
            });
        } else {
            //Create the new folder
            if(!fs.existsSync("./public/photosUpload/" + actorID)) {
                fs.mkdir("./public/photosUpload/" + actorID, function(err) {
                    if (err) {
                        console.log(err);
                        audit.logEvent('[fs]', 'Meals ctrl', 'Upload image', "Folder path",
                                       './public/photosUpload/', 'failed','FS attempted to create a folder');
                        callback(err);
                    } 
                });
            } else {
                //Create the new file
                var newPath = "./public/photosUpload/" + actorID + "/image" + filename + fileExtension;
                fs.readFile(file.path, function (err, data) {
                    if(err){
                        console.log(err);
                        callback(err);
                    } else {
                        fs.writeFile(newPath, data, function (err) {
                            if (err) {
                                console.log(err);
                                audit.logEvent('[fs]', 'Meals ctrl', 'Upload image', "File path", newPath, 'failed',
                                               'FS attempted to create a file');
                                callback(err);
                            } 
                            else {
                                callback(null, {image: actorID + "/image" + filename + fileExtension});
                            }
                        });        
                    }
                });
            }
            
        }
    } else {
        audit.logEvent(userID, 'Meals ctrl', 'Upload image', '', '', 'failed', 'The user tried to upload a photo but the file was too large');
        callback(null, {tooLarge: true});
    }
}

exports.mealStats = function(req, res) {
    // console.log('RADI');
    var token = tokenManager.getToken(req.headers);
    if(token != null) {
        //console.log('PARAMS:', req.params);
        var actorID = jsonwebtoken.decode(token).id;
        if (req.originalUrl.indexOf("/meal") === 4) {
            if (req.params.from !== undefined && req.params.to !== undefined) {
                mealStats(actorID, {from: req.params.from, to: req.params.to}, function(err, entries){
                    if (err){
                        res.status(500).send(err);
                    } else {
                        // console.log('RADI');
                        //console.log('ENTRIES:',entries);
                        return res.json(entries);
                    }
                });
            } else {
                audit.logEvent(actorID, 'Food', 'Retrieve stats', '', '', 'failed',
                    'The user could not retrieve the meal stats because one or more params of the request was not defined');
                return res.sendStatus(400);
            }
        }
    } else {
        audit.logEvent('[anonymous]', 'Food', 'Meal stats', '', '', 'failed','The user was not authenticated');
        return res.sendStatus(401); 
    }
}

/*
    Returns current daily calories consumption
*/
var mealStats = function(actorID, config, callback) {
    // console.log('RADI');
    var mealProducts = [];
    var totals =  0;
    var localsum = 0;
    //console.log(config.from, config.to);
    dbMeals.mealModel.find({
            userID: actorID,
            datetime: {
                $gt: new Date(config.from),
                $lt: new Date(config.to)
            }
    }, { values: 1 }).exec(function (err, entries) {
        if (err) {
            console.log(err);
            audit.logEvent('[mongodb]', 'Food', 'Retrieve meals', '', '', 'failed', 'Mongodb attempted to retrieve entries');
            callback(err);
        } else {            
            entries.forEach(function(value) {
                //console.log('VALUE:',value);
                value.values.forEach(function(val) { 
                    //console.log('VAL:', val); 
                    mealProducts.push(val.name); 
                }); 
            });
            //console.log('MEAL STATS', mealProducts, typeof(mealProducts));
            dbMeal.mealProductModel.find({ productID: { $in: mealProducts } }).exec(function (err, products) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'Food', 'Retrieve meals', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                    callback(err);
                } else {
                    products.forEach(function(product) {
                        //console.log('PRODUCT:',product);
                        //console.log(entries);
                        var amount =  _(entries).chain().
                            pluck('values').
                            flatten().
                            findWhere({name: product.productID}).
                            value();
                        //console.log('AMOUNT:',amount);
                        //console.log(parseFloat((product.values[0].value).replace(',', '.')));
                        localsum = parseFloat((product.values[0].value).replace(',', '.')*amount.amount);
                        //still need to multiply by the amount eaten
                        //due to some missing values in "kcal" field, this is only an approxmiate kcal total
                        if(!isNaN(localsum)) {
                            totals += localsum;
                        }
                    });
                    //console.log('TOTAL:', totals);
                    callback(null, totals.toFixed(0));
                }
            });            
        }
    });
}

exports.deleteMeal = function(req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null) {
        //console.log('PARAMS:', req.params);
        var actorID = jsonwebtoken.decode(token).id;
        if (req.originalUrl.indexOf("/meal") === 4) {
            if (req.params.id !== undefined) {
                dbMeals.mealModel.remove({ _id: req.params.id }, function(err) {
                    if (err) {
                        console.log(err);
                        audit.logEvent('[mongodb]', 'Entries', 'Delete', '', '', 'failed', 'Mongodb attempted to delete an entry');
                        return res.status(500).send(err);
                    } else {
                        return res.status(200).json({
                            entry_id: req.params.id,
                            removed: true,
                        });
                    }
                });
            } else {
                audit.logEvent(actorID, 'Food', 'Delete meal', '', '', 'failed',
                    'The user could not delete the entry because one or more params of the request was not defined');
                return res.sendStatus(400);
            }
        }
    } else {
        audit.logEvent('[anonymous]', 'Food', 'Delete meal', '', '', 'failed','The user was not authenticated');
        return res.sendStatus(401); 
    }
}

exports.getFood = function (req, res) {
    var token = tokenManager.getToken(req.headers);
    if(token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        var searchTerm = req.params.searchterm || '';
        if (req.originalUrl.indexOf("/meal") === 4) {
            //The actor is the patient
             //var patientCondition = jsonwebtoken.decode(token).condition;
            if(searchTerm !== '') {
                dbUser.userModel.findOne({_id: actorID}, { language: 1 }).exec(function (err, user) {
                    if(err) {
                        res.status(500).send(err);
                    } else {
                         
                             searchFood(searchTerm, 20, user.language, function(err, entries){
                                 if (err){
                                     res.status(500).send(err);
                                 } else {
                                     return res.json(entries);
                                 }
                             });
                         
                    }
                });
            } else {
                return res.json();
            }

        } else {
            return res.sendStatus(400); 
        }
    } else {
        audit.logEvent('[anonymous]', 'Food', 'GetFood', '', '', 'failed','The user was not authenticated');
        return res.sendStatus(401); 
    }
}

/*Search the DB for the specified string from user input*/
exports.searchFood = function(req, res) {
    console.log('SEARCH FOOD');
    var token = tokenManager.getToken(req.headers);
    if(token != null) {
        //console.log('PARAMS:', req.params);
        var actorID = jsonwebtoken.decode(token).id;
        var searchTerm = req.params.searchterm || '';
        var limit = 20;
        if(searchTerm !== ''){
            if (req.originalUrl.indexOf("/meal") === 4) {
                searchFood(searchTerm, limit, function(err, entries){
                     if (err){
                         res.status(500).send(err);
                     } else {
                        //console.log(entries);
                         return res.json(entries);
                     }
                 });
            }
        } else {
            return res.json();
        }
    } else {
        audit.logEvent('[anonymous]', 'Food', 'Search food', '', '', 'failed','The user was not authenticated');
        return res.sendStatus(401); 
    }
}

var searchFood = function(food, limit, language, callback) {
    //console.log('FOOD:', food);
    dbMeal.mealProductModel.find({$text: { $search: food, $language: language}}, {score: {$meta: "textScore"}}).sort({score:{$meta:"textScore"}}).limit(20).exec(function (err, entries) {
        if (err){
             console.log(err);
             audit.logEvent('[mongodb]', 'Food', 'Search Food', '', '', 'failed', 'Mongodb attempted to retrieve entries');
             callback(err);
         }
         else{
            //console.log('ENTRIES:',entries);
            callback(null, entries);
         }
    });
}

exports.saveMeal = function(req, res) {
    console.log('MEALS.JS','SAVE MEAL');
    var token = tokenManager.getToken(req.headers);
    if(token != null) {
        var actorID = jsonwebtoken.decode(token).id;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            if(err){
                console.log(err);
                audit.logEvent('[formidable]', 'Meals ctrl', 'Upload', "photo", 'failed', 
                               "Formidable attempted to read fields & files");
                return res.status(500).send(err);
            } else {
                var file = files.file;
                //console.log('FILES', files);
                //console.log('FIELDS',fields);

                saveMeal(fields, file, actorID, function(err, success){
                     if (err){
                         return res.status(500).send(err);
                     } else {
                         console.log('meals.js - ', success);
                         return res.sendStatus(200);
                     }
                 });
            }
        });
    } else {
        audit.logEvent('[anonymous]', 'Food', 'Save meal', '', '', 'failed','The user was not authenticated');
        return res.sendStatus(401); 
    }
}

var saveMeal = function(fields, file, actorID, callback) {
    var theEntry = {
        userID: actorID,
        type: "",
        values: [],
        datetimeAcquisition: ""
    }
    
    var parsedMeals = JSON.parse(fields.meal);
    //console.log(parsedMeals);
    
    var mealTime = new Date(fields.datetimeAcquisition.toString()).getHours();
    //console.log('Meal time: ', mealTime);
    theEntry.datetimeAcquisition = fields.datetimeAcquisition;

    if(fields.comments) {
        theEntry.comments = fields.comments;
    }

    if(mealTime >= 7 && mealTime < 10) { theEntry.type = "breakfast"; }
    else if(mealTime >=10 && mealTime <15) { theEntry.type = "lunch"; }
    else if(mealTime >=15) { theEntry.type = "dinner"; }

    parsedMeals.forEach(function(m) {
        //console.log('meals.js - ', m);
        theEntry.values.push(m);
    });
    //console.log('meals.js - ', theEntry.values);

    if(file) {
        if(file && file.path){
            uploadImage(file, actorID, undefined, function(err, data) {
                if(err){
                    callback(err);
                } else {
                    if(!data.image){
                        return res.json({errImage: data});
                    } else {
                        theEntry.photo = data.image;
                        dbMeals.mealModel.create(theEntry, function(err, entries) {
                            if (err) {
                                audit.logEvent('[mongodb]', 'Entries', 'Create', '', '', 'failed', 'Mongodb attempted to create an entry');
                                console.log(err);
                                callback(err);
                            } else {
                                callback(null, { inserted: true, entry_id: entries._id});
                            }
                        });
                    }
                }
            });
        }
    } else {
        dbMeals.mealModel.create(theEntry, function(err, entries) {
            if (err) {
                audit.logEvent('[mongodb]', 'Entries', 'Create', '', '', 'failed', 'Mongodb attempted to create an entry');
                console.log(err);
                //return res.status(500).send(err);
                callback(err);
            } else {
                callback(null, { inserted: true, entry_id: entries._id});
            }
        });
    }
}

exports.getProductsDetails = function(req, res) {
    //console.log('getProductsDetails', req.params);
    var token = tokenManager.getToken(req.headers);
      if(token != null) {
        //console.log('PARAMS:', req.params);
        var actorID = jsonwebtoken.decode(token).id;
        var productids = req.params.products || '';
        if(productids !== ''){
            if (req.originalUrl.indexOf("/meal") === 4) {
                searchFoodProductsDetails(productids, function(err, entries){
                     if (err){
                         res.status(500).send(err);
                     } else {
                        //console.log(entries);
                         return res.json(entries);
                     }
                 });
            }
        } else {
            return res.json();
        }
    } else {
        audit.logEvent('[anonymous]', 'Food', 'Save meal', '', '', 'failed','The user was not authenticated');
        return res.sendStatus(401); 
    }
}

var searchFoodProductsDetails = function (productids, callback) {
    //console.log(productids, typeof(productids), JSON.parse(productids), typeof(JSON.parse(productids)));
    dbMeal.mealProductModel.find({ productID: { $in: JSON.parse(productids) } }, {datetime: 0, datetimeAcquisition: 0, comments: 0}).exec(function (err, products) {
                if (err) {
                    console.log(err);
                    audit.logEvent('[mongodb]', 'Food', 'Retrieve food product details', '', '', 'failed', 'Mongodb attempted to retrieve entries');
                    callback(err);
                } else {
                    callback(null, products);
                }
            });
}
