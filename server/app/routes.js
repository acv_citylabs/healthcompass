var jwt = require('express-jwt');
var path = require('path');
var jsonwebtoken = require('jsonwebtoken');
var _ = require('underscore');
var tokenManager = require('./token_manager');
var nconf = require('nconf');
nconf.file("config/server.json");
var secret = nconf.get('token').secret;
var dbUser = require('./models/user');
var aclRoutes = require('./acl/routes.json');
var audit = require('./audit-log');
// var multiparty = require('connect-multiparty'),
//     multipartyMiddleware = multiparty({ uploadDir: '../photosUpload' });

// Controllers
var controllers = {};
controllers.users = require('./controllers/users');
controllers.entries = require('./controllers/entries');
controllers.hemoglobin = require('./controllers/hemoglobin');
controllers.patients = require('./controllers/patients');
controllers.chats = require('./controllers/chats');
controllers.notifications = require('./controllers/notifications');
controllers.tips = require('./controllers/tips');
controllers.quizzs = require('./controllers/quizzs');
controllers.audit = require('./controllers/audit');
controllers.events = require('./controllers/events');
controllers.charts = require('./controllers/charts');
controllers.advice = require('./controllers/advice');
controllers.objectives = require('./controllers/objectives');
controllers.contacts = require('./controllers/contacts');
controllers.ui = require('./controllers/ui');
controllers.techentry = require('./controllers/techentry');
controllers.fitbit = require('./controllers/fitbit');
controllers.withings = require('./controllers/withings');
// controllers.glystats = require('./controllers/glystats');
controllers.glycaemia = require('./controllers/glycaemia');
controllers.meal = require('./controllers/meal');

controllers.angular = function(req, res) { res.sendFile(path.join(__dirname, '../public', 'index.html')); };

controllers.andaman = function(req, res) { 
  var chart = req.params.chartType;
  var possibleTypes = ['weight','insulin','glycaemia','newglycaemia','steps','sport','allcharts'];

  if (_.contains(possibleTypes, chart)) {
    res.sendFile(path.join(__dirname, '../public/andaman_views/' + chart, chart + '.html')); 
  }else {
    return res.sendStatus(404);
  }  
};


//Routes
var routes = [

    // API ROUTES ===============================================================
    // === USERS ROUTES ========================================================
    // Send a link to reset user's passowrd
    {
        path: _.findWhere(aclRoutes, { id: 54 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 54 }).method,
        middleware: [controllers.users.lostPassword]
    },

    // Reset a password
    {
        path: _.findWhere(aclRoutes, { id: 55 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 55 }).method,
        middleware: [controllers.users.resetPassword]
    },

    // Sign in
    {
        path: _.findWhere(aclRoutes, { id: 0 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 0 }).method,
        middleware: [controllers.users.signin]
    },

    // Sign up
    {
        path: _.findWhere(aclRoutes, { id: 1 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 1 }).method,
        middleware: [controllers.users.signup]
    },

    // Sign up Andaman7 users
    {
        path: _.findWhere(aclRoutes, { id: 81 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 81 }).method,
        middleware: [controllers.users.signupa7]
    },

    // Activate a user account
    {
        path: _.findWhere(aclRoutes, { id: 56 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 56 }).method,
        middleware: [controllers.users.activation]
    },

    // Sign out
    {
        path: _.findWhere(aclRoutes, { id: 2 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 2 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.users.signout]
    },

    // Change user's password
    {
        path: _.findWhere(aclRoutes, { id: 13 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 13 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.users.changePassword]
    },

    // Update user infos
    {
        path: _.findWhere(aclRoutes, { id: 33 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 33 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.users.update]
    },

    // Get user's own profile
    {
        path: _.findWhere(aclRoutes, { id: 25 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 25 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.users.profile],
        access: _.findWhere(aclRoutes, { id: 25 }).roles
    },

    // === FITBIT & WITHINGS ==========================================================

    {
        path: _.findWhere(aclRoutes, { id: 71 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 71 }).method,
        //middleware: [jwt({secret: secret}), tokenManager.verifyToken, controllers.fitbit.fitbitAccess]
        middleware: [controllers.fitbit.fitbitAccess],
        access: _.findWhere(aclRoutes, { id: 71 }).roles
    },

    {
        path: _.findWhere(aclRoutes, { id: 72 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 72 }).method,
        middleware: [controllers.fitbit.fitbitCallback],
        access: _.findWhere(aclRoutes, { id: 72 }).roles
    },

    {
        path: _.findWhere(aclRoutes, { id: 73 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 73 }).method,
        middleware: [controllers.withings.withingsAccess],
        access: _.findWhere(aclRoutes, { id: 73 }).roles
    }, {
        path: _.findWhere(aclRoutes, { id: 74 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 74 }).method,
        middleware: [controllers.withings.withingsCallback],
        access: _.findWhere(aclRoutes, { id: 74 }).roles
    },

    // === UI ROUTES ==========================================================    
    // Build user's nav menu
    {
        path: _.findWhere(aclRoutes, { id: 17 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 17 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.ui.nav],
        access: _.findWhere(aclRoutes, { id: 17 }).roles
    },

    // Build user's cards (widgets)
    {
        path: _.findWhere(aclRoutes, { id: 18 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 18 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.ui.cards],
        access: _.findWhere(aclRoutes, { id: 18 }).roles
    },

    // Build user's asks (widgets)
    {
        path: _.findWhere(aclRoutes, { id: 48 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 48 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.ui.asks],
        access: _.findWhere(aclRoutes, { id: 48 }).roles
    },

    // Toggle the state of a card (widgets)
    {
        path: _.findWhere(aclRoutes, { id: 58 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 58 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.ui.toggleCard],
        access: _.findWhere(aclRoutes, { id: 58 }).roles
    },

    // Get cards settings (widgets)
    {
        path: _.findWhere(aclRoutes, { id: 26 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 26 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.ui.settings],
        access: _.findWhere(aclRoutes, { id: 26 }).roles
    },

    // Notify that the user "got it"
    {
        path: _.findWhere(aclRoutes, { id: 61 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 61 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.ui.gotit],
        access: _.findWhere(aclRoutes, { id: 61 }).roles
    },

    // Verify if the user "got it"
    {
        path: _.findWhere(aclRoutes, { id: 62 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 62 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.ui.verifyAppTip],
        access: _.findWhere(aclRoutes, { id: 62 }).roles
    },

    // Audit client and generate a CSV
    {
        path: _.findWhere(aclRoutes, { id: 8 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 8 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.ui.auditClient],
        access: _.findWhere(aclRoutes, { id: 8 }).roles
    },

    // Generate the todo list (widgets)
    {
        path: _.findWhere(aclRoutes, { id: 21 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 21 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.ui.todo],
        access: _.findWhere(aclRoutes, { id: 21 }).roles
    },


    // === IA ROUTES ========================================================
    {
        path: _.findWhere(aclRoutes, { id: 50 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 50 }).method,
        middleware: [jwt({ secret: secret })],
        access: _.findWhere(aclRoutes, { id: 50 }).roles
    }, {
        path: _.findWhere(aclRoutes, { id: 51 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 51 }).method,
        middleware: [jwt({ secret: secret }), controllers.advice.getAdvice],
        access: _.findWhere(aclRoutes, { id: 51 }).roles
    }, {
        path: _.findWhere(aclRoutes, { id: 52 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 52 }).method,
        middleware: [jwt({ secret: secret }), controllers.objectives.setObjectives],
        access: _.findWhere(aclRoutes, { id: 52 }).roles
    }, {
        path: _.findWhere(aclRoutes, { id: 53 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 53 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.objectives.getObjectives],
        access: _.findWhere(aclRoutes, { id: 53 }).roles
    }, {
        path: _.findWhere(aclRoutes, { id: 59 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 59 }).method,
        middleware: [jwt({ secret: secret }), controllers.techentry.settechentry],
        access: _.findWhere(aclRoutes, { id: 59 }).roles
    }, {
        path: _.findWhere(aclRoutes, { id: 60 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 60 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.techentry.gettechentry],
        access: _.findWhere(aclRoutes, { id: 60 }).roles
    }, {
        path: _.findWhere(aclRoutes, { id: 57 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 57 }).method,
        middleware: [jwt({ secret: secret }), controllers.objectives.setObjective],
        access: _.findWhere(aclRoutes, { id: 57 }).roles
    }, {
        path: _.findWhere(aclRoutes, { id: 64 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 64 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.objectives.getObjective],
        access: _.findWhere(aclRoutes, { id: 64 }).roles
    }, {
        path: _.findWhere(aclRoutes, { id: 65 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 65 }).method,
        middleware: [jwt({ secret: secret }), controllers.objectives.setObjectiveForUser],
        access: _.findWhere(aclRoutes, { id: 65 }).roles
    }, {
        path: _.findWhere(aclRoutes, { id: 66 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 66 }).method,
        middleware: [jwt({ secret: secret }), controllers.objectives.getSpline],
        access: _.findWhere(aclRoutes, { id: 66 }).roles
    }, {
        path: _.findWhere(aclRoutes, { id: 67 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 67 }).method,
        middleware: [jwt({ secret: secret }), controllers.objectives.objAtDate],
        access: _.findWhere(aclRoutes, { id: 67 }).roles
    },

    // === ENTRIES ROUTES ========================================================
    // Create a new entry
    {
        path: _.findWhere(aclRoutes, { id: 6 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 6 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.entries.create],
        access: _.findWhere(aclRoutes, { id: 6 }).roles
    },

    // Retrieve entries of a specified type (patient view)
    {
        path: _.findWhere(aclRoutes, { id: 46 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 46 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.entries.list],
        access: _.findWhere(aclRoutes, { id: 46 }).roles
    },

    // Retrieve entries of a specified type (physician view)
    {
        path: _.findWhere(aclRoutes, { id: 88 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 88 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.entries.list],
        access: _.findWhere(aclRoutes, { id: 88 }).roles
    },

    // Retrieve the last entry of a specified type (patient view)
    {
        path: _.findWhere(aclRoutes, { id: 70 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 70 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.entries.last],
        access: _.findWhere(aclRoutes, { id: 70 }).roles
    },

    // Delete an entry
    {
        path: _.findWhere(aclRoutes, { id: 20 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 20 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.entries.delete],
        access: _.findWhere(aclRoutes, { id: 20 }).roles
    },

    // Retrieve entries by type and date
    {
        path: _.findWhere(aclRoutes, { id: 93 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 93 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.entries.entriesFilter],
        access: _.findWhere(aclRoutes, { id: 93 }).roles
    },

    // === GLYCATED HEMOGLOBIN ROUTE ==========================================

    // Retrieve the estimated glycated hemoglobin
    {
        path: _.findWhere(aclRoutes, { id: 68 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 68 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.hemoglobin.computeValue],
        access: _.findWhere(aclRoutes, { id: 68 }).roles
    },
    {
        path: _.findWhere(aclRoutes, { id: 84 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 84 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.hemoglobin.computeValue],
        access: _.findWhere(aclRoutes, { id: 84 }).roles
    },

    // === CHAT ROUTES ========================================================
    // Get the list of my chats
    {
        path: _.findWhere(aclRoutes, { id: 7 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 7 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.chats.readChatsList],
        access: _.findWhere(aclRoutes, { id: 7 }).roles
    },

    // Read the messages after the specified date
    {
        path: _.findWhere(aclRoutes, { id: 15 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 15 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.chats.readChat],
        access: _.findWhere(aclRoutes, { id: 15 }).roles,
        verifyRelationship: true
    },

    // Send a message
    {
        path: _.findWhere(aclRoutes, { id: 9 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 9 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.chats.sendMessage],
        access: _.findWhere(aclRoutes, { id: 9 }).roles,
        verifyRelationship: true
    },

    // Manage WebRTC calls
    {
        path: _.findWhere(aclRoutes, { id: 63 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 63 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.chats.webrtc],
        access: _.findWhere(aclRoutes, { id: 63 }).roles,
        verifyRelationship: true
    },

    //Get food from the search field
    {
        path: _.findWhere(aclRoutes, { id: 87 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 87 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.meal.searchFood],
        access: _.findWhere(aclRoutes, { id: 87 }).roles,
        verifyRelationship: true
    },
    //save a meal
    {
        path: _.findWhere(aclRoutes, { id: 89 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 89 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.meal.saveMeal],
        access: _.findWhere(aclRoutes, { id: 89 }).roles
    },
    //get food products' details
    {
        path: _.findWhere(aclRoutes, { id: 92 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 92 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.meal.getProductsDetails],
        access: _.findWhere(aclRoutes, { id: 92 }).roles
    },
    // Set or reset my archive flag
    {
        path: _.findWhere(aclRoutes, { id: 5 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 5 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.chats.setArchiveFlag],
        access: _.findWhere(aclRoutes, { id: 5 }).rolesroles
    },

    // Delete the chat
    {
        path: _.findWhere(aclRoutes, { id: 10 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 10 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.chats.deleteChat],
        access: _.findWhere(aclRoutes, { id: 10 }).roles
    },

    // Get credentials to get access to the signaling server
    {
        path: _.findWhere(aclRoutes, { id: 37 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 37 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.chats.getSignalingToken],
        access: _.findWhere(aclRoutes, { id: 37 }).roles
    },


    // === NOTIFICATIONS ROUTES ========================================================
    // Retrieve last notifications (30 days)
    {
        path: _.findWhere(aclRoutes, { id: 22 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 22 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.notifications.list],
        access: _.findWhere(aclRoutes, { id: 22 }).roles
    },

    // Retrieve last notifications (7 days)
    {
        path: _.findWhere(aclRoutes, { id: 24 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 24 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.notifications.listLimited],
        access: _.findWhere(aclRoutes, { id: 24 }).roles
    },

    // Update a notification
    {
        path: _.findWhere(aclRoutes, { id: 32 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 32 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.notifications.update],
        access: _.findWhere(aclRoutes, { id: 32 }).roles
    },

    // === TIPS ROUTES ========================================================
    // Retrieve all tips
    {
        path: _.findWhere(aclRoutes, { id: 28 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 28 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.tips.list],
        access: _.findWhere(aclRoutes, { id: 28 }).roles
    },

    // Read a tip
    {
        path: _.findWhere(aclRoutes, { id: 34 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 34 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.tips.read],
        access: _.findWhere(aclRoutes, { id: 34 }).roles
    },

    // Bookmark or unbookmark a tip
    {
        path: _.findWhere(aclRoutes, { id: 36 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 36 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.tips.bookmark],
        access: _.findWhere(aclRoutes, { id: 36 }).roles
    },

    // === QUIZZES ROUTES ========================================================
    // Retrieve all quizzes
    {
        path: _.findWhere(aclRoutes, { id: 75 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 75 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.quizzs.list],
        access: _.findWhere(aclRoutes, { id: 75 }).roles
    },

    // Read a quizz
    {
        path: _.findWhere(aclRoutes, { id: 76 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 76 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.quizzs.read],
        access: _.findWhere(aclRoutes, { id: 76 }).roles
    },

    // Bookmark or unbookmark a quizz
    {
        path: _.findWhere(aclRoutes, { id: 77 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 77 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.quizzs.bookmark],
        access: _.findWhere(aclRoutes, { id: 77 }).roles
    },

    // === AUDIT ROUTES ========================================================
    // Retrieve audit logs by date
    {
        path: _.findWhere(aclRoutes, { id: 27 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 27 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.audit.listByDate],
        access: _.findWhere(aclRoutes, { id: 27 }).roles
    },

    // === PATIENTS ROUTES ========================================================
    // Retrieve a patient by ID in the medical record
    {
        path: _.findWhere(aclRoutes, { id: 3 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 3 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.patients.read],
        access: _.findWhere(aclRoutes, { id: 3 }).roles,
        verifyRelationship: true
    },

    // Retrieve entries of a specified patient and a specified type of entry (doctor view)
    {
        path: _.findWhere(aclRoutes, { id: 16 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 16 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.entries.list],
        access: _.findWhere(aclRoutes, { id: 16 }).roles,
        verifyRelationship: true
    },

    // Build a chart with a specified patient id, a type of chart and a date range (doctor view)
    {
        path: _.findWhere(aclRoutes, { id: 4 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 4 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.charts.build],
        access: _.findWhere(aclRoutes, { id: 4 }).roles,
        verifyRelationship: true
    },

    // Retrieve the estimated glycated hemoglobin value of a specified patient (doctor view)
    {
        path: _.findWhere(aclRoutes, { id: 69 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 69 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.hemoglobin.computeValue],
        access: _.findWhere(aclRoutes, { id: 69 }).roles,
        verifyRelationship: true
    },
    //get meal statistics
    {
        path: _.findWhere(aclRoutes, { id: 90 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 90 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.meal.mealStats],
        access: _.findWhere(aclRoutes, { id: 90 }).roles
    },
    //delete a meal
    {
        path: _.findWhere(aclRoutes, { id: 91 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 91 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.meal.deleteMeal],
        access: _.findWhere(aclRoutes, { id: 91 }).roles
    },

    // === CHARTS ROUTES ==========================================================    
    // Build a chart with a specified type of entry and a date range (patient view)
    {
        path: _.findWhere(aclRoutes, { id: 23 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 23 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.charts.build],
        access: _.findWhere(aclRoutes, { id: 23 }).roles
    },

    // === EVENTS ROUTES ==========================================================
    // Retrieve events in a specified date range
    {
        path: _.findWhere(aclRoutes, { id: 39 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 39 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.events.listByDateRange],
        access: _.findWhere(aclRoutes, { id: 39 }).roles
    },
    // Read an event
    {
        path: _.findWhere(aclRoutes, { id: 40 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 40 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.events.read],
        access: _.findWhere(aclRoutes, { id: 40 }).roles
    },
    // Create or update an event
    {
        path: _.findWhere(aclRoutes, { id: 41 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 41 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.events.createOrUpdate],
        access: _.findWhere(aclRoutes, { id: 41 }).roles
    },
    // Delete an event
    {
        path: _.findWhere(aclRoutes, { id: 42 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 42 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.events.delete],
        access: _.findWhere(aclRoutes, { id: 42 }).roles
    },
    // Retrieve the appointments for a specified date range
    {
        path: _.findWhere(aclRoutes, { id: 85 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 85 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.events.listAppointments],
        access: _.findWhere(aclRoutes, { id: 85 }).roles
    },

    // === CONTACTS ROUTES ==========================================================
    // Search contacts
    {
        path: _.findWhere(aclRoutes, { id: 29 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 29 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.contacts.search],
        access: _.findWhere(aclRoutes, { id: 29 }).roles
    },
    {
        path: _.findWhere(aclRoutes, { id: 86 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 86 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.meal.getFood],
        access: _.findWhere(aclRoutes, { id: 86 }).roles
    },
    // Search accepted contacts (relationship)
    {
        path: _.findWhere(aclRoutes, { id: 47 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 47 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.contacts.searchAccepted],
        access: _.findWhere(aclRoutes, { id: 47 }).roles
    },
    // User's contacts list (relationship request accepted)
    {
        path: _.findWhere(aclRoutes, { id: 30 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 30 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.contacts.list],
        access: _.findWhere(aclRoutes, { id: 30 }).roles
    },

    // User's contacts list (relationship request sent)
    {
        path: _.findWhere(aclRoutes, { id: 43 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 43 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.contacts.list],
        access: _.findWhere(aclRoutes, { id: 43 }).roles
    },

    // User's contacts list (relationship request received)
    {
        path: _.findWhere(aclRoutes, { id: 44 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 44 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.contacts.list],
        access: _.findWhere(aclRoutes, { id: 44 }).roles
    },

    // Create or Update a relationship
    {
        path: _.findWhere(aclRoutes, { id: 31 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 31 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.contacts.createOrUpdate],
        access: _.findWhere(aclRoutes, { id: 31 }).roles
    },

    // Delete a contact
    {
        path: _.findWhere(aclRoutes, { id: 38 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 38 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.contacts.delete],
        access: _.findWhere(aclRoutes, { id: 38 }).roles
    },

    // Read a contact
    {
        path: _.findWhere(aclRoutes, { id: 45 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 45 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.contacts.read],
        access: _.findWhere(aclRoutes, { id: 45 }).roles,
        verifyRelationship: true
    },

    // Read a contact (light view)
    {
        path: _.findWhere(aclRoutes, { id: 19 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 19 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.contacts.read],
        access: _.findWhere(aclRoutes, { id: 19 }).roles
    },

    // Frequent contacts
    {
        path: _.findWhere(aclRoutes, { id: 35 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 35 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.contacts.frequent],
        access: _.findWhere(aclRoutes, { id: 35 }).roles
    },

    // ANDAMAN ROUTES ========================================================
    // Route to handle all andaman requests
    {
        path: _.findWhere(aclRoutes, { id: 80 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 80 }).method,
        middleware: [controllers.andaman]
    },

     // === GLYCAEMIA STATS ==========================================

    // Retrieve statistics on glycaemia
    {
        path: _.findWhere(aclRoutes, { id: 82 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 82 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.glycaemia.getGlyStats],
        access: _.findWhere(aclRoutes, { id: 82 }).roles
    }, 

    {
        path: _.findWhere(aclRoutes, { id: 83 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 83 }).method,
        middleware: [jwt({ secret: secret }), tokenManager.verifyToken, controllers.glycaemia.getGlyStats],
        access: _.findWhere(aclRoutes, { id: 83 }).roles
    },

    // FRONTEND ROUTES ========================================================
    // Route to handle all angular requests
    {
        path: _.findWhere(aclRoutes, { id: 11 }).uri,
        httpMethod: _.findWhere(aclRoutes, { id: 11 }).method,
        middleware: [controllers.angular]
    }
];


module.exports = function(app) {
    _.each(routes, function(route) {
        // console.log("test? " + route.path);
        if (route.path == '/api/users/fitbitcallback' || route.path == '/api/users/withingscallback') {
            var args = _.flatten([route.path, route.middleware]);
            app.get.apply(app, args);
        } 
        else {
            route.middleware.unshift(ensureAuthorized);
            var args = _.flatten([route.path, route.middleware]);
            switch (route.httpMethod.toUpperCase()) {
                case 'GET':
                    app.get.apply(app, args);
                    break;
                case 'POST':
                    app.post.apply(app, args);
                    break;
                case 'PUT':
                    app.put.apply(app, args);
                    break;
                case 'DELETE':
                    app.delete.apply(app, args);
                    break;
                default:
                    throw new Error('Invalid HTTP method specified for route ' + route.path);
                    break;
            }
        }
    });
};


function ensureAuthorized(req, res, next) {
    if (_.contains(["*", "/andaman/:chartType", "/api/users/signin", "/api/users/signup", "/api/users/signupa7", "/api/users/activation", "/api/users/lostPassword", "/api/users/resetPassword", "/api/users/fitbitauth/:type", "/api/users/withingsauth"], req.route.path)) {
        // console.log("PK : " + req.route.path);
        return next();
    } else {
        var token, completeDecodedToken;
        token = tokenManager.getToken(req.headers);
        if (token) completeDecodedToken = jsonwebtoken.decode(token, { complete: true });
        if (completeDecodedToken && typeof completeDecodedToken.payload.id !== 'undefined') {
            if (completeDecodedToken.header.alg === 'HS256') {
                var decodedToken = completeDecodedToken.payload;
                var userID = decodedToken.id;
                dbUser.userModel.findOne({ _id: userID }, function(err, user) {
                    if (err) {
                        audit.logEvent('[mongodb]', 'Routes', 'Ensure authorized', 'userID', userID, 'failed', 'Mongodb attempted to retrieve a user');
                        console.log(err);
                        return res.sendStatus(500);
                    } else {
                        if (user !== null) {
                            var userRole = parseInt(user.role);
                            var route = _.findWhere(routes, {
                                path: req.route.path,
                                httpMethod: req.method
                            });
                            var allowedRoles = route.access;
                            if (typeof(allowedRoles) !== "undefined") {
                                var accessGranted = false;
                                for (i = 0; i < allowedRoles.length; i++) {
                                    if (userRole === allowedRoles[i]) accessGranted = true;
                                }
                                if (accessGranted) {
                                    if (typeof(route.verifyRelationship) !== "undefined" && route.verifyRelationship === true) {
                                        controllers.contacts.verifyRelationship(userID, req.params.id, req.params.username || req.body.username, function(err, authorized) {
                                            if (err) {
                                                return res.sendStatus(500);
                                            } else {
                                                if (authorized) {
                                                    req.forbidden = false;
                                                } else {
                                                    req.forbidden = true;
                                                }
                                                return next();
                                            }
                                        });
                                    } else {
                                        return next();
                                    }
                                } else {
                                    console.log('Forbidden for his role');
                                    audit.logEvent(user.username, 'Routes', 'Ensure authorized', 'route', req.route.path, 'failed',
                                        'The user tried to access a route which is forbidden for his role');
                                    return res.sendStatus(403);
                                }
                            } else { // typeof allowedRoles is undefined
                                return next();
                            }
                        } else {
                            console.log('User not found (' + userID + ')');
                            audit.logEvent('[anonymous]', 'Routes', 'Ensure authorized', 'userID', userID, 'failed', 'User not found');
                            return res.sendStatus(401);
                        }
                    }
                });
            } else {
                console.log('ensureAuthorized received a suspicious token with customized algorithm (' + completeDecodedToken.header.alg + ')');
                audit.logEvent('[anonymous]', 'Routes', 'Ensure authorized', 'token', completeDecodedToken.header.alg, 'failed', 'Received a suspicious token with customized algorithm');
                return res.sendStatus(401);
            }
        } else {
            console.log('ensureAuthorized did not receive a valid token ("', token, '")');
            audit.logEvent('[anonymous]', 'Routes', 'Ensure authorized', 'token', token, 'failed', 'Did not receive a valid token');
            return res.sendStatus(401);
        }
    }
}
