var mongoose = require('mongoose');

// Watch schema
var Schema = mongoose.Schema;
var iaWatch = new Schema({
    userID : { type: String, required: true },
    type : { type: String, required: true },
    endDate : { type: Date, required: true },
	scenario : { type: String, required: true },
	action : { type: String, required: true },
    isActive : { type: Boolean, default: true }
});

//Define Models
var iaWatchModel = mongoose.model('iaWatch', iaWatch);

// Export Models
exports.iaWatchModel = iaWatchModel;