var mongoose = require('mongoose');

// Metric schema
var Schema = mongoose.Schema;
var Metric = new Schema({
	name: { type: String, required: true },
	type: { type: String, required: true },
	isARate: { type: Boolean, default: false },
	period: { type: Number, required: true },
	measurementTimes: [{
		name: { type: String, required: true },
		startTime: { type: Number, required: true },
		stopTime: { type: Number, required: true }
	}]
});

//Define Models
var metricModel = mongoose.model('Metric', Metric);

// Export Models
exports.metricModel = metricModel;