var mongoose = require('mongoose');

// Entry schema
var Schema = mongoose.Schema;
var Objective = new Schema({
    userID: { type: String, required: true },
    select: { type: String, required: true },
    type: { type: String, required: true },
    startValue : { type: Number, required: false },
    values: [{
        type: { type: String, required: false },
        value : { type: Number, required: false },
        subType: { type: String, required: false },
    }],
    startDate : { type: Date, default: Date.now },
    endDate : { type: Date, default: Date.now },
	points : { type : Array , "default" : [] }
});


//Define Models
var objectiveModel = mongoose.model('Objective', Objective);

// Export Models
exports.objectiveModel = objectiveModel;