var mongoose = require('mongoose');

// Notification schema
var Schema = mongoose.Schema;
var iaFollowup = new Schema({
    userID : { type: String, required: true },
    objectiveID : { type: String, required: true },
	lastEntryID: { type: String, required: true },
	
    type: { type: String, required: true },
    period: { type: String, required: true },
    date : { type: Date, default: Date.now },
    endDate : { type: Date, default: Date.now },
    lastCheck : { type: Date, default: Date.now },
    isARate : { type: Boolean, required: true },
    isActive : { type: Boolean, default: true },
    severity: { type: Number, default: 50 },
});

//Define Models
var iaFollowupModel = mongoose.model('iaFollowup', iaFollowup);

// Export Models
exports.iaFollowupModel = iaFollowupModel;