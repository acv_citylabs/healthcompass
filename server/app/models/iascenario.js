var mongoose = require('mongoose');

// Scenario schema
var Schema = mongoose.Schema;
var iaScenario = new Schema({
    userID : { type: String, required: true },
    value: { type: String, required: true },
    type: { type: String, required: true },
    datetime : { type: Date, default: Date.now },
	learned: { type: Boolean, default: false},
});

//Define Models
var iaScenarioModel = mongoose.model('iaScenario', iaScenario);

// Export Models
exports.iaScenarioModel = iaScenarioModel;