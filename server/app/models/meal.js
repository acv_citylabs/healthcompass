var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Entry schema
var Meal = new Schema({
    userID: { type: String, required: true }, //user ID
    type: { type: String, required: true}, //breakfast, snack, lunch, dinner
    values: [{
        name: { type: String, required: true }, //product ID
        amount: { type: String, required: true }, //number of servings
    }],
    datetimeAcquisition : { type: Date, default: Date.now },
    comments : String,
    photo: { type: String, required: false},
    datetime : { type: Date, default: Date.now },
    isSkipped: { type: Boolean, default: false },
	isValidated : { type: Boolean, default: false }
});


//Define Models
var mealModel = mongoose.model('Meal', Meal);

// Export Models
exports.mealModel = mealModel;
