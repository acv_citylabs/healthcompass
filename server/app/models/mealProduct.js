var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Meal product schema
var MealProduct = new Schema({
    productID: { type: String, required: true },
    translation: [{
        language: { type: String, required: true },
        name: { type: String, required: true, text: true}, //product name
        foodGroup: { type: String, required: true }, //meat, egg and fish
        foodSubGroup: { type: String, required: true }, //raw meat
        foodSubGroupType: { type: String, required: false } //offals
    }],
    values: [{
        type: { type: String, required: false }, //fats, salt, chromium, sodium, carbs, calories etc.
        value : { type: String, required: false }, //intake in grams, spoons, or mg
    }],
    serving: { type: String, required: false }, //1 spoon, 1 bowl, 1 plate etc.
    grade: { type: String, required: false , default: 0},
    datetimeAcquisition : { type: Date, default: Date.now },
    comments : { type: String, required: false }
});


//Define Models
var mealProductModel = mongoose.model('MealProduct', MealProduct);
//MealProduct.index({ name: "text"}); // schema level

// Export Models
exports.mealProductModel = mealProductModel;