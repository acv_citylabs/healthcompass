var dbMetric = require('../models/metric');
var Schedule = require('./schedule');
var Supervisor = require('./supervisor');
var Intel = require('./intel');
//var async = require('async');

function Cron(){
	this._schedule = new Schedule();
	this._supervisor = new Supervisor();
	this._intel = null;
	this._parameters = [];
	this.init();
};

Cron.prototype.init = function init(parameters){
	this._intel = new Intel();
	if (!parameters) this._parameters = this.setDefaultParameters();
	this._checkMetrics();
	this._supervisor.init(this._parameters.supervisor, this._intel);
	this._intel.registerSupervisor(this._supervisor);
	this._intel.init(this._parameters.intel);
	this._schedule.init(this._parameters.schedule);
	this._schedule.addObserver(this._supervisor);
	this._schedule.addObserver(this._intel);
	this.run();
};

Cron.prototype.setDefaultParameters = function setDefaultParameters(){
	var defaultParameters = {
		supervisor: {
			checkMissingEntries : true,
			checkGaps : true,
			checkFollowUps : true,
			FOLLOWUP_LENGTH : 15,
			SEVERITY_PENALTY : 4
		},
		schedule: {
			interval: 1*60*60*1000 // 1 hours step.
		},
		intel: {
		}
	}
	return defaultParameters;
}

Cron.prototype.run = function run(){
	console.log("-- IA Module disabled.");
	//this._schedule.run();
};

Cron.prototype._checkMetrics = function checkMetrics(){
	var metrics = [
		{
			name: "Blood Pressure",
			type: "blood_pressure",
			isARate: true,
			period: 5,
			measurementTimes: [
				{
					name: "morning",
					startTime: 8,
					stopTime: 11
				},
				{
					name: "noon",
					startTime: 11,
					stopTime: 15
				},
				{
					name: "afternoon",
					startTime: 15,
					stopTime: 20
				}
			]
		},{
			name: "Glucose",
			type: "glucose",
			isARate: true,
			period: 5,
			measurementTimes: [
				{
					name: "morning",
					startTime: 8,
					stopTime: 11
				},
				{
					name: "noon",
					startTime: 11,
					stopTime: 15
				},
				{
					name: "afternoon",
					startTime: 15,
					stopTime: 20
				}
			]
		}
	];
	for (var m in metrics){
		dbMetric.metricModel.update(
			{ name : metrics[m].name },
			{
				name : metrics[m].name,
				type : metrics[m].type,
				isARate : metrics[m].isARate,
				period : metrics[m].period,
				measurementTimes : metrics[m].measurementTimes
			},
			{ upsert : true }
		).exec(function(err, ret) {
			if (err) console.log(err);
		});
	}
};

exports = module.exports = new Cron();
