var qh = require('./queriesHandler');
/***************************************************************/
function Intel(){
	this._step = 24; // Once a day.
	this._perceptron_mode = "normal";
	this._HMM_mode = "normal";
	this._supervisor = null;
};

Intel.prototype.init = function init(parameters) {
	// Oh hello there
};

Intel.prototype.registerSupervisor = function registerSupervisor(supervisor) {
	this._supervisor = supervisor;
};

Intel.prototype.update = function update(today) {
	console.log("[CRON JOB] - Intel updated.");
	this.predictBadScenarii();
	this.flushOutdatedWatches();
	this.toggleMode();
};

Intel.prototype.toggleMode = function toggleMode() {
	// Check if we need to switch perceptron & HMM in learning mode for someone.
	// Autoevaluation periodique.
	// TODO
};

Intel.prototype.flushOutdatedWatches = function flushOutdatedWatches() {
	var that = this;
	// Remove outdated watches. Learn if failure.
	qh.listUsersByRoles([1,2,3], function(err, users){users.forEach(function(user){
		qh.listActiveIAWatchByUser(user, function(err, watches){watches.forEach(function(watch){
			if((watch.scenario == "B" || watch.scenario == "N") && watch.endDate < new Date()) qh.disableIAWatch(watch); // Success
			if((watch.scenario == "G" && watch.endDate < new Date())) that.scenarioFound(user, "learning", scenario, watch.type, function(){}); // Failure & learn
		})});
	})});
}


Intel.prototype.scenarioFound = function scenarioFound(user, mode, scenario, variable, callback) {
	if(variable == "sport") variable = "P";
	if(variable == "glycaemia") variable = "G";
	if(variable == "diet") variable = "D";
	if(scenario == "W") scenario = "B";
	if(scenario == "H") scenario = "G";
	
	// Should we trigger a learn ? Prelearning throws a random action but will recieve the outcome later through the supervisor, in learning mode
	if (mode == "normal" && this._perceptron_mode == "learning") mode = "prelearning";
	
	var arguments = {
		'userID' : user._id,
		'mode' : mode,
		'scenario' : scenario+variable,
		'age' : this.getUserAge(user),
		'rigor' : 1,
		'powerUser' : 1
	};
	
	/* Generic unit test training set
	var arguments = {'userID' : user._id, 'mode' : "learningBatchFromFile", 'fileName' : "trainingperceptron.dat"};
	*/
	
	var output = '';

	var spawn = require('child_process').spawn,
		pythonProcess = spawn('python', ['app/python/perceptron.py', JSON.stringify(arguments)]);
		pythonProcess.stdout.on('data', function(data) { output += data; });
		pythonProcess.stderr.on('data', function(data) { output += 'stderr: ' + data; });
		pythonProcess.on('close', function (code) { 
			//console.log(output); 
			return callback(output);
		});
		
	
};



Intel.prototype.getUserAge = function getUserAge(user) {
	if (user.birthdate){
		return new Date(new Date - user.birthdate).getFullYear()-1970;
	} else {
		return 0
	}
};

Intel.prototype.predictBadScenarii = function scenarioFound() {
	var that = this;
	if(this._HMM_mode == 'normal'){
		qh.listUsersByRoles([1,2,3], function(err, users){users.forEach(function(user){
			var lastScenarii = {};
			var lastEntries = {};
			qh.listObjectivesByUser(user, function(err, objectives){
				objectives.forEach(function(objective){
					qh.listLastIAScenarioByUserTByType(user, objective.select, function(err, scenario){
						if (err) return console.log(err);
						lastScenarii[objective.select] = (scenario) ? scenario : 0 ;
						qh.lastEntryByUserByType(user, objective.select, function(err, entry){
							if (err) return console.log(err);
							lastEntries[objective.select] = (entry) ? entry : 0 ;
							if(Object.keys(lastScenarii).length == objectives.length && Object.keys(lastEntries).length == objectives.length) that.getLastPeriodState(user, objectives, lastScenarii, lastEntries);
						});
					});
				});
			});
		});});
	}
}
	
Intel.prototype.getRigor = function getRigor(user) {
	return 1;
}

Intel.prototype.getFrequency = function getFrequency(user) {
	return 1;
}
	
Intel.prototype.getLastPeriodState = function getLastPeriodState(user, objectives, lastScenarii, lastEntries) {
	var a = {"BG":-1,"BP":-1,"BD":-1};
	for (var i = 0 ; i < objectives.length ; i++) {
		var type = objectives[i].select;
		switch (type){
			case "glycaemia":
				var refDate = new Date();
				refDate.setDate(refDate.getDate() - 1);
				if(lastScenarii[type]!=0) {
					if(lastScenarii[type].datetime > refDate) a["BG"] = 1;
					else if(lastEntries[type].datetimeAcquisition > refDate) a["BG"] = 0;
				} else if(lastEntries[type].datetimeAcquisition > refDate) a["BG"] = 0;
				break;
			case "sport":
				var refDate = new Date();
				refDate.setDate(refDate.getDate() - 7);
				if(lastScenarii[type]!=0) {
					if(lastScenarii[type].datetime > refDate) a["BP"] = 1;
					else if(lastEntries[type].datetimeAcquisition > refDate) a["BP"] = 0;
				} else if(lastEntries[type].datetimeAcquisition > refDate) a["BP"] = 0;
				break;
			case "diet":
				var refDate = new Date();
				refDate.setDate(refDate.getDate() - 1);
				if(lastScenarii[type]!=0) {
					if(lastScenarii[type].datetime > refDate) a["BD"] = 1;
					else if(lastEntries[type].datetimeAcquisition > refDate) a["BD"] = 0;
				} else if(lastEntries[type].datetimeAcquisition > refDate) a["BD"] = 0;
				break;
			}
	}
	a["RI"] = this.getRigor(user);
	a["FR"] = this.getFrequency(user);
	a["userID"] = user._id;
	a["mode"] = "normal";
	
	/* Generic unit test training set
	var arguments = {'userID' : user._id, 'mode' : "learningBatchFromFile", 'fileName' : "bayesian.dat"};
	*/
	var that = this
	var output = '';
	var spawn = require('child_process').spawn,
		pythonProcess = spawn('python', ['app/python/bayesian.py', JSON.stringify(a)]);
		pythonProcess.stdout.on('data', function(data) { output += data; });
		pythonProcess.stderr.on('data', function(data) { output += 'stderr: ' + data; });
		pythonProcess.on('close', function (code) { 
			that._supervisor.warnUser(user, output);
		});
	
};

///////////////////////////////////////////////
// OLD UNUSED CODE
///////////////////////////////////////////////

Intel.prototype.getTreshold = function getTreshold(user, objective) {
	// At a specific date .
	return Math.abs(objective.value - objective.startValue) * 0.2;
};
	
module.exports = Intel;