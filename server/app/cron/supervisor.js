var qh = require('./queriesHandler');
var utils = require('./utils');
var objectiveController = require('../controllers/objectives');
var Polyfit = require('./polyfit');

/*******************************************************************************/
function Supervisor(){
	this._step = 3; // 8 times a day 
	this._intel = null;
	//this.FOLLOWUP_LENGTH = 15;
	//this.SEVERITY_PENALTY = 4;
	this.today = new Date("2015-10-08T00:00:00");
	//qh.techRemoveFollowUps();
	//qh.techRemoveNotifications();
	
};
Supervisor.prototype.init = function init(parameters, intel) {
	//this._checkMissingEntriesFlag = parameters.checkMissingEntries;
	//this._checkGapsFlag = parameters.checkGaps;
	//this._checkFollowUpsFlag = parameters.checkFollowUps;
	this._intel = intel;
};
Supervisor.prototype.update = function update(today) {
	this.today = today;
	console.log("[CRON JOB] - Supervisor updated.");
	//if(this._checkMissingEntriesFlag) this.checkMissingEntries();
	//if(this._checkGapsFlag) this.checkGaps();
	//if(this._checkFollowUpsFlag) this.checkFollowUps();
	this.newCheckGaps();
};

Supervisor.prototype.newCheckGaps = function newCheckGaps(){

	var that = this;

	qh.listUsersByRoles([1,2,3], function(err, users){users.forEach(function(user){
		//qh.insertDummyGlycaemia(user); //For unit testing, provide history.
		qh.listObjectivesByUser(user, function(err, objectives){objectives.forEach(function(objective){
			var userEntries = [];
			qh.listNewEntriesByUserForObjective(user, objective, function(err, newEntries){
				if(newEntries.length > 0){

					switch(objective.select){
						case 'glycaemia':
						
							//Get at least 10 for spline and 40 for polyfit
							var nbOldEntries = 50 - newEntries.length;
							qh.listNbOldEntriesByUserForObjective(user, objective, nbOldEntries, function(err, oldEntries){
								if(err) console.log(err);
								var entries = newEntries.concat(oldEntries);
								entries = entries.reverse();
								if (entries.length >= 30){
								
									// Then we talk. Getting the upper bound
									var glycaemiaMax = 0;
									var glycaemiaMin = objective.value;
									for(var i=0; i < objectives.length ; i++){
										if(objectives[i].select == "glycaemiaMax") glycaemiaMax = objectives[i].value;
									}
								
									var splineEntries = entries.splice(entries.length-10, 10);

									
									// Polynomial fit to obtain user habits
									var x = [], y = [], sum = 0;
									for(var i=0; i < entries.length ; i++){
										var temp = entries[i].datetimeAcquisition.getHours()+(entries[i].datetimeAcquisition.getMinutes()/60);
										x.push(temp);
										y.push(entries[i].value/100);
										sum += parseFloat(entries[i].value);
									}
									var average = sum / entries.length;
									var poly = new Polyfit(x, y);
									var solver = poly.getPolynomial(14);
									
									// Cubic hermite spline on residual after habits removal
									var splineData = [];
									for(var i=0; i < splineEntries.length ; i++){
										var temp = splineEntries[i].datetimeAcquisition.getHours()+(splineEntries[i].datetimeAcquisition.getMinutes()/60);
										var correction = (solver(temp)*100) - average;
										splineData.push(i);
										splineData.push(splineEntries[i].value - correction);
									}
									// Which direction do we consider ? Glycemia or glycemiamax ? We take the average of the two last entries, for consistency.
									var reference = (splineData[splineData.length-1] +  splineData[splineData.length-3] ) / 2;
									var direction = (reference - glycaemiaMin > glycaemiaMax - reference) ? -1 : 1;
									var shapes = utils.getShapes(utils.getSpline(splineData), direction);
									that.findScenario(user, objective.select, shapes);
								}
							});
							break;
						case 'sport':
							// We need to sum up weekly data - globalize
							// By the way this is not working, since it is called sport, and sport entries are activities
							var nbOldEntries = 30 - newEntries.length;
							qh.listNbOldEntriesByUserForObjective(user, objective, nbOldEntries, function(err, oldEntries){
								if(err) console.log(err);
								var entries = newEntries.concat(oldEntries);
								entries = entries.reverse();
								var globalized = {};
								var week = 0;
								for(var i=0; i < entries.length ; i++){
									week = (entries[i].datetimeAcquisition.getYear()-2016)*52 + utils.getWeek(entries[i].datetimeAcquisition);
									if(!(week in globalized)) globalized[week] = 0;
									if(entries[i].values.length > 0)
										globalized[week] += entries[i].value * Math.log(entries[i].values[0].value);
								}
								if (globalized.length > 8){
									// We take the before-last week as a reference since the last week is still ongoing
									var direction = (objective.value > globalized[week-1]) ? 1 : -1;
									var shapes = utils.getShapes(utils.getSpline(splineData), direction);
									that.findScenario(user, objective.select, shapes);
								}
							});
						case 'diet': return
						case 'weight': return
						default: return
					}
				}
				/*newEntries.forEach(function(entry){
					qh.validateEntry(entry, function(err){ if(err) console.log(err); });
				});*/
			});
		})});		
	})});
}

Supervisor.prototype.findScenario = function findScenario(user, type, shapes) {
	var that = this;
	var lastShapes = shapes.slice(-4);
	var scenario = "";
	var action = "";
	
	//console.log (shapes);
	
	// Scenarii detection
	var scenarii = {
		G: [[7,6,5], [7,6,5], [7,6,5], [7,6,2]], //Good
		H: [[3,2,1], [3,2,1], [3,2,1], [3,4,5]], //Hope
		W: [[7,6,5], [7,6,5], [7,6,5], [5,4,3]], //Warning
		B: [[3,2,1], [3,2,1], [3,2,1], [2,1]],   //Bad
		N: [[3,4,5], [3,4,5], [3,4,5], [3,4,5]], //Neutral
	}

	for (var i in scenarii){
		var found;
		for (var s in lastShapes){
			found = false;
			// Compare first shape to first group
			for (var ii in scenarii[i][s]) {
				if (lastShapes[s] == scenarii[i][s][ii]) found = true; 
			}
			if(!found) break;
		}
		if(found) scenario = i;
	}
	if(scenario) that._intel.scenarioFound(user, "normal", scenario, type, function(action){
		qh.createIAScenario(user, type, scenario, function(err, createdScenario){
			if (err) return console.log(err);
			// If scenario is neutral / bad and perceptron was learning, make it learn !
			qh.listActiveIAWatchByUserByType(user, type, function(err, watch){
				if (watch){
					qh.disableIAWatch(watch);
					if(scenario == "B" || scenario == "N") that._intel.scenarioFound(user, "learning", scenario, type, function(output){return});
					// One shoud put here the code below, but its a bit ugly then and if breaks.
				}
			});
			// In case of perceptron learning, setup a IAwatch

			if(action.indexOf("/") > -1) action = action.split("/");

			if (action[0] == "prelearn" && !(scenario == "N" && type == "glycaemia")){
				qh.listActiveIAWatchByUserByType(user, type, function(err, watch){
					if (err) return console.log(err);
					if (!watch){
						var watchEndDate = new Date();
						var numberOfDaysToAdd = (type == "sport") ? 7 : 1;
						watchEndDate.setDate(watchEndDate.getDate() + numberOfDaysToAdd);
						qh.createIAWatch(user, type, scenario, watchEndDate, action[1]);
					}
				});

			} 
			// Acting
			switch(action.trim()){
				case "Notification":
					if(scenario=="G") that.createGenericNotification(user, 1, type, "Félicitations, continuez comme cela !");
					if(scenario=="N") that.createGenericNotification(user, 1, type, "N'oubliez pas vos objectifs !");
					if(scenario=="B") that.createGenericNotification(user, 1, type, "Aie, ne vous relâchez pas, courage !");				
					break;
				case "Email": 
					console.log("ACTION! "+action);
					break;
				case "Calendar": 
					console.log("ACTION! "+action);
					break;
			}
		});	
	});
}

Supervisor.prototype.warnUser = function warnUser(user, output){
	output = output.split(",");
	if(parseFloat(output[0]) > 0.7)	this.createGenericNotification(user, 3, "sport",     "Faites attention aux exercices physiques, aujourd'hui en particulier!");
	if(parseFloat(output[1]) > 0.7)	this.createGenericNotification(user, 3, "diet",      "Faites attention à vos repas, aujourd'hui en particulier!");
	if(parseFloat(output[2]) > 0.7)	this.createGenericNotification(user, 3,"glycaemia",  "Faites attention à votre glycémie, aujourd'hui en particulier!");
}

Supervisor.prototype.createGenericNotification = function createGenericNotification(user, priority, type, content){
	if (priority < 3){
		qh.hasNoRecentNotification(user, type, function(){
			qh.createGenericNotification(user, type, content);
		});
	} else qh.createGenericNotification(user, type, content);
}










///////////////////////////////////////////////
// OLD UNUSED CODE
///////////////////////////////////////////////

Supervisor.prototype.checkGaps = function checkGaps() {
	var that = this;
	qh.listUsersByRoles([1,2,3], function(err, users){
		users.forEach(function(user){
			qh.listObjectivesByUser(user, function(err, objectives){
				objectives.forEach(function(objective){
					// Find unvalidated entries bound to objective
					qh.listNewEntriesByUserForObjective(user, objective, function(err, entries){
						entries.forEach(function(entry){
							that.checkEntryVSObjective(user, objective, entry);
						});
					});
				});
			});
 		});
	});
};

Supervisor.prototype.checkEntryVSObjective = function checkEntryVSObjective(user, objective, entry) {
	var that = this;
	if (entry.datetime > objective.startDate && entry.datetime < objective.endDate){

		var threshold = Math.abs(objective.value - objective.startValue) * 0.2;
		var threshold = this._intel.getTreshold(user, objective);
		var daysTotal = Math.ceil((objective.endDate - objective.startDate)/86400000);
		var daysLived = Math.ceil((entry.datetime - objective.startDate)/86400000) - 1;

		// Get today current objective
		
		/*
		var todaycurrentObj = 0;
		switch(objective.type) {
			case "Linear":
				var slope = (objective.value - objective.startValue) / daysTotal;
				var todaycurrentObj = (slope * daysLived + objective.startValue);
				break;
			case "Slow":
				var slope = (objective.value - objective.startValue) /
					(daysTotal * daysTotal);
				todaycurrentObj = (slope * daysLived * daysLived) 
					+ objective.startValue;
				break;
			case "Fast":
				var slope = (objective.value - objective.startValue) /
					Math.sqrt(daysTotal);
				todaycurrentObj = (slope * Math.sqrt(daysLived))
					+ objective.startValue;
				break;
		}
		*/
		
		todaycurrentObj = Math.round(objectiveController.objAtDateInt(objective, that.today)*10)/10;
		
		var overshooting = true;
		var undershooting = true;
		// Hardcoded, must be cleaned.
		if (entry.type == 'glucose' ) { overshooting = false; undershooting = false; }
		if (entry.type == 'weight') { overshooting = false; undershooting = false; }
		if (entry.type == 'fruitsVegetables') { overshooting = true; undershooting = false; }
		if (entry.type == 'walking') { overshooting = true; undershooting = false; }
		if (entry.type == 'intensiveSports') { overshooting = true; undershooting = false; }
		if (entry.type == 'blood_pressure') { overshooting = false; undershooting = false; }
		if (entry.type == 'nibbling') { overshooting = false; undershooting = true; }
		if (entry.type == 'water') { overshooting = true; undershooting = false; }
		if (entry.type == 'fishMeat') { overshooting = false; undershooting = false; }
		if (entry.type == 'starchy') { overshooting = false; undershooting = false; }
		if (entry.type == 'hotDrinks') { overshooting = false; undershooting = false; }
		if (entry.type == 'dairyProducts') { overshooting = false; undershooting = false; }
		if (entry.type == 'alcoholicBeverages') { overshooting = false; undershooting = true; }

		if ( 	(entry.value > (todaycurrentObj + threshold) && overshooting  == false) ||
				(entry.value < (todaycurrentObj - threshold) && undershooting == false) ){
					this.updateTracking(user, objective, entry, todaycurrentObj); // Notifier
				}
				
		// Validate
		qh.validateEntry(entry, function(err, res){});
	}
};

Supervisor.prototype.updateTracking = function updateTracking(user, objective, entry, todaycurrentObj) {
	var that = this;
	qh.getActiveFollowUp(user, entry, function(err, followUps){
		if (err) console.log(err);
		else {	
			// If no followup already, make one
			if (followUps.length == 0) that.newIAFollowup(user, objective, entry, todaycurrentObj);
			else {
			// Else, if user had no recent notification, make one
				qh.hadNoRecentNotification(user, entry.type, that.today, false, function(flag){
					if(flag){
						qh.createGapNotification(user, objective, entry, todaycurrentObj, that.today);
					}
				});
				qh.worsenSeverity(followUps[0], that.SEVERITY_PENALTY);
			}
		}
	});
}

Supervisor.prototype.newIAFollowup = function newIAFollowup(user, objective, entry, todaycurrentObj) {
	var that = this;
	qh.getMetricOfType(entry.type, function(err, metric){
		if (err) console.log(err);
		var period = metric.period;
		var isARate = metric.isARate;
		var endDate = new Date(that.today.getTime() + that.FOLLOWUP_LENGTH * that.hoursToMs(period));
		newFollowup = {
			userID : user._id,
			objectiveID : objective._id,
			lastEntryID : entry._id,
			type : entry.type,
			period : period,
			endDate : endDate,
			isARate : isARate,
			severity : that.FOLLOWUP_LENGTH
		};
		qh.createIAFollowup(newFollowup, function(err, ret){
			qh.createGapNotification(user, objective, entry, todaycurrentObj, that.today);
		});
	});
}

Supervisor.prototype.hoursToMs = function hoursToMs(hours) {
	return hours*60*60*1000;
}

/*******************************************************************************/

Supervisor.prototype.checkMissingEntries = function missingEntriesCheck() {
	var that = this;
	qh.listUsersByRoles([1,2,3], function(err, users){
		users.forEach(function(user){
			qh.getAllActiveFollowUp(user, function(err, followups){
				followups.forEach(function(followup){
					// If there is no reminder already
					qh.hadNoRecentNotification(user, followup.type, that.today, true, function(flag){
						if (flag == true) {
							qh.getMetricOfType(followup.type, function(err, metric){
								// Get the last entry
								qh.lastEntryByUserByType(user, metric.type, function(err, entry){
									if(err) return console.log(err);
									// Is it older than 2 periods ? (or 2 periods + overnight if applicable ?)
									var delay = (metric.period < 24 && that.today - entry.datetime > that.hoursToMs(24)) ?
										2 * metric.period + 9 : 2 * metric.period;
									//console.log("!!!!! "+ that.today +" - "+ entry.datetime +" = "+ (that.today - entry.datetime) +" > "+that.hoursToMs(delay));
									if (that.today - entry.datetime > that.hoursToMs(delay)){
										console.log("Missing entry!");
										qh.createReminderNotification(user, {_id: followup.objectiveID}, entry, that.today);
									}
								});
							});
						}
					});
				});
			});
		});
	});
}

/*******************************************************************************/

Supervisor.prototype.checkFollowUps = function checkFollowUps() {
	// Update the severities
	// Inactivate the follow-up if severity is zero or if endDate.
	var that = this;
	qh.getActiveFollowups(function(err, followups){
		if (err) return console.log(err);
		followups.forEach(function(followup){
			// TODO : toutes les 5 heures pour les follow-up shorts, ca sera problematique.
			var temp = new Date(followup.lastCheck.getTime() + that.hoursToMs(followup.period));
			if (temp < that.today){
				qh.lowerFollowUpSeverity(followup);
				console.log("Follow-up severity went down.");
				qh.setFollowUpLastCheck(followup, that.today);
			}
			if(followup.endDate < that.today) {
				qh.setFollowUpState(followup, false);
				console.log("A followup ended.");
			}
			if(followup.severity < 0) {
				qh.setFollowUpState(followup, false);
				qh.createGratsNotification(followup, that.today);
				console.log("A followup ended");
			}
		});
	}); 
};

/*******************************************************************************/

module.exports = Supervisor;