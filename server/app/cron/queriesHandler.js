var dbObj            = require('../models/objective');
var db           	 = require('../models/user');
var dbEntry      	 = require('../models/entry');
var dbIAFollowUp     = require('../models/iafollowup');
var dbIANotification = require('../models/notification');
var dbEntry          = require('../models/entry');
var dbMetric		 = require('../models/metric');
var dbIAWatch		 = require('../models/iawatch');
var dbIAScenario	 = require('../models/iascenario');
var fs               = require('fs');


// User
exports.listUsersByRoles = function(roles, callback){
	db.userModel.find({
		//roles : {$in : roles} TODO : fix here
	}).exec(function(err, users) {
		if (err) return callback(err, null);
		return callback(null, users);
	});
}

// Scenarii
exports.listIAScenarioByUserByType = function(user, type, callback){
	dbIAScenario.iaScenarioModel.find({
		userID : user._id,
		type : type
	}).exec(function(err, scenarii) {
		if (err) return callback(err, null);
		return callback(null, scenarii);
	});
}

exports.listLastIAScenarioByUserTByType = function(user, type, callback){
	dbIAScenario.iaScenarioModel.findOne({
		userID : user._id,
		type : type
	}).sort({"datetime" : -1})
	  .exec(function(err, scenario) {
		if (err) return callback(err, null);
		return callback(null, scenario);
	});
}

exports.createIAScenario = function(user, type, value, callback){
	dbIAScenario.iaScenarioModel.create({
		userID : user._id,
		type : type,
		value : value,
	}, function(err, scenario) { 
		if (err) return callback(err, null);
		return callback(null, scenario);
	});	
}

// watches
exports.listIAWatchByUserByType = function(user, type, callback){
	dbIAWatch.iaWatchModel.find({
		userID : user._id,
		type : type
	}).exec(function(err, watches) {
		if (err) return callback(err, null);
		return callback(null, watches);
	});
}

exports.listActiveIAWatchByUserByType = function(user, type, callback){
	dbIAWatch.iaWatchModel.findOne({
		userID : user._id,
		type : type,
		isActive : true
	}).exec(function(err, watch) {
		if (err) return callback(err, null);
		return callback(null, watch);
	});
}

exports.listActiveIAWatchByUser = function(user, callback){
	dbIAWatch.iaWatchModel.find({
		userID : user._id,
		isActive : true
	}).exec(function(err, watches) {
		if (err) return callback(err, null);
		return callback(null, watches);
	});
}

exports.createIAWatch = function(user, type, scenario, endDate, action){
	dbIAWatch.iaWatchModel.create({
		userID : user._id,
		type : type,
		scenario : scenario,
		endDate : endDate,
		action : action
	}, function(err, res) { 
		if (err){console.log(err);}
	});
}

exports.disableIAWatch = function(watch){
	dbIAWatch.iaWatchModel.update({_id : watch._id},{
		isActive : false
	}, function(err, res) { 
		if (err) return console.log(err);
	});
}

// entries
exports.validateEntry = function(entry, callback){
	dbEntry.entryModel.update({_id:entry._id}, {
		isValidated : true
	}, function(err, res) {
		if (err) return callback(err, null);
		return callback(null, true);
	});
}

exports.listEntriesByUser = function(user, callback){
	dbEntry.entryModel.find({
		userID : user._id
	}).sort({"datetimeAcquisition" : 1})
	.exec(function(err, entries) {
		if (err) return callback(err, null);
		return callback(null, entries);
	});
}

exports.listEntriesByUserByType = function(user, type, callback){
	dbEntry.entryModel.find({
		userID : user._id,
		type : type
	}).sort({"datetimeAcquisition" : 1})
	.exec(function(err, entries) {
		if (err) return callback(err, null);
		return callback(null, entries);
	});
}

exports.lastEntryByUserByType = function(user, type, callback){
	dbEntry.entryModel.findOne({
		userID : user._id,
		type : type
	}).sort({"datetimeAcquisition" : -1})
	.exec(function(err, entry) {
		if (err) return callback(err, 0);
		else return callback(null, entry);
	});
}

exports.listObjectivesByUser = function(user, callback){
	dbObj.objectiveModel.find({
		userID : user._id,
	}) .exec(function(err, objectives) {
		if (err) return callback(err, null);
		return callback(null, objectives);
	});
}

exports.listNewEntriesByUserForObjective = function(user, objective, callback){
	dbEntry.entryModel.find({
		userID : user._id,
		type : objective.select,
		isValidated : false
	}).sort({"datetimeAcquisition" : -1})
	    .exec(function(err, entries) {
		if (err) return callback(err, null);
		return callback(null, entries);
	});
}

exports.listNbOldEntriesByUserForObjective = function(user, objective, nb, callback){
	dbEntry.entryModel.find({
		userID : user._id,
		type : objective.select,
		isValidated : true
	}).sort({"datetimeAcquisition" : -1}).limit(nb)
	   .exec(function(err, entries) {
		if (err) return callback(err, null);
		return callback(null, entries);
	});
}
	
exports.validateEntry = function(entry, callback){
	dbEntry.entryModel.update({_id:entry._id}, {
		isValidated : true
	}, function(err, res) {
		if (err) return callback(err, null);
		return callback(null, true);
	});
}

// notifications
exports.hasNoRecentNotification = function(user, subType, callback){
	dbIANotification.notificationModel.findOne({
		userID : user._id,
		subType : subType
	}).sort({"datetime" : -1})
	.exec(function(err, notification) {
		if (err) return console.log(err);
		if (notification){
			d = new Date()
			rewind = (notification.subType == "sport") ? 2*24 : 10 ; 
			d.setHours(d.getHours() - rewind);
			d.setHours(d.getHours() - 24);
			if (notification.datetime > d) return;
		}
		return callback();
	});
}

exports.createGenericNotification = function(user, subType, content){
	dbIANotification.notificationModel.create({
		userID : user._id,
		type : "ia",
		subType : subType,
		content: content
	}, function(err, notification) {
		if (err) return console.log(err);
	});
}

			
/**************************************/

exports.techRemoveFollowUps = function(){
	dbIAFollowUp.iaFollowupModel.remove({}).exec(function(err, ret) {
		if (err) console.log(err);
		else console.log("Followups removed");
	});		
}
exports.techRemoveNotifications = function(){
	dbIANotification.notificationModel.remove({}).exec(function(err, ret) {
		if (err) console.log(err);
		else console.log("Notifications removed");
	});		
}

exports.insertDummyGlycaemia = function(user){
	var dummydata = JSON.parse(fs.readFileSync('./app/cron/dummyData/glycaemia.json', 'utf8'));
	for(var i=0 ; i < dummydata.length; i++){
		dbEntry.entryModel.create({
			userID : user._id,
			type : "glycaemia",
			value : dummydata[i].value,
			isValidated: true,
			isSkipped: false,
			datetimeAcquisition: new Date(dummydata[i].datetime),
			datetime: new Date(dummydata[i].datetime)
		}, function(err, temp) { 
			if (err){console.log(err);}
			else{ console.log("Entry recorded"); }
		});
	}
	console.log("Dummy glycaemia inserted");

}



///////////////////////////////////////////////
// OLD UNUSED CODE
///////////////////////////////////////////////

// :-/
exports.isNewEntriesByUserForObjective = function(user, objective, callback){
	dbEntry.entryModel.find({
		userID : user._id,
		type : objective.select,
		isValidated : false
	}).limit(1).exec(function(err, entry) {
		if (err) return callback(err);
		if (entry) return callback(null);
	});
}

/*
exports.getActiveFollowUp = function(user, entry, callback){
	dbIAFollowUp.iaFollowupModel.find({
		userID : user._id,
		type : entry.type,
		isActive : true
	}) .exec(function(err, followups) {
		if (err) return callback(err, null);
		return callback(null, followups);
	});
}

exports.getAllActiveFollowUp = function(user, callback){
	dbIAFollowUp.iaFollowupModel.find({
		userID : user._id,
		isActive : true
	}) .exec(function(err, followups) {
		if (err) return callback(err, null);
		return callback(null, followups);
	});
}
	
exports.worsenSeverity = function(followUp, SEVERITY_PENALTY){
	var newSeverity =  followUp.severity + SEVERITY_PENALTY;
	dbIAFollowUp.iaFollowupModel.update({_id : followUp._id},{severity : newSeverity},upsert = false)
	.exec(function(err, ret) {
		if (err) console.log(err);
	});
}

exports.getMetricOfType = function(type, callback){
	dbMetric.metricModel.findOne({
		type : type,
	}) .exec(function(err, metric) {
		if (err) return callback(err, null);
		return callback(null, metric);
	});
}

exports.createIAFollowup = function(followup, callback){
	dbIAFollowUp.iaFollowupModel.create(followup, function(err, ret) {
		if (err) console.log(err);
		return callback(null, ret);
	});
}

exports.getActiveFollowups = function(callback){
	dbIAFollowUp.iaFollowupModel.find({
		isActive : true
	}) .exec(function(err, followups) {
		if (err) console.log(err);
		return callback(null, followups);
	});
}

exports.setFollowUpState = function(followup, newState){
	dbIAFollowUp.iaFollowupModel.update({_id : followup._id},{isActive : newState}, upsert = false)
	.exec(function(err, ret) {
		if (err) console.log(err);
	});
}

exports.setFollowUpLastCheck = function(followup, newLastCheck){
	dbIAFollowUp.iaFollowupModel.update({_id : followup._id},{lastCheck : newLastCheck}, upsert = false)
	.exec(function(err, ret) {
		if (err) console.log(err);
	});
}

exports.lowerFollowUpSeverity = function(followup){
	var newSeverity =  followup.severity - 1;
	dbIAFollowUp.iaFollowupModel.update({_id : followup._id},{severity : newSeverity},upsert = false)
	.exec(function(err, ret) {
		if (err) return console.log(err);
	});
}

exports.hadNoRecentNotification = function(user, type, today, isReminder, callback){
	dbIANotification.notificationModel.find({
		userID : user._id,
		isReminder : isReminder,
		type : type
	})
	.sort({"datetime" : -1})
	.limit(1)
	.exec(function(err, notification) {
		if (err) return console.log(err);		
		if (notification.length > 0){
			dbMetric.metricModel.findOne({
				type : type,
			}) .exec(function(err, metric) {
				if (err) return console.log(err);
				// If less than 2 metrics period between now and last notification
				if ((Math.ceil((today - notification[0].datetime)/3600000)) <= metric.period * 2 ) return false;
				else return callback(true);
			});
		} else return callback(true);
	});
}

exports.createGapNotification = function(user, objective, entry, todaycurrentObj, today){
	var content = "Your "+entry.type+" is "+entry.value+", which is too "+ (todaycurrentObj < entry.value ? "high " : "low ")+"compared to your daily objective of "+todaycurrentObj;
	createNotification(user, objective._id, entry, today, false, content);
}

exports.createReminderNotification = function(user, objectiveID, entry, today){
	var content = "Do not forget to enter data about "+entry.type+" !";
	createNotification(user, objectiveID, entry, today, true, content);
}

exports.createGratsNotification = function(followup, today){
	dbEntry.entryModel.findOne({_id:followup.lastEntryID}).exec(function(err, entry){
		if(err) return console.log(err);
		db.userModel.findOne({_id:followup.userID}).exec(function(err2, user){
			if(err2) return console.log(err2);
			var content = "Congratulations, your "+entry.type+" is back on track, go on !";
			createNotification(user, followup.objectiveID, entry, today, false, content);
		});
	});
}

exports.createGenericNotification = function(user, objective, entry, today, content){
	var newNotification = {
		userID : user._id,
		objectiveID : objective._id,
		lastEntryID : entry._id,
		type : "ia",
		subType : entry.type,
		datetime : today,
		content: content
	};	
	dbIANotification.notificationModel.create(newNotification, function(err, temp) {
		if (err)console.log(err);
		else{console.log("Notification created. Message : "+newNotification.content);}
	});
}
		
									
createNotification = function(user, objectiveID, entry, today, isReminder, notificationContent){
	var newNotification = {
		userID : user._id,
		objectiveID : objectiveID,
		lastEntryID : entry._id,
		type : "ia",
		subType : entry.type,
		datetime : today,
		isReminder : isReminder,
		content: notificationContent
	};	
	dbIANotification.notificationModel.create(newNotification, function(err, temp) {
		if (err)console.log(err);
		else{console.log("Notification created. Message : "+newNotification.content);}
	});
}
*/