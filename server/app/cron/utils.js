var nconf           = require('nconf');
//Config emails (TODO)
nconf.file("config/server.json");
var tokenSecret     = nconf.get('token').secret;
var recaptchaSecret = nconf.get('reCAPTCHA').secret;
var mailerConfig = nconf.get('mailer');
var nodemailer      = require('nodemailer');

// Cubic hermite spline interpolation.
exports.getSpline = function(p, t, n){
	var _pts = [], results = [], x, y, t1x, t2x, t1y, t2y, c1, c2, c3, c4, st, t, i;
	_pts 				=  p.slice(0); // slice needed ?
	var tension 		= (t != null && t!= "undefined") ? t : 0.4;
	var numOfSegments 	= (n != null && n!= "undefined") ? n : 16;

	//_pts = pts.slice(0);
	// Duplicate beginning and end
	_pts.unshift(p[1]); 
	_pts.unshift(p[0]);
	_pts.push(p[p.length - 2]);
	_pts.push(p[p.length - 1]);

	for (i=2; i < (_pts.length - 4); i+=2) {	
		// loop goes through each segment between the 2 pts + 1e point before and after
		for (t=0; t <= numOfSegments; t++) {
			// Tension vectors
			t1x = (_pts[i+2] - _pts[i-2]) * tension;
			t2x = (_pts[i+4] - _pts[i]) * tension;
			t1y = (_pts[i+3] - _pts[i-1]) * tension;
			t2y = (_pts[i+5] - _pts[i+1]) * tension;

			// Step
			st = t / numOfSegments;

			// Cardinals
			c1 =   2 * Math.pow(st, 3)  - 3 * Math.pow(st, 2) + 1; 
			c2 = -(2 * Math.pow(st, 3)) + 3 * Math.pow(st, 2); 
			c3 =       Math.pow(st, 3)  - 2 * 
			Math.pow(st, 2) + st; 
			c4 =       Math.pow(st, 3)  -     Math.pow(st, 2);

			// Spline points with common control vectors
			x = c1 * _pts[i]    + c2 * _pts[i+2] + c3 * t1x + c4 * t2x;
			y = c1 * _pts[i+1]  + c2 * _pts[i+3] + c3 * t1y + c4 * t2y;

			results.push(x);
			results.push(y);
		}
	}
	//printSplineIn a file(results)
	return results;
}

printSpline = function(data){
	// --------------------------------------------------------
	// Useful to output spline in a file, and excel it.
	var fs = require('fs');
	var log_file = fs.createWriteStream(__dirname + '/debug.log', {flags : 'w'});
	var log_stdout = process.stdout;
	// --------------------------------------------------------
	// TODO : there is a repetition, spline must be fixed.
	for (i = 0 ; i < temp.length ; i += 2){
		strData = [data[i], data[i+1]];
		log_file.write(util.format(strData) + '\n');
		log_stdout.write(util.format(strData) + '\n');
	}
}

exports.getShapes = function(p, direction){
	var firstDderivatives = [];
	var oldSection = 0, counter = 1, shapes = [];
	var counter = 1;
	//var direction = ( p[p.length] < o ) ? 1 : -1;
	for (i=2; i < (p.length); i+=2) { if (p[i] >= counter){	// For each main point
		var center = Math.ceil(i - (i - oldSection) / 2 );
		if (center % 2 != 0) center -= 1; // We want even.
		oldSection = i;
		counter += 1;
		
		// First normalized derivative
		//df = (p[center+1] - p[center-1]) / (p[center] - p[center-2]);
		ndf = ((p[center+1] - p[center-1]) / p[center-1] ) / (p[center] - p[center-2]);
		// Second normalized derivative
		ndf1 = ((p[center-1] - p[center-3]) / - p[center-3])/ (p[center-2] - p[center-4]);
		ndf2 = ((p[center+3] - p[center+1]) / - p[center+1])/ (p[center+2] - p[center]);
		//ndf = df1 / p[center-1];
		nddf = (ndf2 - ndf1) / (p[center-4] - p[center]);
		//nddf = ddf / p[center-3];
		
		
		// Correcting for objective direction.
		ndf *= direction;
		nddf *= direction;
		
		// Setting up shapes
		if( ndf > 0.08 ){
			if (nddf > 0.1) shapes.push(7);
			else if (nddf < -0.1) shapes.push(5);
			else shapes.push(6);
		}
		else if( ndf < -0.08 ){
			if (nddf > 0.1) shapes.push(3);
			else if (nddf < -0.1) shapes.push(1);
			else shapes.push(2);
		}
		else shapes.push(4);
	}}
	return shapes;
}

exports.getWeek = function(date){
    var d = new Date(date);
    d.setHours(0,0,0);
    d.setDate(d.getDate()+4-(d.getDay()||7));
    return Math.ceil((((d-new Date(d.getFullYear(),0,1))/8.64e7)+1)/7);
}

//usage : sendMail({to: user.email, subject: 'Password Reset', html: result}, callback(err)})
function sendMail(mail, callback) {
    var smtpTransport = nodemailer.createTransport({
        host: mailerConfig.host,
        port: mailerConfig.port,
        secure: true,
        auth: {
            user: mailerConfig.auth.user,
            pass: mailerConfig.auth.pass
        }
    });

    var mailOptions = {
        to: mail.to,
        from: mailerConfig.sender.name + ' <' + mailerConfig.sender.address + '>',
        subject: mail.subject,
        html: mail.html
    };
    
    smtpTransport.sendMail(mailOptions, function(err) {
        if(err){
            console.log(err);
            callback(err);
        }
        callback(null);
    });
}






