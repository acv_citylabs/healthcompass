/***************************************************************/
function Schedule(){
		this._interval = 60*60*1000;
		this._observersList = [];
		this.today = null;
};

Schedule.prototype.addObserver = function addObserver(obj) {
	this._observersList.push( [obj, obj._step] );
};

Schedule.prototype.removeObserver = function removeObserver(obj) {
	for(var i in this._observersList){
		if (this._observersList[i] == obj) this._observersList.splice(i,1);
	}
};

Schedule.prototype.notifyObservers = function notifyObservers() {
	this.today = new Date();
	for(var i in this._observersList){
		this._observersList[i][1] -= 1;
		if (!this._observersList[i][1]) {
			this._observersList[i][0].update(this.today);
			this._observersList[i][1] = this._observersList[i][0]._step;
		}
	}
};

Schedule.prototype.init = function init(parameters) {
	this._interval = parameters.interval;
};

Schedule.prototype.run = function run() {
	var request = require('request');
	setInterval(this.notifyObservers.bind(this), this._interval);
}
	
module.exports = Schedule;