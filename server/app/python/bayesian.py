# -*- coding: cp1252 -*-
"""
    Called in the end of the day.
    Prepares the probabilities of bad events on the next day.
"""
import os, json, sys
from random import random
directory = os.getcwd()+'/app/python/'

class HMM:
    def __init__(self, data):
        self.BG = [[0,0]]
        self.BP = [[0,0]]
        self.BD = [[0,0]]
        self.prob = {}
        self.sampleSize = 20000
        self.initVariables(data["userID"])
        #if data['mode'] == "learning" : self.learn(data)
        if data['mode'] == "learningBatchFromFile" : self.learnBatchFromFile()
        if data['mode'] == "normal" : print self.predict(data)
        self.saveVariables(data["userID"])
         
    def initVariables(self, userID):
        if not os.path.exists('bayesian.dat'):
            file(directory+'bayesian.dat', 'w').close()
        else :
            with open (directory+'bayesian.dat') as f: lines = f.readlines()
            for line in lines :
                #for line in f:
                line = line.split("/")
                if line[0] == userID:
                    data = json.loads(line[1])
                    self.BG = data["history"]["BG"]
                    self.BP = data["history"]["BP"]
                    self.BD = data["history"]["BD"]
                    self.prob = data["probabilities"]
                    return
        # Default priors
        self.prob = { "BP|BGY,BPY,BDY" : 0.6,
                      "BP|BGY,GPY,BDY" : 0.6,
                      "BP|GGY,BPY,BDY" : 0.6,
                      "BP|GGY,GPY,BDY" : 0.6,

                      "BD|BGY,BP,BDY" : 0.6,
                      "BD|BGY,GP,BDY" : 0.6,
                      "BD|GGY,BP,BDY" : 0.6,
                      "BD|GGY,GP,BDY" : 0.6,

                      "BG|BGY,BP,BD" : 0.6,
                      "BG|BGY,GP,BD" : 0.6,
                      "BG|GGY,BP,BD" : 0.6,
                      "BG|GGY,GP,BD" : 0.6
                     }

    def updateVariables(self, data):
        #if we have new evidences, insert them
        if float(data['BG']) != -1 :
            if self.BG : self.BG[-1][1] = float(data['BG'])
            else : self.BG[0] = [0, float(data['BG'])]
        if float(data['BP']) != -1 :
            if self.BP : self.BP[-1][1] = float(data['BP'])
            else : self.BP[0] = [0, float(data['BP'])]
        if float(data['BD']) != -1 :
            if self.BD : self.BD[-1][1] = float(data['BD'])
            else : self.BD[0] = [0, float(data['BD'])]

    def saveVariables(self, userID):
        with open (directory+'bayesian.dat') as f: lines = f.readlines()
        for l in lines :
            if l.split("/")[0] == userID : lines.remove(l)
        encode = "{ \"history\": { \"BG\" : "+json.dumps(self.BG)+", \"BP\" : "+json.dumps(self.BP)+", \"BD\" : "+json.dumps(self.BD)+"}, \"probabilities\" : "+json.dumps(self.prob)+"}"
        lines.append(userID + "/" + encode + "\n")
        with open (directory+'bayesian.dat', 'w') as f:
            for l in lines : f.write(l)
        
    def predict(self, data):
        self.updateVariables(data)
        BPY = self.BP[-1][1]
        BDY = self.BD[-1][1]
        BGY = self.BG[-1][1]
        # no evidence yet, only beliefs.
        BP = self.samplePhysical(BGY, BPY, BDY)
        BD = self.sampleDiet(BGY, BP, BDY)
        BG = self.sampleGlycemia(BGY, BP, BD)
        
        self.BP.append([self.BP[-1][0]+1, BP])
        self.BD.append([self.BD[-1][0]+1, BD])
        self.BG.append([self.BG[-1][0]+1, BG])
        
        return str(BP)+","+str(BD)+","+str(BG)
            
    #Code is a bit redundant here, but i should change the data structure
    def samplePhysical(self, BGY, BPY, BDY):
        proportion = 0.0
        for i in range(self.sampleSize):
            if random() < BGY :
                if random() < BPY :
                    if random() < BDY :
                        if random() < self.prob["BP|BGY,BPY,BDY"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BP|BGY,BPY,BDY"]) : proportion += 1
                else :
                    if random() < BDY :
                        if random() < self.prob["BP|BGY,GPY,BDY"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BP|BGY,GPY,BDY"]) : proportion += 1
            else :
                if random() < BPY :
                    if random() < BDY :
                        if random() < self.prob["BP|GGY,BPY,BDY"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BP|GGY,BPY,BDY"]) : proportion += 1
                else :
                    if random() < BDY :
                        if random() < self.prob["BP|GGY,GPY,BDY"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BP|GGY,GPY,BDY"]) : proportion += 1
        return float(proportion / self.sampleSize)

    def sampleDiet(self, BGY, BP, BDY):
        proportion = 0.0
        for i in range(self.sampleSize):
            if random() < BGY :
                if random() < BP :
                    if random() < BDY :
                        if random() < self.prob["BD|BGY,BP,BDY"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BD|BGY,BP,BDY"]) : proportion += 1
                else :
                    if random() < BDY :
                        if random() < self.prob["BD|BGY,GP,BDY"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BD|BGY,GP,BDY"]) : proportion += 1
            else :
                if random() < BP :
                    if random() < BDY :
                        if random() < self.prob["BD|GGY,BP,BDY"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BD|GGY,BP,BDY"]) : proportion += 1
                else :
                    if random() < BDY :
                        if random() < self.prob["BD|GGY,GP,BDY"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BD|GGY,GP,BDY"]) : proportion += 1
        return float(proportion / self.sampleSize)

    def sampleGlycemia(self, BGY, BP, BD):
        proportion = 0.0
        for i in range(self.sampleSize):
            if random() < BGY :
                if random() < BP :
                    if random() < BD :
                        if random() < self.prob["BG|BGY,BP,BD"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BG|BGY,BP,BD"]) : proportion += 1
                else :
                    if random() < BD :
                        if random() < self.prob["BG|BGY,GP,BD"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BG|BGY,GP,BD"]) : proportion += 1
            else :
                if random() < BP :
                    if random() < BD :
                        if random() < self.prob["BG|GGY,BP,BD"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BG|GGY,BP,BD"]) : proportion += 1
                else :
                    if random() < BD :
                        if random() < self.prob["BG|GGY,GP,BD"] : proportion += 1
                    else :
                        if random() < (1 - self.prob["BG|GGY,GP,BD"]) : proportion += 1
        return float(proportion / self.sampleSize)

    #Need to provide userID. File is static.
    def learnBatchFromFile(self):
        (t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12) =(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        (t1d, t2d, t3d, t4d, t5d, t6d, t7d, t8d, t9d, t10d, t11d, t12d) =(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        with open(directory+'trainingbayesian.dat') as f: lines = f.readlines()
        #Too complicated for maximum likelihood methods
        #Using empirical frequency estimate
        for line in lines:
            line = line.split("/")
            userID = line.pop(0)
            line = line[0].split()
            
            BGY = True if line[0] == "1" else False
            BPY = True if line[1] == "1" else False
            BDY = True if line[2] == "1" else False
            FR  = True if line[3] == "1" else False
            RI  = True if line[4] == "1" else False
            BG  = True if line[5] == "1" else False
            BP  = True if line[6] == "1" else False
            BD  = True if line[7] == "1" else False
            GGY = True if line[0] == "0" else False
            GPY = True if line[1] == "0" else False
            GDY = True if line[2] == "0" else False
            GG  = True if line[5] == "0" else False
            GP  = True if line[6] == "0" else False
            GD  = True if line[7] == "0" else False

            if BGY and BPY and BDY:
                if BP : t1 += 1
                t1d +=1
            if BGY and GPY and BDY:
                if BP : t2 += 1
                t2d +=1
            if GGY and BPY and BDY:
                if BP : t3 += 1
                t3d +=1
            if GGY and GPY and BDY:
                if BP : t4 += 1
                t4d +=1

            if BGY and BP and BDY:
                if BP : t5 += 1
                t5d +=1
            if BGY and GP and BDY:
                if BP : t6 += 1
                t6d +=1
            if GGY and BP and BDY:
                if BP : t7 += 1
                t7d +=1
            if GGY and GP and BDY:
                if BP : t8 += 1
                t8d +=1

            if BGY and BP and BD:
                if BP : t9 += 1
                t9d +=1
            if BGY and GP and BD:
                if BP : t10 += 1
                t10d +=1
            if GGY and BP and BD:
                if BP : t11 += 1
                t11d +=1
            if GGY and GP and BD:
                if BP : t12 += 1
                t12d +=1

        # Catch ZeroDivisionError (happen if not enough data)
        self.prob = { "BP|BGY,BPY,BDY" : t1/t1d,
                      "BP|BGY,GPY,BDY" : t2/t2d,
                      "BP|GGY,BPY,BDY" : t3/t3d,
                      "BP|GGY,GPY,BDY" : t4/t4d,

                      "BD|BGY,BP,BDY" : t5/t5d,
                      "BD|BGY,GP,BDY" : t6/t6d,
                      "BD|GGY,BP,BDY" : t7/t7d,
                      "BD|GGY,GP,BDY" : t8/t8d,
                      
                      "BG|BGY,BP,BD" : t9/t9d,
                      "BG|BGY,GP,BD" : t10/t10d,
                      "BG|GGY,BP,BD" : t11/t11d,
                      "BG|GGY,GP,BD" : t12/t12d
                      }

if len(sys.argv)>1:
    args = json.loads(sys.argv[1])
    #HMM({"userID":args["userID"], "mode":"learningBatchFromFile"}) #Unit test training dataset
    HMM(args)
else :
    print "Missing JSON of arguments"
    print "Usage : bayesian.py {userID, state (normal, learning...), BP, BD, BG, age, powerUser, rigor}"



