#perceptron
'''
Triggered :
When any of the Bad, Neutral, Good scenarii happen

Inputs :
userID
BG
BP
BD
GG
GP
GD
NP
ND
age
frequency
rigor

Classes :
ExortNotification
ExortEmail
ExortCalendar

Possible modes :
1) Normal
2) Learning (only knows he did wrong)
3) Learning in batch (knows the answer)

improvements : add a relative weight in learning
'''
import sys, os, json, random
directory = os.getcwd()+'/app/python/'

class perceptron():
    def __init__(self, data):
        self.classes = ("Notification", "Email", "Calendar")
        self.features = ("BG", "BP", "BD",
                         "NP", "ND",
                         "GG", "GP", "GD",
                         "age", "powerUser", "rigor")

        self.weights = self.setupWeights(data["userID"])
        if data['mode'] == "learning" : self.learn(data)
        if data['mode'] == "learningBatchFromFile" : self.learnBatchFromFile(data)
        if data['mode'] == "normal" : print self.classify(data)
        if data['mode'] == "prelearning" : self.prelearn(data)
        self.saveWeights(data["userID"])

    #returns weights of the user, or an empty table
    def setupWeights(self, userID):
        if not os.path.exists(directory+'perceptron.dat'):
            file(directory+'perceptron.dat', 'w').close()
        else :
            with open (directory+'perceptron.dat') as f:
                for line in f :
                    line = line.split("/")
                    if line[0] == userID: return json.loads(line[1])
        return {c: { f:0.0 for f in self.features} for c in self.classes}

    #encodes weights of the user
    def saveWeights(self, userID):
        with open (directory+'perceptron.dat') as f:
            lines = f.readlines()
        for l in lines :
            if l.split("/")[0] == userID : lines.remove(l)
        lines.append(userID + "/" +json.dumps(self.weights) + "\n")
        with open (directory+'perceptron.dat', 'w') as f:
            for l in lines : f.write(l)


    def classify(self, data):
        #Data parsing
        x = self.vectorize1(data)

        #classifying
        y = {c:[] for c in self.classes}
        for c in self.classes :
            for f in self.features : y[c].append(self.weights[c][f] * x[f])
        ysum = {}
        for c in self.classes: ysum[c] = sum(y[c])
        return max(ysum.iterkeys(), key=(lambda key: ysum[key]))


    #learns
    def learn(self, data):
        #Data parsing
        x = self.vectorize1(data)

        #classifying
        y = {c:[] for c in self.classes}
        for c in self.classes :
             for f in self.features : y[c].append(self.weights[c][f] * x[f])
        ysum = {}
        for c in self.classes: ysum[c] = sum(y[c])
        res = max(ysum.iterkeys(), key=(lambda key: ysum[key]))
        
        #learning
        for c in self.classes:
            for f in self.features:
                if c == res : self.weights[c][f] -= x[f]
                else : self.weights[c][f] += x[f]/(len(self.classes)-1)
        print("learning")
        self.printWeights()


    def learnBatchFromFile(self, data):
        with open(directory+data['fileName']) as f: lines = f.readlines()
        for line in lines :
            #Data parsing
            (yt, x) = self.vectorize2(line)

            #classifying
            y = {c:[] for c in self.classes}
            for c in self.classes :
                 for f in self.features : y[c].append(self.weights[c][f] * x[f])
            ysum = {}
            for c in self.classes: ysum[c] = sum(y[c])
            res = max(ysum.iterkeys(), key=(lambda key: ysum[key]))
            
            #learning
            if res != yt:
                for f in self.features:
                    self.weights[yt][f] += x[f]
                    self.weights[res][f] -= x[f]
        print "[CRON JOB] - Intel - Perceptron weights updated in batch for user "+data["userID"]
        self.printWeights()

    def prelearn(self, data):
        return "prelearn/"+random.choice(self.classes)

    def vectorize1(self, data):
        x = {}
        for f in self.features: x[f] = 0.0
        x[data["scenario"]] = 1.0
        x["age"] = data["age"]
        x["rigor"] = data["rigor"]
        x["powerUser"] = data["powerUser"]
        return x   

    def vectorize2(self, data):
        if type(data) is str:
            #Data parsing
            x = {}
            #x = [0 for f in self.features for c in self.classes]
            data = data.split()
            yt = data.pop(0)
            for f in self.features: x[f] = float(data.pop(0))
            return (yt, x)
        if type(data) is dict:
            yt = data["expected"]
            x = {}
            for f in self.features: x[f] = 0.0
            x[data["scenario"]] = 1.0
            x["age"] = data["age"]
            x["rigor"] = data["rigor"]
            x["powerUser"] = data["powerUser"]
            return (yt, x)

    def printWeights(self):
        for c in self.weights.values(): print c
        print "-------"

if len(sys.argv)>1:
    args = json.loads(sys.argv[1])
    p = perceptron(args)
else :
    print "Missing JSON of arguments"
    print "Usage : perceptron.py {userID, state (normal, learning...), scenario (BG, GP...), age, powerUser, rigor}"




'''
	scenario: GP
	age : 30
	powerUser: 1
	state: normal
	rogir : 1
	u : id....


args = sys.argv[1].split()
print args
if args[1] == "baseLearning" :
    #Unit test training dataset
    with open(directory+'trainingonce.txt') as f: lines = f.readlines()
    trainingSet = []
    for line in lines :
        trainingSet.append(line)
    p = perceptron(args[0], "learning", trainingSet)
elif args[1] == "normal" :
    data = ""
    if args[2]== "BG" : data = "1 0 0 0 0 0 0 0 "
    if args[2]== "BP" : data = "0 1 0 0 0 0 0 0 "
    if args[2]== "BD" : data = "0 0 1 0 0 0 0 0 "
    if args[2]== "NP" : data = "0 0 0 1 0 0 0 0 "
    if args[2]== "ND" : data = "0 0 0 0 1 0 0 0 "
    if args[2]== "GG" : data = "0 0 0 0 0 1 0 0 "
    if args[2]== "GP" : data = "0 0 0 0 0 0 1 0 "
    if args[2]== "GD" : data = "0 0 0 0 0 0 0 1 "
    p = perceptron(args[0], args[1], data+" "+args[3]+" "+args[4]+" "+args[5]);
elif args[1] == "learning" :
    data = ""
    if args[3]== "BG" : data = "1 0 0 0 0 0 0 0 "
    if args[3]== "BP" : data = "0 1 0 0 0 0 0 0 "
    if args[3]== "BD" : data = "0 0 1 0 0 0 0 0 "
    if args[3]== "NP" : data = "0 0 0 1 0 0 0 0 "
    if args[3]== "ND" : data = "0 0 0 0 1 0 0 0 "
    if args[3]== "GG" : data = "0 0 0 0 0 1 0 0 "
    if args[3]== "GP" : data = "0 0 0 0 0 0 1 0 "
    if args[3]== "GD" : data = "0 0 0 0 0 0 0 1 "
    p = perceptron(args[0], args[1], args[2] + data+" "+args[4]+" "+args[5]+" "+args[6]);

#p = perceptron("helloju", "learning", trainingSet)

'''
