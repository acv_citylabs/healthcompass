(function() {
    'use strict';

    angular.module('app.andaman', [
        'oc.lazyLoad',
        'angular-jwt',
        'TokenInterceptorService',
        'datetimeFilters',
        'mappingFilters',
        'limitFilters',
        'gettext',
        'AutofocusDirective',
        'pickadate',
        'angularModalService',
        'highcharts-ng',
    ]);

    /**
     * App config
     */
    angular.module('app.andaman').config(configFct);

    configFct.$inject = ['$httpProvider', '$locationProvider'];

    function configFct($httpProvider, $locationProvider) {
        $.material.init();
        $locationProvider.html5Mode(true);
        $httpProvider.interceptors.push('TokenInterceptor');
    }


})();