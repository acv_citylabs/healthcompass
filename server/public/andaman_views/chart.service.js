(function () {

  'use strict';

  angular.module('app.andaman').service('Chart', ChartService);

  ChartService.$inject = ['$http'];
  function ChartService($http) {
    var service = {
      build : build
    };

    return service;

    function build(config) {
      return $http.get('/api/charts/' + config.type + '/from/' + config.from +
          '/to/' + config.to);
    }
  }

})();