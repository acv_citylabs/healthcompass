(function() {

    'use strict';

    angular.module('app.andaman').directive('newglychart', NewGlyChartDirective);

    function NewGlyChartDirective(gettextCatalog, $window, $filter, $log, Chart, $ocLazyLoad, $injector) {

        var directive = {
            scope: {
                title: '@',
                name: '@'
            },
            restrict: 'E',
            templateUrl: 'andaman_views/chart.html',
            link: linkFct
        };

        return directive;

        function linkFct($scope, $element, $attrs) {

            // Copy the basic configuration
            $scope.chart = JSON.parse(JSON.stringify(aChart()));

            //Copy the matrix chart config
            $scope.matrixLowGlucose = JSON.parse(JSON.stringify(matrixChart()));

            $scope.initChart = function() {

                $scope.view = '';
                $scope.unit = ' mmol/L';

                $scope.matrixLowGlucose.xAxis.labels = {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M', this.value);
                    },
                    overflow: 'justify',
                    step: 2
                };

                // Global configuration
                Highcharts.setOptions({
                    global: {
                        useUTC: false
                    }
                });

                $scope.width = $window.innerWidth;

                angular.element($window).on('resize', function() {

                    $scope.width = $window.innerWidth;

                    //Define responsive rules
                    if ($scope.width < 500) {
                        $scope.chart.yAxis.title.text = '';
                        $scope.chart.xAxis.labels.rotation = -45;
                        $scope.matrixLowGlucose.xAxis.labels.rotation = -45;
                        $scope.matrixLowGlucose.options.chart.spacingLeft = 35;
                    } else {
                        $scope.chart.yAxis.title.text = $scope.unit;
                        $scope.chart.xAxis.labels.rotation = 0;
                        $scope.matrixLowGlucose.xAxis.labels.rotation = 0;
                        $scope.matrixLowGlucose.options.chart.spacingLeft = 70;
                    }
                    // manually $digest required as resize event
                    // is outside of angular
                    $scope.$digest();
                });

                angular.element(document).ready(function() {
                    $scope.width = $window.innerWidth;

                    //Define responsive rules
                    if ($scope.width < 500) {
                        $scope.chart.yAxis.title.text = '';
                        $scope.chart.xAxis.labels.rotation = -45;
                        $scope.matrixLowGlucose.xAxis.labels.rotation = -45;
                        $scope.matrixLowGlucose.options.chart.spacingLeft = 35;
                    } else {
                        $scope.chart.yAxis.title.text = $scope.unit;
                        $scope.chart.xAxis.labels.rotation = 0;
                        $scope.matrixLowGlucose.xAxis.labels.rotation = 0;
                        $scope.matrixLowGlucose.options.chart.spacingLeft = 70;
                    }
                });

                // Create a personalized tooltip
                $scope.chart.options.tooltip = {
                    shared: true,
                    valueSuffix: 'mmol/L',
                    formatter: function() {
                        var hour = Highcharts.dateFormat('%H:%M', this.x);
                        var median = this.y;
                        var percentile1090low = this.points[2].point['low'];
                        var percentile1090high = this.points[2].point['high'];
                        var percentile2575low = this.points[1].point['low'];
                        var percentile2575high = this.points[1].point['high'];
                        var tooltip = hour + '<br />';
                        tooltip += '<span style="color:' + this.points[0].color + '">\u25CF</span> Median: <strong>' + median + ' ' + $scope.unit + '</strong><br />';
                        tooltip += '<span style="color:' + this.points[2].color + '">\u25CF</span> 10th to 90th percentile: <strong>' + percentile1090low + ' - ' + percentile1090high + ' ' + $scope.unit + '</strong><br />';
                        tooltip += '<span style="color:' + this.points[1].color + '">\u25CF</span> 25th to 75th percentile: <strong>' + percentile2575low + ' - ' + percentile2575high + ' ' + $scope.unit + '</strong>';
                        return tooltip;

                    }
                }

                // Define X axis
                $scope.chart.xAxis.type = 'datetime';
                $scope.chart.xAxis.labels = {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M', this.value);
                    },
                    overflow: 'justify',
                    step: 2
                };

                // Define Y axis
                $scope.chart.yAxis.min = 0;
                $scope.chart.yAxis.max = 12;

                //Insert low and high threshold plotLines
                $scope.chart.yAxis.plotLines.push({
                    value: 0,
                    width: 2,
                    color: '#ff6666',
                    zIndex: 1,
                    label: {
                        text: '',
                        align: 'left',
                        x: 5,
                        y: 20
                    }
                });

                $scope.chart.yAxis.plotLines.push({
                    value: 0,
                    width: 2,
                    color: '#ff6666',
                    zIndex: 1.5,
                    label: {
                        text: '',
                        align: 'right',
                        x: 5,
                        y: 20
                    }
                });

            }
            $scope.initChart();

            // Build the chart
            $scope.build = function(view) {
                var from;
                switch (view) {
                    case '1m':
                        $scope.view = '1m';
                        from = new Date(new Date().setDate(new Date().getDate() - 30));
                        break;
                    case '3m':
                        $scope.view = '3m';
                        from = new Date(new Date().setDate(new Date().getDate() - 90));
                        break;
                    case '6m':
                        $scope.view = '6m';
                        from = new Date(new Date().setDate(new Date().getDate() - 180));
                        break;
                    case 'list':
                        $scope.view = 'list';
                        break;
                    default:
                        $scope.view = '';
                        from = new Date(new Date().setDate(new Date().getDate() - 30));
                        break;
                }

                if (view !== 'list') {
                    $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                        var Chart = $injector.get('Chart');
                        Chart.build({
                            type: 'newGly',
                            from: from,
                            to: new Date()
                        }).success(function(data) {
                            for (var i = 0; i < data.series.length; i++) {
                                for (var k = 0; k < data.series[i].data.length; ++k) {
                                    if (data.series[i].data[k][2] > $scope.chart.yAxis.max) {
                                        $scope.chart.yAxis.max = data.series[i].data[k][2] + 1; //find and set a maximum value of yAxis
                                    }
                                    if (data.series[i].data[k][1] < $scope.chart.yAxis.min) {
                                        $scope.chart.yAxis.min = data.series[i].data[k][1]; //find and set a minimum value of yAxis
                                    }
                                }
                            }

                            $scope.chart.series = data.series;

                            for (var i = 0; i < data.mealTimes.length; i++) {
                                if (data.mealTimes[i].data != null) {
                                    /*  $scope.matrixLowGlucose.xAxis.plotLines[i] = {
                                        value: null,
                                        width: 1,
                                        color: 'black',
                                        dashStyle: 'dash',
                                        label: {
                                          useHTML: true,
                                          text: '',
                                          align: 'right',
                                          rotation: 0,
                                          y: -25,
                                          x: 8
                                        }
                                      }; */
                                    //meal plotlines
                                    $scope.chart.xAxis.plotLines[i] = {
                                        value: 0,
                                        width: 1,
                                        color: 'black',
                                        dashStyle: 'dash',
                                        label: {
                                            text: '',
                                            align: 'center',
                                            y: 12,
                                            x: 0
                                        }
                                    };

                                    /* switch (i) {
                                       case 3:
                                         $scope.matrixLowGlucose.xAxis.plotLines[i].label.text = '<i class="fa fa-bed fa-2x" aria-hidden="true"></i>'
                                         break;
                                       default:
                                         $scope.matrixLowGlucose.xAxis.plotLines[i].label.text = '<i class="fa fa-apple fa-2x" aria-hidden="true"></i>'
                                     }
                                     $scope.matrixLowGlucose.xAxis.plotLines[i].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[i].data, 0, 0, 0);*/
                                    $scope.chart.xAxis.plotLines[i].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[i].data, 0, 0, 0);
                                }
                            }

                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        });
                    });


                    /* Plot mean line */
                    if (view !== 'list') {
                        $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                            var Chart = $injector.get('Chart');
                            Chart.build({
                                type: 'glycaemia',
                                from: from,
                                to: new Date
                            }).success(function(data) {

                                //Insert low and high threshold plotLines
                                $scope.chart.yAxis.plotLines.push({
                                    value: data.mean,
                                    width: 2,
                                    color: 'blue',
                                    zIndex: 1,
                                    dashStyle: 'ShortDot',
                                    label: {
                                        text: 'mean' + " (" + $filter('number')(data.mean, 1) + $scope.unit + ")",
                                        align: 'center',
                                        x: 5,
                                        y: 20
                                    }
                                });

                            }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type: 'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                            });
                        });
                    }




                    // Get objectives
                    $ocLazyLoad.load('js/services/ObjectivesService.js').then(function() {
                        var Objective = $injector.get('Objective');
                        Objective.getObjective('glycaemia').success(function(data) {
                            if (data) {
                                if (data.values[0].type == 'max') {
                                    $scope.chart.yAxis.plotLines[1].value = data.values[0].value;
                                    $scope.chart.yAxis.plotLines[1].label.text = " (" + $filter('number')(data.values[0].value, 1) + $scope.unit + ")";
                                    $scope.chart.yAxis.plotLines[2].value = data.values[1].value;
                                    $scope.chart.yAxis.plotLines[2].label.text = " (" + $filter('number')(data.values[1].value, 1) + $scope.unit + ")";
                                } else {
                                    $scope.chart.yAxis.plotLines[1].value = data.values[1].value;
                                    $scope.chart.yAxis.plotLines[1].label.text = " (" + $filter('number')(data.values[1].value, 1) + $scope.unit + ")";
                                    $scope.chart.yAxis.plotLines[2].value = data.values[0].value;
                                    $scope.chart.yAxis.plotLines[2].label.text = " (" + $filter('number')(data.values[0].value, 1) + $scope.unit + ")";
                                }
                            } else {
                                // Default objectives
                                // TYPICAL MAX
                                $scope.chart.yAxis.plotLines[1].value = 180;
                                //TYPICAL MIN
                                $scope.chart.yAxis.plotLines[2].value = 80;

                            }

                            var rangeBand = $scope.chart.yAxis.plotLines[2].value;
                            $scope.chart.yAxis.plotBands = [{
                                    color: '#fff0f0',
                                    from: 0,
                                    to: $scope.chart.yAxis.plotLines[2].value
                                },
                                {
                                    color: '#fff0f0',
                                    from: $scope.chart.yAxis.plotLines[1].value,
                                    to: $scope.chart.yAxis.plotLines[1].value + rangeBand
                                },


                                /* {
                                     color: '#ebfaeb',
                                     from: $scope.chart.yAxis.plotLines[1].value,
                                     to: $scope.chart.yAxis.plotLines[0].value,
                                 }*/
                            ]

                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        });
                    });
                } else {
                    $scope.$parent.buildList({
                        type: 'newGly'
                    }, function(data) {
                        $scope.list = data;
                    });
                }
            }

            // ===================== FOR THE VIDEO - TEMPORARY  ===============================================
            // 
            if ($window.innerWidth <= 450) {

                $scope.chart.yAxis.plotLines.push({
                    value: 10, //new Date(new Date().setDate(3)).setHours(8, 0, 0, 0),// Date.UTC(2017,10, 3, 7, 0),
                    width: 2,
                    color: 'white',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-apple fa-2x" aria-hidden="true"></i>',
                        rotation: 0,
                        x: 80,
                        y: 10
                    }
                });

                $scope.chart.yAxis.plotLines.push({
                    value: 10, //new Date(new Date().setDate(3)).setHours(8, 0, 0, 0),// Date.UTC(2017,10, 3, 7, 0),
                    width: 2,
                    color: 'white',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-apple fa-2x" aria-hidden="true"></i>',
                        rotation: 0,
                        x: 135,
                        y: 10
                    }
                });

                $scope.chart.yAxis.plotLines.push({
                    value: 10, //new Date(new Date().setDate(3)).setHours(8, 0, 0, 0),// Date.UTC(2017,10, 3, 7, 0),
                    width: 2,
                    color: 'white',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-apple fa-2x" aria-hidden="true"></i>',
                        rotation: 0,
                        x: 185,
                        y: 10
                    }
                });

                $scope.chart.yAxis.plotLines.push({
                    value: 10, //new Date(new Date().setDate(3)).setHours(8, 0, 0, 0),// Date.UTC(2017,10, 3, 7, 0),
                    width: 2,
                    color: 'white',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-bed fa-2x" aria-hidden="true"></i>',
                        rotation: 0,
                        x: 245,
                        y: 10
                    }
                });
            }

            // ===================== FOR THE VIDEO - TEMPORARY  ===============================================



            $scope.build();

            /* Return a chart structure in .json format */
            function aChart() {
                return {
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime',
                        opposite: true,
                        crosshair: true,
                        tickInterval: 3600 * 1000,
                        labels: {},
                        plotLines: []
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                        gridLineWidth: 0,
                        minorGridLineWidth: 0,
                        plotLines: [{
                            value: 0,
                            width: 1.5,
                            color: '#000000',
                            zIndex: 5,
                            label: {
                                text: '',
                                align: 'left',
                                x: 5
                            }
                        }],
                        min: 0,
                        max: 22,
                        lineColor: '#2E2E1E',
                        lineWidth: 2
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                enabled: false
                            },
                            fillOpacity: 0.2
                        }
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: 30
                    },
                    credits: {
                        enabled: false
                    },

                    series: [],

                    options: {
                        chart: {},
                        tooltip: {
                            shared: true
                        }
                    },
                    noData: gettextCatalog.getString('No Data')
                };
            }
            //end function aChart();
            function matrixChart() {
                return {
                    title: {
                        text: ''
                    },
                    options: {
                        legend: {
                            enabled: false
                        },
                        chart: {
                            type: 'scatter',
                            // marginTop: 40,
                            // spacingLeft: 70
                        },
                        tooltip: {}
                    },
                    plotOptions: {
                        series: {
                            lineWidth: 0
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        type: 'datetime',
                        min: new Date(new Date().setDate(3)).setHours(0, 0, 0, 0),
                        max: new Date(new Date().setDate(3)).setHours(24, 0, 0, 0),
                        tickInterval: 3600 * 1000,
                        plotLines: []
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                        labels: {
                            enabled: false
                        },
                        min: 2,
                        max: 5,
                        lineColor: '#2E2E1E',
                        lineWidth: 2,
                        gridLineWidth: 0,
                        minorGridLineWidth: 0
                    },
                    series: [{
                        name: 'Variability below median',
                        data: [{
                            x: Date.UTC(2017, 10, 3, 3, 30),
                            y: 5.5,
                            marker: {
                                enabled: false
                            }
                        }, {
                            x: Date.UTC(2017, 10, 3, 9, 0),
                            y: 5.5,
                            marker: {
                                enabled: false
                            }
                        }, {
                            x: Date.UTC(2017, 10, 3, 14, 0),
                            y: 5.5,
                            marker: {
                                enabled: false
                            }
                        }, {
                            x: Date.UTC(2017, 10, 3, 18, 0),
                            y: 5.5,
                            marker: {
                                enabled: false
                            }
                        }, {
                            x: Date.UTC(2017, 10, 3, 21, 30),
                            y: 5.5,
                            marker: {
                                enabled: false
                            }
                        }]
                    }],
                    noData: gettextCatalog.getString('No Data'),
                    size: {
                        // height: '150px'
                        height: '0px'
                    }
                };
            }
        }
    }

})();