(function() {

    'use strict';

    angular.module('app.andaman').directive('weightchart', WeightChartDirective);

    WeightChartDirective.$inject = ['gettextCatalog', '$window', '$filter', '$log', 'Chart'];

    function WeightChartDirective(gettextCatalog, $window, $filter, $log, Chart) {

        var directive = {
            scope: {
                title: '@',
                name: '@'
            },
            restrict: 'E',
            templateUrl: 'andaman_views/chart.html',
            link: linkFct
        };

        return directive;

        function linkFct($scope, $element, $attrs) {

            $scope.build = build;
            // Copy the basic configuration
            $scope.chart = JSON.parse(JSON.stringify(aChart()));

            activate();

            function activate() {
                initChart();
                build('3m');
            }



            /**
             * 
             */
            function initChart() {

                // Global configuration
                Highcharts.setOptions({ global: { useUTC: false } });

                $scope.fractionSize = 1;
                $scope.view = '';
                $scope.unit = 'kg';

                // Data
                $scope.chart.series[0] = {
                    name: gettextCatalog.getString('Weight'),
                    id: 'weight',
                    color: '#827717',
                    fillOpacity: 0.2
                };


                // Create a personalized tooltip
                $scope.chart.options.tooltip.formatter = function() {
                    return $filter('ddMMyyyy')(new Date(this.x).toISOString()) + '<br><span style="color:#827717;">●</span>  <b>' + this.y + '</b> ' + $scope.unit;
                }

                // Define X axis
                $scope.chart.xAxis.min = new Date(new Date().setDate(new Date().getDate() - 30)).getTime();
                $scope.chart.xAxis.max = (new Date).getTime();
                $scope.chart.xAxis.type = 'datetime';
                $scope.chart.xAxis.labels = {
                    overflow: 'justify',
                    formatter: function() {
                        return Highcharts.dateFormat("%b %e", this.value);
                    }
                }

                // Define Y axis
                $scope.chart.yAxis.plotLines[0].label.text =
                    gettextCatalog.getString('obj.');
                $scope.chart.yAxis.title.text = $scope.unit;

                // Size
                if ($window.innerWidth < 535) {
                    $scope.chart.size.height = 250;
                }
            }

            /**
             * Build the chart
             */
            function build(view) {

                var buildCallback = function(data) {
                    $scope.chart.series[0].data = data;
                    var last_weight = data[data.length - 1][1];
                    $scope.chart.yAxis.min = last_weight * 0.95; // Last_weight * 0.92
                    $scope.chart.series[0].type = 'areaspline';

                };

                var from;

                switch (view) {
                    case '6m':
                        $scope.view = '6m';
                        from = new Date(new Date().setDate(new Date().getDate() - 180));

                        break;
                    case '1y':
                        $scope.view = '1y';
                        from = new Date(new Date().setDate(new Date().getDate() - 360));
                        break;
                    case 'list':
                        $scope.view = 'list';
                        break;
                    default:
                        $log.debug("Test");
                        $scope.view = '3m';
                        from = new Date(new Date().setDate(new Date().getDate() - 90));
                        break;
                }


                $scope.chart.xAxis.min = from.getTime();

                $log.debug('From :', from);

                Chart.build({
                    type: $scope.chart.series[0].id, // weight
                    from: from,
                    to: new Date
                }).success(function(data) {
                    for (var i = 0; i < $scope.chart.series.length; i++) {
                        $scope.chart.series[i].data = [];
                    }
                    $scope.chart.xAxis.dateTimeLabelFormats = {
                        month: '%e. %b',
                        year: '%b'
                    };
                    buildCallback(data);
                }).error(function(status, data) {
                    for (var i = 0; i < $scope.chart.series.length; i++) {
                        $scope.chart.series[i].data = [];
                    }
                    buildCallback(null);
                });
            }


            /* Return a chart structure under .json format */
            function aChart() {
                return {
                    options: {
                        chart: {},
                        tooltip: {
                            enabled: true
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                stacking: '',
                                dataLabels: {
                                    enabled: false
                                }
                            }
                        },
                    },
                    series: [{
                        data: []
                    }],
                    title: {
                        text: ''
                    },
                    xAxis: {
                        labels: {},
                        title: {
                            text: ''
                        }
                    },
                    yAxis: {
                        // min : 35,
                        plotLines: [{
                            color: '#000',
                            dashStyle: 'Dash',
                            width: 1,
                            zIndex: 99,
                            label: {
                                x: 0
                            }
                        }],
                        title: {
                            text: ''
                        },
                        stackLabels: {
                            style: {
                                color: '#D32F2F'
                            },
                            enabled: false,
                            align: 'right'
                        },
                        formatter: function() {}
                    },
                    noData: 'No Data',
                    size: {
                        height: ''
                    }
                };
            }


        }
    }

})();