(function() {

    'use strict';

    angular.module('app.andaman').directive('stepschart', StepsChartDirective);
    //InsulinChartDirective.$inject = ['gettextCatalog', '$window', '$filter', '$log', 'Chart', '$ocLazyLoad', '$injector'];

    function StepsChartDirective(gettextCatalog, $window, $filter, $log, Chart, $ocLazyLoad, $injector) {

        var directive = {
            scope: {
                title: '@',
                name: '@'
            },
            restrict: 'E',
            templateUrl: 'andaman_views/chart.html',
            link: linkFct
        };

        return directive;

        function linkFct($scope, $element, $attrs) {

            // Copy the basic configuration
            $scope.chart = JSON.parse(JSON.stringify(aChart()));


            $scope.initChart = function() {
                $scope.fractionSize = 1;
                $scope.view = '';
                $scope.unit = gettextCatalog.getString('Steps');

                // Global configuration
                Highcharts.setOptions({ global: { useUTC: false } });

                // Define chart type
                $scope.chart.options.chart.type = 'areaspline';


                // Data
                $scope.chart.series[0] = {
                    name: gettextCatalog.getString('Steps'),
                    id: 'steps',
                    color: '#00806A',
                    fillOpacity: 0.2
                };

                // Create a personalized tooltip
                $scope.chart.options.tooltip.formatter = function() {
                    return $filter('ddMMyyyy')(new Date(this.x).toISOString()) + '<br><span style="color:#00897b;">●</span>  <b>' + this.y + '</b> ' + $scope.unit;
                }

                // Define X axis
                $scope.chart.xAxis.type = 'datetime';
                $scope.chart.xAxis.labels = {
                    overflow: 'justify'
                };
                $scope.chart.xAxis.dateTimeLabelFormats = {
                    month: '%e. %b',
                    year: '%b'
                };
                $scope.chart.xAxis.min = new Date(new Date().setDate(new Date().getDate() - 30)).getTime();
                $scope.chart.xAxis.max = (new Date).getTime();

                // Define Y axis
                $scope.chart.yAxis.plotLines[0].label.text = gettextCatalog.getString('obj.');
                $scope.chart.yAxis.title.text = $scope.unit;
                $scope.chart.yAxis.max = 11000;
                $scope.chart.yAxis.min = 3000; // TO DO, min steps


                //Size
                if ($window.innerWidth < 535) {
                    $scope.chart.size.height = 250;
                }

                // Get objective
                $ocLazyLoad.load('js/services/ObjectivesService.js').then(function() {
                    var Objective = $injector.get('Objective');
                    Objective.getObjective('steps').success(function(data) {
                        if (data) {
                            var obj = '';
                            if (data.values[0])
                                obj = data.values[0].value;
                            else if (data.value)
                                obj = data.value;
                            else if (data.startValue)
                                obj = data.startValue;

                            $scope.chart.yAxis.plotLines[0].value = obj;
                            $scope.chart.yAxis.plotLines[0].label.text = $scope.chart.yAxis.plotLines[0].label.text + "    (" + obj + $scope.unit + ")";

                            //$scope.chart.yAxis.min = 0; 
                            //$scope.chart.yAxis.max = obj * 1.5; 
                        }
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            priority: 2
                        });
                    });
                });


            }
            $scope.initChart();
            //Build a list
            //
            $scope.buildList = function(config, callback) {
                $ocLazyLoad.load('js/services/EntryService.js').then(function() {
                    var Entry = $injector.get('Entry');
                    Entry.list({
                        type: config.type,
                        subType: config.subType,
                    }).success(function(data) {
                        callback(data);
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            priority: 2
                        });
                        callback(null);
                    });
                });
            };

            //Build a chart
            $scope.buildChart = function(chart, config, callback) {
                $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                    var Chart = $injector.get('Chart');
                    Chart.build({
                        type: chart.series[0].id,
                        from: config.from,
                        to: config.to
                    }).success(function(data) {
                        for (var i = 0; i < chart.series.length; i++) {
                            chart.series[i].data = [];
                        }
                        callback(data);
                    }).error(function(status, data) {
                        for (var i = 0; i < chart.series.length; i++) {
                            chart.series[i].data = [];
                        }
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            priority: 2
                        });
                        callback(null);
                    });
                });
            };

            /**
             * Build the chart
             */
            $scope.build = function(view) {
                $scope.view = view;
                var from;
                switch (view) {
                    case '1m':
                        $scope.view = '1m';
                        from = new Date(new Date().setDate(new Date().getDate() - 30));

                        break;
                    case '3m':
                        $scope.view = '3m';
                        from = new Date(new Date().setDate(new Date().getDate() - 90));
                        break;
                    case '6m':
                        $scope.view = '6m';
                        from = new Date(new Date().setDate(new Date().getDate() - 180));
                        break;
                    case 'list':
                        $scope.view = 'list';
                        break;
                    default:
                        $scope.view = '';
                        from = new Date(new Date().setDate(new Date().getDate() - 30));
                        break;
                }

                if (view !== 'list') {
                    $scope.chart.xAxis.min = from.getTime();
                    $scope.buildChart($scope.chart, { from: from, to: new Date }, function(data) {
                        $scope.chart.series[0].data = data;

                        for (var i = 0; i < data.length; ++i) {
                            if (data[i][1] > $scope.chart.yAxis.max) {
                                $scope.chart.yAxis.max = data[i][1];
                            }
                        }
                    });
                } else {
                    $scope.buildList({ type: 'steps' }, function(data) {
                        $scope.list = data;
                    });
                }

            }

            $scope.build('');


            /* Return a chart structure under .json format */
            function aChart() {
                return {
                    options: {
                        chart: {},
                        tooltip: {
                            enabled: true
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                stacking: '',
                                dataLabels: {
                                    enabled: false
                                }
                            }
                        },
                    },
                    series: [{
                        data: []
                    }],
                    title: {
                        text: ''
                    },
                    xAxis: {
                        labels: {},
                        title: {
                            text: ''
                        }
                    },
                    yAxis: {
                        // min : 35,
                        plotLines: [{
                            color: '#000',
                            dashStyle: 'Dash',
                            width: 1,
                            zIndex: 99,
                            label: {
                                x: 0
                            }
                        }],
                        title: {
                            text: ''
                        },
                        stackLabels: {
                            style: {
                                color: '#D32F2F'
                            },
                            enabled: false,
                            align: 'right'
                        },
                        formatter: function() {}
                    },
                    noData: 'No Data',
                    size: {
                        height: ''
                    }
                };
            }


        }
    }

})();