(function() {

    'use strict';

    angular.module('app.andaman').directive('sportchart', SportChartDirective);

    function SportChartDirective(gettextCatalog, $window, $filter, $log, Chart, $ocLazyLoad, $injector) {

        var directive = {
            scope: {
                title: '@',
                name: '@'
            },
            restrict: 'E',
            templateUrl: 'andaman_views/chart.html',
            link: linkFct
        };

        return directive;

        function linkFct($scope, $element, $attrs) {

            // Copy the basic configuration
            $scope.chart = JSON.parse(JSON.stringify(aChart()));

            var months = new Array(gettextCatalog.getString("January"),
                gettextCatalog.getString("February"),
                gettextCatalog.getString("March"),
                gettextCatalog.getString("April"),
                gettextCatalog.getString("May"),
                gettextCatalog.getString("June"),
                gettextCatalog.getString("July"),
                gettextCatalog.getString("August"),
                gettextCatalog.getString("September"),
                gettextCatalog.getString("October"),
                gettextCatalog.getString("November"),
                gettextCatalog.getString("December"));

            $scope.initChart = function() {

                $scope.fractionSize = 1;
                $scope.view = '';
                $scope.unit = gettextCatalog.getString('hours');


                // Global configuration
                Highcharts.setOptions({ global: { useUTC: false } });

                // Define chart type
                $scope.chart.options.chart.type = 'column';

                $scope.chart.options.legend.enabled = true;
                // $scope.chart.options.legend.reversed = true;


                // Disable tooltip
                $scope.chart.options.tooltip.enabled = false;

                //Define X axis
                $scope.chart.xAxis.categories = [];

                //Define Y axis
                $scope.chart.yAxis.plotLines[0].label.text = gettextCatalog.getString('obj.');

                //Size
                if ($window.innerWidth < 535) {
                    $scope.chart.size.height = 250;
                }

                // Design options
                $scope.chart.options.plotOptions.series.stacking = 'normal';

                // Get objective
                $ocLazyLoad.load('js/services/ObjectivesService.js').then(function() {
                    var Objective = $injector.get('Objective');
                    Objective.getObjective('sport').success(function(data) {
                        if (data) {
                            $scope.chart.yAxis.plotLines[0].value = data.values[0].value;
                            $scope.chart.yAxis.plotLines[0].label.text = $scope.chart.yAxis.plotLines[0].label.text + " (" + data.values[0].value + gettextCatalog.getString('h/week') + ")";
                        }
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            priority: 2
                        });
                    });
                });


            }
            $scope.initChart();

            /**
             * Build the chart
             */
            $scope.build = function(view) {
                var from;
                switch (view) {
                    case '3m':
                        $scope.view = '3m';
                        $scope.chart.xAxis.title.text = gettextCatalog.getString('months');
                        $scope.chart.yAxis.title.text = gettextCatalog.getString('hours per week');
                        from = new Date(new Date().setDate(new Date().getDate() - 90));
                        break;
                    case '6m':
                        $scope.view = '6m';
                        $scope.chart.xAxis.title.text = gettextCatalog.getString('months');
                        $scope.chart.yAxis.title.text = gettextCatalog.getString('hours per week');
                        from = new Date(new Date().setDate(new Date().getDate() - 180));
                        break;
                    case 'list':
                        $scope.view = 'list';
                        break;
                    default:
                        $scope.view = '';
                        $scope.chart.xAxis.title.text = gettextCatalog.getString('weeks');
                        $scope.chart.yAxis.title.text = gettextCatalog.getString('hours');
                        from = new Date(new Date().setDate(new Date().getDate() - 30));
                        break;
                }

                if (view !== 'list') {
                    $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                        var Chart = $injector.get('Chart');
                        Chart.build({
                            type: 'sport',
                            from: from,
                            to: new Date
                        }).success(function(data) {
                            for (var i = 0; i < data.categories.length; i++) {
                                if (view === '3m' || view === '6m') {
                                    data.categories[i] = months[data.categories[i] - 1];
                                } else {
                                    data.categories[i] = data.categories[i].substr(0, data.categories[i].indexOf('/')) + ' ' + months[data.categories[i].substr(data.categories[i].indexOf('/') + 1, data.categories[i].length) - 1].substr(0, 3);

                                }
                            }

                            for (var i = 0; i < data.series.length; i++) {
                                data.series[i].name = gettextCatalog.getString(data.series[i].name);
                            }

                            $scope.chart.xAxis.categories = data.categories;
                            $scope.chart.series = data.series;
                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        });
                    });
                } else {
                    $scope.buildList({ type: 'activity', subType: 'sport' }, function(data) {
                        $scope.list = data;
                    });
                }
            }


            $scope.build('');
            //Build a list
            //
            $scope.buildList = function(config, callback) {
                $ocLazyLoad.load('js/services/EntryService.js').then(function() {
                    var Entry = $injector.get('Entry');
                    Entry.list({
                        type: config.type,
                        subType: config.subType,
                    }).success(function(data) {
                        callback(data);
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            priority: 2
                        });
                        callback(null);
                    });
                });
            };

            /* Return a chart structure under .json format */
            function aChart() {
                return {
                    options: {
                        chart: {},
                        tooltip: {
                            enabled: true
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                stacking: '',
                                dataLabels: {
                                    enabled: false
                                }
                            }
                        },
                    },
                    series: [{
                        data: []
                    }],
                    title: {
                        text: ''
                    },
                    xAxis: {
                        labels: {},
                        title: {
                            text: ''
                        }
                    },
                    yAxis: {
                        // min : 35,
                        plotLines: [{
                            color: '#000',
                            dashStyle: 'Dash',
                            width: 1,
                            zIndex: 99,
                            label: {
                                x: 0
                            }
                        }],
                        title: {
                            text: ''
                        },
                        stackLabels: {
                            style: {
                                color: '#D32F2F'
                            },
                            enabled: false,
                            align: 'right'
                        },
                        formatter: function() {}
                    },
                    noData: 'No Data',
                    size: {
                        height: ''
                    }
                };
            }


        }
    }

})();