angular.module('AppointmentsDirective', []).directive('appointments', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, $log) {
    return {
        restrict: 'E',
        templateUrl: 'templates/dashboard/cards/directives/appointments.html',
        scope: {
            title: '@',
            name: '@'
        },
        link: function($scope, $element, $attrs) {

      //   	$scope.appointments = [{name: "Johan Kimble", time: "10:15, 08/02/2018"},
      //   		{name: "Rocky Balboa", time: "12:00, 09/02/2018"},
      //   		{name: "John Doe", time: "13:55, 11/02/2018"},
      //   		{name: "Ben Richards", time: "15:00, 11/02/2018"}  		
		    // ];

		    $scope.appointments = [];
		    $scope.message = "";

		    $ocLazyLoad.load('js/services/EventService.js').then(function() {
		    	var Event = $injector.get('Event');
		    	Event.listAppointments({
		    		from: new Date(),
		    		//to: new Date(new Date().setDate(new Date().getDate() + 5))
		    	}).success(function (appointments) {
		    		//console.log(appointments);
		    		var time;
		    		if(appointments.length > 0) {
		    			appointments.forEach(function (appt) {
		    				var current = new Date(appt.start);
		    				var endtime = new Date(appt.end);
		    				time = ('0' + current.getHours()).slice(-2) + ':'
		    					   + ('0' + current.getMinutes()).slice(-2) +' - '
		    					   + ('0' + endtime.getHours()).slice(-2) + ':'
		    					   + ('0' + endtime.getMinutes()).slice(-2) + 'h, ' 
		    					   + ('0' + current.getDate()).slice(-2) + '/'
		    					   + ('0' + (current.getMonth()+1)).slice(-2) + '/'
		    					   + current.getFullYear();
		    				$scope.appointments.push({name: appt.title, time: time });
		    			});
		    			$scope.appNo = $scope.appointments.length;
		    		} else {
		    			$scope.message = "";
		    			$scope.message = 'No appointments scheduled';
		    		}		    		
		    	}).error(function (status, data) {
		    		$rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
		    	});
		    });
		}
    }
});