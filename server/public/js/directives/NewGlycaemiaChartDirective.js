angular.module('NewGlycaemiaChartDirective', []).directive('newglychart', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, $window, $filter, $state, $log, ModalService) {
    return {
        restrict: 'E',
        templateUrl: 'templates/dashboard/cards/directives/chart.html',
        scope: {
            title: '@',
            name: '@',
            objective: '@'
        },
        link: function($scope, $element, $attrs) {
            //=================== plotLines structure ==============
            //xAxis: [0] -> breakfast, [1] -> lunch, [2] -> dinner, [3] -> sleeptime, UPPER POSITIONS ARE RESERVED FOR EVENTS in 1-DAY VIEW
            //yAxis: [0] -> lower objective, [1] -> upper objective, [2] -> mean score
            //=================== plotLines structure ==============

                $scope.isHiddenLocal = function(name){
                    return $rootScope.isHiddenGlobal.agp;
                }
            $scope.view = ''; //set the initial view of the data (1m, 2m, 3m, list)
            $scope.unit = ' mg/dL';

            // Global configuration
            Highcharts.setOptions({ global: { useUTC: false } });

            // Copy the basic configuration
            $scope.chart = JSON.parse(JSON.stringify($scope.$parent.bChart));
            //$scope.chart.xAxis.labels.step = 2;

            //Copy the matrix chart config
            $scope.matrixChart = JSON.parse(JSON.stringify($scope.$parent.matrixChart));
            var datePolygon = new Date(new Date().setDate(3));

            $scope.matrixChart.options.chart.events.load =  function() {
                    var chart = this;
                    var width = document.getElementById("matrixChart").offsetWidth;
                    var height = document.getElementById("matrixChart").offsetHeight;
                    //console.log(this);
                    //var bottom = $(window).height() - link;
                    //console.log('WIDTH:', width);
                    //console.log('HEIGHT:', height);
                    chart.renderer.text('HYPOGLYCAEMIA', (width/2 -20), height - 130)
                    .css({
                            fontSize: '14px',
                            color: '#666666',
                            zIndex: 100
                    }).add();

                    chart.renderer.text('HYPERGLYCAEMIA', (width/2 - 20), 70)
                    .css({
                            fontSize: '14px',
                            color: '#666666',
                            zIndex: 100
                    }).add();
            }

            $scope.matrixChart.options.tooltip.formatter = function() {
                                var risk, firstH, lastH;
                                firstH = this.series.processedXData[0];
                                lastH = this.series.processedXData[3];

                                if (this.point.color === 'rgba(255,102,102,0.75)') {
                                    risk = '<h4>' + new Date(firstH).getHours() + ' h - ' + new Date(lastH).getHours() + ' h </h4>';
                                    if (this.series.name.indexOf("hyper") > -1) {
                                        risk += '<span style="font-weight: bold">Hyperglycaemia</span> <br />',
                                            risk += 'Risk: High <br />',
                                            risk += '<i class="far fa-frown fa-3x" style=" color: #003399"></i>';
                                    } else if (this.series.name.indexOf("hypo") > -1) {
                                        risk += '<span style="font-weight: bold">Hypoglycaemia</span> <br />',
                                            risk += 'Risk: High <br />',
                                            risk += '<i class="far fa-frown fa-3x" style=" color: #003399"></i>';
                                    }
                                } else if (this.series.color === 'rgba(255,153,51,0.75)') {
                                    risk = '<h4>' + new Date(firstH).getHours() + ' h - ' + new Date(lastH).getHours() + ' h </h4>';
                                    if (this.series.name.indexOf("hyper") > -1) {
                                        risk += '<span style="font-weight: bold">Hyperglycaemia </span><br />',
                                            risk += 'Risk: Moderate <br />',
                                            risk += '<i class="fas fa-meh fa-3x" style=" color: #003399"></i>';
                                    } else if (this.series.name.indexOf("hypo") > -1) {
                                        risk += '<span style="font-weight: bold">Hypoglycaemia </span><br />',
                                            risk += 'Risk: Moderate <br />',
                                            risk += '<i class="fas fa-meh fa-3x" style=" color: #003399"></i>';
                                    }
                                } else {
                                    risk = '<h4>' + new Date(firstH).getHours() + ' h - ' + new Date(lastH).getHours() + ' h </h4>';
                                    if (this.series.name.indexOf("hyper") > -1) {
                                        risk += '<span style="font-weight: bold">Hyperglycaemia </span><br />',
                                            risk += 'Risk: Low <br />',
                                            risk += '<i class="fas fa-smile fa-3x" style=" color: #003399"></i>';
                                    } else if (this.series.name.indexOf("hypo") > -1) {
                                        risk += '<span style="font-weight: bold">Hypoglycaemia </span><br />',
                                            risk += 'Risk: Low <br />',
                                            risk += '<i class="fas fa-smile fa-3x" style=" color: #003399"></i>';
                                    }
                                }
                                return risk;
                            }
                            
            $scope.matrixChart.xAxis.labels.formatter = function() {
                                return Highcharts.dateFormat('%H:%M', this.value);
                            }

            // $scope.matrixChart.options.legend.labelFormatter = function() {
            //                     console.log(this.color);
            //                     if (this.color === 'rgba(255,102,102,0.75)') {
            //                         return '<span style="color:' + this.color + '">\u2B24</span> High Risk';
            //                     }
            //                     if (this.color === 'rgba(255,153,51,0.75)') {
            //                         return '<span style="color:' + this.color + '">\u2B24</span> Moderate Risk';
            //                     }
            //                     if (this.color === 'rgba(204,255,204,0.75)') {
            //                         return '<span style="color:' + this.color + '">\u2B24</span> Low Risk';
            //                     }
            //                 }

            //Define X axis
            $scope.chart.xAxis.type = 'datetime';

            $scope.chart.xAxis.labels = {
                formatter: function() {
                    return Highcharts.dateFormat('%H:%M', this.value);
                },
                overflow: 'justify',
                //rotation: 0,
                step: 2
            };

            $scope.chart.yAxis.min = 30;
            $scope.chart.yAxis.max = 150;

            // Define Y axis
            $scope.chart.yAxis.title.text = $scope.unit;
            // Objective lines
            $scope.chart.yAxis.plotLines[0].label.text = 'max';
            $scope.chart.yAxis.plotLines[1].label.text = 'min';
            $scope.chart.yAxis.plotLines[1].label.verticalAlign = 'bottom';
            //$scope.chart.yAxis.plotLines[0].color = '#ff6666';
            //scope.chart.yAxis.plotLines[0].width = 2;

            $scope.width = $window.innerWidth;

            angular.element($window).on('resize', function() {

                $scope.width = $window.innerWidth;
                //Define responsive rules
                    if ($scope.width < 500) {
                        $scope.chart.yAxis.title.text = '';
                        $scope.matrixChart.yAxis.title.text = '';
                        $scope.chart.xAxis.labels.rotation = -45;
                        $scope.matrixChart.xAxis.labels.rotation = -45;
                    } else {
                        $scope.chart.yAxis.title.text = $scope.unit;
                        $scope.chart.xAxis.labels.rotation = 0;
                        $scope.matrixChart.xAxis.labels.rotation = 0;
                        $scope.matrixChart.yAxis.title.text = 'Title';
                    }
                    // manually $digest required as resize event is outside angular
                    $scope.$digest();
            });

            angular.element(document).ready(function() {
                $scope.width = $window.innerWidth;

                    //Define responsive rules
                    if ($scope.width < 500) {
                        $scope.chart.yAxis.title.text = '';
                        $scope.matrixChart.yAxis.title.text = '';
                        $scope.chart.xAxis.labels.rotation = -45;
                        $scope.matrixChart.xAxis.labels.rotation = -45;
                    } else {
                        $scope.chart.yAxis.title.text = $scope.unit;
                        $scope.chart.xAxis.labels.rotation = 0;
                        $scope.matrixChart.xAxis.labels.rotation = 0;
                        $scope.matrixChart.yAxis.title.text = 'Title';
                    }
            });

            $scope.chart.options.tooltip = {
                shared: true,
                valueSuffix: 'mmol/L',
                formatter: function() {
                    var hour = Highcharts.dateFormat('%H:%M', this.x);
                    var median = this.y.toFixed(1);
                    var percentile1090low = this.points[2].point['low'].toFixed(1);
                    var percentile1090high = this.points[2].point['high'].toFixed(1);
                    var percentile2575low = this.points[1].point['low'].toFixed(1);
                    var percentile2575high = this.points[1].point['high'].toFixed(1);
                    var tooltip = '<h4>' + hour + '</h4> <br/>';
                    tooltip += '<span style="color:' + this.points[0].color + '">\u25CF</span> Median: <strong>' + median + ' ' + $scope.unit + '</strong><br />';
                    tooltip += '<span style="color:' + this.points[2].color + '">\u25CF</span> 10th to 90th percentile: <strong>' + percentile1090low + ' - ' + percentile1090high + ' ' + $scope.unit + '</strong><br />';
                    tooltip += '<span style="color:' + this.points[1].color + '">\u25CF</span> 25th to 75th percentile: <strong>' + percentile2575low + ' - ' + percentile2575high + ' ' + $scope.unit + '</strong>';
                    return tooltip;

                }
            }

            // Get objectives
            $ocLazyLoad.load('js/services/ObjectivesService.js').then(function() {
                var Objective = $injector.get('Objective');
                Objective.getObjective('glycaemia').success(function(data) {
                    if (data) {
                        if (data.values[0].type == 'max') {
                            $scope.chart.yAxis.plotLines[0].value = data.values[0].value;
                            $scope.chart.yAxis.plotLines[0].label.text = $scope.chart.yAxis.plotLines[0].label.text + " (" + $filter('number')(data.values[0].value, 1) + $scope.unit + ")";
                            $scope.chart.yAxis.plotLines[1].value = data.values[1].value;
                            $scope.chart.yAxis.plotLines[1].label.text = $scope.chart.yAxis.plotLines[1].label.text + " (" + $filter('number')(data.values[1].value, 1) + $scope.unit + ")";
                        } else {
                            $scope.chart.yAxis.plotLines[1].value = data.values[0].value;
                            $scope.chart.yAxis.plotLines[1].label.text = $scope.chart.yAxis.plotLines[1].label.text + " (" + $filter('number')(data.values[0].value, 1) + $scope.unit + ")";
                            $scope.chart.yAxis.plotLines[0].value = data.values[1].value;
                            $scope.chart.yAxis.plotLines[0].label.text = $scope.chart.yAxis.plotLines[0].label.text + " (" + $filter('number')(data.values[1].value, 1) + $scope.unit + ")";
                        }
                    } else {
                        // TYPICAL MAX
                        $scope.chart.yAxis.plotLines[0].value = 180;
                        //TYPICAL MIN
                        $scope.chart.yAxis.plotLines[1].value = 80;
                    }

                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });
            });

            $scope.chart.credits = {
                enabled: false
            }

            // Build the chart
            $scope.build = function(view) {
                var from;

                switch (view) {
                    case '1d':
                        $scope.view = '1d';
                        from = new Date(new Date().setDate(new Date().getDate() - 1));
                        break;
                    case '14d':
                        $scope.view = '14d';
                        from = new Date(new Date().setDate(new Date().getDate() - 14));
                        break;
                    case '1m':
                        $scope.view = '1m';
                        from = new Date(new Date().setDate(new Date().getDate() - 30));
                        break;
                    case '3m':
                        $scope.view = '3m';
                        from = new Date(new Date().setDate(new Date().getDate() - 90));
                        break;
                    case '6m':
                        $scope.view = '6m';
                        from = new Date(new Date().setDate(new Date().getDate() - 180));
                        break;
                    case 'list':
                        $scope.view = 'list';
                        break;
                    default:
                        $scope.view = '1m';
                        from = new Date(new Date().setDate(new Date().getDate() - 30));
                        break;
                }

                if (view !== 'list') {
                    $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                        var Chart = $injector.get('Chart');
                        Chart.build({
                            type: 'newGly',
                            from: from,
                            to: new Date()
                        }).success(function(data) {
                            //console.log(data);
                            for (var i = 0; i < data.series.length; i++) {
                                for (var k = 0; k < data.series[i].data.length; ++k) {
                                    if (data.series[i].data[k][2] > $scope.chart.yAxis.max) {
                                        $scope.chart.yAxis.max = data.series[i].data[k][2] + 1; //find and set a maximum value of yAxis
                                    }
                                    if (data.series[i].data[k][1] < $scope.chart.yAxis.min) {
                                        $scope.chart.yAxis.min = data.series[i].data[k][1]; //find and set a minimum value of yAxis
                                    }
                                }
                            }

                            $scope.chart.series = data.series;
                            //console.log(data.series);
                            //console.log('Meal times: ', data.mealTimes);
                            //console.log(data)
                            if (data.mealTimes.length > 0 && data.mealTimes[0].data != null && data.mealTimes[1].data != null && data.mealTimes[2].data != null && data.mealTimes[3].data != null) {
                                //$scope.matrixLowGlucose.xAxis.plotLines[0].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[0].data, 0, 0, 0);
                                //$scope.matrixLowGlucose.xAxis.plotLines[1].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[1].data, 0, 0, 0);
                                //$scope.matrixLowGlucose.xAxis.plotLines[2].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[2].data, 0, 0, 0);
                                //$scope.matrixLowGlucose.xAxis.plotLines[3].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[3].data, 0, 0, 0);

                                $scope.chart.xAxis.plotLines[0].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[0].data, 0, 0, 0);
                                $scope.chart.xAxis.plotLines[1].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[1].data, 0, 0, 0);
                                $scope.chart.xAxis.plotLines[2].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[2].data, 0, 0, 0);
                                $scope.chart.xAxis.plotLines[3].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[3].data, 0, 0, 0);
                            }

                            var counter = 0;
                            var matrixData = data.mealTimes.length;
                            var meal = data.mealTimes;
                            var plotLineObjMatrix = {
                                    value: null,
                                    width: 1,
                                    color: '#e6e6e6',
                                    dashStyle: 'solid',
                                    zIndex: 100,
                                        label: {
                                            useHTML: true,
                                            text: '',
                                            align: 'right',
                                            rotation: 0,
                                            y: -35,
                                            x: 10
                                        }
                                };
                            var plotLineObjChart = {
                                value: null,
                                width: 1,
                                color: '#e6e6e6',
                                dashStyle: 'solid',
                                zIndex: 50
                                
                            };  

                            data.mealTimes.forEach(function(entry) {
                                $scope.matrixChart.xAxis.plotLines[counter] = jQuery.extend(true, {}, plotLineObjMatrix);
                                $scope.chart.xAxis.plotLines[counter] = jQuery.extend(true, {}, plotLineObjChart);
                                $scope.chart.xAxis.plotLines[counter].value = new Date(new Date().setDate(3)).setHours(entry.data, 0, 0, 0);
                                $scope.matrixChart.xAxis.plotLines[counter].value = new Date(new Date().setDate(3)).setHours(entry.data, 0, 0, 0);
                                if (counter === 3) {
                                    $scope.matrixChart.xAxis.plotLines[counter].label.text = '<i class="fas fa-bed fa-2x" aria-hidden="true"></i>';
                                } else {
                                    $scope.matrixChart.xAxis.plotLines[counter].label.text = '<i class="fas fa-utensils fa-2x" aria-hidden="true"></i>';
                                }
                                counter++;
                            });

                            $scope.matrixChart.series = [];

                            var counterRed = true,
                                counterOrange = true,
                                counterGreen = true;

                            var polygonObj = {
                                    name: 'hypo' + i,
                                    type: 'polygon',
                                    data: [],
                                    color: Highcharts.Color('#ff6666').setOpacity(0.75).get(),
                                    //enableMouseTracking: true,
                                    showInLegend: true,
                                    zIndex: 50
                                };


                                for (var i = 0; i < matrixData; i++) {
                                    //console.log(meal);
                                    if (i == 0) {
                                        var polygon = jQuery.extend(true, {}, polygonObj);
                                        polygon.data.push([new Date(datePolygon.setMonth(new Date().getMonth())).setHours(0, 0, 0, 0), 0]);
                                        polygon.data.push([new Date(datePolygon.setMonth(new Date().getMonth())).setHours(0, 0, 0, 0), 6]);
                                        polygon.data.push([new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 6]);
                                        polygon.data.push([new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 0]);
                                        polygon.color = Highcharts.Color('#ff9933').setOpacity(0.75).get();
                                        if (counterRed === false) {
                                            polygon.showInLegend = false;
                                        }
                                        counterOrange = false;

                                        var polygon2 = jQuery.extend(true, {}, polygonObj);
                                        polygon2.data[0] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(0, 0, 0, 0), 10];
                                        polygon2.data[1] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(0, 0, 0, 0), 16];
                                        polygon2.data[2] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 16];
                                        polygon2.data[3] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 10];
                                        polygon2.color = Highcharts.Color('#ccffcc').setOpacity(0.75).get();
                                        if (counterGreen === false) {
                                            polygon2.showInLegend = false;
                                        }
                                        counterGreen = false;
                                    } else {
                                        var polygon = jQuery.extend(true, {}, polygonObj);
                                        polygon.data[0] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i - 1].data, 0, 0, 0), 0];
                                        polygon.data[1] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i - 1].data, 0, 0, 0), 6];
                                        polygon.data[2] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data , 0, 0, 0), 6];
                                        polygon.data[3] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 0];
                                        if (counterRed === false) {
                                            polygon.showInLegend = false;
                                        }
                                        counterRed = false;

                                        var polygon2 = jQuery.extend(true, {}, polygonObj);
                                        polygon2.data[0] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i - 1].data, 0, 0, 0), 10];
                                        polygon2.data[1] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i - 1].data, 0, 0, 0), 16];
                                        polygon2.data[2] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 16];
                                        polygon2.data[3] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data , 0, 0, 0), 10];
                                        polygon2.color = Highcharts.Color('#ff9933').setOpacity(0.75).get();
                                        if (counterOrange === false) {
                                            polygon2.showInLegend = false;
                                        }
                                        counterOrange = false;
                                    }
                                    if (i == matrixData -1) {
                                        var lastPolygon = jQuery.extend(true, {}, polygonObj);
                                        lastPolygon.data[0] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 0];
                                        lastPolygon.data[1] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 6];
                                        lastPolygon.data[2] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(24, 0, 0, 0), 6];
                                        lastPolygon.data[3] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(24, 0, 0, 0), 0];
                                        lastPolygon.color = Highcharts.Color('#ff9933').setOpacity(0.75).get();
                                        if (counterOrange === false) {
                                            lastPolygon.showInLegend = false;
                                        }
                                        counterOrange = false;
                                        $scope.matrixChart.series.push(lastPolygon);

                                        var lastPolygon2 = jQuery.extend(true, {}, polygonObj);
                                        lastPolygon2.data[0] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 10];
                                        lastPolygon2.data[1] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 16];
                                        lastPolygon2.data[2] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(24, 0, 0, 0), 16];
                                        lastPolygon2.data[3] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(24, 0, 0, 0), 10];
                                        lastPolygon2.color = Highcharts.Color('#ccffcc').setOpacity(0.75).get();
                                        if (counterGreen === false) {
                                            lastPolygon2.showInLegend = false;
                                        }
                                        counterGreen = false;
                                        $scope.matrixChart.series.push(lastPolygon2);
                                    }

                                    $scope.matrixChart.series.push(polygon);
                                    $scope.matrixChart.series.push(polygon2);                                                             
                                }

                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        });
                    });

                    /* Plot mean line */
                    if (view !== 'list') {
                        $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                            var Chart = $injector.get('Chart');
                            Chart.build({
                                type: 'glycaemia',
                                from: from,
                                to: new Date
                            }).success(function(data) {
                                //Insert the mean line
                                $scope.chart.yAxis.plotLines[2].value = data.mean;
                                $scope.chart.yAxis.plotLines[2].label.text = 'mean' + " (" + $filter('number')(data.mean, 1) + $scope.unit + ")";
                            }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type: 'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                            });
                        });
                    }
                } else {
                    $scope.$parent.buildList({ type: 'glycaemia' }, function(data) {
                        $scope.list = data;
                    });
                }
                //console.log('xAxis:', $scope.chart.xAxis.plotLines);
                //console.log('yAxis:', $scope.chart.yAxis.plotLines);
            }
            //console.log($scope.chart);
            // First build
            $scope.build('1m');
        }
    }
});