angular.module('NewGlycaemiaChartDirective', []).directive('newglychart', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, $window, $filter, $state, $log, ModalService) {
<<<<<<< HEAD
    return {
        restrict: 'E',
        templateUrl: 'templates/dashboard/cards/directives/chart.html',
        scope: {
            title: '@',
            name: '@',
            objective: '@'
        },
        link: function($scope, $element, $attrs) {
            $scope.view = ''; //set the initial view of the data (1m, 2m, 3m, list)
            $scope.unit = 'mg/dL';
=======
  return {
    restrict: 'E',
    templateUrl: 'templates/dashboard/cards/directives/chart.html',
    scope: {
      title: '@',
      name: '@',
      objective: '@'
    },
    link: function($scope, $element, $attrs) {
      $scope.view = ''; //set the initial view of the data (1m, 2m, 3m, list)
      $scope.unit = 'mmol/L';
>>>>>>> 7cdd22fbed7aea7550bda07899bf33f35e599468

      // Global configuration
      Highcharts.setOptions({
        global: {
          useUTC: false
        }
      });

<<<<<<< HEAD
            // Copy the basic configuration
            $scope.chart = JSON.parse(JSON.stringify($scope.$parent.bChart));
            //$scope.chart.xAxis.labels.step = 2;
=======
      // Copy the basic configuration
      $scope.chart = JSON.parse(JSON.stringify($scope.$parent.bChart));
      //$scope.chart.xAxis.labels.step = 2;
>>>>>>> 7cdd22fbed7aea7550bda07899bf33f35e599468

      //Copy the matrix chart config
      $scope.matrixLowGlucose = JSON.parse(JSON.stringify($scope.$parent.matrixChart));

<<<<<<< HEAD
            $scope.matrixLowGlucose.xAxis.labels = {
                formatter: function() {
                    return Highcharts.dateFormat('%H:%M', this.value);
                },
                overflow: 'justify',
                step: 2
            };

            //matrix chart tooltip formatter
            /*$scope.matrixLowGlucose.options.tooltip = { 
              formatter: function() {
                //$log.debug('This: ', this);
                  return this.x; //this is not working FIX!!

              }
            };*/

            $scope.width = $window.innerWidth;

            angular.element($window).on('resize', function() {

                $scope.width = $window.innerWidth;
                //console.log('RESIZE');

                //Define responsive rules
                if ($scope.width < 500) {
                    $scope.chart.yAxis.title.text = '';
                    //$scope.chart.xAxis.labels.step = 2;
                    $scope.chart.xAxis.labels.rotation = -45;
                } else {
                    $scope.chart.yAxis.title.text = $scope.unit;
                    $scope.chart.xAxis.labels.rotation = 0;
                    //$scope.chart.xAxis.labels.step = 2;
                }
                // manually $digest required as resize event
                // is outside of angular
                $scope.$digest();
            });

            angular.element(document).ready(function() {
                $scope.width = $window.innerWidth;
                //console.log('READY');

                //Define responsive rules
                if ($scope.width < 500) {
                    $scope.chart.yAxis.title.text = '';
                    //$scope.chart.xAxis.labels.step = 2;
                    $scope.chart.xAxis.labels.rotation = -45;
                    //console.log('LESS THAN 500');
                } else {
                    $scope.chart.yAxis.title.text = $scope.unit;
                    $scope.chart.xAxis.labels.rotation = 0;
                    //$scope.chart.xAxis.labels.step = 2;
                    //console.log('MORE THAN 500');
                }
            });

            //Define X axis
            $scope.chart.xAxis.type = 'datetime';

            $scope.chart.xAxis.labels = {
                formatter: function() {
                    return Highcharts.dateFormat('%H:%M', this.value);
                },
                overflow: 'justify',
                //rotation: 0,
                step: 2
            };
=======
      $scope.matrixLowGlucose.xAxis.labels = {
        formatter: function() {
          return Highcharts.dateFormat('%H:%M', this.value);
        },
        overflow: 'justify',
        step: 2
      };

      $scope.matrixLowGlucose.options.tooltip = {
        enabled: false
          /*,
                        formatter: function() {
                            return this.x;
                        }*/
      };

      //Define X axis
      $scope.chart.xAxis.type = 'datetime';

      $scope.chart.xAxis.labels = {
        formatter: function() {
          return Highcharts.dateFormat('%H:%M', this.value);
        },
        overflow: 'justify',
        rotation: -45,
        step: 2
      };

      angular.element($window).on('resize', function() {
        $scope.width = $window.innerWidth;

        //Define responsive rules
        if ($scope.width < 500) {
          $scope.chart.yAxis.title.text = '';
          $scope.chart.xAxis.labels.rotation = -45;
          $scope.matrixLowGlucose.xAxis.labels.rotation = -45;
          $scope.matrixLowGlucose.options.chart.spacingLeft = 35;
        } else {
          $scope.chart.yAxis.title.text = $scope.unit;
          $scope.chart.xAxis.labels.rotation = 0;
          $scope.matrixLowGlucose.xAxis.labels.rotation = 0;
          $scope.matrixLowGlucose.options.chart.spacingLeft = 65;
        }
        // manually $digest required as resize event event is outside of angular
        $scope.$digest();
      });

      angular.element(document).ready(function() {
        $scope.width = $window.innerWidth;

        //Define responsive rules
        if ($scope.width < 500) {
          $scope.chart.yAxis.title.text = '';
          $scope.chart.xAxis.labels.rotation = -45;
          $scope.matrixLowGlucose.xAxis.labels.rotation = -45;
          $scope.matrixLowGlucose.options.chart.spacingLeft = 35;
>>>>>>> 7cdd22fbed7aea7550bda07899bf33f35e599468

        } else {
          $scope.chart.yAxis.title.text = $scope.unit;
          $scope.chart.xAxis.labels.rotation = 0;
          $scope.matrixLowGlucose.xAxis.labels.rotation = 0;
          $scope.matrixLowGlucose.options.chart.spacingLeft = 65;
        }
      });
      $scope.chart.yAxis.min = 0;
      $scope.chart.yAxis.max = 12;

<<<<<<< HEAD
            // Define Y axis
            $scope.chart.yAxis.title.text = $scope.unit;
            // Objective lines
            $scope.chart.yAxis.plotLines[0].label.text = 'max';
            $scope.chart.yAxis.plotLines.push({
                value: 0,
                color: '#ff6666',
                dashStyle: 'solid',
                width: 2,
                zIndex: 99,
                label: {
                    x: 0,
                    align: 'right'
                }
            });
            $scope.chart.yAxis.plotLines[1].label.text = 'min';
            $scope.chart.yAxis.plotLines[0].color = '#ff6666';
            $scope.chart.yAxis.plotLines[0].width = 2;

            $scope.chart.yAxis.plotLines[0].dashStyle = 'solid';

            // ===================== FOR THE VIDEO - TEMPORARY  ===============================================
            // 
            if ($window.innerWidth <= 450) {

                $scope.chart.yAxis.plotLines.push({
                    value: 10, //new Date(new Date().setDate(3)).setHours(8, 0, 0, 0),// Date.UTC(2017,10, 3, 7, 0),
                    width: 2,
                    color: 'black',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-apple fa-2x" aria-hidden="true"></i>',
                        rotation: 0,
                        x: 80,
                        y: 10
                    }
                });

                $scope.chart.yAxis.plotLines.push({
                    value: 10, //new Date(new Date().setDate(3)).setHours(8, 0, 0, 0),// Date.UTC(2017,10, 3, 7, 0),
                    width: 2,
                    color: 'black',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-apple fa-2x" aria-hidden="true"></i>',
                        rotation: 0,
                        x: 135,
                        y: 10
                    }
                });

                $scope.chart.yAxis.plotLines.push({
                    value: 10, //new Date(new Date().setDate(3)).setHours(8, 0, 0, 0),// Date.UTC(2017,10, 3, 7, 0),
                    width: 2,
                    color: 'black',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-apple fa-2x" aria-hidden="true"></i>',
                        rotation: 0,
                        x: 185,
                        y: 10
                    }
                });

                $scope.chart.yAxis.plotLines.push({
                    value: 10, //new Date(new Date().setDate(3)).setHours(8, 0, 0, 0),// Date.UTC(2017,10, 3, 7, 0),
                    width: 2,
                    color: 'black',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-bed fa-2x" aria-hidden="true"></i>',
                        rotation: 0,
                        x: 245,
                        y: 10
                    }
                });
            }

            // ===================== FOR THE VIDEO - TEMPORARY  ===============================================


            $scope.chart.options.tooltip = {
                shared: true,
                valueSuffix: 'mmol/L',
                formatter: function() {
                    var hour = Highcharts.dateFormat('%H:%M', this.x);
                    var median = this.y.toFixed(0);
                    var percentile1090low = this.points[2].point['low'].toFixed(0);
                    var percentile1090high = this.points[2].point['high'].toFixed(0);
                    var percentile2575low = this.points[1].point['low'].toFixed(0);
                    var percentile2575high = this.points[1].point['high'].toFixed(0);
                    var tooltip = '<h4>' + hour + '</h4> <br/>';
                    tooltip += '<span style="color:' + this.points[0].color + '">\u25CF</span> Median: <strong>' + median + ' ' + $scope.unit + '</strong><br />';
                    tooltip += '<span style="color:' + this.points[2].color + '">\u25CF</span> 10th to 90th percentile: <strong>' + percentile1090low + ' - ' + percentile1090high + ' ' + $scope.unit + '</strong><br />';
                    tooltip += '<span style="color:' + this.points[1].color + '">\u25CF</span> 25th to 75th percentile: <strong>' + percentile2575low + ' - ' + percentile2575high + ' ' + $scope.unit + '</strong>';
                    return tooltip;

                }
            }

            // Get objectives
            $ocLazyLoad.load('js/services/ObjectivesService.js').then(function() {
                var Objective = $injector.get('Objective');
                Objective.getObjective('glycaemia').success(function(data) {
                    if (data) {
                        if (data.values[0].type == 'max') {
                            $scope.chart.yAxis.plotLines[0].value = data.values[0].value;
                            $scope.chart.yAxis.plotLines[0].label.text = $scope.chart.yAxis.plotLines[0].label.text + " (" + $filter('number')(data.values[0].value, 1) + $scope.unit + ")";
                            $scope.chart.yAxis.plotLines[1].value = data.values[1].value;
                            $scope.chart.yAxis.plotLines[1].label.text = $scope.chart.yAxis.plotLines[1].label.text + " (" + $filter('number')(data.values[1].value, 1) + $scope.unit + ")";
                        } else {
                            $scope.chart.yAxis.plotLines[1].value = data.values[0].value;
                            $scope.chart.yAxis.plotLines[1].label.text = $scope.chart.yAxis.plotLines[1].label.text + " (" + $filter('number')(data.values[0].value, 1) + $scope.unit + ")";
                            $scope.chart.yAxis.plotLines[0].value = data.values[1].value;
                            $scope.chart.yAxis.plotLines[0].label.text = $scope.chart.yAxis.plotLines[0].label.text + " (" + $filter('number')(data.values[1].value, 1) + $scope.unit + ")";
                        }
                    }

                    $scope.chart.yAxis.plotBands = [{
                            color: '#fff0f0',
                            from: 0,
                            to: $scope.chart.yAxis.plotLines[1].value
                        },
                        {
                            color: '#fff0f0',
                            from: $scope.chart.yAxis.plotLines[0].value,
                            to: 300
                        },



                        /* {
                             color: '#ebfaeb',
                             from: $scope.chart.yAxis.plotLines[1].value,
                             to: $scope.chart.yAxis.plotLines[0].value,
                         }*/
                    ]

                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });
            });

            $scope.chart.credits = {
                enabled: false
            }

            // Build the chart
            $scope.build = function(view) {
                var from;

                switch (view) {
                    case '1m':
                        $scope.view = '1m';
                        from = new Date(new Date().setDate(new Date().getDate() - 30));
                        break;
                    case '3m':
                        $scope.view = '3m';
                        from = new Date(new Date().setDate(new Date().getDate() - 90));
                        break;
                    case '6m':
                        $scope.view = '6m';
                        from = new Date(new Date().setDate(new Date().getDate() - 180));
                        break;
                    case 'list':
                        $scope.view = 'list';
                        break;
                    default:
                        $scope.view = '';
                        from = new Date(new Date().setDate(new Date().getDate() - 30));
                        break;
                }

                if (view !== 'list') {
                    $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                        var Chart = $injector.get('Chart');
                        Chart.build({
                            type: 'newGly',
                            from: from,
                            to: new Date()
                        }).success(function(data) {
                            for (var i = 0; i < data.series.length; i++) {
                                for (var k = 0; k < data.series[i].data.length; ++k) {
                                    if (data.series[i].data[k][2] > $scope.chart.yAxis.max) {
                                        $scope.chart.yAxis.max = data.series[i].data[k][2] + 1; //find and set a maximum value of yAxis
                                    }
                                    if (data.series[i].data[k][1] < $scope.chart.yAxis.min) {
                                        $scope.chart.yAxis.min = data.series[i].data[k][1]; //find and set a minimum value of yAxis
                                    }
                                }
                            }

                            $scope.chart.series = data.series;
                            $scope.stdev = data.stdev;
                            //console.log(data.series);

                            //console.log('Meal times: ', data.mealTimes);
                            if (data.mealTimes[0].data != null && data.mealTimes[1].data != null && data.mealTimes[2].data != null && data.mealTimes[3].data != null) {
                                $scope.matrixLowGlucose.xAxis.plotLines[0].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[0].data, 0, 0, 0);
                                $scope.matrixLowGlucose.xAxis.plotLines[1].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[1].data, 0, 0, 0);
                                $scope.matrixLowGlucose.xAxis.plotLines[2].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[2].data, 0, 0, 0);
                                $scope.matrixLowGlucose.xAxis.plotLines[3].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[3].data, 0, 0, 0);
                            }

                            $scope.chart.xAxis.plotLines[0].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[0].data, 0, 0, 0);
                            $scope.chart.xAxis.plotLines[1].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[1].data, 0, 0, 0);
                            $scope.chart.xAxis.plotLines[2].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[2].data, 0, 0, 0);
                            $scope.chart.xAxis.plotLines[3].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[3].data, 0, 0, 0);

                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        });
                    });




                    $ocLazyLoad.load('js/services/GlycHemoglobinService.js').then(function() {
                        var Glyc = $injector.get('GlycHemoglobin');
                        Glyc.computeValue().success(function(data) {

                            $scope.glyc = data.value;
                            $scope.IFCCValue = ($scope.glyc - 2.152) / 0.09148;

                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        });
                    });
                } else {
                    $scope.$parent.buildList({ type: 'newGly' }, function(data) {
                        $scope.list = data;
                    });
                }
            }

            // First build
            $scope.build();
        }
    }
=======
      //Insert objectives plotLines placeholders
      $scope.chart.yAxis.plotLines.push({
        value: 0,
        width: 1.5,
        color: '#94d631',
        zIndex: 1,
        label: {
          text: '',
          align: 'left',
          x: 5,
          y: 20
        }
      });

      $scope.chart.yAxis.plotLines.push({
        value: 0,
        width: 1,
        color: '#94d631',
        zIndex: 1.5,
        label: {
          text: '',
          align: 'left',
          x: 5,
          y: 20
        }
      });

      $scope.chart.options.tooltip = {
        shared: true,
        valueSuffix: 'mmol/L',
        formatter: function() {
          var hour = Highcharts.dateFormat('%H:%M', this.x);
          var median = this.y;
          var percentile1090low = this.points[2].point['low'];
          var percentile1090high = this.points[2].point['high'];
          var percentile2575low = this.points[1].point['low'];
          var percentile2575high = this.points[1].point['high'];
          var tooltip = hour + '<br />';
          tooltip += '<span style="color:' + this.points[0].color + '">\u25CF</span> Median: <strong>' + median + ' ' + $scope.unit + '</strong><br />';
          tooltip += '<span style="color:' + this.points[2].color + '">\u25CF</span> 10th to 90th percentile: <strong>' + percentile1090low + ' - ' + percentile1090high + ' ' + $scope.unit + '</strong><br />';
          tooltip += '<span style="color:' + this.points[1].color + '">\u25CF</span> 25th to 75th percentile: <strong>' + percentile2575low + ' - ' + percentile2575high + ' ' + $scope.unit + '</strong>';
          return tooltip;

        }
      }

      $scope.chart.credits = {
        enabled: false
      }

      // Build the chart
      $scope.build = function(view) {
        var from;

        switch (view) {
          case '1m':
            $scope.view = '1m';
            from = new Date(new Date().setDate(new Date().getDate() - 30));
            break;
          case '3m':
            $scope.view = '3m';
            from = new Date(new Date().setDate(new Date().getDate() - 90));
            break;
          case '6m':
            $scope.view = '6m';
            from = new Date(new Date().setDate(new Date().getDate() - 180));
            break;
          case 'list':
            $scope.view = 'list';
            break;
          default:
            $scope.view = '';
            from = new Date(new Date().setDate(new Date().getDate() - 30));
            break;
        }

        if (view !== 'list') {
          $ocLazyLoad.load('js/services/ChartService.js').then(function() {
            var Chart = $injector.get('Chart');
            Chart.build({
              type: 'newGly',
              from: from,
              to: new Date()
            }).success(function(data) {
              for (var i = 0; i < data.series.length; i++) {
                for (var k = 0; k < data.series[i].data.length; ++k) {
                  if (data.series[i].data[k][2] > $scope.chart.yAxis.max) {
                    $scope.chart.yAxis.max = data.series[i].data[k][2] + 1; //find and set a maximum value of yAxis
                  }
                  if (data.series[i].data[k][1] < $scope.chart.yAxis.min) {
                    $scope.chart.yAxis.min = data.series[i].data[k][1]; //find and set a minimum value of yAxis
                  }
                }
              }

              $scope.chart.series = data.series;

              for (var i = 0; i < data.mealTimes.length; i++) {
                if (data.mealTimes[i].data != null) {
                  $scope.matrixLowGlucose.xAxis.plotLines[i] = {
                    value: null, //new Date(new Date().setDate(3)).setHours(8, 0, 0, 0),// Date.UTC(2017,10, 3, 7, 0),
                    width: 1,
                    color: 'black',
                    dashStyle: 'dash',
                    label: {
                      useHTML: true,
                      text: '',
                      align: 'right',
                      rotation: 0,
                      y: -25,
                      x: 8
                    }
                  };

                  $scope.chart.xAxis.plotLines[i] = { //meal plotlines
                    value: 0, //Date.UTC(2017,10, 3, 7, 0),
                    width: 1,
                    color: 'black',
                    dashStyle: 'dash',
                    label: {
                      text: '',
                      align: 'center',
                      y: 12,
                      x: 0
                    }
                  };

                  switch (i) {
                    case 3:
                      $scope.matrixLowGlucose.xAxis.plotLines[i].label.text = '<i class="fa fa-bed fa-2x" aria-hidden="true"></i>'
                      break;
                    default:
                      $scope.matrixLowGlucose.xAxis.plotLines[i].label.text = '<i class="fa fa-apple fa-2x" aria-hidden="true"></i>'
                  }
                  $scope.matrixLowGlucose.xAxis.plotLines[i].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[i].data, 0, 0, 0);
                  $scope.chart.xAxis.plotLines[i].value = new Date(new Date().setDate(3)).setHours(data.mealTimes[i].data, 0, 0, 0);
                }
              }

            }).error(function(status, data) {
              $rootScope.rootAlerts.push({
                type: 'danger',
                msg: gettextCatalog.getString('An error occurred, please try again later'),
                priority: 2
              });
            });
          });

          // Get objectives
          $ocLazyLoad.load('js/services/ObjectivesService.js').then(function() {
            var Objective = $injector.get('Objective');
            Objective.getObjective('glycaemia').success(function(data) {
              if (data) {
                if (data.values[0].type == 'max') {
                  $scope.chart.yAxis.plotLines[1].value = data.values[0].value;
                  $scope.chart.yAxis.plotLines[1].label.text = " (" + $filter('number')(data.values[0].value, 1) + $scope.unit + ")";
                  $scope.chart.yAxis.plotLines[2].value = data.values[1].value;
                  $scope.chart.yAxis.plotLines[2].label.text = " (" + $filter('number')(data.values[1].value, 1) + $scope.unit + ")";
                } else {
                  $scope.chart.yAxis.plotLines[1].value = data.values[1].value;
                  $scope.chart.yAxis.plotLines[1].label.text = " (" + $filter('number')(data.values[1].value, 1) + $scope.unit + ")";
                  $scope.chart.yAxis.plotLines[2].value = data.values[0].value;
                  $scope.chart.yAxis.plotLines[2].label.text = " (" + $filter('number')(data.values[0].value, 1) + $scope.unit + ")";
                }
              }
            }).error(function(status, data) {
              $rootScope.rootAlerts.push({
                type: 'danger',
                msg: gettextCatalog.getString('An error occurred, please try again later'),
                priority: 2
              });
            });
          });
        } else {
          $scope.$parent.buildList({
            type: 'newGly'
          }, function(data) {
            $scope.list = data;
          });
        }
      }

      // First build
      $scope.build();
    }
  }
>>>>>>> 7cdd22fbed7aea7550bda07899bf33f35e599468
});