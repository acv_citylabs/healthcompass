
angular.module('AskHeartRateDirective', []).directive('askheartrate', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope) {
    return {
        restrict: 'A',
        templateUrl: 'templates/dashboard/asks/directives/ask_heartrate.html',
        link: function($scope, $element, $attrs) {
           
            $scope.value = 65;
            $scope.hrType = "atRest";

            //Answer
            $scope.answer = function(){
            	
                $scope.$parent.addEntry({type: 'heartrate', value: $scope.value, subType: $scope.hrType}, function(){
                	$scope.$parent.buildDashboard();
                });
            }           
        }
    }
});