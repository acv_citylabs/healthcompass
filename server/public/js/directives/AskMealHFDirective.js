angular.module('AskMealHFDirective', []).directive('askmealhf', function(gettextCatalog, $state, $window, $ocLazyLoad, $injector, $rootScope) {
    return {
        restrict: 'A',
        templateUrl: 'templates/dashboard/asks/directives/ask_mealhf.html',
        link: function($scope, $element, $attrs) {
            // Display a title in accordance with time
        	
            if((new Date >= new Date().setHours(5, 0, 0) && new Date <= new Date().setHours(11, 0, 0))){
                $scope.title = gettextCatalog.getString("Did you have some of these for breakfast ?");
                $scope.meal = "breakfast";
            } else if (new Date >= new Date().setHours(11, 0, 0) && new Date <= new Date().setHours(17, 0, 0)){
            	$scope.title = gettextCatalog.getString("Did you have some of these for lunch ?");
            	$scope.meal = "lunch";
            } else {
            	$scope.title = gettextCatalog.getString("Did you have some of these for dinner ?");
            	$scope.meal = "dinner";
            }
            

        	$scope.drinksExpand = false;

            $scope.liquids = [{
            	name : gettextCatalog.getString("Water / juice"),
            	ml: [200,300,500],
            	values: [{value: 0, img : {'background': 'url(./img/food/smallGlass.png) no-repeat center center', 'background-size': 'cover'}},
            	      {value: 0, img : {'background': 'url(./img/food/normalGlass.png) no-repeat center center', 'background-size': 'cover'}},
            	      {value: 0, img : {'background': 'url(./img/food/bigGlass.png) no-repeat center center', 'background-size': 'cover'}}],
            	color : 0
            },{
            	name : gettextCatalog.getString("Coffee / tea / milk"),
            	ml: [80,200,350],
            	values: [{value: 0, img : {'background': 'url(./img/food/smallCup.png) no-repeat center center', 'background-size': 'cover'}},
            	         {value: 0, img : {'background': 'url(./img/food/normalCup.png) no-repeat center center', 'background-size': 'cover'}},
            	         {value: 0, img : {'background': 'url(./img/food/bigCup.png) no-repeat center center', 'background-size': 'cover'}}],
                color : 0
            },{
            	name : gettextCatalog.getString("Soda"),
            	ml: [250,330,500],
            	values: [{value: 0, img : {'background': 'url(./img/food/smallCan.png) no-repeat center center', 'background-size': 'cover'}},
            	         {value: 0, img : {'background': 'url(./img/food/normalCan.png) no-repeat center center', 'background-size': 'cover'}},
            	         {value: 0, img : {'background': 'url(./img/food/bigCan.png) no-repeat center center', 'background-size': 'cover'}}],
                color : 1
            },{
            	name : gettextCatalog.getString("Beer"),
            	ml: [250,330,500],
            	values: [{value: 0, img : {'background': 'url(./img/food/smallCan.png) no-repeat center center', 'background-size': 'cover'}},
            	         {value: 0, img : {'background': 'url(./img/food/normalCan.png) no-repeat center center', 'background-size': 'cover'}},
            	         {value: 0, img : {'background': 'url(./img/food/bigCan.png) no-repeat center center', 'background-size': 'cover'}}],
                color : 2
            },{
            	name : gettextCatalog.getString("Wine"),
            	ml: [125,250,1000],
            	values: [{value: 0, img : {'background': 'url(./img/food/smallWine.png) no-repeat center center', 'background-size': 'cover'}},
            	         {value: 0, img : {'background': 'url(./img/food/normalWine.png) no-repeat center center', 'background-size': 'cover'}},
            	         {value: 0, img : {'background': 'url(./img/food/bigWine.png) no-repeat center center', 'background-size': 'cover'}}],
                color : 2
            }];
            
            $scope.salt =[{
            	category : gettextCatalog.getString("Charcuterie"),
            	values : [{value: 0, inc: 1, name : gettextCatalog.getString("Cooked ham"), unit: gettextCatalog.getString("Slice(s)"), muted:""}
            	         ,{value: 0, inc: 1, name : gettextCatalog.getString("Smoked ham"), unit: gettextCatalog.getString("Slice(s)"), muted:""}
            	         ,{value: 0, inc: 1, name : gettextCatalog.getString("Pâté"), unit: gettextCatalog.getString("Slice(s)"), muted:""}],
            	mg: [500,1340,360],
            	expand: false
            },{
            	category : gettextCatalog.getString("Dairy Products"),
            	values : [{value: 0, inc: 1, name : gettextCatalog.getString("Allowed cheeses"), unit: gettextCatalog.getString("Slice(s)"), muted:"(ex : allégés, à tartiner, mozzarella, ricotta, Affligem, Chimay, chèvre, Emmenthal, Gouda jeune, Maredsous, Philadelphia)"}
            	         ,{value: 0, inc: 1, name : gettextCatalog.getString("Not recommended cheeses"), unit: gettextCatalog.getString("Slice(s)"), muted:"(ex : Babybel, Bleu, Brie, Camembert, Cantal, Comté, feta, fromages vieux, parmesan, Reblochon, Roquefort)"}
            	         ,{value: 0, inc: 1, name : gettextCatalog.getString("Yoghurt/Fromage blanc"), unit: gettextCatalog.getString("Pot(s)"), muted:""}],
            	mg: [140,300,45],
            	expand: false
            },{
            	category : gettextCatalog.getString("Seafood"),
            	values : [{value: 0, inc: 50, name : gettextCatalog.getString("Canned fish"), unit: gettextCatalog.getString("Gram(s)"), muted:""}
            	         ,{value: 0, inc: 50, name : gettextCatalog.getString("Shellfish & crustaceans"), unit: gettextCatalog.getString("Gram(s)"), muted:""}
            	         ,{value: 0, inc: 50, name : gettextCatalog.getString("Smoked fish"), unit: gettextCatalog.getString("Gram(s)"), muted:""}],
            	mg: [4.2, 8, 12.8],
            	expand: false
            },{
            	category : gettextCatalog.getString("Dish"),
            	values : [{value: false, name : gettextCatalog.getString("Homemade dish"), unit: "", muted:""}
            	         ,{value: 0, inc: 50, name : gettextCatalog.getString("Frozen dish"), unit: gettextCatalog.getString("Gram(s)"), muted:""}
            	         ,{value: 0, inc: 50, name : gettextCatalog.getString("Dish with canned food"), unit: gettextCatalog.getString("Gram(s)"), muted:""}
            	         ,{value: 0, inc: 1, name : gettextCatalog.getString("Bread"), unit: gettextCatalog.getString("Slice(s)"), muted:""}
            	         ,{value: 0, inc: 1, name : gettextCatalog.getString("Viennoiseries"), unit: "", muted:""}
            	         ,{value: 0, inc: 1, name : gettextCatalog.getString("Cereals"), unit: gettextCatalog.getString("Bowl(s)"), muted:""}],
            	mg: [0,3.06, 3.06, 310, 270, 410],
            	expand: false
            },{
            	category : gettextCatalog.getString("Condiment"),
            	values : [{value: 0, inc: 1, name : gettextCatalog.getString("Salt"), unit: gettextCatalog.getString("Spoon(s)"), muted:""}
            	         ,{value: 0, inc: 1, name : gettextCatalog.getString("Sauces"), unit: gettextCatalog.getString("Spoon(s)"), muted:"(ex : moutarde, mayonnaise,...)"}
            	         ,{value: 0, inc: 1, name : gettextCatalog.getString("Herb spices"), unit: gettextCatalog.getString("Spoon(s)"), muted:""}
            	         ,{value: 0, inc: 1, name : gettextCatalog.getString("Spices powder"), unit: gettextCatalog.getString("Spoon(s)"), muted:""}
            	         ,{value: 0, inc: 1, name : gettextCatalog.getString("Stock"), unit: gettextCatalog.getString("Spoon(s)"), muted:""}],
            	mg: [2000, 130, 1, 1500, 2150],
            	expand: false
            }];
            
            $scope.isNumber = angular.isNumber;
        	
        	
            $scope.decIfGreaterThanZero = function(val, dec){
            	if((val - dec) >= 0){
            		return val - dec;
            	}else{
            		return val;
            	}
            }

            //Answer
            $scope.answer = function(){
                
            	var totalSalt = 0;
            	var len = $scope.salt.length;
                
                for(var i = 0; i < len; ++i) {
                	var len2 = $scope.salt[i].values.length;
                	for(var k = 0; k < len2; ++k) {
                		totalSalt += ($scope.salt[i].mg[k] * $scope.salt[i].values[k].value);
                	}
                }
                
                var totalLiquid = 0;
                var len2 = $scope.liquids.length;
                
                for(var i = 0; i < len2;++i) {
                    totalLiquid += ($scope.liquids[i].ml[0] * $scope.liquids[i].values[0].value);
                    totalLiquid += ($scope.liquids[i].ml[1] * $scope.liquids[i].values[1].value);
                    totalLiquid += ($scope.liquids[i].ml[2] * $scope.liquids[i].values[2].value);
                }
                
                
                $scope.$parent.addEntry({type: 'salt', value: totalSalt}, function(){
                	$scope.$parent.addEntry({type: 'liquid', value: totalLiquid}, function(){
                		$scope.$parent.addEntry({type: 'mealhf', value: 1}, function(){
                    		$scope.$parent.buildDashboard();
                		});
                	});
                });
            }
        }
    }
});