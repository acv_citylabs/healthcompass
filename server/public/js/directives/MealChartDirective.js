angular.module('MealChartDirective', []).directive('mealchart', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, $log,ModalService) {
    return {
        restrict: 'E',
        templateUrl: 'templates/dashboard/cards/directives/chart.html',
        scope: {
            title: '@',
            name: '@',
            objective: '@'
        },
        link: function($scope, $element, $attrs) {
            //$ocLazyLoad.load('additional_components/highcharts-ng/dist/highcharts-ng.min.js').then(function () {
                $scope.isHiddenLocal = function(name){
                    return $rootScope.isHiddenGlobal.meal;
                }

                $scope.fractionSize = 0;
                $scope.view = '';
                $scope.meal = true;

                // Global configuration
                Highcharts.setOptions({global: {useUTC : false}});

                // Copy the basic configuration
                $scope.chart = JSON.parse(JSON.stringify($scope.$parent.aChart));

                // var piechart = {
                //     chart: {
                //         plotBackgroundColor: null,
                //         plotBorderWidth: 0,
                //         plotShadow: false
                //     },
                //     title: {
                //         text: 'Browser<br>shares<br>2017',
                //         align: 'center',
                //         verticalAlign: 'middle',
                //         y: 40
                //     },
                //     tooltip: {
                //         pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                //     },
                //     plotOptions: {
                //         pie: {
                //             dataLabels: {
                //                 enabled: true,
                //                 distance: -50,
                //                 style: {
                //                     fontWeight: 'bold',
                //                     color: 'white'
                //                 }
                //             },
                //             startAngle: -90,
                //             endAngle: 90,
                //             center: ['50%', '75%']
                //         }
                //     },
                //     series: [{
                //         type: 'pie',
                //         name: 'Browser share',
                //         innerSize: '50%',
                //         data: [
                //             ['Chrome', 58.9],
                //             ['Firefox', 13.29],
                //             ['Internet Explorer', 13],
                //             ['Edge', 3.78],
                //             ['Safari', 3.42],
                //             {
                //                 name: 'Other',
                //                 y: 7.61,
                //                 dataLabels: {
                //                     enabled: false
                //                 }
                //             }
                //         ]
                //     }]
                // };
                var piechart = {
                    chart: {
                        type: 'solidgauge'
                    },

                    title: null,

                    pane: {
                        center: ['50%', '85%'],
                        size: '140%',
                        startAngle: -90,
                        endAngle: 90,
                        background: {
                            backgroundColor: '#EEE',
                            innerRadius: '60%',
                            outerRadius: '100%',
                            shape: 'arc'
                        }
                    },

                    tooltip: {
                        enabled: false
                    },

                    // the value axis
                    yAxis: {
                        stops: [
                            [0.1, '#55BF3B'], // green
                            [0.5, '#DDDF0D'], // yellow
                            [0.9, '#DF5353'] // red
                        ],
                        lineWidth: 0,
                        minorTickInterval: null,
                        tickAmount: 2,
                        title: {
                            y: -70
                        },
                        labels: {
                            y: 16
                        }
                    },

                    plotOptions: {
                        solidgauge: {
                            dataLabels: {
                                y: 5,
                                borderWidth: 0,
                                useHTML: true
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        max: 200,
                        title: {
                            text: 'Speed'
                        }
                    },

                    credits: {
                        enabled: false
                    },

                    series: [{
                        name: 'Speed',
                        data: [80],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                                   '<span style="font-size:12px;color:silver">km/h</span></div>'
                        },
                        tooltip: {
                            valueSuffix: ' km/h'
                        }
                    }]
                };

                // $scope.piechart = piechart;
                // console.log($scope.piechart);

                // Define chart type
                $scope.chart.options.chart.type = 'bar';
                
                // Create a personalized tooltip
                $scope.chart.options.tooltip.enabled = false;
                
                //Define X axis
                $scope.chart.xAxis.categories = [];
                
                //Define Y axis
                $scope.chart.yAxis.title.text = gettextCatalog.getString('Score');
                $scope.chart.yAxis.plotLines[0].label.text = gettextCatalog.getString('obj.');
                $scope.chart.yAxis.min = 0;
                $scope.chart.yAxis.max = 20;
                
                //Size
                $scope.chart.size.height = 220;
                
                // Design options
                $scope.chart.options.plotOptions.series.stacking = 'normal';
                $scope.chart.options.legend.enabled = true;
                $scope.chart.options.legend.reversed = true;

                // Build the chart
                $scope.build = function(view) {
                    switch(view){
                        case '2m':
                            $scope.view = '2m';
                            from = new Date(new Date().setDate(new Date().getDate() - 60));
                        break;
                        case '3m':
                            $scope.view = '3m';
                            from = new Date(new Date().setDate(new Date().getDate() - 90));
                        break;
                        case 'list':
                            $scope.view = 'list';                            
                        break;
                        default:
                            $scope.view = '';
                            from = new Date(new Date().setDate(new Date().getDate() - 30));
                        break;
                    }
                    
                    if(view !== 'list'){
                        $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                            var Chart = $injector.get('Chart');
                            Chart.build({
                                type: 'meal',
                                from: from,
                                to: new Date
                            }).success(function(data) {
                                for(var i=0; i< data.categories.length; i++){
                                    data.categories[i] = gettextCatalog.getString(data.categories[i]);
                                }
                                for(var i=0; i< data.series.length; i++){
                                    data.series[i].name = gettextCatalog.getString(data.series[i].name);
                                }
                                $scope.chart.xAxis.categories = data.categories;
                                $scope.chart.series = data.series;
                            }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type:'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                            });
                        });


                        $ocLazyLoad.load('js/services/ObjectivesService.js').then(function() {
                            var Obj = $injector.get('Objective');
                            //retrieve the meal statistics
                            Obj.getObjective('meal').success(function (data) {
                                //console.log(data);
                                if(data) {
                                    $scope.caloriesObjective = data.values[1].value;
                                } else {
                                    //$scope.caloriesObjective = 'No data';
                                    $scope.caloriesObjective = 2100;
                                }
                            }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type:'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                            });
                        });

                        $ocLazyLoad.load('js/services/FoodService.js').then(function() {
                            var Food = $injector.get('Food');
                            //retrieve the meal statistics
                            Food.getMealStats({
                                from: new Date(new Date(new Date().setDate(new Date().getDate())).setHours(0,0,0,0)),
                                to: new Date
                            }).success(function (data) {
                                //console.log(data);
                                $scope.caloriesConsumed = data;
                                $scope.caloriesRemaining = $scope.caloriesObjective != 'No data' ? $scope.caloriesObjective - data : $scope.caloriesRemaining = 0;
                            }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type:'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                            });
                        });


                    } else {
                        $scope.$parent.buildList({type: 'newMeals'}, function(data){
                            //console.log(data);
                            $scope.list = data;
                        });
                    }
                }
                
                // First build
                $scope.build();
            //});
        }
    }
});