angular.module('BolusAssistant', []).directive('assistant', function ($log, gettextCatalog, $ocLazyLoad, $injector, $stateParams, $state, $rootScope, $window, $timeout, $q, ModalService) {
    return {
        restrict: 'E',
        templateUrl: 'templates/dashboard/cards/directives/bolusassistant.html',
        scope: {
            title: '@',
            name: '@'
        },
        link: function ($scope, $element, $attrs) {
            $ocLazyLoad.load(['js/services/ObjectivesService.js', 'js/services/EntryService.js']).then(function () {
                // In case of long windows and small-screens devices, allows to 'reinitialize' the scroll to the top of the page.
                // $window.scrollTo(0, 0);

                /* ============================ START DECLARATION ================================= */
                var sens_breakfast, sens_lunch, sens_dinner;
                var optGlyMin, optGlyMax, postPrandGlyMax;
                var Objective = $injector.get('Objective');
                var Entry = $injector.get('Entry');

                // Exercise correction factor. 3 Rows -> Duration. 6 Columns -> Intensity
                var corrfactors = [
                    [0.90, 0.86, 0.82, 0.78, 0.74, 0.7],
                    [0.75, 0.70, 0.65, 0.60, 0.55, 0.5],
                    [0.67, 0.60, 0.53, 0.44, 0.37, 0.3]
                ];

                var ksucs = [1.3, 1.3, 1.3];


                $scope.config = {
                    types_context: [
                        { name: gettextCatalog.getString('Before meal'), value: 'meal' },
                        { name: gettextCatalog.getString('Before sport'), value: 'sport' },
                        { name: gettextCatalog.getString('Between meals'), value: 'btwmeals' }
                    ],
                    types_meals: [
                        { name: 'Breakfast', value: 'breakfast' },
                        { name: 'Lunch', value: 'lunch' },
                        { name: 'Dinner', value: 'dinner' }
                    ],
                    values: [
                        { name: gettextCatalog.getString('Current glycaemia'), value: '' },
                        // { name: gettextCatalog.getString('Optimal glycaemia'), value: '' }
                    ],
                    context: { name: gettextCatalog.getString('Case'), value: 'meal' },
                    meal: [
                        { name: gettextCatalog.getString('Estimated carbs'), value: '' },
                        { name: gettextCatalog.getString('Moment'), value: '' }
                    ],
                    sport: [
                        { name: gettextCatalog.getString('Physical activity intensity'), value: 'intensity' },
                        { name: gettextCatalog.getString('Physical activity duration'), value: 'duration' }
                    ]
                };


                /* ============================ END DECLARATION ================================= */

                $scope.onInit = function () {
                    $scope.showSug = false;
                    $scope.showMeal = true;
                    $scope.showSport = false;
                    $scope.weight = '';
                    $scope.showRecommend = false;
                    $scope.bol_meal = 0.0;
                    $scope.bol_corr = 0.0;
                    $scope.bol_total = 0.0;
                    $scope.opt_gly = 0.0;
                    $scope.resuc = 0.0;
                    $scope.iob = 0.0;
                    $scope.correction = 0.0;
                    $scope.sport = {
                        planned: false,
                        duration: 1,
                        intensity: 1,
                    };
                }

                $scope.onInit();

                $scope.changeContext = function (context) {
                    $scope.config.context.value = context;
                    $scope.showFct();
                }

                $scope.getSportIntensity = function () {
                    switch ($scope.sport.intensity) {
                        case '1':
                            return "Light";
                            break;
                        case '2':
                            return "Moderate";
                            break;
                        case '3':
                            return "Vigorous";
                            break;
                    };
                }

                $scope.showFct = function () {
                    //var val = $scope.config.context.value.value;
                    var val = $scope.config.context.value;

                    if (val == 'meal') {
                        $scope.showSug = false;
                        $scope.showMeal = true;
                        $scope.sport.planned = false;
                        $scope.showRecommend = false;
                    }

                    if (val == 'sport') {
                        $scope.showSug = true;
                        $scope.showMeal = false;
                        $scope.sport.planned = true;
                        $scope.showRecommend = false;
                        $scope.config.meal[0].value = 0;

                    }

                    if (val == 'btwmeals') {
                        $scope.showSug = false;
                        $scope.showMeal = false;
                        $scope.sport.planned = false;
                        $scope.showRecommend = false;
                        $scope.config.meal[0].value = 0;
                    }
                    //$log.debug(val,$scope.showMeal,$scope.showSport);
                }


                $scope.getAlert = function () {
                    var context = $scope.config.context.value.value;
                    var curr_gly = $scope.config.values[0].value;
                    var ok = true;

                    if (curr_gly > 250) {
                        $rootScope.rootAlerts.push({
                            type: 'warning',
                            msg: gettextCatalog.getString("Warning : Your blood glucose level is anormally high. If not already done, you should take your insulin bolus as soon as possible."),
                            priority: 2
                        });
                    }

                    switch (context) {
                        case 'meal':
                            break;

                        case 'sport':
                            if (curr_gly <= 55) {
                                $rootScope.rootAlerts.push({
                                    type: 'warning',
                                    msg: gettextCatalog.getString("Warning : Your blood glucose level is very low, you should avoid intense physical activity !"),
                                    priority: 2
                                });
                            }

                            ok = false;
                            break;

                        case 'btwmeals':
                            if (curr_gly <= 45) {
                                $rootScope.rootAlerts.push({
                                    type: 'warning',
                                    msg: gettextCatalog.getString("Warning : Your blood glucose level is very low, you should eat a snack soon."),
                                    priority: 2
                                });
                            }

                            ok = false;
                            break;
                    }
                    return ok;
                }



                var getSportSugar = function (callback) {
                    var sugar;
                    var int = $scope.sport.intensity;
                    getWeight(function (err, weight) {
                        if (err || weight == null) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('Failed to retrieve weight for getSugarBolus'),
                                priority: 2
                            });
                            callback("KO", null);
                        } else {
                            $scope.weight = weight;
                            sugar = ksucs[int - 1] * (1 - getCorrFactor()) * weight;
                            //$log.debug("Facteur1", ksucs[int - 1], "Corr factor exo", getCorrFactor());
                            callback(null, sugar);
                        }
                    });
                }


                // Exercise correction factor
                var getCorrFactor = function () {
                    var factor, int, dur;
                    if ($scope.sport.planned) {
                        int = $scope.sport.intensity;
                        dur = $scope.sport.duration;
                        //$log.debug("Corr fac :", int, dur);
                        factor = corrfactors[int - 1][dur - 1];
                    } else {
                        factor = 1;
                    }
                    return factor;
                }

                var getCorrSugar = function (igRatio, bol_corr, iob) {
                    return (bol_corr - iob) * 10 / igRatio;
                }


                // === Retrieve insulin personal settings ==== 
                Objective.getObjective('insulin').success(function (data) {
                    if (data) {
                        for (i = 0; i < data.values.length; i++) {
                            // $log.debug(data.values[i].subType,data.values[i].type,data.values[i].value);
                            switch (data.values[i].type) {
                                case 'sensibility':
                                    switch (data.values[i].subType) {
                                        case 'breakfast':
                                            sens_breakfast = data.values[i].value;
                                            break;
                                        case 'lunch':
                                            sens_lunch = data.values[i].value;
                                            break;
                                        case 'dinner':
                                            sens_dinner = data.values[i].value;
                                            break;
                                        default:
                                            break;
                                    };
                                case 'correction':
                                    switch (data.values[i].subType) {
                                        case 'factor':
                                            Entry.setHyperCorrParam('factor', data.values[i].value);
                                            break;
                                        case 'tresh':
                                            Entry.setHyperCorrParam('tresh', data.values[i].value);
                                            break;
                                        default:
                                            break;
                                    };
                                    // $scope.correction = data.values[i].value;
                                    break;
                                case 'bgtarget':
                                    switch (data.values[i].subType) {
                                        case 'min':
                                            Entry.setGlyTarget('min', data.values[i].value);
                                            break;
                                        case 'max':
                                            Entry.setGlyTarget('max', data.values[i].value);
                                            break;
                                        case 'postprand':
                                            Entry.setGlyTarget('postprand', data.values[i].value);
                                            break;
                                        default:
                                            break;
                                    };
                                case 'schema':
                                    break;
                                default:
                                    break;
                            }
                        }

                    } else {
                        ModalService.showModal({
                            templateUrl: "templates/modals/warning.html",
                            controller: function ($scope, close) {
                                $scope.text = "Please fill in your insulin settings before calculating bolus dose.";
                                $scope.close = function (result) {
                                    close(result, 500); // close, but give 500ms for bootstrap to animate
                                };
                            }
                        }).then(function (modal) {
                            modal.element.modal();
                            modal.close.then(function (result) {
                                $state.go("home.dashboard.objective", { card: 'insulin' });
                            });
                        });
                    }

                }).error(function (status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });


                var getOptGly = function (callback) {
                    $ocLazyLoad.load('js/services/EntryService.js').then(function () {
                        var Entry = $injector.get('Entry');
                        var _MS_TO_MIN = 60 * 1000;
                        var _MS_TO_HRS = 36e5;

                        // Get glycemic targets from insulin settings 
                        var bgTargets = Entry.getGlyTargets();
                        var min = bgTargets[0],
                            max = bgTargets[1],
                            prand = bgTargets[2];
                        var _OPT_aft = (min + prand) / 2; // mg/dL
                        var _OPT_bef = (min + max) / 2;
                        // $log.debug("getOptGly :", bgTargets, _OPT_bef, _OPT_aft);


                        // Depending on last meal entry, choose the appropriate BG target 
                        var b = new Date();
                        var a, opt_gly;
                        $scope.config.type = 'meal';
                        $scope.config.subType = undefined;
                        Entry.last($scope.config).success(function (data) {
                            if (data) {
                                // $log.debug$log.debug("Last meal :  \n", data);
                                a = new Date(data.datetimeAcquisition);
                                delta_t = (Math.abs(a - b) / _MS_TO_MIN).toFixed(2);
                                opt_gly = (delta_t < 120) ? _OPT_aft : _OPT_bef;
                                callback(null, opt_gly);
                            } else {
                                // $log.debug("No meal was found during getOptGly process.");
                                callback(null, _OPT_bef);
                            };
                        }).error(function (status, data) {
                            callback(status, null);
                            // $log.debug('Failed to retrieve meal data for opt gly');
                            /* $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('Failed to retrieve the last insulin entry'),
                                priority: 2
                            }); */
                        });
                    });
                };


                var getWeight = function (callback) {
                    $ocLazyLoad.load('js/services/EntryService.js').then(function () {
                        var Entry = $injector.get('Entry');
                        $scope.config.type = 'weight';
                        $scope.config.subType = undefined;
                        Entry.last($scope.config).success(function (data) {
                            if (data) {
                                callback(null, data.value);
                            } else {
                                // $log.debug("No weight entry was found.");
                                ModalService.showModal({
                                    templateUrl: "templates/modals/warning.html",
                                    controller: function ($scope, close) {
                                        $scope.text = "In order to calculate the exercise correction factor, we need to know your weight.";
                                        $scope.close = function (result) {
                                            close(result, 500); // close, but give 500ms for bootstrap to animate
                                        };
                                    }
                                }).then(function (modal) {
                                    modal.element.modal();
                                    modal.close.then(function (result) {
                                        $state.go("home.dashboard.add", { card: 'weight' });
                                    });
                                });
                            };
                        }).error(function (status, data) {
                            callback(status, null);
                            // $log.debug('Failed to retrieve weight');
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('Failed to retrieve weight entry'),
                                priority: 2
                            });
                        });
                    });
                };

                // Opens modal for more details on physical activity intensity levels
                $scope.showInfo = function () {
                    ModalService.showModal({
                        templateUrl: "templates/modals/sport.html",
                        controller: function ($scope, close) {
                            $scope.text = "test";
                            $scope.close = function (result) {
                                close(result, 500); // close, but give 500ms for bootstrap to animate
                            };
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            console.log("modal closed");
                        });
                    });
                }

                // Opens modal for more details on typical 'resucrage' food
                $scope.showSugarTable = function () {
                    ModalService.showModal({
                        templateUrl: "templates/modals/sugarTable.html",
                        controller: function ($scope, close) {
                            $scope.text = "test";
                            $scope.close = function (result) {
                                close(result, 500); // close, but give 500ms for bootstrap to animate
                            };
                        }
                    }).then(function (modal) {
                        modal.element.modal();
                        modal.close.then(function (result) {
                            $state.go("home.dashboard.estimateinsu");
                        });
                    });
                }


                $(function () {
                    // Enables popover
                    $("[data-toggle=popover]").popover();
                });

                var getIOB = function (callback) {
                    var iob = 0.0;
                    $ocLazyLoad.load('js/services/EntryService.js').then(function () {
                        var Entry = $injector.get('Entry');
                        var _RAPID_INSULIN_DURATION = 600; //Minutes
                        var _MS_TO_MIN = 60 * 1000;
                        var _MS_TO_HRS = 36e5;
                        var a, b, value, type, delta_t, iob;

                        // DB Query
                        $scope.config.type = 'insulin';
                        $scope.config.subType = 'rapid';
                        Entry.last($scope.config).success(function (data) {
                            if (data) {
                                // Insulin units
                                value = data.value;

                                // Delta T
                                b = new Date();
                                a = new Date(data.datetimeAcquisition);
                                delta_t = ((b - a) / _MS_TO_MIN).toFixed(2);

                                if (delta_t < _RAPID_INSULIN_DURATION && delta_t > 0) {
                                    iob = (value * (1 - delta_t / _RAPID_INSULIN_DURATION)).toFixed(2);
                                } else { iob = 0; }

                                /*$log.debug("From the last rapid-insulin injection taken ", delta_t,
                                    " minutes ago, it remains :", iob, " active units."); */
                                callback(null, iob);
                            } else {
                                //$log.debug("No insulin was found during getIOB process.");
                                callback(null, 0);
                            }
                        }).error(function (status, data) {
                            callback(status, null);
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('Failed to retrieve the last insulin entry'),
                                priority: 2
                            });
                        });
                    });
                };


                var estimateInsu = function (opt_gly, iob, type) {
                    var hyperCorrParams = Entry.getHyperCorrParams();
                    var factor = hyperCorrParams[0],
                        tresh = hyperCorrParams[1];
                    // $log.debug("PARAAAAMS :", hyperCorrParams);

                    getWeight(function (err, weight) {
                        if (err || !weight) {
                            callback("KO", null);
                        } else {
                            $scope.weight = weight;
                        }
                    });

                    $scope.correction = factor;
                    $scope.opt_gly = opt_gly;
                    $scope.iob = iob;
                    $scope.igratio = getIGRatio();

                    var curr_gly = $scope.config.values[0].value;
                    var carbs = (type == 'btwmeals' || type == 'sport') ? 0 : $scope.config.meal[0].value;
                    var delta_gly = curr_gly - opt_gly;

                    if (curr_gly > tresh) {
                        $scope.bol_corr = delta_gly / factor;
                    } else {
                        $scope.bol_corr = 0;
                    }

                    $scope.bol_meal = (carbs * getIGRatio()) / 10;
                    $scope.bol_total = ($scope.bol_meal + $scope.bol_corr - iob) * $scope.corrfactor;
                    $scope.bol_total = Math.round($scope.bol_total * 2) / 2;
                    //($scope.bol_total).toFixed(1);
                    $scope.aboveTresh = ($scope.bol_total < 1) ? false : true;
                }


                // Calculates params based on insulin settings
                var getParams = function (callback) {
                    getOptGly(function (err, opt_gly) {
                        if (err || opt_gly == null) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('Failed to retrieve the last meal entry for opt_gly'),
                                priority: 2
                            });
                            callback("KO", null, null);
                        } else {
                            // $log.debug("Opt_gly :", opt_gly);
                            getIOB(function (err, iob) {
                                if (err || iob == null) {
                                    $rootScope.rootAlerts.push({
                                        type: 'danger',
                                        msg: gettextCatalog.getString('Failed to retrieve the last insulin entry'),
                                        priority: 2
                                    });
                                    callback("KO", null, null);
                                } else {
                                    // $log.debug("IOB :", iob);
                                    callback(null, opt_gly, iob);
                                }
                            });

                        }
                    });
                }


                $scope.getRecommend = function () {
                    // Context = between meals || before meal (+sport) || before sport
                    var context = $scope.config.context.value;
                    getParams(function (err, opt_gly, iob) {
                        if (err || opt_gly == null || iob == null) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('Error in EstimateCtrl-getParams. Sorry for the inconvenience.'),
                                priority: 2
                            });
                        } else {
                            $scope.corrfactor = getCorrFactor();
                            estimateInsu(opt_gly, iob);
                            switch (context) {
                                // RESUCRAGE in case of planned physical activity
                                case 'sport':
                                    $log.debug("Sugar : ", $scope.sugar);
                                    $log.debug("Tresh", $scope.aboveTresh);
                                    getSportSugar(function (err, sugS) {
                                        if (err || sugS == null) {
                                            $rootScope.rootAlerts.push({
                                                type: 'danger',
                                                msg: gettextCatalog.getString('Error in EstimateCtrl-getParams-getSportSugar. Sorry for the inconvenience.'),
                                                priority: 2
                                            });
                                            // $log.debug("getRecommend() failed", sugS);
                                        } else {
                                            var sugC = getCorrSugar($scope.igratio, $scope.bol_corr, iob);
                                            $scope.sugar = ((sugS - sugC) < 0) ? 0 : (sugS - sugC);
                                            $scope.sugar = Math.round($scope.sugar);
                                            $scope.aboveTresh = ($scope.sugar < 5) ? false : true;
                                            $log.debug("Sugar : ", $scope.sugar);
                                            $log.debug("Tresh", $scope.aboveTresh);
                                        }

                                    });
                                    break;
                                // RESUCRAGE in case of hypoglycaemia, between meals
                                case 'btwmeals':
                                    if (($scope.bol_corr - iob) < 0) {
                                        $scope.sugar = -Math.round(getCorrSugar($scope.igratio, $scope.bol_corr, iob));
                                        if (iob > 2) {
                                            $scope.alert.iob = true;
                                        }
                                        $scope.aboveTresh = ($scope.sugar < 5) ? false : true;
                                        $scope.showSug = true;
                                    } else {
                                        $scope.showSug = false;
                                        $scope.sugar = 0;
                                    }
                                    break;
                            }
                        }
                    });
                    $scope.showRecommend = true;

                }


                $scope.filterMeal = function (item) {
                    if (item.typev == 'meal') {
                        return item;
                    }
                }

                // Check if entries needed for recommendation are filled
                $scope.verify = function (type) {
                    var context = $scope.config.context.value;
                    var curr_gly = $scope.config.values[0].value;
                    var carbs = $scope.config.meal[0].value;
                    var ok = false;

                    switch (context) {
                        case 'meal':
                            ok = (carbs && curr_gly) ? true : false;

                            break;
                        case 'sport':
                            ok = (curr_gly) ? true : false;
                            break;
                        case 'btwmeals':
                            ok = (curr_gly) ? true : false;
                            break;
                    }
                    return ok;
                }


                $scope.addBolus = function (boltotal) {
                    Entry.setRecommendedBolus(boltotal);
                    $state.go("home.dashboard.add", { "card": 'insulin' });
                }


                var getIGRatio = function () {
                    var currenthour = (new Date().getHours() + ((new Date().getMinutes()) / 60)).toFixed(1);
                    var igRatio;
                    switch (true) {
                        case (currenthour >= 5.5 && currenthour < 9.5):
                            igRatio = sens_breakfast;
                            break;
                        case (currenthour >= 9.5 && currenthour < 11.5):
                            igRatio = sens_breakfast;
                            break;
                        case (currenthour >= 11.5 && currenthour < 15.5):
                            igRatio = sens_lunch;
                            break;
                        case (currenthour >= 15.5 && currenthour < 17.5):
                            igRatio = sens_lunch;
                            break;
                        case (currenthour >= 17.5 && currenthour < 21.5):
                            igRatio = sens_dinner;
                            break;
                        case (currenthour >= 21.5 && currenthour < 23.5):
                            igRatio = sens_dinner;
                            break;
                        default:
                            igRatio = 1;
                            break;
                    }
                    return igRatio;
                }

            });
        }
    }
});