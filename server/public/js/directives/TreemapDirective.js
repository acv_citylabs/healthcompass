angular.module('TreemapDirective', []).directive('treemappatients', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, $log) {
	return {
		restrict: 'A',
		templateUrl: 'templates/dashboard/cards/directives/treemap.html',
		scope: {
			title: '@'
		},
		link: function($scope, $element, $attrs) {
			$scope.config = {
				name: 'treemap'
			};

			$scope.activeMenu = 'all';

			//$scope.titleTreemap = 'Patient treemap';

			var hba1cTreemap = [{
				name: 'Joey Rambo',
				value: 6.8,
				colorValue: 1
			}, {
				name: 'Rocky Balboa',
				value: 8.3,
				colorValue: 2
			}, {
				name: 'Johan Kimble',
				value: 8.6,
				colorValue: 3
			}, {
				name: 'Charles Richards',
				value: 9.3,
				colorValue: 4
			}, {
				name: 'Ben Richards',
				value: 7.4,
				colorValue: 5
			}, {
				name: 'John Dow',
				value: 10.1,
				colorValue: 6
			}];

			var hypoTreemap = [{
				name: 'Joey Rambo',
				value: 12,
				colorValue: 1
			}, {
				name: 'Rocky Balboa',
				value: 18,
				colorValue: 2
			}, {
				name: 'Johan Kimble',
				value: 31,
				colorValue: 3
			}, {
				name: 'Charles Richards',
				value: 25,
				colorValue: 4
			}, {
				name: 'Ben Richards',
				value: 15,
				colorValue: 5
			}, {
				name: 'John Dow',
				value: 18,
				colorValue: 6
			}];

			var hyperTreemap = [{
				name: 'Joey Rambo',
				value: 15,
				colorValue: 1
			}, {
				name: 'Rocky Balboa',
				value: 23,
				colorValue: 2
			}, {
				name: 'Johan Kimble',
				value: 9,
				colorValue: 3
			}, {
				name: 'Charles Richards',
				value: 18,
				colorValue: 4
			}, {
				name: 'Ben Richards',
				value: 23,
				colorValue: 5
			}, {
				name: 'John Dow',
				value: 36,
				colorValue: 6
			}];

			var glycvarTreemap = [{
				name: 'Joey Rambo',
				value: 28,
				colorValue: 1
			}, {
				name: 'Rocky Balboa',
				value: 30,
				colorValue: 2
			}, {
				name: 'Johan Kimble',
				value: 17,
				colorValue: 3
			}, {
				name: 'Charles Richards',
				value: 25,
				colorValue: 4
			}, {
				name: 'Ben Richards',
				value: 12,
				colorValue: 5
			}, {
				name: 'John Dow',
				value: 40,
				colorValue: 6
			}];

			var cardioTreemap = [{
				name: 'Joey Rambo',
				value: 17,
				colorValue: 1
			}, {
				name: 'Rocky Balboa',
				value: 16,
				colorValue: 2
			}, {
				name: 'Johan Kimble',
				value: 18,
				colorValue: 3
			}, {
				name: 'Charles Richards',
				value: 36,
				colorValue: 4
			}, {
				name: 'Ben Richards',
				value: 22,
				colorValue: 5
			}, {
				name: 'John Dow',
				value: 49,
				colorValue: 6
			}];

			$scope.changeDataset = function(param) {
				sortTreemap(param);
			};

			var treemapData = {
				'Extreme Risk': {
					'John Doe': {
						'hba1c': '29.2',
						'hypo': '23.6',
						'hyper': '12.1',
						'glycvar': '22.6',
						'cardio': '29.6'
					},
					'Ben Richards': {
						'hba1c': '9.4',
						'hypo': '17',
						'hyper': '23',
						'glycvar': '24',
						'cardio': '25'
					}
				},
				'High Risk': {
					'Charles Richards': {
						'hba1c': '9.3',
						'hypo': '20',
						'hyper': '18',
						'glycvar': '24',
						'cardio': '28'
					}
				},
				'Moderate Risk': {
					'Johan Kimble': {
						'hba1c': '8.6',
						'hypo': '31',
						'hyper': '9',
						'glycvar': '18.6',
						'cardio': '18'
					},
					'Rocky Balboa': {
						'hba1c': '5.6',
						'hypo': '4.5',
						'hyper': '9.6',
						'glycvar': '20.2',
						'cardio': '23.6'
					}
				},
				'Low Risk': {
					'Joey Rambo': {
						'hba1c': '6.8',
						'hypo': '12',
						'hyper': '10',
						'glycvar': '20',
						'cardio': '17'
					}
				}
			};

			var treemapData2 = {
					'Extreme': {
						'John Doe': {
							'riskScore': '45.2',
							 'values': {
							 	'hba1c': '29.2',
								'hypo': '23.6',
								'hyper': '12.1',
								'glycvar': '15.6',
								'cardio': '28.6'
							 }							
						},
						'Rocky Balboa': {
							'riskScore': '49.7',
							 'values': {
							 	'hba1c': '5.6',
								'hypo': '4.5',
								'hyper': '9.6',
								'glycvar': '19.2',
								'cardio': '23.6'
							 }
						}
					},
					'High': {
						'Someone New': {
							'riskScore': '39.6',
							'values': {
								'hba1c': '7.2',
								'hypo': '4.1',
								'hyper': '8.6',
								'glycvar': '19.6',
								'cardio': '20.6'
							}
						},
						'Fourth Person': {
							'riskScore': '34.3',
							'values': {
								'hba1c': '5.6',
								'hypo': '4.3',
								'hyper': '9.6',
								'glycvar': '14.8',
								'cardio': '21.2'
							}							
						}
					},
					'Moderate': {
						'Someone New': {
							'riskScore': '27.9',
							'values': {
								'hba1c': '7.2',
								'hypo': '6.3',
								'hyper': '4.6',
								'glycvar': '13.5',
								'cardio': '19.6'
							}							
						},
						'Fourth Person': {
							'riskScore': '22.6',
							'values': {
								'hba1c': '5.6',
								'hypo': '6.3',
								'hyper': '9.6',
								'glycvar': '14.2',
								'cardio': '17.6'
							}							
						}
					},
					'Low': {
						'Someone New': {
							'riskScore': '8.7',
							'values': {
								'hba1c': '7.2',
								'hypo': '5.3',
								'hyper': '7.5',
								'glycvar': '10.6',
								'cardio': '10.6'
							}							
						},
						'Fourth Person': {
							'riskScore': '4.3',
							'values': {
								'hba1c': '2.4',
								'hypo': '4.8',
								'hyper': '7.5',
								'glycvar': '11.2',
								'cardio': '11.6'
							}							
						}
					}
				},
				points = [],
				riskP,
				riskVal,
				riskI = 0,
				patientP,
				patientI,
				paramP,
				paramI,
				risk,
				patient,
				param,
				paramName = {
					'riskScore': 'risk Score',
					'hba1c': 'HbA1c',
					'hypo': 'Hyperglycaemia',
					'hyper': 'Hyperglycaemia',
					'glycvar': 'Glycaemic Variability',
					'cardio': 'Cardiovascular Score'
				}, patientsNum;
			Highcharts.setOptions({colors: ['#FF5C6C', '#D46C76','#AA7D81','#7F8D8C','#559E96', '#2AAEA1','#00BFAC']});

			function sortTreemap(parameter) {
				points = [], riskVal = 0, patientI = 0, paramI = 0, riskI = 0;
				for (risk in treemapData) { //for each risk level (extreme, high, moderate, low)
					if (treemapData.hasOwnProperty(risk)) { 
						patientsNum = Object.keys(treemapData[risk]).length;
						riskVal = 0; //total average score accumulator
						riskP = { //highest area object containing risk levels
							id: 'id_' + riskI,
							name: risk + ' (' + patientsNum + ')', //e.g. Extreme, High, Moderate, Low
							color: Highcharts.getOptions().colors[riskI] //pick a color from the default color array
						};
						patientI = 0; //local average score accumulator
						for (patient in treemapData[risk]) { //for each patient
							if (treemapData[risk].hasOwnProperty(patient)) {
								patientP = {
									id: riskP.id + '_' + patientI,
									name: patient,
									parent: riskP.id
								};
								points.push(patientP);
								paramI = 0; //show parameters but add up the risk scores
								for (param in treemapData[risk][patient]) { //for each paremeter
									if (treemapData[risk][patient].hasOwnProperty(param)) {
										paramP = {
											id: patientP.id + '_' + paramI,
											name: paramName[param],
											parent: patientP.id,
											value: Math.round(+treemapData[risk][patient][param])
										};
											if(parameter === param) {
												riskVal += paramP.value; //add up all patients' values HERE
											} else if(parameter === 'ALL') {
												riskVal += paramP.value; //add up all patients' values HERE
											}
											
										points.push(paramP);
										paramI = paramI + 1;
									}
								}
								patientI = patientI + 1; //count patients
							}
						}
						riskP.value = Math.round(riskVal / patientI);
						points.push(riskP);
						riskI = riskI + 2; //move the color position
					}
				}
				//console.log(points);
				$scope.treemap = {
					series: [{
						type: 'treemap',
						layoutAlgorithm: 'squarified',
						allowDrillToNode: true,
						animationLimit: 1000,
						dataLabels: {
							enabled: false
						},
						levelIsConstant: false,
						levels: [{
							level: 1,
							layoutAlgorithm: 'sliceAndDice', //try different algorithms
							dataLabels: {
								enabled: true,
								style: {
		                    		fontSize: '18px'
		                		}
							},
							borderWidth: 3
						}],
						data: points
					}],
					title: {
						text: ''
					}
				};
				//console.log(points);
			}
			sortTreemap('ALL');
		}
	}
});