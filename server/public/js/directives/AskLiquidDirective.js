angular.module('AskLiquidDirective', []).directive('askliquid', function(gettextCatalog, $state, $window, $ocLazyLoad, $injector, $rootScope, ModalService) {
    return {
        restrict: 'A',
        templateUrl: 'templates/dashboard/asks/directives/ask_liquid.html',
        link: function($scope, $element, $attrs) {
            
        	$scope.title = gettextCatalog.getString("How much liquid did you drink today ?");
        	$scope.liquid =  {
            		wine : {
            			value: 0,
            			coeff: 1
            		},
            		beer : {
            			value: 0,
            			coeff: 1            			
            		},
            		soda : {
            			value: 0,
            			coeff: 1
            		},
            		bigCoffee : {
            			value: 0,
            			coeff: 250
            		},
            		avgCoffee : {
            			value: 0,
            			coeff: 150
            		},
            		smallCoffee : {
            			value: 0,
            			coeff: 80
            		},
            		water : {
            			value: 0,
            			coeff: 200
            		}
            };
            
            //Answer
            $scope.answer = function(){
                
                var total = 0;
                
                for (var prop in $scope.liquid) {
                    if ($scope.liquid.hasOwnProperty(prop)) {
                    	total += ($scope.liquid[prop].value * $scope.liquid[prop].coeff);
                    }
                }
                
                $scope.$parent.addEntry({type: 'liquid', value: total}, function(){
                    
                    $scope.$parent.buildDashboard();
                });
            }
        }
    }
});