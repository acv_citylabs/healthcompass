angular.module('DemographicsDirective', []).directive('demographics', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, $log) {
	return {
		restrict: 'A',
		templateUrl: 'templates/dashboard/cards/directives/demographics.html',
		scope: {
            title: '@',
            name: '@'
        },
		link: function($scope, $element, $attrs) {
			$scope.config = {
				name: 'demographics'
			};
			
			$scope.titleDemographics = 'Patient Demographics';

			$scope.numWomen = 520;
			$scope.numMen = 741;
			$scope.avgAge = 52.5;

			$scope.numDm1 = 250;
			$scope.numDm2 = 1011;
		}
	}
});