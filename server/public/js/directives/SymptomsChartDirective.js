angular.module('SymptomsChartDirective', []).directive('symptomschart', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, $window, $filter, $state, ModalService) {
    return {
        restrict: 'E',
        templateUrl: 'templates/dashboard/cards/directives/chart.html',
        scope: {
            title: '@',
            name: '@',
            objective: '@'
        },
        link: function($scope, $element, $attrs) {
            $scope.isHiddenLocal = function(name) {
                return $rootScope.isHiddenGlobal.symptoms;
            }

            $scope.fractionSize = 1;
            $scope.view = '';
            $scope.unit = '';
            $scope.symptoms = true;

            // Global configuration
            Highcharts.setOptions({ global: { useUTC: false } });

            // Copy the basic configuration
            $scope.chart = JSON.parse(JSON.stringify($scope.$parent.aChart));

            // Define chart type
            $scope.chart.options.chart.type = 'spline';

            $scope.chart.options.legend.enabled = true;

            // Create a personalized tooltip
            $scope.chart.options.tooltip.formatter = function() {
                return $filter('ddMMyyyy')(new Date(this.x).toISOString()) + '<br><span style="color:#6200ea;">●</span>  <b>' + this.y + '</b> ' + $scope.unit;
            }

            // Define X axis
            $scope.chart.xAxis.type = 'datetime';
            $scope.chart.xAxis.labels = {
                overflow: 'justify'
            };
            $scope.chart.xAxis.dateTimeLabelFormats = {
                month: '%e. %b',
                year: '%b'
            };
            $scope.chart.xAxis.min = new Date(new Date().setDate(new Date().getDate() - 7)).getTime();
            $scope.chart.xAxis.max = (new Date).getTime();

            $scope.chart.yAxis.labels = {
                formatter: function() {
                    if (this.value == 0) {
                        return '<img src="img/good.png" style="width:24px; height:24px; background:no-repeat center center;background-size:cover;padding:2px;"/>';
                    } else if (this.value == 1) {
                        return '<img src="img/notWell.png" style="width:24px; height:24px; background:no-repeat center center;background-size:cover;padding:2px;" />';
                    } else if (this.value == 2) {
                        return '<img src="img/bad.png" style="width:24px; height:24px; background:no-repeat center center;background-size:cover;padding:2px;" />';
                    } else {
                        return '';
                    }
                },
                useHTML: true
            };
            $scope.chart.yAxis.title.text = $scope.unit;
            $scope.chart.yAxis.min = 0;
            $scope.chart.yAxis.max = 2;


            /*
            $scope.chart.yAxis.plotBands = [{
                color: '#f1f8e9', 
                from: 0, 
                to: 0.5
              },{
                color: '#fffde7', 
                from: 0.5,
                to: 1.5
              },{
                  color: '#fbe9e7', 
                  from: 1.5,
                  to: 2
                }];
            */
            //Size
            if ($window.innerWidth < 535) {
                $scope.chart.size.height = 250;
            }

            // Build the chart
            $scope.build = function(view) {
                switch (view) {
                    case '2w':
                        $scope.view = '2w';
                        from = new Date(new Date().setDate(new Date().getDate() - 14));
                        $scope.chart.xAxis.min = new Date(new Date().setDate(new Date().getDate() - 14)).getTime();
                        break;
                    case '1m':
                        $scope.view = '1m';
                        from = new Date(new Date().setDate(new Date().getDate() - 30));
                        $scope.chart.xAxis.min = new Date(new Date().setDate(new Date().getDate() - 30)).getTime();
                        break;
                    case 'list':
                        $scope.view = 'list';
                        break;
                    default:
                        $scope.view = '';
                        from = new Date(new Date().setDate(new Date().getDate() - 7));
                        $scope.chart.xAxis.min = new Date(new Date().setDate(new Date().getDate() - 7)).getTime();
                        break;
                }

                if (view !== 'list') {
                    $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                        var Chart = $injector.get('Chart');
                        Chart.build({
                            type: 'symptoms',
                            from: from,
                            to: new Date()
                        }).success(function(data) {
                            for (var i = 0; i < data.series.length; i++) {
                                data.series[i].name = gettextCatalog.getString(data.series[i].name);
                            }
                            //$scope.chart.xAxis.categories = data.categories;
                            $scope.chart.series = data.series;
                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        });
                    });
                } else {
                    $scope.$parent.buildList({ type: 'symptoms' }, function(data) {

                        for (var i = 0; i < data.length; ++i) {
                            data[i].buttons = [];
                            for (var k = 0; k < data[i].values.length; ++k)
                                if (data[i].values[k].value == 0) {
                                    data[i].buttons.push({
                                        title: gettextCatalog.getString('Good'),
                                        img: { 'background': 'url(./img/good.png) no-repeat center center', 'background-size': 'cover', 'opacity': '1.0' },
                                    });
                                } else if (data[i].values[k].value == 1) {
                                data[i].buttons.push({
                                    title: gettextCatalog.getString('NotWell'),
                                    img: { 'background': 'url(./img/notWell.png) no-repeat center center', 'background-size': 'cover', 'opacity': '1.0' },
                                });
                            } else {
                                data[i].buttons.push({
                                    title: gettextCatalog.getString('Bad'),
                                    img: { 'background': 'url(./img/bad.png) no-repeat center center', 'background-size': 'cover', 'opacity': '1.0' },
                                });
                            }
                        }

                        $scope.list = data;
                    });
                }
            }

            // First build
            $scope.build();

        }
    }
});