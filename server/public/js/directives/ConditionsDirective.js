angular.module('ConditionsDirective', []).directive('conditions', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, $window, $filter, $state, $log) {
    return {
        restrict: 'E',
        templateUrl: 'templates/dashboard/cards/directives/conditions.html',
        scope: {
            title: '@',
            name: '@'
        },
        link: function($scope, $element, $attrs) {
        	 $ocLazyLoad.load('js/services/UserService.js').then(function () {
        		 var User = $injector.get('User'), minDate = new Date('1900-01-01'), maxDate = new Date().setDate(new Date().getDate() - 1);

        		 $scope.conditionsList = [
	                {name: gettextCatalog.getString('Diabetes'), value: 'd1', values: [{value : 'd1', name: 'Type I'},{value : 'd2', name: 'Type II'}]},
	                /*{name: gettextCatalog.getString("Alzheimer's Disease"), value: 'a'},*/
	                {name:gettextCatalog.getString('Heart Failure'), value:'hf', values: []}
	             ];
        		 
        		//Get user profile
    	        User.read().success(function (data) {
    	            // Select the right conditions
    	            if(data.condition){
    	            	var len = $scope.conditionsList.length;
    	                var conditions = []
    	                for(var i = 0; i < len; ++i){
    	                	if($scope.conditionsList[i].values.length > 0){
    	                		var len2 = $scope.conditionsList[i].values.length;
    	                		for(var k = 0; k < len2; ++k){
    	                			var index = data.condition.indexOf($scope.conditionsList[i].values[k].value);
    	                			if(index > -1){    	                		
    	    	                		$scope.conditionsList[i].checked = true;
    	    	                		$scope.conditionsList[i].value = $scope.conditionsList[i].values[k].value;
    	    	                	} 
    	                		}
    	                	}else{
    		                	if(data.condition.indexOf($scope.conditionsList[i].value) > -1){
    		                		$scope.conditionsList[i].checked = true;
    		                	} else{
    		                		$scope.conditionsList[i].checked = false;
    		                	}              		
    	                	}
    	                }
    	            }
    	            
    	            // Add it to the scope
    	            $scope.profile = data;
    	            $scope.profile.conditions = $scope.conditionsList;
    	            
    	        }).error(function (status, data) {
    	            $rootScope.rootAlerts.push({
    	                type:'danger',
    	                msg: gettextCatalog.getString('An error occurred, please try again later'),
    	                priority: 2
    	            });
    	        });

    	        // Update basic informations
    	        $scope.updateBasic = function () {
    	            
    	                // Conditions
    	                var len = $scope.profile.conditions.length;
    	                var conditions = []
    	                for(var i = 0; i < len; ++i){
    	                	if($scope.profile.conditions[i].checked){
    	                		conditions.push($scope.profile.conditions[i].value);
    	                	}
    	                }
    	                
    	                // Update
    	                User.update({
    	                    condition: conditions
    	                }).success(function (data) {
    	                	if(data.activated)
    	                        $window.localStorage.token = data.token;
    	                	
    	                    // Go to main profile page and reload it
    	                    $state.go("home.dashboard.main", {}, {reload: true});
    	                }).error(function (status, data) {
    	                    $rootScope.rootAlerts.push({
    	                        type:'danger',
    	                        msg: gettextCatalog.getString('An error occurred, please try again later'),
    	                        priority: 2
    	                    });
    	                });
    	        };
            
        	 });        	
        }
    }
});