
angular.module('AskMoriskyDirective', []).directive('askmorisky', 
                                                    function(gettextCatalog, $ocLazyLoad, $injector, $rootScope) {
    return {
        restrict: 'A',
        templateUrl: 'templates/dashboard/asks/directives/ask_morisky.html',
        link: function($scope, $element, $attrs) {
        	$ocLazyLoad.load('js/services/TokenInterceptorService.js').then(function() {
            var TI = $injector.get('TokenInterceptor');
            
            var conditions = TI.decode().condition;    
            
            //Randomly choose a condition to ask about
            var len = conditions.length;
            var i = Math.floor((Math.random() * len));
            
            $scope.morisky = {
            		conditionTxt : "votre maladie",
            		condition : conditions[i],
            		question1 : 0,
            		question2 : 0,
            		question3 : 0,
            		question4 : 0
            };
            
            switch(conditions[i]){
            	case 'd1' : case 'd2' : $scope.morisky.conditionTxt = "votre diabète"; break;
            	case 'hf' : $scope.morisky.conditionTxt = "votre coeur"; break;
            	
            }
            //Answer
            $scope.answer = function(){
            		var value = $scope.morisky.question1 + $scope.morisky.question2;
            		value += $scope.morisky.question3 + $scope.morisky.question4;
                $scope.$parent.addEntry({type: 'morisky', value: value, subType: $scope.morisky.condition}, function(){
                	$scope.$parent.buildDashboard();
                });
            }           
        	});
        }
    }
});