angular.module('AskSaltDirective', []).directive('asksalt', function(gettextCatalog, $state, $window, $ocLazyLoad, $injector, $rootScope, ModalService) {
    return {
        restrict: 'A',
        templateUrl: 'templates/dashboard/asks/directives/ask_salt.html',
        link: function($scope, $element, $attrs) {
            // Display a title in accordance with time
        	/*
            if((new Date >= new Date().setHours(7, 0, 0) && new Date <= new Date().setHours(10, 0, 0))){
                $scope.title = gettextCatalog.getString("Did you eat some of these for breakfast ?");
                $scope.meal = "breakfast";
            } else if (new Date >= new Date().setHours(12, 0, 0) && new Date <= new Date().setHours(17, 0, 0)){
            	$scope.title = gettextCatalog.getString("Did you eat one of these for lunch ?");
            	$scope.meal = "lunch";
            } else {
            	$scope.title = gettextCatalog.getString("Did you eat some of these for dinner ?");
            	$scope.meal = "dinner";
            }
            */
        	$scope.title = gettextCatalog.getString("Did you eat some of these today ?");
            $scope.liquid =  {
            		allowedCheese : {
            			value: 0,
            			coeff: 74
            		},
            		dangerousCheese : {
            			value: 0,
            			coeff: 254            			
            		},
            		cookedMeat : {
            			value: 0,
            			coeff: 500
            		},
            		smokedMeat : {
            			value: 0,
            			coeff: 1340
            		},
            		pate : {
            			value: 0,
            			coeff: 400
            		},
            		bread : {
            			value: 0,
            			coeff: 600
            		},
            		viennoiserie : {
            			value: 0,
            			coeff: 300
            		},
            		dish : {
            			value: 'homemade',
            			coeff: 1
            		},
            		seafood : {
            			value: false,
            			coeff: 400
            		},
            		sauce : {
            			value: false,
            			coeff: 130
            		},
            		stock : {
            			value: false,
            			coeff: 1000
            		},
            		spices : {
            			value: false,
            			coeff: 500
            		},
            		addedSalt : {
            			value: 0,
            			coeff: 400
            		},
            		otherSalt : {
            			value: 0,
            			coeff: 1000
            		}
            };
            
            //Answer
            $scope.answer = function(){
                
                var total = 0;
                
                for (var prop in $scope.salt) {
                    if ($scope.salt.hasOwnProperty(prop)) {
                    	if(typeof $scope.salt[prop].value !== 'string')
                    		total += ($scope.salt[prop].value * $scope.salt[prop].coeff);
                    	else if($scope.salt[prop].value == 'readyMade')
                    		total += 750;
                    	else if($scope.salt[prop].value == 'restaurant')
                    		total += 600;
                    }
                }
                
                $scope.$parent.addEntry({type: 'salt', value: total}, function(){
                    
                    $scope.$parent.buildDashboard();
                });
            }
        }
    }
});