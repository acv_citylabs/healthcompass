angular.module('AskStepsDirective', []).directive('asksteps', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope) {
    return {
        restrict: 'A',
        templateUrl: 'templates/dashboard/asks/directives/ask_steps.html',
        link: function($scope, $element, $attrs) {
            $ocLazyLoad.load('js/services/ObjectivesService.js').then(function() {
                var Objective = $injector.get('Objective');
                $scope.steps = 5000;
                $scope.objective = undefined;

                Objective.getObjective('steps').success(function(data){
                    if (data) {
                        $scope.objective = data.startValue;
                    }
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type:'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });

                //Answer
                $scope.answer = function(){
                    $scope.$parent.addEntry({type: 'steps', value: $scope.steps}, function(){
                        if($scope.objective > 0){
                        	var val = [];
                        	val.push({
                                type: 'steps',
                                value : $scope.objective
                            });
                        	Objective.setObjective({
                                objective: {
                                    select : 'steps',
                                    type : 'Linear',
                                    startValue : $scope.objective,
                                    value : $scope.objective,
                                    values: val,
                                    startDate : new Date(),
                                    endDate : new Date(),
                                    points : []
                                }
                            }).success(function(data){
                                $scope.$parent.buildDashboard();
                            }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type:'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                            });
                        } else {
                            $rootScope.rootAlerts.push({
                                type:'info',
                                msg: gettextCatalog.getString('Target value must be greater than 1'),
                                priority: 4
                            });
                        }
                    });
                }
            });
        }
    }
});