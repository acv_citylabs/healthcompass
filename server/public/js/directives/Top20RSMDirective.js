angular.module('Top20RSMDirective', []).directive('top20rsm', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, $log) {
    return {
        restrict: 'A',
        scope: {},
        templateUrl: 'templates/dashboard/cards/directives/top20rsm.html',
        link: function($scope, $element, $attrs) {

            $ocLazyLoad.load('js/services/ContactService.js').then(function() {
                var Contact = $injector.get('Contact');

                function getMyPatients(callback) {
                    Contact.accepted().success(function(data) {
                        callback(null, data);
                    }).error(function(status, data1) {
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            priority: 2
                        });
                        callback(status, null);
                    });
                }



                getMyPatients(function callback(err, patients) {
                    if (err) {
                        $log.debug('Top20RSMDirective - Failed to get patients list', err);
                    } else {
                        $log.debug("Patients :", patients);
                    }
                });

                $log.debug("My patients :", $scope.mypatients);

                $scope.changeView = function(type) {

                    $log.debug("Trends view :", $scope.trendsView);

                    switch (type) {
                        case 'trends':
                            $scope.trendsView = true;
                            $log.debug("Trends view :", $scope.trendsView);
                            break;
                        default:
                            $scope.trendsView = false;
                            break
                    }

                }

                function randomVal(min, max) {
                    return (Math.random() * (max - min) + min).toFixed(1);
                };

                /* Color level in Tree map */
                $scope.treemapcol = function(val, min, max, type) {
                    // TYPE : from orange => 60 / from green => 100
                    // First component of HSL color code should vary between 60 (orange) to 0 (red)
                    // hsl(x,100,80)
                    // We return the corresponding x which is proportionnal to the delta (measure-treshold)
                    var std = (val - min) / (max - min);
                    var color = type * (std);
                    if (color < 0) {
                        color = 0;
                    }
                    return color;
                }

                if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
                    $scope.demoname = 'John Dow';
                    $log.debug("$scope.demoname :", $scope.demoname);

                } else {
                    $scope.demoname = 'John Doe';
                }

                $scope.sortType = 'last'; // set the default sort type
                $scope.sortReverse = false; // set the default sort order
                $scope.searchText = ''; // set the default search/filter term


                /* FOR demo purposes */
                $scope.optimals = {
                    hba1c: 6,
                    hypo: 8,
                    hyper: 8,
                    glyvar: 20,
                    risk: 15
                };

                // To be calculated according to users data
                $scope.min = {
                    hba1c: 6.8,
                    hypo: 12,
                    hyper: 9,
                    glyvar: 12,
                    risk: 16,
                };


                $scope.max = {
                    hba1c: 10.1,
                    hypo: 31,
                    hyper: 36,
                    glyvar: 40,
                    risk: 49,
                };

                $scope.trendsmin = {
                    hba1c: -5.2,
                    hypo: -6.6,
                    hyper: -4.1,
                    glyvar: -7.7,
                    risk: -2.4,
                };


                $scope.trendsmax = {
                    hba1c: 3.2,
                    hypo: 7.3,
                    hyper: 6.3,
                    glyvar: 7.0,
                    risk: 4.9,
                };


                $scope.users = [

                    {
                        id: 1,
                        first: 'Joey',
                        last: 'Rambo',
                        hba1c: {
                            value: 6.8,
                            trend: 3.2,
                        },
                        hypo: {
                            value: 12,
                            trend: 2.5,
                        },
                        hyper: {
                            value: 15,
                            trend: -4.1,
                        },
                        glyvar: {
                            value: 28,
                            trend: -0.8,
                        },
                        risk: {
                            value: 17,
                            trend: -1.4,
                        }
                    },
                    {
                        id: 2,
                        first: 'Rocky',
                        last: 'Balboa',
                        hba1c: {
                            value: 8.3,
                            trend: -0.4,
                        },
                        hypo: {
                            value: 18,
                            trend: -0.6,
                        },
                        hyper: {
                            value: 23,
                            trend: 6.3,
                        },
                        glyvar: {
                            value: 30,
                            trend: 7.0,
                        },
                        risk: {
                            value: 16,
                            trend: 4.9,
                        }
                    },
                    {
                        id: 3,
                        first: 'Johan',
                        last: 'Kimble',
                        hba1c: {
                            value: 8.6,
                            trend: -5.2,
                        },
                        hypo: {
                            value: 31,
                            trend: -6.6
                        },
                        hyper: {
                            value: 9,
                            trend: -2.4
                        },
                        glyvar: {
                            value: 17,
                            trend: -3.0
                        },
                        risk: {
                            value: 18,
                            trend: -1.3
                        }
                    },
                    {
                        id: 4,
                        first: 'Charles',
                        last: 'Richards',
                        hba1c: {
                            value: 9.3,
                            trend: 1.0,
                        },
                        hypo: {
                            value: 25,
                            trend: 7.3,
                        },
                        hyper: {
                            value: 18,
                            trend: 5.2,
                        },
                        glyvar: {
                            value: 25,
                            trend: 0.9,
                        },
                        risk: {
                            value: 36,
                            trend: 2.7
                        }
                    },
                    {
                        id: 5,
                        first: 'Ben',
                        last: 'Richards',
                        hba1c: {
                            value: 7.4,
                            trend: 3.1
                        },
                        hypo: {
                            value: 15,
                            trend: 2.5
                        },
                        hyper: {
                            value: 23,
                            trend: -0.4
                        },
                        glyvar: {
                            value: 12,
                            trend: -7.7
                        },
                        risk: {
                            value: 22,
                            trend: 1.3
                        }
                    },
                    {
                        id: 6,
                        first: 'John',
                        last: 'Dow',
                        hba1c: {
                            value: 10.1,
                            trend: 1.8
                        },
                        hypo: {
                            value: 18,
                            trend: -6.5
                        },
                        hyper: {
                            value: 36,
                            trend: 5.7
                        },
                        glyvar: {
                            value: 40,
                            trend: 4.8
                        },
                        risk: {
                            value: 49,
                            trend: -2.4
                        }
                    }

                ];



            });
        }
    }
})