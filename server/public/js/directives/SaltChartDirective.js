
angular.module('SaltChartDirective', []).directive('saltchart', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, $window, $filter, $state, $log, ModalService) {
    return {
        restrict: 'E',
        templateUrl: 'templates/dashboard/cards/directives/chart.html',
        scope: {
            title: '@',
            name: '@',
            objective: '@'
        },
        link: function($scope, $element, $attrs) {
                $scope.fractionSize = 1;
                $scope.view = '';
                $scope.unit = 'Na(mg)';
                 $scope.isHiddenLocal = function(name){
                    return $rootScope.isHiddenGlobal.salt;
                }

                
                // Global configuration
                Highcharts.setOptions({global: {useUTC : false}});

                // Copy the basic configuration
                $scope.chart = JSON.parse(JSON.stringify($scope.$parent.aChart));
                
                // Define chart type
                $scope.chart.options.chart.type = 'line';
                
                // Data
                $scope.chart.series[0] = {name: gettextCatalog.getString('Salt'), id: 'salt', color: '#616161'};
                
                // Create a personalized tooltip
                $scope.chart.options.tooltip.formatter = function() {
                    return $filter('ddMMyyyy')(new Date(this.x).toISOString()) + '<br><span style="color:#616161;">●</span>  <b>' + this.y + '</b> ' + $scope.unit;
                }
                
                // Define X axis
                $scope.chart.xAxis.type = 'datetime';
                $scope.chart.xAxis.labels = {
                    overflow: 'justify'
                };
                $scope.chart.xAxis.dateTimeLabelFormats = {
                    month: '%e. %b',
                    year: '%b'
                };
                $scope.chart.xAxis.min = new Date(new Date().setDate(new Date().getDate() - 30)).getTime();
                $scope.chart.xAxis.max = (new Date).getTime();
                
                // Define Y axis
                $scope.chart.yAxis.plotLines[0].label.text = gettextCatalog.getString('max.');
                $scope.chart.yAxis.title.text = $scope.unit;
                $scope.chart.yAxis.max = 9000;
                
                //Size
                if($window.innerWidth<535){
                    $scope.chart.size.height = 250;
                }

                // Get objective
                $ocLazyLoad.load('js/services/ObjectivesService.js').then(function() {
                    var Objective = $injector.get('Objective');
                    Objective.getObjective('salt').success(function(data){
                        if (data) {
                        	var obj = '';
                        	if(data.values[0])
                        		obj = data.values[0].value;
                        	else if(data.value)
                        		obj = data.value;
                        	else if(data.startValue)
                        		obj = data.startValue;
                        	
                        	$scope.chart.yAxis.plotLines[0].value = obj;
                            $scope.chart.yAxis.plotLines[0].label.text = $scope.chart.yAxis.plotLines[0].label.text + " (" + obj + $scope.unit + ")";

                            $scope.chart.yAxis.min = 0; 
                            $scope.chart.yAxis.max = obj * 1.5; 
                        }
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type:'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            priority: 2
                        });
                    });
                });
                
                // Build the chart
                $scope.build = function(view) {
                    var from;
                    switch(view){
                        case '1m':
                            $scope.view = '1m';
                            from = new Date(new Date().setDate(new Date().getDate() - 30));
                            
                        break;
                        case '3m':
                            $scope.view = '3m';
                            from = new Date(new Date().setDate(new Date().getDate() - 90));
                        break;
                        case '6m':
                            $scope.view = '6m';
                            from = new Date(new Date().setDate(new Date().getDate() - 180));                      
                        break;
                        case 'list':
                            $scope.view = 'list';                            
                        break;
                        default:
                            $scope.view = '';
                            from = new Date(new Date().setDate(new Date().getDate() - 30));
                        break;
                    }
                    
                    if(view !== 'list'){
                    	$scope.chart.xAxis.min = from.getTime();
                        $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                            var Chart = $injector.get('Chart');
                            Chart.build({
                                type: 'salt',
                                from: from,
                                to: new Date()
                            }).success(function(data) {
                            	
                                for(var i=0; i< data.series.length; i++){
                                    data.series[i].name = gettextCatalog.getString(data.series[i].name);
                                    for(var k = 0; k < data.series[i].data.length; ++k){
                                    	if(data.series[i].data[k][1] > $scope.chart.yAxis.max){
                                    		$scope.chart.yAxis.max = data.series[i].data[k][1];
                                    	}
                                    	if(data.series[i].data[k][1] < $scope.chart.yAxis.min){
                                    		$scope.chart.yAxis.min = data.series[i].data[k][1];
                                    	}
                                    }
                                }
                                
                                $scope.chart.series = data.series;
                                
                            }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type:'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                            });
                        });
                    } else {
                        $scope.$parent.buildList({type: 'salt'}, function(data){
                            $scope.list = data;
                        });
                    }
                }
                
                // First build
                $scope.build();
            
        }
    }
});