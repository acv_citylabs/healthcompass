angular.module('CustomRangeAgpDirective', ['multipleDatePicker'])
    .directive('agpphysician', function(gettextCatalog, $ocLazyLoad, $injector, $rootScope, $log, $window, $filter) {
        return {
            restrict: 'E',
            templateUrl: 'templates/patient/agpPhysician.html',       
            link: function($scope, $element, $attrs) {

                var from;
                var to;
                var customFrom;
                var customTo;
                var dailyUpperObjective;
                var dailyLowerObjective;
                var datePolygon = new Date(new Date().setDate(3));

                $scope.username = $scope.$parent.username;

                $scope.selectedDays1 = [];
                $scope.selectedDays2 = [];
                $scope.multipleDates = [];
                $scope.button = false;

                $scope.dateSelected = 'daterange';
                $scope.today = moment();

                $scope.view = '';
                $scope.unit = 'mg/dL';

                $scope.showButton = function(){
                    $scope.button = true;
                };

                $scope.hideButton = function(){
                    $scope.button = false;
                };

                $scope.showDate = function(date) {
                    customFrom = new Date(new Date(date).setHours(0,0,0,0));
                    customTo = new Date(new Date(date).setHours(23,59,59,0));                    
                    $scope.build('custom1d');
                    // console.log(customFrom);
                    // console.log(customTo);
                };

				$scope.oneDaySelectionOnly1 = function (event, date) {				    
				    $scope.selectedDays1.length = 0;
				};

				$scope.oneDaySelectionOnly2 = function (event, date) {
				    $scope.selectedDays2.length = 0;
				};

				$scope.cleanMultipleDays = function () {
					$scope.multipleDates = [];
				};

				$scope.cleanDateRangeSelection = function (event, date) {
					$scope.selectedDays1 = [];
					$scope.selectedDays2 = [];
				};				                

                // Global configuration
                Highcharts.setOptions({ global: { useUTC: false } });

                var dailyView = {
                    title: {
                        text: ''
                    },

                    xAxis: {
                        type: 'datetime',
                        tickInterval: 3600 * 1000,
                        min: 1517612400000,
                        max: 1517698740000,
                        labels: {
                            // formatter: function() {
                            //     return Highcharts.dateFormat('', this.value);
                            // },
                            enabled: false
                            //overflow: 'justify',
                            //rotation: 0,
                            //step: 2
                        },
                        visible: false
                    },

                    yAxis: {
                        startOnTick: false,
                        title: {
                            text: ''
                        },
                        plotLines: [{
                            value: 120,
                            width: 1.3,
                            color: '#9e9e9e',
                            dashStyle: 'solid',
                            zIndex: 5,
                            label: {
                                text: '',
                                align: 'left',
                                x: 5
                            }
                        },{
                            value: 75,
                            color: '#9e9e9e',
                            dashStyle: 'solid',
                            width: 1.3,
                            zIndex: 99,
                            label: {
                                x: 0,
                                y: 15,
                                align: 'right'
                            }
                        }],
                        plotBands: [{ // mark the objectives range
                            color: '#FCFFC5',
                            from: 75,
                            to: 120
                        }],
                        labels: {
                            enabled: false
                        },
                        min: 20,
                        max: 250
                    },

                    legend: {
                        enabled: false
                    },

                    options: {
                        legend: {
                            enabled: false
                        },
                        tooltip: {}
                    },

                    series: [{
                        name: 'Glucose',
                        connectNulls: true,
                        data: [],
                        type: 'spline',
                        color: '#003399',
                        marker: { enabled: false}
                    }],

                    responsive: {
                        rules: []
                    },
                    size: {
                        height: '150px'
                    },
                    noData: gettextCatalog.getString('No Data')
                };

                //console.log('MIN:', dailyView.xAxis.min);

                //$scope.dailyGlucoseViews = [{name: 'dailyview1'},{name: 'dailyview2'},{name: 'dailyview3'},{name: 'dailyview4'},{name: 'dailyview5'},{name: 'dailyview6'},{name: 'dailyview7'}];

                $scope.dailyview1 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview2 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview3 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview4 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview5 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview6 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview7 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview8 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview9 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview10 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview11 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview12 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview13 = JSON.parse(JSON.stringify(dailyView));
                $scope.dailyview14 = JSON.parse(JSON.stringify(dailyView));

                $scope.dailyview1.options.tooltip = $scope.dailyview2.options.tooltip = 
                $scope.dailyview3.options.tooltip = $scope.dailyview4.options.tooltip = 
                $scope.dailyview5.options.tooltip = $scope.dailyview6.options.tooltip = 
                $scope.dailyview7.options.tooltip = $scope.dailyview8.options.tooltip = 
                $scope.dailyview9.options.tooltip = $scope.dailyview10.options.tooltip = 
                $scope.dailyview11.options.tooltip = $scope.dailyview12.options.tooltip = 
                $scope.dailyview13.options.tooltip = $scope.dailyview14.options.tooltip = {
                    formatter: function() {
                        var hour = Highcharts.dateFormat('%b,%e %H:%M', this.x);
                        var median = this.y.toFixed(1);
                        var tooltip = '<h4>' + hour + ' h</h4> <br/>';
                        tooltip += '<span style="color:' + this.point.color + '">\u25CF</span> Median: <strong>' + median + ' ' + $scope.unit + '</strong><br />';
                        return tooltip;
                    }
                };

                //Matrix chart configuration
                $scope.matrixChart = {
                    title: {
                        text: ''
                    },
                    options: {
                        legend: {
                            useHTML: true,
                            labelFormatter: function() {
                                if (this.color === 'rgba(255,102,102,0.75)') {
                                    return '<span style="color:' + this.color + '">\u2B24</span> High Risk';
                                }
                                if (this.color === 'rgba(255,153,51,0.75)') {
                                    return '<span style="color:' + this.color + '">\u2B24</span> Moderate Risk';
                                }
                                if (this.color === 'rgba(204,255,204,0.75)') {
                                    return '<span style="color:' + this.color + '">\u2B24</span> Low Risk';
                                }
                            }
                        },
                        chart: {
                            //type: 'polygon',
                            marginTop: 50//,
                            //spacingLeft: 65
                            ,
                            events: {
                            	load: function() {
                            			var chart = this;
	          							var width = document.getElementById("matrixChart").offsetWidth;
	          							var height = document.getElementById("matrixChart").offsetHeight;
	          							//console.log(this);
	          							//var bottom = $(window).height() - link;
	          							//console.log('WIDTH:', width);
	          							//console.log('HEIGHT:', height);
	          							chart.renderer.text('HYPOGLYCAEMIA', (width/2 -20), height - 130)
										.css({
												fontSize: '14px',
										        color: '#666666',
										        zIndex: 100
										}).add();

										chart.renderer.text('HYPERGLYCAEMIA', (width/2 - 20), 70)
										.css({
												fontSize: '14px',
										        color: '#666666',
										        zIndex: 100
										}).add();
          						}
                            }
                        },
                        tooltip: {
                            useHTML: true,
                            formatter: function() {
                                var risk, firstH, lastH;
                                firstH = this.series.processedXData[0];
                                lastH = this.series.processedXData[3];

                                if (this.point.color === 'rgba(255,102,102,0.75)') {
                                    risk = '<h4>' + new Date(firstH).getHours() + ' h - ' + new Date(lastH).getHours() + ' h </h4>';
                                    if (this.series.name.indexOf("hyper") > -1) {
                                        risk += '<span style="font-weight: bold">Hyperglycaemia</span> <br />',
                                            risk += 'Risk: High <br />',
                                            risk += '<i class="fas fa-frown fa-3x" style=" color: #003399"></i>';
                                    } else if (this.series.name.indexOf("hypo") > -1) {
                                        risk += '<span style="font-weight: bold">Hypoglycaemia</span> <br />',
                                            risk += 'Risk: High <br />',
                                            risk += '<i class="fas fa-frown fa-3x" style=" color: #003399"></i>';
                                    }
                                } else if (this.series.color === 'rgba(255,153,51,0.75)') {
                                    risk = '<h4>' + new Date(firstH).getHours() + ' h - ' + new Date(lastH).getHours() + ' h </h4>';
                                    if (this.series.name.indexOf("hyper") > -1) {
                                        risk += '<span style="font-weight: bold">Hyperglycaemia </span><br />',
                                            risk += 'Risk: Moderate <br />',
                                            risk += '<i class="fas fa-meh fa-3x" style=" color: #003399"></i>';
                                    } else if (this.series.name.indexOf("hypo") > -1) {
                                        risk += '<span style="font-weight: bold">Hypoglycaemia </span><br />',
                                            risk += 'Risk: Moderate <br />',
                                            risk += '<i class="fas fa-meh fa-3x" style=" color: #003399"></i>';
                                    }
                                } else {
                                    risk = '<h4>' + new Date(firstH).getHours() + ' h - ' + new Date(lastH).getHours() + ' h </h4>';
                                    if (this.series.name.indexOf("hyper") > -1) {
                                        risk += '<span style="font-weight: bold">Hyperglycaemia </span><br />',
                                            risk += 'Risk: Low <br />',
                                            risk += '<i class="fas fa-smile fa-3x" style=" color: #003399"></i>';
                                    } else if (this.series.name.indexOf("hypo") > -1) {
                                        risk += '<span style="font-weight: bold">Hypoglycaemia </span><br />',
                                            risk += 'Risk: Low <br />',
                                            risk += '<i class="fas fa-smile fa-3x" style=" color: #003399"></i>';
                                    }
                                }
                                return risk;
                            }
                        }
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                states: {
                                    hover: {
                                        enabled: false
                                    }
                                },
                                enabled: false
                            }
                        },
                        polygon: {
			                    events: {
			                        legendItemClick: function () {
			                            return false; //does not work!! FIX THIS!!
			                        }
			                    }
			            	}
                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        type: 'datetime',
                        min: new Date(new Date().setDate(3)).setHours(0, 0, 0, 0),
                        max: new Date(new Date().setDate(3)).setHours(24, 0, 0, 0),
                        tickInterval: 3600 * 1000,
                        plotLines: [],
                        labels: {
                            formatter: function() {
                                return Highcharts.dateFormat('%H:%M', this.value);
                            },
                            overflow: 'justify',
                            //rotation: 0,
                            step: 2
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Title',
                            style: {
				                visibility: 'hidden'
				            }
                        },
                        labels: {
                            enabled: true,
                            formatter: function() {
                            	return 100;
                            },
                            style: {
                            	visibility: 'hidden'
                            }
                        },
                        min: 0,
                        max: 16,
                        lineColor: '#e6e6e6',
                        lineWidth: 3,
                        gridLineWidth: 0,
                        minorGridLineWidth: 0
                    },
                    series: [],
                    noData: gettextCatalog.getString('No Data'),
                    size: {
                        height: '280px'
                    }
                };

                //B chart configuration
                var bChart = {
                    title: {
                        text: ''
                    },
                    xAxis: {
                        type: 'datetime',
                        opposite: true,
                        crosshair: true,
                        tickInterval: 3600 * 1000,
                        labels: {
                        	rotation: 0
                        },
                        plotLines: []
                    },
                    yAxis: {
                        title: {
                            text: ''
                        },
                        gridLineWidth: 0,
                        minorGridLineWidth: 0,
                        plotLines: [{
                            value: null,
                            width: 1.3,
                            color: '#9e9e9e',
                            dashStyle: 'dash',
                            zIndex: 5,
                            label: {
                                text: '',
                                align: 'left',
                                x: 5
                            }
                        },{
		                    value: null,
		                    color: '#9e9e9e',
		                    dashStyle: 'dash',
		                    width: 1.3,
		                    zIndex: 99,
		                    label: {
		                        x: 0,
		                        y: 15,
		                        align: 'right'
		                    }
		                }],
                        min: 30,
                        max: 150,
                        lineColor: '#e6e6e6',
                        lineWidth: 3
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                enabled: false
                            },
                            fillOpacity: 0.2,
                        }
                    },
                    legend: {
                        align: 'center',
                        verticalAlign: 'top',
                        floating: true,
                        x: 0,
                        y: 30
                    },
                    credits: {
                        enabled: false
                    },

                    series: [],

                    responsive: {
                        rules: []
                    },

                    options: {
                        chart: {},
                        tooltip: {
                            shared: true
                        },
                        legend: {
                        	enabled: false
                        }
                    },
                    noData: gettextCatalog.getString('No Data')
                };

                $scope.chart = JSON.parse(JSON.stringify(bChart));

                //Define X axis
                $scope.chart.xAxis.type = 'datetime';

                $scope.chart.xAxis.labels = {
                    formatter: function() {
                        return Highcharts.dateFormat('%H:%M', this.value);
                    },
                    overflow: 'justify',
                    //rotation: 0,
                    step: 2
                };

                $scope.chart.yAxis.min = 30;
                $scope.chart.yAxis.max = 150;          

                // Define Y axis
                $scope.chart.yAxis.title.text = $scope.unit;
                
                // Objective lines
                $scope.chart.yAxis.plotLines[0].label.text = 'max';
                $scope.chart.yAxis.plotLines[1].label.text = 'min';
                $scope.chart.yAxis.plotLines[1].label.verticalAlign = 'bottom';

                $scope.width = $window.innerWidth;

                angular.element($window).on('resize', function() {

                    $scope.width = $window.innerWidth;

                    //Define responsive rules
                    if ($scope.width < 500) {
                        $scope.chart.yAxis.title.text = '';
                        $scope.matrixChart.yAxis.title.text = '';
                        $scope.chart.xAxis.labels.rotation = -45;
                        $scope.matrixChart.xAxis.labels.rotation = -45;
                    } else {
                        $scope.chart.yAxis.title.text = $scope.unit;
                        $scope.chart.xAxis.labels.rotation = 0;
                        $scope.matrixChart.xAxis.labels.rotation = 0;
                        $scope.matrixChart.yAxis.title.text = 'Title';
                    }
                    // manually $digest required as resize event is outside angular
                    $scope.$digest();
                });

                angular.element(document).ready(function() {
                    $scope.width = $window.innerWidth;

                    //Define responsive rules
                    if ($scope.width < 500) {
                        $scope.chart.yAxis.title.text = '';
                        $scope.matrixChart.yAxis.title.text = '';
                        $scope.chart.xAxis.labels.rotation = -45;
                        $scope.matrixChart.xAxis.labels.rotation = -45;
                    } else {
                        $scope.chart.yAxis.title.text = $scope.unit;
                        $scope.chart.xAxis.labels.rotation = 0;
                        $scope.matrixChart.xAxis.labels.rotation = 0;
                        $scope.matrixChart.yAxis.title.text = 'Title';
                    }
                });

                $scope.chart.options.tooltip = {
                    shared: true,
                    valueSuffix: 'mmol/L',
                    formatter: function() {
                        var hour = Highcharts.dateFormat('%H:%M', this.x);
                        var median = this.y.toFixed(1);
                        var percentile1090low = this.points[2].point['low'].toFixed(1);
                        var percentile1090high = this.points[2].point['high'].toFixed(1);
                        var percentile2575low = this.points[1].point['low'].toFixed(1);
                        var percentile2575high = this.points[1].point['high'].toFixed(1);
                        var tooltip = '<h4>' + hour + '</h4> <br/>';
                        tooltip += '<span style="color:' + this.points[0].point.color + '">\u25CF</span> Median: <strong>' + median + ' ' + $scope.unit + '</strong><br />';
                        tooltip += '<span style="color:' + this.points[2].point.color + '">\u25CF</span> 10th to 90th percentile: <strong>' + percentile1090low + ' - ' + percentile1090high + ' ' + $scope.unit + '</strong><br />';
                        tooltip += '<span style="color:' + this.points[1].point.color + '">\u25CF</span> 25th to 75th percentile: <strong>' + percentile2575low + ' - ' + percentile2575high + ' ' + $scope.unit + '</strong>';
                        return tooltip;

                    }
                };

                // Get objectives
                  $ocLazyLoad.load('js/services/ObjectivesService.js').then(function() {
                      var Objective = $injector.get('Objective');
                      Objective.getObjectives($scope.$parent.username).success(function(data) {
                        var idxObj;
                        data.forEach(function(obj, index) {
                            if(obj.select == "glycaemia") {
                                idxObj =  index;
                            }
                        });
                        if(data[idxObj]) {
                              $scope.chart.yAxis.plotLines[1].value = data[idxObj].values[0].value;
                              $scope.chart.yAxis.plotLines[1].label.text = $scope.chart.yAxis.plotLines[1].label.text + " (" + $filter('number')(data[idxObj].values[0].value, 1) + ' ' + $scope.unit + ")";
                              $scope.chart.yAxis.plotLines[0].value = data[idxObj].values[1].value;
                              $scope.chart.yAxis.plotLines[0].label.text = $scope.chart.yAxis.plotLines[0].label.text + " (" + $filter('number')(data[idxObj].values[1].value, 1) + ' ' + $scope.unit + ")";
                              dailyUpperObjective = data[idxObj].values[0].value; 
                              dailyLowerObjective = data[idxObj].values[1].value;
                          }
                      }).error(function(status, data) {
                          $rootScope.rootAlerts.push({
                              type: 'danger',
                              msg: gettextCatalog.getString('An error occurred, please try again later'),
                              priority: 2
                          });
                      });
                  }); 

                function getDatesFromMoment() {
                	var dates = [];
                	$scope.multipleDates.forEach(function(date) {
                		dates.push(date._d);
                	});
                	return dates;
                }

                function findMaxDate(dates) {
                    var datemax = 0;
                    var current;
                    dates.forEach(function (date) {
                        current = new Date(date);
                        if(current > datemax) {datemax = current;}
                    });
                    //console.log(datemax);
                    return datemax;
                }

                //toggle the display of custom range input fields
                $scope.custom = function(toggle) {
                    $scope.showCustom = toggle;
                }

                // Build the chart
                $scope.build = function(view) {
                    var matrixConfig;
                    var dateConfig;      

                    switch (view) {
	                    case '1d':
	                        $scope.view = '1d';
	                        from = new Date(new Date(new Date().setDate(new Date().getDate())).setHours(0,0,0,0));
	                        to = new Date;
	                        matrixConfig = 'events';
	                        $scope.custom(false);
	                        break;
	                    case '14d':
	                        $scope.view = '14d';
	                        from = new Date(new Date().setDate(new Date().getDate() - 14));
	                        to = new Date;
	                        matrixConfig = 'meals';
	                        $scope.custom(false);
	                        break;
	                    case '1m':
	                        $scope.view = '1m';
	                        from = new Date(new Date().setDate(new Date().getDate() - 30));
	                        to = new Date;
	                        matrixConfig = 'meals';
	                        $scope.custom(false);
	                        break;
	                    case '3m':
	                        $scope.view = '3m';
	                        from = new Date(new Date().setDate(new Date().getDate() - 90));
	                        to = new Date;
	                        matrixConfig = 'meals';
	                        $scope.custom(false);
	                        break;
	                    case '6m':
	                        $scope.view = '6m';
	                        from = new Date(new Date().setDate(new Date().getDate() - 180));
	                        to = new Date;
	                        matrixConfig = 'meals';
	                        $scope.custom(false);
	                        break;
	                    case 'custom':                    	
	                    	$scope.view = 'custom';

	                    	switch($scope.dateSelected) {
	                    		case 'datepick':
	                    			if($scope.multipleDates.length == 0) {
		    							$rootScope.rootAlerts.push({
						                     type:'warning',
						                     msg: gettextCatalog.getString('Please select at least one date!'),
						                     priority: 3
						                 });
                                        from = new Date().setHours(23,59,59,59);
                                        to = new Date().setHours(23,59,59,59);
		    							break;
			                    	}
	                    			from = getDatesFromMoment(); //get and array containing all selected dates
	                    			to = null;

                                    if(from.length === 1) { //if one day is selected
                                        matrixConfig = 'events'; //show a 1-day view
                                        dateConfig = 'daterange';
                                        to = getDatesFromMoment(); //there is only one date in the array
                                        console.log('TO:', to);
                                    } else {
                                        matrixConfig = 'meals'; //show a classical 'meals' matrix
                                        dateConfig = 'datepick';
                                    }                  			
	                    			
	                    			break;
	                    		case 'daterange':
	                    			if($scope.selectedDays1.length == 0 || $scope.selectedDays2.length == 0) {
		    							$rootScope.rootAlerts.push({
						                     type:'warning',
						                     msg: gettextCatalog.getString('Please select the start and end date!'),
						                     priority: 3
						                 });
		    							break;
			                    	} else {
	                    				from = $scope.selectedDays1[0]._d;
		                    			to = $scope.selectedDays2[0]._d;
		                    			matrixConfig = 'meals';
		                    			dateConfig = 'daterange';
		                    			break;
	                    			}	                    			
	                    	}
	                        $scope.custom(true);
	                        break;
                        case 'custom1d':
                            $scope.view = '1d';
                            from = customFrom;
                            to = customTo;
                            matrixConfig = 'events';
                            dateConfig = 'dailyGlucose',
                            $scope.custom(false);
                            break;
	                    case 'list':
	                        $scope.view = 'list';
	                        break;
	                    default:
	                        $scope.view = '';
	                        from = new Date(new Date().setDate(new Date().getDate() - 30));
	                        to = new Date;
	                        matrixConfig = 'meals';
	                        $scope.custom(false);
	                        break;
                    }

                    //set dates in the header of AGP
                    if($scope.dateSelected !== 'datepick' || $scope.view !== 'custom') {
                        $scope.from = moment().year(from.getFullYear()).month(from.getMonth()).date(from.getDate()).format('D/M/YYYY');
                        $scope.to = moment().year(to.getFullYear()).month(to.getMonth()).date(to.getDate()).format('D/M/YYYY');
                    }               

                    //Set week dates for the daily glucose profiles seection
                    if(view !== 'custom' || view === 'custom' && $scope.dateSelected === 'daterange') {                                           
                        $scope.week2 = moment().year(to.getFullYear()).month(to.getMonth()).date(to.getDate() - 7).format('D MMM YYYY') + ' - ' + moment().year(to.getFullYear()).month(to.getMonth()).date(to.getDate() - 1).format('D MMM YYYY');
                        $scope.week1 = moment().year(to.getFullYear()).month(to.getMonth()).date(to.getDate() - 14).format('D MMM YYYY') + ' - ' + moment().year(to.getFullYear()).month(to.getMonth()).date(to.getDate() - 8).format('D MMM YYYY');
                    }
                                        
                    if (view !== 'list') {
                        $ocLazyLoad.load('js/services/PatientService.js').then(function() {
                            var Patient = $injector.get('Patient');
                            Patient.chart({
                                username: $scope.$parent.username,
                                type: 'newGly',
                                from: from,
                                to: to,
                                matrixconfig: matrixConfig,
                                dateconfig: dateConfig
                            }).success(function(data) {
                                //console.log(data);
                                for (var i = 0; i < data.series.length; i++) {
                                    for (var k = 0; k < data.series[i].data.length; ++k) {
                                        if (data.series[i].data[k][2] > $scope.chart.yAxis.max) {
                                            $scope.chart.yAxis.max = data.series[i].data[k][2] + 1; //find and set a maximum value of yAxis
                                        }
                                        if (data.series[i].data[k][1] < $scope.chart.yAxis.min) {
                                            $scope.chart.yAxis.min = data.series[i].data[k][1]; //find and set a minimum value of yAxis
                                        }
                                    }
                                }

                                $scope.chart.series = data.series;

                                var counter = 0;
                                var matrixData;
                                var meal;
                                var plotLineObjMatrix = {
					            	value: null,
					            	width: 1,
					            	color: '#e6e6e6',
					            	dashStyle: 'solid',
					            	zIndex: 100,
					            		label: {
					            			useHTML: true,
					                    	text: '',
					                    	align: 'right',
					                    	rotation: 0,
					                    	y: -35,
					                        x: 10
					                    }
					            };
					            var plotLineObjChart = {
		                            value: null,
		                            width: 1,
		                            color: '#e6e6e6',
		                            dashStyle: 'solid',
		                            zIndex: 50
		                            
		                        };		                        
		                        
                                if (matrixConfig === 'meals') {
                                    //meal times plotlines in matrix chart
                                    matrixData = data.mealTimes.length;
                                    meal = data.mealTimes;
                                    
                                    $scope.matrixChart.xAxis.plotLines = [];
                                    $scope.chart.xAxis.plotlines = [];

                                    counter = 0;

                                    data.mealTimes.forEach(function(entry) {
                                    	$scope.matrixChart.xAxis.plotLines[counter] = jQuery.extend(true, {}, plotLineObjMatrix);
                                    	$scope.chart.xAxis.plotLines[counter] = jQuery.extend(true, {}, plotLineObjChart);
                                        $scope.chart.xAxis.plotLines[counter].value = new Date(new Date().setDate(3)).setHours(entry.data, 0, 0, 0);
                                        $scope.matrixChart.xAxis.plotLines[counter].value = new Date(new Date().setDate(3)).setHours(entry.data, 0, 0, 0);
                                        if (counter === 3) {
                                            $scope.matrixChart.xAxis.plotLines[counter].label.text = '<i class="fas fa-bed fa-2x" aria-hidden="true"></i>';
                                        } else {
                                            $scope.matrixChart.xAxis.plotLines[counter].label.text = '<i class="fas fa-utensils fa-2x" aria-hidden="true"></i>';
                                        }
                                        counter++;
                                    });

                                } else if (matrixConfig === 'events') { //events are now: meals, insulin, activity
                                    //event times plotlines in matrix chart
                                    matrixData = data.eventTimes.length;
                                    meal = data.eventTimes;

                                    $scope.matrixChart.xAxis.plotLines = [];
                                    $scope.chart.xAxis.plotLines = [];

                                    counter = 0;
                                    //console.log('EVENTS',data.eventTimes);
                                    data.eventTimes.forEach(function(entry) {
                                        //console.log(entry);
                                        $scope.matrixChart.xAxis.plotLines[counter] = jQuery.extend(true, {}, plotLineObjMatrix);
                                        $scope.chart.xAxis.plotLines[counter] = jQuery.extend(true, {}, plotLineObjChart); 
                                        var resolve = resolveOverlap(entry.data); 

                                        if(entry.type == 'insulin') {                                                                            	                                  	
                                        	if(resolve !== -1) {                                        		
                                        		$scope.matrixChart.xAxis.plotLines[counter].value = new Date(new Date().setDate(3)).setHours(entry.data, 0, 0, 0);
                                        		$scope.chart.xAxis.plotLines[counter].value = new Date(new Date().setDate(3)).setHours(entry.data, 0, 0, 0);
                                        		$scope.matrixChart.xAxis.plotLines[counter].label.text = '<i class="fas fa-medkit fa-2x" aria-hidden="true"></i>';                                        		
                                        	}
                                        	if(resolve === true) {
                                        		 
                                        		$scope.matrixChart.xAxis.plotLines[counter].label.y = -5; 
                                        	}
                                        	counter++;
                                        }
                                        if(entry.type == 'activity') {
                                        	if(resolve !== -1) {                                        		
                                        		$scope.matrixChart.xAxis.plotLines[counter].value = new Date(new Date().setDate(3)).setHours(entry.data, 0, 0, 0);
                                        		$scope.chart.xAxis.plotLines[counter].value = new Date(new Date().setDate(3)).setHours(entry.data, 0, 0, 0);
                                        		$scope.matrixChart.xAxis.plotLines[counter].label.text = '<i class="fas fa-heart fa-2x" aria-hidden="true"></i>';                                        		
                                        	}
                                        	if(resolve === true) {
                                        		$scope.matrixChart.xAxis.plotLines[counter].label.y = -5; 
                                        	}
                                        	counter++;
                                        }
                                        if(entry.type == 'meal') {
                                        	if(resolve !== -1) {                                        		
                                        		$scope.matrixChart.xAxis.plotLines[counter].value = new Date(new Date().setDate(3)).setHours(entry.data, 0, 0, 0);
                                        		$scope.chart.xAxis.plotLines[counter].value = new Date(new Date().setDate(3)).setHours(entry.data, 0, 0, 0);
                                        		$scope.matrixChart.xAxis.plotLines[counter].label.text = '<i class="fas fa-utensils fa-2x" aria-hidden="true"></i>';                                        		
                                        	}
                                        	if(resolve === true) {                                        	
                                        		$scope.matrixChart.xAxis.plotLines[counter].label.y = -5; 
                                        	}
                                        	counter++;
                                        }
                                    });
                                }

                                //the function checks if there is an overlap between events. Based on the return, the event will (not) be pushed in the matrix
                                //max 2 events at each hour are allowed for the UI clarity purposes
                                function resolveOverlap(entry) {
                                		var found = false;
                                		var counter = 0;
                                		$scope.matrixChart.xAxis.plotLines.forEach(function(line) {
	                                		if(line.value === new Date(new Date().setDate(3)).setHours(entry, 0, 0, 0)) {
	                                			found = true;
	                                			counter++;
	                                		}
	                                	});
	                                	if(found === false) {
	                                		return false;
	                                	} else if(found === true && counter < 2) { //allow max 2 events 
	                                		return true;
	                                	} else if(counter >=2){
	                                		return -1;
	                                	}
                                };

                                $scope.matrixChart.series = [];

                                var counterRed = true,
                                    counterOrange = true,
                                    counterGreen = true;

                                var polygonObj = {
                                        name: 'hypo' + i,
                                        type: 'polygon',
                                        data: [],
                                        color: Highcharts.Color('#ff6666').setOpacity(0.75).get(),
                                        showInLegend: true,
                                        zIndex: 50
                                    };

                                for (var i = 0; i < matrixData; i++) {
                                    //console.log(meal);
                                    if (i == 0) {

                                        // HYPO line
                                        var polygon = jQuery.extend(true, {}, polygonObj);
                                        polygon.data.push([new Date(datePolygon.setMonth(new Date().getMonth())).setHours(0, 0, 0, 0), 0]);
                                        polygon.data.push([new Date(datePolygon.setMonth(new Date().getMonth())).setHours(0, 0, 0, 0), 6]);
                                        polygon.data.push([new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 6]);
                                        polygon.data.push([new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 0]);
                                        polygon.color = Highcharts.Color('#ff9933').setOpacity(0.75).get();
                                        if (counterRed === false) {
                                            polygon.showInLegend = false;
                                        }
                                        counterOrange = false;

                                        // HYPERGLYCAEMIA line
                                        var polygon2 = jQuery.extend(true, {}, polygonObj);
                                        polygon2.name = 'hyper';
                                        polygon2.data[0] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(0, 0, 0, 0), 10];
                                        polygon2.data[1] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(0, 0, 0, 0), 16];
                                        polygon2.data[2] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 16];
                                        polygon2.data[3] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 10];
                                        polygon2.color = Highcharts.Color('#ccffcc').setOpacity(0.75).get();
                                        if (counterGreen === false) {
                                            polygon2.showInLegend = false;
                                        }
                                        counterGreen = false;
                                    // Events in the middle
                                    } else {
                                        var polygon = jQuery.extend(true, {}, polygonObj);
                                        polygon.data[0] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i - 1].data, 0, 0, 0), 0];
                                        polygon.data[1] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i - 1].data, 0, 0, 0), 6];
                                        polygon.data[2] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data , 0, 0, 0), 6];
                                        polygon.data[3] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 0];
                                        polygon.color = Highcharts.Color('#ff9933').setOpacity(0.75).get();
                                        if (counterRed === false) {
                                            polygon.showInLegend = false;
                                        }
                                        counterRed = false;

                                        var polygon2 = jQuery.extend(true, {}, polygonObj);
                                        polygon2.name = 'hyper';

                                        polygon2.data[0] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i - 1].data, 0, 0, 0), 10];
                                        polygon2.data[1] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i - 1].data, 0, 0, 0), 16];
                                        polygon2.data[2] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 16];
                                        polygon2.data[3] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data , 0, 0, 0), 10];
                                        polygon2.color = Highcharts.Color('#ff9933').setOpacity(0.75).get();
                                        if (counterOrange === false) {
                                            polygon2.showInLegend = false;
                                        }
                                        counterOrange = false;
                                    }
                                    // Last event
                                    if (i == matrixData -1) {
                                        var lastPolygon = jQuery.extend(true, {}, polygonObj);
                                        lastPolygon.data[0] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 0];
                                        lastPolygon.data[1] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 6];
                                        lastPolygon.data[2] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(24, 0, 0, 0), 6];
                                        lastPolygon.data[3] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(24, 0, 0, 0), 0];
                                        lastPolygon.color = Highcharts.Color('#ccffcc').setOpacity(0.75).get();
                                        if (counterOrange === false) {
                                            lastPolygon.showInLegend = false;
                                        }
                                        counterOrange = false;
                                        $scope.matrixChart.series.push(lastPolygon);

                                        var lastPolygon2 = jQuery.extend(true, {}, polygonObj);
                                        lastPolygon2.name = 'hyper';
                                        lastPolygon2.data[0] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 10];
                                        lastPolygon2.data[1] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(meal[i].data, 0, 0, 0), 16];
                                        lastPolygon2.data[2] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(24, 0, 0, 0), 16];
                                        lastPolygon2.data[3] = [new Date(datePolygon.setMonth(new Date().getMonth())).setHours(24, 0, 0, 0), 10];
                                        lastPolygon2.color = Highcharts.Color('#ff6666').setOpacity(0.75).get();
                                        if (counterGreen === false) {
                                            lastPolygon2.showInLegend = false;
                                        }
                                        counterGreen = false;
                                        $scope.matrixChart.series.push(lastPolygon2);
                                    }

                                    $scope.matrixChart.series.push(polygon);
                                    $scope.matrixChart.series.push(polygon2);                                                             
                                }
                                //console.log($scope.matrixChart.series);

                                //CONFIG DAILY GLUCOSE PROFILES
                                
                                var daysArray = [];
                                var counter = 1;
                                var b, curr, diff, t;
                                var index;
                                var i = 1;

                                //THIS LOOP EMPTIES THE DATASETS OF ALL DAILY GLUCOSE CHARTS
                                for(i = 1; i < 15; i++) {
                                    index = $scope["dailyview"+i];
                                    index.series[0].data = [];
                                }

                                if(data.dailyGlucose.length > 0) {

                                    var currentDay = data.dailyGlucose[0][1];
                                    //console.log(currentDay);
                                    data.dailyGlucose.forEach(function (entry) {
                                       day = new Date(entry[1]).getDate();
                                       if(new Date(entry[1]).getDate() === new Date(currentDay).getDate() ) {
                                            if(daysArray[counter] === undefined) { daysArray[counter] = new Array(); }    
                                            daysArray[counter].push(entry);
                                       } else {
                                            counter++;
                                            currentDay = entry[1];
                                            day = new Date(entry[1]).getDate();
                                            if(daysArray[counter] === undefined) { daysArray[counter] = new Array(); } 
                                            daysArray[counter].push(entry);
                                       }
                                       
                                    });
                                    //console.log(daysArray);
                                    //console.log('length', daysArray.length);
                                    if(to === null && dateConfig == 'datepick') {
                                        to = findMaxDate(from);
                                        //console.log(from);
                                    }
                                    for(i = 1; i < daysArray.length; i++) {                                        

                                        b = new Date(daysArray[i][0][1]);
                                        curr = moment().year(b.getFullYear()).month(b.getMonth()).date(b.getDate());
                                        diff = moment().year(to.getFullYear()).month(to.getMonth()).date(to.getDate() - 1).diff(curr, 'days');
                                        t = 14 - diff;
                                        //console.log(curr);
                                        //console.log(diff);
                                        //console.log('t',t);
                                        index = $scope["dailyview"+t];
                                        $scope["date"+t] = new Date(daysArray[i][0][1]);

                                        //index.series[0].data = []; //wipe all data from the series array
                                        index.xAxis.min = new Date(new Date(daysArray[i][0][1]).setHours(0,0,0,0)).getTime();
                                        index.xAxis.max = new Date(new Date(daysArray[i][0][1]).setHours(23,59,59,0)).getTime();
                                        index.yAxis.plotBands[0].from = dailyLowerObjective;
                                        index.yAxis.plotBands[0].to = dailyUpperObjective;
                                        index.yAxis.plotLines[0].value = dailyLowerObjective;
                                        index.yAxis.plotLines[1].value = dailyUpperObjective;                                            

                                        daysArray[i].forEach(function(entry) {
                                            b = new Date(entry[1]);
                                            curr = moment().year(b.getFullYear()).month(b.getMonth()).date(b.getDate());
                                            diff = moment().year(to.getFullYear()).month(to.getMonth()).date(to.getDate() - 1).diff(curr, 'days');
                                            //console.log('diff:', diff);

                                            index.series[0].data.push([entry[1], Number(entry[0])]);
                                            //if(Number(entry[0]) > index.yAxis.max) { index.yAxis.max = Number(entry[0]).toFixed(0); }

                                        });
                                        //console.log(index.series);
                                    }
                                }
                            }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type: 'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                            });
                        });

                    } else {
                        $scope.matrixChart.series = [];
                        $scope.chart.series = [];
                        $scope.$parent.buildList({ type: 'glycaemia' }, function(data) {
                            $scope.list = data;
                            //console.log(data);
                        });
                    }
                }
                // First build
                $scope.build();
            }
        }
    });