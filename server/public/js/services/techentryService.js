angular.module('techentryService', []).factory('techentry', function($http) {
    return {
        settechentry: function(user, array, array2) {
            return $http.post('/api/techentry/'+user.username+'/addtechentry/'+angular.toJson(array)+'/deltechentry/'+angular.toJson(array2));
        },
        gettechentry: function(user) {
             return $http.get('/api/techentry/'+user.username+'/gettechentry/');
        }
    }
});