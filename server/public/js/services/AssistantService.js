angular.module('AssistantService', []).factory('Assistant', function($http) {
    return {
        getAdvice: function(user, choice) {
            return $http.get('/api/advice/'+user.userID+'/choice/'+choice);
        },
        sayHello: function(user, type) {
            return "HELLO, WORLD!";
        },
        getHello: function(user, choice) {
            return "HELLO, WORLD! for user "+user.userID+" with choice "+choice;
            //return $http.get('/api/advice/'+user.userID+'/choice/'+choice);
        },
        getHello2: function(user, choice) {
            return $http.get('/api/advice/'+user.userID+'/choice/'+choice);
        },
        listByPatientByType: function(user, type) {
            return $http.get('/api/patients/'+user.userID+'/entries/'+type);
        },
        
        list: function(user) {
            return $http.get('/api/patients/'+user.userID+'/entries/');
        },
        
        listByDate: function(user, daterange) {
            return $http.get('/api/patients/'+user.userID+'/entries/from/'+daterange.from+'/to/'+daterange.to);
        },
        
        listByTypeByDate: function(user, type, daterange) {
            return $http.get('/api/patients/'+user.userID+'/entries/'+type+'/from/'+daterange.from+'/to/'+daterange.to);
        },
        
        create: function(entry) {
            return $http.post('/api/entries', entry);
        }
    }
});
//TODO