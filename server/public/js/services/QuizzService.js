angular.module('QuizzService', []).factory('Quizz', function($http) {

    var allQuizzes = [];
    var quizzLength = 1;
    var quizzesBeingDoneIds = [];
    var nonCompletedQuizzesIds = [];
    var successfulQuizzesIds = [];
    var score = 0;
    var allQuizzMode = true;

    return {
        list: function() {
            return $http.get('/api/quizzs');
        },
        read: function(id) {
            return $http.get('/api/quizzs/' + id);
        },
        bookmark: function(quizz) {
            return $http.put('/api/quizzs/bookmarks', quizz);
        },
        getAllQuizzes: function() {
            return allQuizzes;
        },
        setQuizzes: function(quizzes) {
            allQuizzes = quizzes;
        },
        getNonCompletedQuizzes: function() {
            var iMax = nonCompletedQuizzesIds.length;
            var quizzes = [];

            for (var i = 0; i < iMax; ++i) {
                quizzes.push(allQuizzes[nonCompletedQuizzesIds[i]]);
            }

            return quizzes;
        },
        setNonCompletedQuizzes: function(quizzes) {
            nonCompletedQuizzesIds = [];
            var iMax = allQuizzes.length;
            for (var i = 0; i < iMax; ++i) {
                var jMax = quizzes.length;
                for (var j = 0; j < jMax; ++j) {
                    if (quizzes[j].id === allQuizzes[i].id) {
                        nonCompletedQuizzesIds.push(i);
                    }
                }
            }
        },
        getSuccessfulQuizzes: function() {
            var iMax = successfulQuizzesIds.length;
            var quizzes = [];

            for (var i = 0; i < iMax; ++i) {
                quizzes.push(allQuizzes[successfulQuizzesIds[i]]);
            }

            return quizzes;
        },
        setSuccessfulQuizzes: function(quizzes) {
            successfulQuizzesIds = [];
            var iMax = allQuizzes.length;
            for (var i = 0; i < iMax; ++i) {
                var jMax = quizzes.length;
                for (var j = 0; j < jMax; ++j) {
                    if (quizzes[j].id === allQuizzes[i].id) {
                        successfulQuizzesIds.push(i);
                    }
                }
            }
        },
        getQuizzesBeingDone: function() {
            var iMax = quizzesBeingDoneIds.length;
            var quizzes = [];

            for (var i = 0; i < iMax; ++i) {
                quizzes.push(allQuizzes[quizzesBeingDoneIds[i]]);
            }

            return quizzes;
        },
        setQuizzesBeingDone: function(quizzes) {
            quizzesBeingDoneIds = [];
            var iMax = allQuizzes.length;
            for (var i = 0; i < iMax; ++i) {
                var jMax = quizzes.length;
                for (var j = 0; j < jMax; ++j) {
                    if (quizzes[j].id === allQuizzes[i].id) {
                        quizzesBeingDoneIds.push(i);
                    }
                }
            }
        },
        getScore: function() {
            return score;
        },
        setScore: function(s) {
            score = s;
        },
        refreshScore: function() {
            var score = 0;

            // Sort achieved and not achieved questions
            for (var i = 0; i < allQuizzes.length; i++) {
                if (allQuizzes[i].bookmarked) {
                    score++;
                }
            }
            // Global score
            score = ((score / allQuizzes.length) * 100).toFixed(0);
            return score;
        },
        getQuizzMode: function() {
            return allQuizzMode;
        },

        setQuizzMode: function(isModeOn) {
            allQuizzMode = isModeOn;
        },

        setQuizzLength: function(length) {
            quizzLength = length;
        },

        getQuizzLength: function() {
            return quizzLength;
        },

        getProgressPercent: function() {
            if (allQuizzMode=='All') {
                var lenCUR = quizzesBeingDoneIds.length;
            } else {
                var lenCUR = nonCompletedQuizzesIds.length;
            }
            return ((quizzLength - lenCUR) * 100 / (quizzLength)).toFixed(1);

        }


    };
});