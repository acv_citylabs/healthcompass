angular.module('GlycaemiaService', []).factory('Glycaemia', function($http, $log) {
    return {
        getGlyStats: function(lowerb, upperb, timebound, hourlowerb, hourupperb) {
            return $http.get('/api/glystats/' + lowerb + '/' + upperb + '/' + timebound + '/' + hourlowerb + '/' + hourupperb);
        },
        getGlyStatsDoctor: function(stat) {
        	return $http.get('/api/doctorglystats/' + stat.username + '/' + stat.lowerb + '/' + stat.upperb + '/' + stat.timebound + '/' + stat.hourlowerb + '/' + stat.hourupperb);
        }
    }
});