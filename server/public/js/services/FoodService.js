angular.module('FoodService', []).factory('Food', function($http) {
    return {
        searchFood: function(food) {
        	return $http.get('/api/meal/getfood/' + food.searchterm);
        },
        saveMeal: function(food) {
        	return $http.post('/api/meal/save/' + angular.toJson(food));
        },
        getMealStats: function(food) {
        	return $http.get('/api/meal/stats/from/' + food.from + '/to/' + food.to);
        },
        delete: function(meal) {
        	return $http.delete('/api/meal/id/' + meal.id);
        },
        getMealProducts(products) {
            return $http.get('/api/meal/getproducts/' + angular.toJson(products));
        }
    }
});