angular.module('ObjectivesService', []).factory('Objective', function($http) {
    return {
		
		// Doctor side
		setObjectiveForUser: function(username, array) {
            return $http.post('/api/objectives/setObjective/'+angular.toJson(array)+'/for/'+username);
		},
        setObjectives: function(username, array, array2) {
            return $http.post('/api/objectives/'+username+'/addObjectives/'+angular.toJson(array)+'/delObjectives/'+angular.toJson(array2));
        },
        getObjectives: function(username) {
             return $http.get('/api/objectives/'+username+'/getObjectives/');
        },
		
		// Patient side
        setObjective: function(array) {
			//console.log('/api/objectives/setObjective/'+angular.toJson(array));
            return $http.post('/api/objectives/setObjective/',array);
        },

        getObjective: function(select) {
            return $http.get('/api/objectives/getObjective/'+select);
        },
		getSpline: function(points, tension, segments){
			return $http.get('/api/objectives/getSpline/'+points+'/'+tension+'/'+segments);
		},
		objAtDate: function(type, date){
			return $http.get('/api/objectives/objAtDate/'+date+'/ofType/'+type);
		}

    }
});