angular.module('EntryService', []).factory('Entry', function($http, $log) {

    /* Default - Recommended from ADA - glycemic targets */
    var optGlyMin = 80;
    var optGlyMax = 130;
    var postPrandGlyMax = 180;
    var corrHyperTreshold = optGlyMax;
    var corrHyperFactor;
    // If suggestBcEntru == true, AddCtrl will retrieve data calculated from Bolus Calculator (EstimateCtrl)
    var suggestBcEntry = false;
    var recommendedBolus;

    return {
        list: function(config) {
            $log.debug('/api/entries/type/' + config.type + '/subtype/' + config.subType);
            return $http.get('/api/entries/type/' + config.type + '/subtype/' + config.subType);

        },

        listPatient: function(config) {
            return $http.get('/api/patiententries/type/' + config.type + '/username/' + config.username);
        },

        last: function(config) {
            return $http.get('/api/entries/last/type/' + config.type + '/subtype/' + config.subType);
        },

        create: function(entry) {
            return $http.post('/api/entries', entry);
        },

        delete: function(entry) {
            return $http.delete('/api/entries/' + entry.id);
        },

        setGlyTarget: function(type, val) {
            // $log.debug("EntryService.js - setGlyTarget - val :", val);
            switch (type) {
                case 'min':
                    optGlyMin = val;
                    break;
                case 'max':
                    optGlyMax = val;
                    break;
                case 'postprand':
                    postPrandGlyMax = val;
                    break;
                default:
                    break;
            };
        },

        setHyperCorrParam: function(type, val) {
            // $log.debug("EntryService.js - setCorrParam - val :", val);
            switch (type) {
                case 'factor':
                    corrHyperFactor = val;
                    break;
                case 'tresh':
                    if (val >= optGlyMax) {
                        corrHyperTreshold = val;
                    } else {
                        val = optGlyMax;
                    }
                    break;
                default:
                    break;
            };
        },

        setRecommendedBolus: function(boltotal) {
            // $log.debug("EntryService - setRecommendedBolus - value :", boltotal);
            if (boltotal == -1) {
                suggestBcEntry = false;
            } else {
                suggestBcEntry = true;
                recommendedBolus = boltotal;
            }
        },

        getGlyTargets: function() {
            return [optGlyMin, optGlyMax, postPrandGlyMax];
        },

        getHyperCorrParams: function() {
            return [corrHyperFactor, corrHyperTreshold];
        },

        getRecommendedBolus: function() {
            var ret = suggestBcEntry ? recommendedBolus : -1;
            // $log.debug("EntryService - getRecommendedBolus - value :", ret);
            return ret;
        },
        
        filterEntries: function(config) {
            return $http.get('/api/entries/type/' + angular.toJson(config.type) + '/subtype/' + config.subType + '/from/' + config.from + '/to/' + config.to);
        }

    }
});