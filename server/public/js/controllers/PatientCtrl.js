angular.module('PatientCtrl', [
        [
            'bower_components/highcharts/highcharts.js',
            'css/templates/dashboard.css'
        ]
    ]).controller('PatientController', function($log, $scope, $state, $stateParams, gettextCatalog, $ocLazyLoad, $injector, $filter, $rootScope, ModalService) {

        // Patient username
        $scope.username = $stateParams.username;        
        
        if ($scope.username !== "") {
            
            $ocLazyLoad.load('js/services/ContactService.js').then(function() {
                var Contact = $injector.get('Contact');

                // Read the profile of the patient
                Contact.read({
                    username: $scope.username
                }).success(function(data) {
                    $scope.cards = ['profile', 'medicalRecord'];

                    // Add glycaemia chart if patient condition is diabetes 
                    // if(data.condition === 'd1' || data.condition === 'd2'){
                    $scope.cards.push('chart_glycaemia');
                    // }

                    // Parse birth date of patient
                    if (data.birthdate !== undefined) {
                        data.birthdate = $filter('ddMMyyyy')(data.birthdate);
                    } else {
                        data.birthdate = gettextCatalog.getString('unknown');
                    }

                    // Add it to the scope
                    $scope.profile = data;                    
                    $scope.patientName = data.firstname + ' ' + data.lastname;
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });

            });

            $ocLazyLoad.load('js/services/GlycaemiaService.js').then(function() {
                var Stats = $injector.get('Glycaemia');
                var statsALL, stats14D, stats7D;

                $scope.getGlyTrends = function(delay) {
                    // Reference statistics = 2 months
                    // Reference Bounds according to American Diabetes Association : 80-180 mg/dL
                    // We should ideally set them according to the user and physician.
                    Stats.getGlyStatsDoctor({
                        username: $scope.username,
                        timebound: 56,
                        lowerb: 80,
                        upperb: 180,
                        hourlowerb: 0,
                        hourupperb: 23
                    }).success(function(statsREF) {
                        // statsREF
                        $scope.stats = statsREF;
                        $log.debug("Stats: ", $scope.stats);
                        Stats.getGlyStatsDoctor({
                            username: $scope.username,
                            timebound: delay,
                            lowerb: 80,
                            upperb: 180,
                            hourlowerb: 0,
                            hourupperb: 23
                        }).success(function(statsDELAY) {

                            $scope.trends = {};
                            for (var key in statsREF) {
                                if (statsREF.hasOwnProperty(key)) {
                                    // Compute trends
                                    var temp = ((statsDELAY[key] - statsREF[key]) / statsREF[key]).toFixed(1);
                                    // Eliminate NaN
                                    if (isNaN(temp)) {
                                        $scope.trends[key] = 0;
                                    } else {
                                        $scope.trends[key] = Number(temp);
                                    }
                                }

                            }
                            /* $scope.trends = {
                                 glymean: ((statsDELAY.glymean - statsREF.glymean) / statsREF.glymean).toFixed(1),
                                 hypoperc: ((statsDELAY.hypoperc - statsREF.hypoperc) / statsREF.hypoperc).toFixed(1),
                                 hyperperc: ((statsDELAY.hyperperc - statsREF.hyperperc) / statsREF.hyperperc).toFixed(1),
                                 glyvar: ((statsDELAY.glyvar - statsREF.glyvar) / statsREF.glyvar).toFixed(1),
                                 risk: ((statsDELAY.risk - statsREF.risk) / statsREF.risk).toFixed(1)
                             } */

                            $log.debug("Trends: ", $scope.trends);

                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });

                        });

                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            priority: 2
                        });

                    });
                }

                $scope.getGlyTrends(7);

            });

           $ocLazyLoad.load(['js/services/GlycHemoglobinService.js', 'js/services/GlycaemiaService.js']).then(function() {
                var GlycHemoglobin = $injector.get('GlycHemoglobin');
                // Retrieve the glycated hemoglobin
                GlycHemoglobin.computeValueDoctor({ username: $scope.username }).success(function(data) {
                    // Add it to the scope
                    $scope.glycHemo = data;
                    // Get number of days the value is computed on
                    $scope.nbDays = new Date().getTime();
                    $scope.nbDays = $scope.nbDays - (new Date($scope.glycHemo.start).getTime());
                    $scope.nbDays = parseInt($scope.nbDays / (1000 * 60 * 60 * 24));
                    // NGSP = [0.09148 * IFCC] + 2.152)
                    $scope.IFCCValue = (($scope.glycHemo.value - 2.152) / 0.09148).toFixed(2);
                    $scope.isGood = data.isGood;

                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });
            });
            $scope.unit = 'mg/dL'
        } else {
            $state.go("home.dashboard.main");
        }

        // Delete an entry
        $scope.deleteEntry = function(entry, callback) {
            ModalService.showModal({
                templateUrl: "templates/modals/deleteEntry.html",
                controller: function($scope, close) {
                    $scope.entry = entry;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if (result) {
                        $ocLazyLoad.load('js/services/EntryService.js').then(function() {
                            var Entry = $injector.get('Entry');
                            Entry.delete({ id: entry._id }).success(function(data) {
                                $rootScope.rootAlerts.push({
                                    type: 'success',
                                    msg: gettextCatalog.getString("The entry has been removed"),
                                    priority: 5
                                });
                                // Rebuild dashboard
                                //$scope.build();
                            }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type: 'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                            });
                        });
                    }
                });
            });
        }

        //Build a list
        $scope.buildList = function(config, callback) {
            $ocLazyLoad.load('js/services/EntryService.js').then(function() {
                var Entry = $injector.get('Entry');
                Entry.listPatient({
                    type: config.type,
                    username: $scope.username
                }).success(function(data) {
                    callback(data);
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                    callback(null);
                });
            });
        };
    }).controller('CardPatientChartGlycaemiaController', function($log, $scope, gettextCatalog, $ocLazyLoad, $injector, $filter, $window, $rootScope) {
        $ocLazyLoad.load('additional_components/highcharts-ng/dist/highcharts-ng.min.js').then(function() {

            $scope.view = '';
            $scope.unit = 'mg/dl';

            // Create a basic Highcharts configuration
            var aChart = {
                options: {
                    chart: {},
                    tooltip: {
                        enabled: true
                    },
                    legend: {
                        enabled: false
                    },
                    plotOptions: {
                        series: {
                            stacking: '',
                            dataLabels: {
                                enabled: false
                            }
                        }
                    },
                },
                series: [
                    { data: [] }
                ],
                title: {
                    text: ''
                },
                xAxis: {
                    labels: {},
                    title: {
                        text: ''
                    }
                },
                yAxis: {
                    plotLines: [{
                        color: '#000',
                        dashStyle: 'Dash',
                        width: 1,
                        zIndex: 99,
                        label: {
                            x: 0
                        }
                    }],
                    title: {
                        text: ''
                    },
                    stackLabels: {
                        style: {
                            color: '#D32F2F'
                        },
                        enabled: false,
                        align: 'right'
                    },
                    formatter: function() {}
                },
                noData: gettextCatalog.getString('No Data'),
                size: {
                    height: ''
                }
            };

            // Global configuration
            Highcharts.setOptions({ global: { useUTC: false } });

            // Copy the basic configuration
            $scope.chart = JSON.parse(JSON.stringify(aChart));

            // Define chart type
            $scope.chart.options.chart.type = 'scatter';

            // Create a personalized tooltip
            $scope.chart.options.tooltip.formatter = function() {
                var origdate = $filter('smartDatetime')(this.point.origdate);
                var time = $filter('HHmm')(new Date(this.x).toISOString());
                return time + ' (' + origdate + ')<br><span style="color:#D32F2F;">●</span>  <b>' + this.y + '</b> ' + $scope.unit;
            }

            // Define X axis
            $scope.chart.xAxis.type = 'datetime';
            $scope.chart.xAxis.labels = {
                formatter: function() {
                    return Highcharts.dateFormat('%H', this.value);
                },
                overflow: 'justify'
            };
            $scope.chart.xAxis.tickInterval = 3600 * 1000;

            // Define Y axis
            $scope.chart.yAxis.title.text = $scope.unit;

            // Objective lines
            $scope.chart.yAxis.plotLines[0].label.text = 'max';
            $scope.chart.yAxis.plotLines[0].value = 200;

            $scope.chart.yAxis.plotLines.push({
                value: 0,
                color: 'blue',
                dashStyle: 'ShortDot',
                width: 2,
                zIndex: 99,
                label: {
                    x: 0
                }
            });
            $scope.chart.yAxis.plotLines[1].label.text = 'min';
            $scope.chart.yAxis.plotLines[1].value = 80;
            $scope.chart.yAxis.plotLines[0].color = 'blue';
            $scope.chart.yAxis.plotLines[0].width = 2;

            $scope.chart.yAxis.plotLines[0].dashStyle = 'ShortDot';



            // Mean line
            $scope.chart.yAxis.plotLines.push({
                value: 0,
                color: '#D32F2F',
                dashStyle: 'ShortDot',
                width: 2,
                zIndex: 99,
                label: {
                    x: 0,
                    style: { color: '#D32F2F' }
                }
            });


            // Size
            if ($window.innerWidth < 535) {
                $scope.chart.size.height = 250;
            }

            // Build the chart
            $scope.build = function(view) {
                var from;
                switch (view) {
                    case '2m':
                        $scope.view = '2m';
                        from = new Date(new Date().setDate(new Date().getDate() - 60));
                        break;
                    case '3m':
                        $scope.view = '3m';
                        from = new Date(new Date().setDate(new Date().getDate() - 90));
                        break;
                    case '6m':
                        $scope.view = '6m';
                        from = new Date(new Date().setDate(new Date().getDate() - 180));
                        break;
                    default:
                        $scope.view = '';
                        from = new Date(new Date().setDate(new Date().getDate() - 30));
                        break;
                }

                $ocLazyLoad.load('js/services/PatientService.js').then(function() {
                    var Patient = $injector.get('Patient');
                    Patient.chart({
                        username: $scope.$parent.username,
                        type: 'glycaemia',
                        from: from,
                        to: new Date
                    }).success(function(data) {
                        $scope.chart.yAxis.plotLines[2].value = data.mean;
                        $scope.chart.yAxis.plotLines[2].label.text = "";
                        $scope.chart.yAxis.plotLines[2].label.text = $scope.chart.yAxis.plotLines[2].label.text + " (" + $filter('number')(data.mean, 1) + $scope.unit + ")";
                        $scope.chart.series = data.series;
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            priority: 2
                        });
                    });
                });
            }

            // First build
            $scope.build();
        });
    })
    .controller('CardMedicalRecordController', function($scope, $ocLazyLoad, $injector, $rootScope) {
        $ocLazyLoad.load('js/services/PatientService.js').then(function() {
            var Patient = $injector.get('Patient');

            // Read the patient medical record 
            Patient.medicalRecord({
                username: $scope.$parent.username
            }).success(function(data) {
                $scope.patient = data;
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type: 'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                    priority: 2
                });
            });
        });
    });