angular.module('AchievementsCtrl', [
    [
        'css/templates/dashboard.css',
        'css/templates/main.css'
    ]
]).controller('AchievementsController', function($scope, $state, $window, gettextCatalog, $ocLazyLoad, $injector, $stateParams, ModalService) {
    $ocLazyLoad.load('js/services/QuizzService.js').then(function() {

         $scope.achievements = {
            quizzmodules: [
                { name: gettextCatalog.getString('Alimentation'),score:70},
                { name: gettextCatalog.getString('Medication'), score:0},
                { name: gettextCatalog.getString('Functional Insulinotherapy'),score:30 },
                { name: gettextCatalog.getString('Physical Activity'),score:0 }
            ],
            challenges: [
                { name: gettextCatalog.getString('Fruit ninja'),score:'Lvl 1 '},
                { name: gettextCatalog.getString('Monitoring master'), score:'Lvl 3 '},
                { name: gettextCatalog.getString('Athlete'),score: 'Lvl 2 '}
            ],

        };
    });
});
