angular.module('ObjectiveCtrl', [
        [
            'css/templates/dashboard.css'
        ]
    ])
    .controller('ObjectiveController', function($scope, $log, gettextCatalog, $ocLazyLoad, $injector, $stateParams, $state, $rootScope, $window, ModalService) {
        $ocLazyLoad.load('js/services/ObjectivesService.js').then(function() {
            // In case of long windows and small-screens devices, allows to 'reinitialize' the scroll to the top of the page.
            $window.scrollTo(0, 0);
            //console.log($stateParams);

            var Objective = $injector.get('Objective');
            switch ($stateParams.card) {
                case 'glycaemia':
                    $scope.config = {
                        name: 'glycaemia',
                        objective: gettextCatalog.getString("Objective"),
                        title: gettextCatalog.getString("Glycaemia"),
                        unit: 'mg/dl',
                        placeholder: gettextCatalog.getString("Value"),
                        values: [
                            { name: gettextCatalog.getString("Min"), type: 'min' },
                            { name: gettextCatalog.getString("Max"), type: 'max' }
                        ]
                    };
                    break;
                case 'glycaemiaPatient':
                    $scope.config = {
                        name: 'glycaemia',
                        objective: gettextCatalog.getString("Objective"),
                        title: gettextCatalog.getString("Glycaemia"),
                        unit: 'mg/dl',
                        placeholder: gettextCatalog.getString("Value"),
                        values: [
                            { name: gettextCatalog.getString("Min"), type: 'min' },
                            { name: gettextCatalog.getString("Max"), type: 'max' }
                        ],
                        role: 'physician'
                    };
                    break;
                case 'insulin':
                    $scope.config = {
                        name: 'insulin',
                        objective: gettextCatalog.getString("Settings"),
                        title: gettextCatalog.getString("Insulin"),
                        types: [
                            { name: gettextCatalog.getString('Rapid') + " (2-5h)", value: 'rapid' },
                            { name: gettextCatalog.getString('Regular') + " (5-7h)", value: 'short' },
                            { name: gettextCatalog.getString('Mixed') + " (10-12h)", value: 'mixed' },
                            { name: gettextCatalog.getString('Intermediate') + " (10-12h)", value: 'intermediate' },
                            { name: gettextCatalog.getString('Long') + " (24h)", value: 'long' }
                        ],
                        values: [
                            { name: gettextCatalog.getString('Morning'), when: 'morning', typev: 'schema' },
                            { name: gettextCatalog.getString('Midday'), when: 'midday', typev: 'schema' },
                            { name: gettextCatalog.getString('Evening'), when: 'evening', typev: 'schema' },
                            { name: gettextCatalog.getString('Bedtime'), when: 'bedtime', typev: 'schema' },
                            { name: gettextCatalog.getString('Breakfast'), when: 'breakfast', typev: 'sensibility' },
                            { name: gettextCatalog.getString('Lunch'), when: 'lunch', typev: 'sensibility' },
                            { name: gettextCatalog.getString('Dinner'), when: 'dinner', typev: 'sensibility' },
                            { name: gettextCatalog.getString('Factor'), when: 'factor', typev: 'correction' },
                            { name: gettextCatalog.getString('Treshold'), when: 'tresh', typev: 'correction' },

                            // when : '' corresponds to subType
                            { name: gettextCatalog.getString('Min'), when: 'min', typev: 'bgtarget' },
                            { name: gettextCatalog.getString('Max'), when: 'max', typev: 'bgtarget' },
                            { name: gettextCatalog.getString('Prandial Max'), when: 'postprand', typev: 'bgtarget' }
                        ]
                    };
                    break;
                case 'insulinPatient':
                    $scope.config = {
                        name: 'insulin',
                        objective: gettextCatalog.getString("Settings"),
                        title: gettextCatalog.getString("Insulin"),
                        types: [
                            { name: gettextCatalog.getString('Rapid') + " (2-5h)", value: 'rapid' },
                            { name: gettextCatalog.getString('Regular') + " (5-7h)", value: 'short' },
                            { name: gettextCatalog.getString('Mixed') + " (10-12h)", value: 'mixed' },
                            { name: gettextCatalog.getString('Intermediate') + " (10-12h)", value: 'intermediate' },
                            { name: gettextCatalog.getString('Long') + " (24h)", value: 'long' }
                        ],
                        values: [
                            { name: gettextCatalog.getString('Morning'), when: 'morning', typev: 'schema' },
                            { name: gettextCatalog.getString('Midday'), when: 'midday', typev: 'schema' },
                            { name: gettextCatalog.getString('Evening'), when: 'evening', typev: 'schema' },
                            { name: gettextCatalog.getString('Bedtime'), when: 'bedtime', typev: 'schema' },
                            { name: gettextCatalog.getString('Breakfast'), when: 'breakfast', typev: 'sensibility' },
                            { name: gettextCatalog.getString('Lunch'), when: 'lunch', typev: 'sensibility' },
                            { name: gettextCatalog.getString('Dinner'), when: 'dinner', typev: 'sensibility' },
                            { name: gettextCatalog.getString('Factor'), when: 'factor', typev: 'correction' },
                            { name: gettextCatalog.getString('Treshold'), when: 'tresh', typev: 'correction' },

                            // when : '' corresponds to subType
                            { name: gettextCatalog.getString('Min'), when: 'min', typev: 'bgtarget' },
                            { name: gettextCatalog.getString('Max'), when: 'max', typev: 'bgtarget' },
                            { name: gettextCatalog.getString('Prandial Max'), when: 'postprand', typev: 'bgtarget' }
                        ],
                        role: 'physician'
                    };
                    break;
                case 'sport':
                    $scope.config = {
                        name: 'sport',
                        objective: gettextCatalog.getString("Objective"),
                        title: gettextCatalog.getString("Sport"),
                        unit: gettextCatalog.getString("h/week"),
                        value: { name: gettextCatalog.getString("Duration"), value: '' },
                        placeholder: gettextCatalog.getString("Value"),
                    };
                    break;
                case 'weight':
                    $scope.config = {
                        name: 'weight',
                        objective: gettextCatalog.getString("Objective"),
                        title: gettextCatalog.getString("Weight"),
                        unit: 'kg',
                        value: { name: gettextCatalog.getString("Weight"), value: '' },
                        placeholder: gettextCatalog.getString("Value")
                    };
                    break;
                case 'meal':
                    $scope.config = {
                        name: 'meal',
                        objective: gettextCatalog.getString("Monitoring"),
                        title: gettextCatalog.getString("Meal"),
                        help: gettextCatalog.getString("The feature Diet Monitoring helps you to eat well"),
                        help2: "Set food nutrients objectives",
                        values: [
                                { name: "Calories", unit: "kcal", type: "calories", value: 2000 },
                                { name: "Sugar", unit: "grams", type: "sugar", value: 15 },
                                { name: "Fats", unit: "grams", type: "fats", value: 50 },
                                { name: "Sodium", unit: "grams", type: "sodium", value: 10 }
                        ]
                    };
                    $scope.position = true;
                    break;
                case 'steps':
                    $scope.config = {
                        name: 'steps',
                        objective: gettextCatalog.getString("Objective"),
                        title: gettextCatalog.getString("Number of steps"),
                        unit: gettextCatalog.getString("Steps"),
                        value: { name: gettextCatalog.getString("Number of steps"), value: '' },
                        placeholder: gettextCatalog.getString("Value")
                    };
                    break;
                case 'salt':
                    $scope.config = {
                        name: 'salt',
                        objective: gettextCatalog.getString("Max"),
                        title: gettextCatalog.getString("Salt"),
                        unit: 'mg',
                        value: { name: gettextCatalog.getString("Salt"), value: '' },
                        placeholder: gettextCatalog.getString("Value")
                    };
                    break;
                case 'liquid':
                    $scope.config = {
                        name: 'liquid',
                        objective: gettextCatalog.getString("Max"),
                        title: gettextCatalog.getString("Liquid"),
                        unit: 'ml',
                        value: { name: gettextCatalog.getString("Liquid"), value: '' },
                        placeholder: gettextCatalog.getString("Value")
                    };
                    break;
            }

            var ServiceObjective;
            if($scope.config.role === 'physician') {
                ServiceObjective = Objective.getObjectives($stateParams.user);
            } else {
                ServiceObjective = Objective.getObjective($scope.config.name);
            }
            ServiceObjective.success(function(data) {
                //console.log("DATA", data);
                //console.log($scope.config);
                if (data) {
                    switch ($stateParams.card) {
                        case 'insulin':
                            for (i = 0; i < data.values.length; i++) {
                                for (j = 0; j < $scope.config.values.length; j++) {
                                    if (data.values[i].subType === $scope.config.values[j].when) {
                                        $scope.config.values[j].type = data.values[i].type;
                                        $scope.config.values[j].value = data.values[i].value;
                                    }
                                }
                            }
                            break;
                        case 'insulinPatient':
                            for(var a = 0; a < data.length; a++) {
                                if(data[a].select === 'insulin') {
                                    for(i = 0; i < data[a].values.length; i++) {
                                        for (j = 0; j < $scope.config.values.length; j++) {
                                            if (data[a].values[i].subType === $scope.config.values[j].when) {
                                                $scope.config.values[j].value = data[a].values[i].value;
                                                $scope.config.values[j].value = data[a].values[i].value;
                                            }
                                        }
                                    }
                                    break;
                                }
                            }
                            break;
                        case 'glycaemia':
                            for (i = 0; i < data.values.length; i++) {
                                for (j = 0; j < $scope.config.values.length; j++) {
                                    if (data.values[i].type === $scope.config.values[j].type) {
                                        $scope.config.values[j].value = data.values[j].value;
                                    }
                                }
                            }
                            break;
                        case 'glycaemiaPatient':
                            for (i = 0; i < data.length; i++) {
                                for (j = 0; j < $scope.config.values.length; j++) {                                    
                                    if (data[i].values[j] && data[i].values[j].type === $scope.config.values[j].type) {
                                        $scope.config.values[j].value = data[i].values[j].value;
                                    }
                                }
                            }
                            break;
                        case 'meal':
                            $scope.position = true;
                            for (i = 0; i < data.values.length; i++) {
                                for (j = 0; j < $scope.config.values.length; j++) {                                    
                                    if (data.values[i].type === $scope.config.values[j].type) {
                                        $scope.config.values[j].value = data.values[i].value;
                                    }
                                }
                            }
                            break;
                        default:
                            $scope.config.value.value = data.values[0].value;
                            break;
                    }
                }
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type: 'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                    priority: 2
                });
            });

            $scope.verify = function() {
                var ok = true;
                var count = 0;

                switch ($stateParams.card) {
                    case 'insulin':
                    case 'insulinPatient':
                        $scope.errors = {
                            valid: false,
                            minmax: false,
                            prandmax: false,
                            sensibility: false,
                            corrtresh: false,
                            notComplete: false,
                            corrfact: false,
                        };

                        for (i = 0; i < $scope.config.values.length; i++) {
                            var val = $scope.config.values[i].value;
                            var subType = $scope.config.values[i].when;
                            var tmp = $scope.config.values[i].typev;
                            var min, max, prandmax;
                            var irbreak, irlunch, irdinner;
                            var factor, corrTresh;

                            // Add gug
                            if (val &&
                                (tmp == 'correction' || tmp == 'sensibility' || tmp == 'bgtarget')) {
                                $scope.config.values[i].type = tmp;
                                count++;

                            }

                            // Glycaemia targets
                            if (val && tmp == 'bgtarget') {
                                switch (subType) {
                                    case 'min':
                                        min = val;
                                        break;

                                    case 'max':
                                        max = val;
                                        break;

                                    case 'postprand':
                                        prandmax = val;
                                        break;
                                }
                                if (min >= max || min < 50 || min > 120 || max < 100 || max > 250) $scope.errors.minmax = true;
                                if (prandmax <= max || prandmax > 300) $scope.errors.prandmax = true;
                                $log.debug("Prandmax: ", prandmax, max, (prandmax <= max || prandmax > 300));
                            }



                            // Insulin-to-carb ratios
                            if (val && tmp == 'sensibility') {
                                if (val < 1 || val > 30) $scope.errors.sensibility = true;
                            }

                            // Correction
                            if (val && tmp == 'correction') {
                                switch (subType) {
                                    case 'tresh':
                                        corrTresh = val;
                                        break;

                                    case 'factor':
                                        factor = val;
                                        break;
                                }
                                if (corrTresh < max || corrTresh > 250) $scope.errors.corrtresh = true;
                                if (factor < 0 || factor > 150) $scope.errors.corrfact = true;
                            }
                        }

                        if (count != 8) {
                            /* Correction, sensibilty and glycemic targets = 8 mandatory parameters */
                            $scope.errors.notComplete = true;
                        }

                        $scope.errors.valid = (!$scope.errors.minmax && !$scope.errors.prandmax && !$scope.errors.notComplete && !$scope.errors.corrfact && !$scope.errors.corrtresh && !$scope.errors.sensibility)
                        $log.debug("Errors :", $scope.errors);
                        return $scope.errors.valid;

                    case 'glycaemia':
                    case 'glycaemiaPatient':
                        if (!$scope.config.values[0].value || !$scope.config.values[1].value) {
                            ok = false;
                        }
                        return ok;
                        break;
                    case 'meal':
                        if(!$scope.config.values[0].value || !$scope.config.values[1].value || !$scope.config.values[2].value || !$scope.config.values[3].value) {
                            ok = false;
                        }
                        return ok;
                    default:
                        if (!$scope.config.value.value) { //this should be value.value ??? YES
                            ok = false;
                        }
                        return ok;
                }
            }

            $scope.filterSensibilities = function(item) {
                if (item.typev == 'sensibility') {
                    return item;
                }
            }

            $scope.filterCorrection = function(item) {
                if (item.typev == 'correction') {
                    return item;
                }
            }

            $scope.filterSchema = function(item) {
                if (item.typev == 'schema') {
                    return item;
                }
            }

            $scope.filterBgTargets = function(item) {
                if (item.typev == 'bgtarget') {
                    return item;
                }
            }

            $scope.showInfoGly = function() {
                ModalService.showModal({
                    templateUrl: "templates/modals/glycemicTargets.html",
                    controller: function($scope, close) {
                        $scope.text = "test";
                        $scope.close = function(result) {
                            close(result, 500); // close, but give 500ms for bootstrap to animate
                        };
                    }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
                        $state.go("home.dashboard.estimateinsu");
                    });
                });
            }

            $scope.deleteObjective = function() {
                //TODO deleteObjective
            };


            $scope.setObjective = function() {
                var values = [];
                if (($stateParams.card === 'glycaemia' || $stateParams.card === 'glycaemiaPatient') && $scope.config.values[0].value > $scope.config.values[1].value) {
                    $rootScope.rootAlerts.push({
                        type: 'warning',
                        msg: gettextCatalog.getString("The minimum cannot be larger than the maximum"),
                        priority: 3
                    });
                } else {
                    switch ($stateParams.card) {
                        case 'insulin':
                        case 'insulinPatient':
                            for (i = 0; i < $scope.config.values.length; i++) {
                                if ($scope.config.values[i].value) {
                                    values.push({ value: $scope.config.values[i].value, type: $scope.config.values[i].type, subType: $scope.config.values[i].when });
                                }
                            }

                            break;
                        case 'glycaemia':
                        case 'glycaemiaPatient':
                            for (i = 0; i < $scope.config.values.length; i++) {
                                if ($scope.config.values[i].value) {
                                    values.push({ value: $scope.config.values[i].value, type: $scope.config.values[i].type });
                                }
                            }
                            break;
                        case 'meal':
                            if ($scope.position) {
                                values.push({ value: 1 });
                                for (i = 0; i < $scope.config.values.length; i++) {
                                    if ($scope.config.values[i].value) {
                                        values.push({value: $scope.config.values[i].value, type: $scope.config.values[i].type});
                                        //console.log($scope.config.values[i].value, $scope.config.values[i].type);
                                    }
                                }
                            } else {
                                //TODO deleteObjective
                            }
                            break;

                        default:
                            values.push({ value: $scope.config.value.value });
                            break;
                    }
                    //console.log(values);
                    var ServiceSetObjective;
                    if($scope.config.role === 'physician') {
                        ServiceSetObjective = Objective.setObjectiveForUser(
                            $stateParams.user, {
                            select: $scope.config.name,
                            type: 'Linear',
                            values: values
                        }).success(function(data) {
                            $state.go("home.patientsagp", {username: $stateParams.user});
                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        });
                    } else {
                        ServiceSetObjective = Objective.setObjective({
                            objective: {
                                select: $scope.config.name,
                                type: 'Linear',
                                values: values
                            }
                        }).success(function(data) {
                            $state.go("home.dashboard.main");
                        }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        });
                    }
                }
            }
        });
    });