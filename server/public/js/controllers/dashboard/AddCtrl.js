angular.module('AddCtrl', [
        [
            'bower_components/pickadate/lib/themes/classic.css',
            'bower_components/pickadate/lib/themes/classic.date.css',
            'bower_components/pickadate/lib/themes/classic.time.css',
            'css/templates/dashboard.css'
        ]
    ])
    .controller('AddController', function($scope, $window, $log, gettextCatalog, $ocLazyLoad, $injector, $stateParams, $state, $rootScope, ModalService, TokenInterceptor, $timeout, Upload) {
        var conditions = TokenInterceptor.decode().condition;

        $window.scrollTo(0, 0);

        $scope.mealTitle = 'Please describe your meal';
        $scope.selectedFood = [];

        // Check if input type 'date' is supported by the current browsers
        $scope.checkDateInput = function() {
            var input = document.createElement('input');
            input.setAttribute('type', 'date');
            var notADateValue = 'not-a-date';
            input.setAttribute('value', notADateValue);
            return !(input.value === notADateValue);
        }

        $scope.more = false;
        $scope.value = undefined;
        $scope.advancedDuration = new Date(new Date().setHours(0, 15, 0, 0));
        $scope.myDate = new Date();

        if (($stateParams.timeslot && new Date() >= new Date().setHours(
                $stateParams.timeslot.from[0],
                $stateParams.timeslot.from[1],
                $stateParams.timeslot.from[2],
                $stateParams.timeslot.from[3]
            ) && new Date() < new Date().setHours(
                $stateParams.timeslot.to[0],
                $stateParams.timeslot.to[1],
                $stateParams.timeslot.to[2],
                $stateParams.timeslot.to[3]
            )) || !$stateParams.timeslot) {
            $scope.myTime = new Date();
        } else {
            $scope.myTime = new Date(new Date().setHours($stateParams.timeslot.suggestion, 0, 0));
        }

        $scope.radios = {
            sport: {
                duration: {
                    value: undefined,
                    levels: [{
                        name: '15',
                        value: '0.25'
                    }, {
                        name: '30',
                        value: '0.5'
                    }, {
                        name: '45',
                        value: '0.75'
                    }, {
                        name: '60',
                        value: '1'
                    }, {
                        name: '90',
                        value: '1.5'
                    }]
                },
                intensity: {
                    value: undefined,
                    levels: [1, 3, 5]
                }
            },
            meal: {
                buttons: [],
                levels: [1, 2, 3, 4, 5]
            },
            mobility: {
                buttons: [{
                    type: 'motor',
                    title: gettextCatalog.getString('Motor vehicule'),
                    value: undefined,
                    icon: 'mdi-maps-directions-car',
                    color: '#F06292',
                }, {
                    type: 'public',
                    title: gettextCatalog.getString('Public transports'),
                    value: undefined,
                    icon: 'mdi-maps-directions-subway',
                    color: '#E91E63',
                }, {
                    type: 'bike',
                    title: gettextCatalog.getString('Bike'),
                    value: undefined,
                    icon: 'mdi-maps-directions-bike',
                    color: '#C2185B',
                }, {
                    type: 'walk',
                    title: gettextCatalog.getString('Walking'),
                    value: undefined,
                    icon: 'mdi-maps-directions-walk',
                    color: '#880E4F',
                }],
                levels: [{
                    name: '15',
                    value: '0.25'
                }, {
                    name: '30',
                    value: '0.5'
                }, {
                    name: '45',
                    value: '0.75'
                }, {
                    name: '60',
                    value: '1'
                }, {
                    name: '90',
                    value: '1.5'
                }]
            }
        };

        var colorFct = function(value) {
            if (value <= 1000)
                return '#d50000';
            if (value <= 3000)
                return '#ff3d00';
            if (value <= 5000)
                return '#ff9100';
            if (value <= 6000)
                return '#ffc400';
            if (value <= 9000)
                return '#ffea00';
            if (value <= 12000)
                return '#c6ff00';
            return '#76ff03';
        };

        $scope.select = function(btnNb, symptom) {
            for (var i = 0; i < $scope.symptoms.length; ++i) {
                if ($scope.symptoms[i] == symptom) {
                    $scope.symptoms[i].buttons[btnNb].img.opacity = 1.0;
                    $scope.symptoms[i].value = $scope.symptoms[i].buttons[btnNb].value;
                    for (var k = 0; k < 3; ++k)
                        if (k != btnNb)
                            $scope.symptoms[i].buttons[k].img.opacity = 0.2;
                }
            }
        };


        if (conditions.indexOf('d1') > -1 || conditions.indexOf('d2') > -1) {
            $scope.radios.meal.buttons.push({
                type: 'slow',
                title: gettextCatalog.getString('Slow sugars'),
                desc: gettextCatalog.getString('bread'),
                img: { 'background': 'url(./img/slow.jpg) no-repeat center center', 'background-size': 'cover' },
                value: undefined,
                score: 2
            });
            $scope.radios.meal.buttons.push({
                type: 'fast',
                title: gettextCatalog.getString('Fast sugars'),
                desc: gettextCatalog.getString('soda'),
                img: { 'background': 'url(./img/fast.jpg) no-repeat center center', 'background-size': 'cover' },
                value: undefined,
                score: 1
            });
            $scope.radios.meal.buttons.push({
                type: 'fats',
                title: gettextCatalog.getString('Fats'),
                desc: gettextCatalog.getString('butter'),
                img: { 'background': 'url(./img/fats.jpg) no-repeat center center', 'background-size': 'cover' },
                value: undefined,
                score: 1
            });
        }

        $ocLazyLoad.load(['js/services/EntryService.js']).then(function() {

            // Pre-filled data, in case of Bolus Calculator Recommmendation
            var Entry = $injector.get('Entry');
            var val = Entry.getRecommendedBolus();
            if (val != -1 && $stateParams.card == 'insulin') {
                $scope.comments = 'BC recommended bolus';
                $scope.value = val;
                $scope.insulinType = 'rapid';
                Entry.setRecommendedBolus(-1);
            };

            /*
            if(conditions.indexOf('hf') > -1){
                $scope.radios.meal.buttons.push({
                    type: 'salt',
                    title: gettextCatalog.getString('Salt'),
                    desc: get<get>


                    </get>textCatalog.getString('salty food'),
                    img: {'background': 'url(./img/salt.jpg) no-repeat center center', 'background-size': 'cover'},
                    value: undefined,
                    score: 1
                });
            }
            */
            switch ($stateParams.card) {
                case 'glycaemia':

                    $scope.config = {
                        name: 'glycaemia',
                        title: gettextCatalog.getString("Glycaemia"),
                        subtitle: gettextCatalog.getString("Glucose level"),
                        input: gettextCatalog.getString("Measured value"),
                        inputPlaceholder: 'mg/dl',
                        commentsPlaceholder: gettextCatalog.getString('e.g.') + ' ' + gettextCatalog.getString('missed meal') + '...'
                    };
                    break;
                case 'insulin':
                    $scope.config = {
                        name: 'insulin',
                        title: gettextCatalog.getString("Insulin"),
                        subtitle: gettextCatalog.getString("Insulin intake"),
                        input: gettextCatalog.getString("Unit(s)"),
                        inputPlaceholder: gettextCatalog.getString('value'),
                        commentsPlaceholder: gettextCatalog.getString('e.g.') + ' ' + gettextCatalog.getString('missed meal') + '...',
                        insulinTypesList: [
                            { name: gettextCatalog.getString('Rapid') + " (2-5h)", value: 'rapid' },
                            { name: gettextCatalog.getString('Short') + " (5-7h)", value: 'short' },
                            { name: gettextCatalog.getString('Mixed') + " (10-12h)", value: 'mixed' },
                            { name: gettextCatalog.getString('Intermediate') + " (10-12h)", value: 'intermediate' },
                            { name: gettextCatalog.getString('Long') + " (24h)", value: 'long' }
                        ]
                    };
                    break;
                case 'sport':
                    $scope.config = {
                        name: 'sport',
                        title: gettextCatalog.getString("Sport"),
                        subtitle: gettextCatalog.getString("Sport"),
                        duration: gettextCatalog.getString("Duration"),
                        intensity: gettextCatalog.getString("Intensity"),
                        commentsPlaceholder: gettextCatalog.getString('e.g.') + ' ' + gettextCatalog.getString('swimming') + '...'
                    };
                    break;
                case 'weight':
                    $scope.config = {
                        name: 'weight',
                        title: gettextCatalog.getString("Weight"),
                        subtitle: gettextCatalog.getString("Weight"),
                        input: gettextCatalog.getString("Measured value"),
                        inputPlaceholder: 'kg',
                        commentsPlaceholder: gettextCatalog.getString('e.g.') + ' ' + gettextCatalog.getString('dressed') + '...'
                    };
                    break;
                case 'bloodpressure':
                    $scope.config = {
                        name: 'bloodpressure',
                        title: gettextCatalog.getString("Blood pressure"),
                        subtitle: gettextCatalog.getString("Blood pressure"),
                        input: gettextCatalog.getString("Measured value"),
                        inputPlaceholder: 'mm Hg',
                        commentsPlaceholder: gettextCatalog.getString('e.g.') + ' ' + '...'
                    };
                    $scope.bp = {
                        diasto: undefined,
                        systo: undefined
                    };
                    break;
                case 'heartrate':
                    $scope.config = {
                        name: 'heartrate',
                        title: gettextCatalog.getString("Heart rate"),
                        subtitle: gettextCatalog.getString("Heart rate"),
                        input: gettextCatalog.getString("Measured value"),
                        inputPlaceholder: 'bpm',
                        commentsPlaceholder: gettextCatalog.getString('e.g.') + ' ' + '...'
                    };
                    $scope.value = 65;
                    $scope.hrType = "atRest";
                    break;
                case 'steps':
                    $scope.config = {
                        name: 'steps',
                        title: gettextCatalog.getString("Number of steps"),
                        subtitle: gettextCatalog.getString("Number of steps"),
                        input: gettextCatalog.getString("Steps taken"),
                        inputPlaceholder: 'nb',
                        sliderOptions: {
                            floor: 0,
                            ceil: 15000,
                            step: 500,
                            showSelectionBar: true,
                            getSelectionBarColor: colorFct,
                            getPointerColor: colorFct,
                            showTicks: true,
                            getTickColor: colorFct
                        },
                        commentsPlaceholder: gettextCatalog.getString('e.g.') + ' ' + gettextCatalog.getString('Uphill') + '...'
                    };
                    $scope.value = 6000;
                    // The range input transforms numbers into strings and then complains
                    // that it has received the wrong type of data...
                    // This is to resolve the problem :
                    $scope.$watch('value', function(val, old) {
                        $scope.value = parseInt(val);
                    });
                    break;
                case 'meal':
                    $scope.config = {
                        name: 'meal',
                        title: gettextCatalog.getString("Meals"),
                        subtitle: gettextCatalog.getString("Meals"),
                        commentsPlaceholder: gettextCatalog.getString('e.g.') + ' ' + gettextCatalog.getString('1 fruit, 1 juice') + '...'
                    };
                    break;
                case 'symptoms':
                    $scope.config = {
                        name: 'symptoms',
                        title: gettextCatalog.getString("Symptoms"),
                        subtitle: gettextCatalog.getString("Symptoms"),
                        commentsPlaceholder: gettextCatalog.getString('e.g.') + ' ' + gettextCatalog.getString('Palpitations the morning') + '...'
                    };
                    var getButtons = function() {
                        return [{
                            title: gettextCatalog.getString('Good'),
                            img: { 'background': 'url(./img/good.png) no-repeat center center', 'background-size': 'cover', 'opacity': '1.0' },
                            value: 0
                        }, {
                            title: gettextCatalog.getString('NotWell'),
                            img: { 'background': 'url(./img/notWell.png) no-repeat center center', 'background-size': 'cover', 'opacity': '1.0' },
                            value: 1
                        }, {
                            title: gettextCatalog.getString('Bad'),
                            img: { 'background': 'url(./img/bad.png) no-repeat center center', 'background-size': 'cover', 'opacity': '1.0' },
                            value: 2
                        }];
                    }

                    $scope.symptoms = [{
                        type: 'dyspnea',
                        title: gettextCatalog.getString('Dyspnea'),
                        buttons: getButtons(),
                        value: -1
                    }, {
                        type: 'sleep',
                        title: gettextCatalog.getString('Sleep'),
                        buttons: getButtons(),
                        value: -1
                    }, {
                        type: 'swellings',
                        title: gettextCatalog.getString('Swellings'),
                        buttons: getButtons(),
                        value: -1
                    }, {
                        type: 'palpitations',
                        title: gettextCatalog.getString('Palpitations'),
                        buttons: getButtons(),
                        value: -1
                    }, {
                        type: 'dizziness',
                        title: gettextCatalog.getString('Dizziness'),
                        buttons: getButtons(),
                        value: -1
                    }, {
                        type: 'fatigue',
                        title: gettextCatalog.getString('Fatigue'),
                        buttons: getButtons(),
                        value: -1
                    }];
                    break;
                case 'mobility':
                    $scope.config = {
                        name: 'mobility',
                        title: gettextCatalog.getString("Mobility"),
                        subtitle: gettextCatalog.getString("Mobility")
                    };
                    break;
                case 'liquid':
                    $scope.config = {
                        name: 'liquid',
                        title: gettextCatalog.getString("Liquid"),
                        subtitle: gettextCatalog.getString("Liquid"),
                        commentsPlaceholder: gettextCatalog.getString('e.g.') + ' ' + gettextCatalog.getString('Drank a lot, it was hot today') + '...'
                    };

                    $scope.liquids = [{
                        name: gettextCatalog.getString("Water / juice"),
                        ml: [200, 300, 500],
                        values: [{ value: 0, img: { 'background': 'url(./img/food/smallGlass.png) no-repeat center center', 'background-size': 'cover' } },
                            { value: 0, img: { 'background': 'url(./img/food/normalGlass.png) no-repeat center center', 'background-size': 'cover' } },
                            { value: 0, img: { 'background': 'url(./img/food/bigGlass.png) no-repeat center center', 'background-size': 'cover' } }
                        ],
                        color: 0
                    }, {
                        name: gettextCatalog.getString("Coffee / tea / milk"),
                        ml: [80, 200, 350],
                        values: [{ value: 0, img: { 'background': 'url(./img/food/smallCup.png) no-repeat center center', 'background-size': 'cover' } },
                            { value: 0, img: { 'background': 'url(./img/food/normalCup.png) no-repeat center center', 'background-size': 'cover' } },
                            { value: 0, img: { 'background': 'url(./img/food/bigCup.png) no-repeat center center', 'background-size': 'cover' } }
                        ],
                        color: 0
                    }, {
                        name: gettextCatalog.getString("Soda"),
                        ml: [250, 330, 500],
                        values: [{ value: 0, img: { 'background': 'url(./img/food/smallCan.png) no-repeat center center', 'background-size': 'cover' } },
                            { value: 0, img: { 'background': 'url(./img/food/normalCan.png) no-repeat center center', 'background-size': 'cover' } },
                            { value: 0, img: { 'background': 'url(./img/food/bigCan.png) no-repeat center center', 'background-size': 'cover' } }
                        ],
                        color: 1
                    }, {
                        name: gettextCatalog.getString("Beer"),
                        ml: [250, 330, 500],
                        values: [{ value: 0, img: { 'background': 'url(./img/food/smallCan.png) no-repeat center center', 'background-size': 'cover' } },
                            { value: 0, img: { 'background': 'url(./img/food/normalCan.png) no-repeat center center', 'background-size': 'cover' } },
                            { value: 0, img: { 'background': 'url(./img/food/bigCan.png) no-repeat center center', 'background-size': 'cover' } }
                        ],
                        color: 2
                    }, {
                        name: gettextCatalog.getString("Wine"),
                        ml: [125, 250, 1000],
                        values: [{ value: 0, img: { 'background': 'url(./img/food/smallWine.png) no-repeat center center', 'background-size': 'cover' } },
                            { value: 0, img: { 'background': 'url(./img/food/normalWine.png) no-repeat center center', 'background-size': 'cover' } },
                            { value: 0, img: { 'background': 'url(./img/food/bigWine.png) no-repeat center center', 'background-size': 'cover' } }
                        ],
                        color: 2
                    }];

                    break;
                case 'salt':
                    $scope.config = {
                        name: 'salt',
                        title: gettextCatalog.getString("Salt"),
                        subtitle: gettextCatalog.getString("Salt"),
                        commentsPlaceholder: gettextCatalog.getString('e.g.') + ' ' + gettextCatalog.getString('Not sure I checked the right boxes') + '...'
                    };

                    $scope.salt = [{
                        category: gettextCatalog.getString("Charcuterie"),
                        values: [{ value: 0, inc: 1, name: gettextCatalog.getString("Cooked ham"), unit: gettextCatalog.getString("Slice(s)"), muted: "" }, { value: 0, inc: 1, name: gettextCatalog.getString("Smoked ham"), unit: gettextCatalog.getString("Slice(s)"), muted: "" }, { value: 0, inc: 1, name: gettextCatalog.getString("Pâté"), unit: gettextCatalog.getString("Slice(s)"), muted: "" }],
                        mg: [500, 1340, 360],
                        expand: false
                    }, {
                        category: gettextCatalog.getString("Dairy Products"),
                        values: [{ value: 0, inc: 1, name: gettextCatalog.getString("Allowed cheeses"), unit: gettextCatalog.getString("Slice(s)"), muted: "(ex : allégés, à tartiner, mozzarella, ricotta, Affligem, Chimay, chèvre, Emmenthal, Gouda jeune, Maredsous, Philadelphia)" }, { value: 0, inc: 1, name: gettextCatalog.getString("Not recommended cheeses"), unit: gettextCatalog.getString("Slice(s)"), muted: "(ex : Babybel, Bleu, Brie, Camembert, Cantal, Comté, feta, fromages vieux, parmesan, Reblochon, Roquefort)" }, { value: 0, inc: 1, name: gettextCatalog.getString("Yoghurt/Fromage blanc"), unit: gettextCatalog.getString("Pot(s)"), muted: "" }],
                        mg: [140, 300, 45],
                        expand: false
                    }, {
                        category: gettextCatalog.getString("Seafood"),
                        values: [{ value: 0, inc: 50, name: gettextCatalog.getString("Canned fish"), unit: gettextCatalog.getString("Gram(s)"), muted: "" }, { value: 0, inc: 50, name: gettextCatalog.getString("Shellfish & crustaceans"), unit: gettextCatalog.getString("Gram(s)"), muted: "" }, { value: 0, inc: 50, name: gettextCatalog.getString("Smoked fish"), unit: gettextCatalog.getString("Gram(s)"), muted: "" }],
                        mg: [4.2, 8, 12.8],
                        expand: false
                    }, {
                        category: gettextCatalog.getString("Dish"),
                        values: [{ value: false, name: gettextCatalog.getString("Homemade dish"), unit: "", muted: "" }, { value: 0, inc: 50, name: gettextCatalog.getString("Frozen dish"), unit: gettextCatalog.getString("Gram(s)"), muted: "" }, { value: 0, inc: 50, name: gettextCatalog.getString("Dish with canned food"), unit: gettextCatalog.getString("Gram(s)"), muted: "" }, { value: 0, inc: 1, name: gettextCatalog.getString("Bread"), unit: gettextCatalog.getString("Slice(s)"), muted: "" }, { value: 0, inc: 1, name: gettextCatalog.getString("Viennoiseries"), unit: "", muted: "" }, { value: 0, inc: 1, name: gettextCatalog.getString("Cereals"), unit: gettextCatalog.getString("Bowl(s)"), muted: "" }],
                        mg: [0, 3.06, 3.06, 310, 270, 410],
                        expand: false
                    }, {
                        category: gettextCatalog.getString("Condiment"),
                        values: [{ value: 0, inc: 1, name: gettextCatalog.getString("Salt"), unit: gettextCatalog.getString("Spoon(s)"), muted: "" }, { value: 0, inc: 1, name: gettextCatalog.getString("Sauces"), unit: gettextCatalog.getString("Spoon(s)"), muted: "(ex : moutarde, mayonnaise,...)" }, { value: 0, inc: 1, name: gettextCatalog.getString("Herb spices"), unit: gettextCatalog.getString("Spoon(s)"), muted: "" }, { value: 0, inc: 1, name: gettextCatalog.getString("Spices powder"), unit: gettextCatalog.getString("Spoon(s)"), muted: "" }, { value: 0, inc: 1, name: gettextCatalog.getString("Stock"), unit: gettextCatalog.getString("Spoon(s)"), muted: "" }],
                        mg: [2000, 130, 1, 1500, 2150],
                        expand: false
                    }];
                    $scope.isNumber = angular.isNumber;

                    break;

                case 'morisky':
                    $scope.config = {
                        name: 'morisky',
                        title: gettextCatalog.getString("Respect du traitement")
                    };
                    break;
            }

            $scope.searchFood = function() {
                $ocLazyLoad.load(['js/services/FoodService.js']).then(function() {
                    var Food = $injector.get('Food');
                    //var foodProducts = [];
                    Food.searchFood({ 
                        searchterm: $scope.searchFoodText.replace(/[^a-zA-Z- àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ]+/g, "") 
                    }).success(function (data) {
                        data.forEach(function(lang) {
                            if($window.localStorage.language == "EN") {
                                lang.name = lang.translation[0].name;
                                lang.foodGroup = lang.translation[0].foodGroup;
                                lang.foodSubGroup = lang.translation[0].foodSubGroup;
                                lang.foodSubGroupType = lang.translation[0].foodSubGroupType;
                            } else if($window.localStorage.language == "FR") {
                                lang.name = lang.translation[1].name;
                                lang.foodGroup = lang.translation[1].foodGroup;
                                lang.foodSubGroup = lang.translation[1].foodSubGroup;
                                lang.foodSubGroupType = lang.translation[1].foodSubGroupType;
                            }
                        });
                        //delete data.translation;
                        //_.without(data.translation, );
                        // for(var k in data) {
                        //     foodProducts[k]=data[k];
                        // }
                        // data.forEach(function(entry) {
                        //     //console.log(_.flatten(entry));
                        //     data
                        // });

                        $scope.foodProducts = data;
                        //console.log($window.localStorage.language);
                        if(data.length == 0) {
                            $scope.noResults = gettextCatalog.getString('No results, try a different query!');
                        }
                        //console.log(data);
                    }).error(function(status, data) {
                            $rootScope.rootAlerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                priority: 2
                            });
                        });
                });
                $scope.show = 'true';
            }

            $scope.carbsTotal = 0;
            var carbsCurrent = 0;

            $scope.addFood = function(food) {

                var selectedProduct =_.find($scope.foodProducts, function(products) {
                    if(products.name == food) {
                        return products;
                    }
                });

                if(_.find($scope.selectedFood, function(product) {
                    if(product.name == selectedProduct.name) { return product; }
                }) === undefined) {
                    $scope.foodProducts = _.without($scope.foodProducts, _.findWhere($scope.foodProducts, {
                                          name: food
                                        }));
                    selectedProduct["amount"] = 1;
                    selectedProduct["totalAmount"] = 100;
                    //console.log(selectedProduct);                
                    $scope.selectedFood.push(selectedProduct);
                    carbsCurrent += parseFloat(selectedProduct.values[2].value.replace(',', '.'));
                    $scope.carbsTotal = carbsCurrent.toFixed(2);
                } else {
                    $rootScope.rootAlerts.push({
                              type: 'warning',
                              msg: gettextCatalog.getString('You have already added this product!'),
                              priority: 2
                          });
                }
                
            }

            $scope.removeFood = function(food) {
                var selectedProductRemove = _.find($scope.selectedFood, function(products) {
                    if(products.name == food) {
                        return products;
                    }
                });
                $scope.selectedFood = _.without($scope.selectedFood, _.findWhere($scope.selectedFood, { 
                                            name: food
                                        }));
                carbsCurrent -= parseFloat(selectedProductRemove.values[2].value.replace(',', '.'));
                $scope.carbsTotal = carbsCurrent.toFixed(2);
            }

            $scope.displayFilename = function() {
                if($scope.mealUpload.photoMeal != null) {
                    $scope.filename = $scope.mealUpload.photoMeal.name;
                }
            }

            $scope.removePhoto = function() {
                $scope.mealUpload.photoMeal = null;
                $scope.filename = null;
            }

            $scope.mealUpload = {};

            $scope.saveMeal = function() {                

                if($scope.selectedFood.length === 0) {
                    $rootScope.rootAlerts.push({
                              type: 'warning',
                              msg: gettextCatalog.getString('Please add at least one food product to the list'),
                              priority: 2
                          });
                } else {
                    //console.log($scope.selectedFood);
                    var entryMeal = {
                        meal: [],
                        datetimeAcquisition: ''
                    };

                    var parseFood = parseSelectedFood($scope.selectedFood);
                    var parseDateTime = mergeDatetime($scope.myDate, $scope.myTime);

                    if($scope.comments !== '') {
                        entryMeal.comments = $scope.comments;
                    }

                    if(parseFood) {
                        entryMeal.meal = parseFood;
                    }

                    if(parseDateTime) {
                        entryMeal.datetimeAcquisition = parseDateTime;
                    }

                    if($scope.mealUpload.photoMeal) {
                        var photo = $scope.mealUpload.photoMeal;
                        entryMeal.file = photo;
                        //console.log(photo);
                        var mealJson = angular.toJson(entryMeal.meal);
                        entryMeal.meal = mealJson;
                        $scope.mealUpload.photoMeal.upload = Upload.upload({
                            url: '/api/meal/save/'+entryMeal,
                            data: entryMeal,
                            method: 'POST'
                        });

                        $scope.mealUpload.photoMeal.upload.then(function (response) {
                            $timeout(function () {
                                //$scope.mealUpload.file.result = response.data;
                                //console.log(response);
                                $state.go('home.dashboard.main');
                                $rootScope.rootAlerts.push({
                                     type:'success',
                                     msg: gettextCatalog.getString('The image was uploaded.'),
                                     priority: 4
                                });
                            });
                        }, function (response) {
                            //console.log(response);
                            if (response.status > 0){
                                $scope.errorMsgUpload = response.status + ': ' + response.data;
                            }
                        }, function (evt) {
                            // Math.min is to fix IE which reports 200% sometimes
                            //$scope.mealUpload.file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    } else {
                        var mealJson = angular.toJson(entryMeal.meal);
                        entryMeal.meal = mealJson;
                        $scope.mealUpload.upload = Upload.upload({
                            url: '/api/meal/save/'+entryMeal,
                            data: entryMeal,
                            method: 'POST'
                        });

                        $scope.mealUpload.upload.then(function (response) {
                            $timeout(function () {
                                //$scope.mealUpload.file.result = response.data;
                                //console.log(response);
                                $state.go('home.dashboard.main');
                                $rootScope.rootAlerts.push({
                                     type:'success',
                                     msg: gettextCatalog.getString('The meal was saved.'),
                                     priority: 4
                                });
                            });
                        }, function (response) {
                            //console.log(response);
                            if (response.status > 0){
                                $scope.errorMsgUpload = response.status + ': ' + response.data;
                            }
                        }, function (evt) {
                            // Math.min is to fix IE which reports 200% sometimes
                            //$scope.mealUpload.file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                        });
                    }
                }
            }

            function parseSelectedFood(food) {
                var foods = [];
                food.forEach(function(entry) {
                    foods.push({name: entry.productID, amount: entry.amount});
                });
                //console.log(foods);
                return foods;
            }

            $scope.calculateAmount = function(index, amount) {
                var previousAmount = $scope.selectedFood[index].totalAmount;
                //console.log($scope.selectedFood[index].totalAmount);
                $scope.selectedFood[index].totalAmount = Number(amount*100).toFixed(0);
                //console.log($scope.selectedFood[index].totalAmount);
                //console.log($scope.selectedFood[index]);
                var carbs = parseFloat($scope.selectedFood[index].values[2].value.replace(',', '.'));
                carbsCurrent -= carbs*previousAmount/100;
                carbsCurrent += carbs*amount;
                $scope.carbsTotal = carbsCurrent.toFixed(2);
            }

            function mergeDatetime(myDate, myTime) {
                return new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), myTime.getHours(), myTime.getMinutes(), myTime.getSeconds());
            }

            function toDuration(time) {
                return (parseInt(time.getMinutes()) / 60) + parseInt(time.getHours());
            }

            //Add an entry
            function addEntry(entry, callback) {
                var currentEntry = {
                    type: entry.type,
                    value: entry.value
                };

                if (entry.myDate && entry.myTime) {
                    currentEntry.datetimeAcquisition = mergeDatetime(entry.myDate, entry.myTime);
                }
                if (entry.subType) {
                    currentEntry.subType = entry.subType;
                }
                if (entry.values) {
                    currentEntry.values = entry.values;
                }
                if (entry.comments) {
                    currentEntry.comments = $scope.comments;
                }
                if (entry.mealPhoto) {
                    currentEntry.mealPhoto = $scope.mealPhoto;
                }

                Entry.create(currentEntry).success(function(data) {
                    callback(data);
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('Failed to add the entry!'),
                        priority: 2
                    });
                });
            };


            $scope.verify = function() {
                var ok = true;
                var count = 0;
                switch ($stateParams.card) {
                    case 'sport':
                        if (!$scope.radios.sport.duration.value && !$scope.more) {
                            ok = false;
                        }
                        if (!$scope.radios.sport.intensity.value) {
                            ok = false;
                        }
                        break;
                    case 'insulin':
                        if (!$scope.value) {
                            ok = false;
                        }
                        if (!$scope.insulinType) {
                            ok = false;
                        }
                        break;
                    case 'bloodpressure':
                        if (!$scope.bp.systo) {
                            ok = false;
                        }
                        if (!$scope.bp.diasto) {
                            ok = false;
                        }
                        break;
                    case 'meal':
                        for (i = 0; i < $scope.radios.meal.buttons.length; i++) {
                            if (!$scope.radios.meal.buttons[i].value) {
                                count++;
                            }
                            if ($scope.radios.meal.buttons.length == count) {
                                ok = false;
                            }
                        }
                        // if($scope.selectedFood.length === 0) {
                        //     ok = false;
                        // }
                        break;
                    case 'mobility':
                        for (i = 0; i < $scope.radios.mobility.buttons.length; i++) {
                            if (!$scope.radios.mobility.buttons[i].value) {
                                count++;
                            }
                            if ($scope.radios.mobility.buttons.length == count) {
                                ok = false;
                            }
                        }

                        break;
                    case 'symptoms':
                        for (i = 0; i < $scope.symptoms.length; i++) {
                            if ($scope.symptoms[i].value < 0) {
                                ok = false;
                            }
                        }
                        break;
                    case 'liquid':
                    case 'salt':
                        ok = true;
                        break;
                    default:
                        if (!$scope.value) {
                            ok = false;
                        }
                        break;
                }
                return ok;
            };


            //Add an entry
            $scope.add = function() {

                var entry = { myDate: $scope.myDate, myTime: $scope.myTime };
                if ($scope.comments) {
                    entry.comments = $scope.comments;
                }

                switch ($stateParams.card) {
                    case 'sport':
                        entry.type = 'activity';
                        entry.subType = $stateParams.card;
                        if ($scope.more) {
                            entry.value = toDuration($scope.advancedDuration);
                        } else {
                            entry.value = $scope.radios.sport.duration.value;
                        }

                        entry.values = [{ type: 'intensity', value: $scope.radios.sport.intensity.value }];
                        addEntry(entry, function() {
                            $state.go("home.dashboard.main");
                        });
                        break;
                    case 'insulin':
                        entry.type = $stateParams.card;
                        entry.value = $scope.value;
                        entry.subType = $scope.insulinType;
                        //  entry.values = [{type:$scope.insulinType}];
                        addEntry(entry, function() {
                            $state.go("home.dashboard.main");
                        });
                        break;
                    case 'bloodpressure':
                        entry.type = $stateParams.card;
                        var values = [];
                        values.push({ type: "systolic", value: "" + $scope.bp.systo + "" });
                        values.push({ type: "diastolic", value: "" + $scope.bp.diasto + "" });
                        entry.values = values;
                        addEntry(entry, function() {
                            $state.go("home.dashboard.main");
                        });
                        break;
                    case 'heartrate':
                        entry.type = $stateParams.card;
                        entry.value = $scope.value;
                        entry.subType = $scope.hrType;
                        addEntry(entry, function() {
                            $state.go("home.dashboard.main");
                        });
                        break;
                    case 'symptoms':
                        entry.type = $stateParams.card;
                        entry.values = [];
                        for (i = 0; i < $scope.symptoms.length; i++) {
                            if ($scope.symptoms[i].value < 0) {
                                $scope.symptoms[i].value = 0;
                            }
                            entry.values.push({ type: $scope.symptoms[i].type, value: $scope.symptoms[i].value });
                        }
                        addEntry(entry, function() {
                            $state.go("home.dashboard.main");
                        });
                        break;
                    case 'meal':
                        var average = 0;
                        var total = 0;
                        var values = [];
                        for (i = 0; i < $scope.radios.meal.buttons.length; i++) {
                            if ($scope.radios.meal.buttons[i].value === undefined) {
                                $scope.radios.meal.buttons[i].value = 0;
                            }
                            values.push({ type: $scope.radios.meal.buttons[i].type, value: $scope.radios.meal.buttons[i].value });
                            average = average + parseFloat($scope.radios.meal.buttons[i].value) * $scope.radios.meal.buttons[i].score;
                            total = total + $scope.radios.meal.buttons[i].score;
                        }
                        entry.value = average;
                        average = average / total;
                        entry.type = 'meal';
                        entry.values = values;
                        addEntry(entry, function() {
                            if (average <= 1.5 || average >= 3.5) {
                                if (average <= 1.5) {
                                    text = gettextCatalog.getString('Your last meal has a low energy intake. Be careful.');
                                } else if (average >= 3.5) {
                                    text = gettextCatalog.getString('Your last meal has a rich trend. Be careful.');
                                }

                                ModalService.showModal({
                                    templateUrl: "templates/modals/warning.html",
                                    controller: function($scope, close) {
                                        $scope.text = text;
                                        $scope.close = function(result) {
                                            close(result, 500); // close, but give 500ms for bootstrap to animate
                                        };
                                    }
                                }).then(function(modal) {
                                    modal.element.modal();
                                    modal.close.then(function(result) {});
                                });
                            }
                            $state.go("home.dashboard.main");
                        });
                        break;
                    case 'mobility':
                        var values = [];
                        for (i = 0; i < $scope.radios.mobility.buttons.length; i++) {
                            if ($scope.radios.mobility.buttons[i].value !== undefined) {
                                values.push({ type: $scope.radios.mobility.buttons[i].type, value: $scope.radios.mobility.buttons[i].value });
                            }
                        }
                        entry.type = 'mobility';
                        entry.values = values;
                        addEntry(entry, function(data) {
                            if (data.duplicate) {
                                $rootScope.rootAlerts.push({
                                    type: 'warning',
                                    msg: gettextCatalog.getString('An entry already exists for this date'),
                                    priority: 3
                                });
                            } else {
                                $state.go("home.dashboard.main");
                            }
                        });
                        break;
                    case 'salt':
                        var total = 0;
                        var len = $scope.salt.length;

                        for (var i = 0; i < len; ++i) {
                            var len2 = $scope.salt[i].values.length;
                            for (var k = 0; k < len2; ++k) {
                                total += ($scope.salt[i].mg[k] * $scope.salt[i].values[k].value);
                            }
                        }

                        entry.type = $stateParams.card;
                        entry.value = total;
                        addEntry(entry, function() {
                            $state.go("home.dashboard.main");
                        });
                        break;

                    case 'liquid':

                        var total = 0;
                        var len = $scope.liquids.length;

                        for (var i = 0; i < len; ++i) {
                            total += ($scope.liquids[i].ml[0] * $scope.liquids[i].values[0].value);
                            total += ($scope.liquids[i].ml[1] * $scope.liquids[i].values[1].value);
                            total += ($scope.liquids[i].ml[2] * $scope.liquids[i].values[2].value);
                        }

                        entry.type = $stateParams.card;
                        entry.value = total;
                        addEntry(entry, function() {
                            $state.go("home.dashboard.main");
                        });
                        break;
                    default:
                        entry.type = $stateParams.card;
                        entry.value = $scope.value;
                        addEntry(entry, function() {
                            $state.go("home.dashboard.main");
                        });
                        break;
                }
            };

            $scope.reset = function(button) {
                if (button.clicked) {
                    button.value = undefined;
                    button.clicked = false;
                } else {
                    button.clicked = true;
                }
            }

            function decToDate(dec) {
                var flo = parseFloat(dec);
                var hours = Math.floor(flo / 60);
                var minutes = Math.floor((flo - (hours * 60)) * 60);
                return new Date(new Date().setHours(hours, minutes, 0, 0));
            }

            $scope.toggleMore = function() {
                if ($scope.config.name === 'sport') {
                    if ($scope.radios.sport.duration.value) {
                        $scope.advancedDuration = decToDate($scope.radios.sport.duration.value);
                    }
                }
                $scope.more = !$scope.more;
            }

            $scope.decIfGreaterThanZero = function(val, dec) {
                if ((val - dec) >= 0) {
                    return val - dec;
                } else {
                    return val;
                }
            }
        });
    });