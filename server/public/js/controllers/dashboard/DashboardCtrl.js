angular.module('DashboardCtrl', [
        [
            'bower_components/highcharts/highcharts.js',
            'css/templates/dashboard.css'
        ]
    ])
    .controller('DashboardController', function($scope, gettextCatalog, $ocLazyLoad, $injector, $rootScope, $window, ModalService, $log) {
        $scope.helper = [];
        $scope.asks = [];
        var helper = {
            msg1: gettextCatalog.getString("Empty dashboard"),
            msg2: gettextCatalog.getString("You can add dashboard cards from Settings.")
        };

        $scope.role = $window.localStorage.role;

        // Merge a date and a time and return the new datetime 
        function mergeDatetime(myDate, myTime) {
            return new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), myTime.getHours(), myTime.getMinutes(), myTime.getSeconds());
        }

        //Build cards
        function cards() {
            $ocLazyLoad.load('js/services/UIService.js').then(function() {
                var UI = $injector.get('UI');
                UI.cards().success(function(data) {
                    $scope.cards = data.cards;
                    if ($scope.cards.length == 0) {
                        $scope.helper = helper;
                    } else {
                        $scope.helper = [];
                    }
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });
            });
        };

        // Build asks
        function ask() {
            $ocLazyLoad.load('js/services/UIService.js').then(function() {
                var UI = $injector.get('UI');
                UI.asks().success(function(data) {
                    $scope.asks = [];
                    if (data.ask !== null) {
                        $scope.asks.push(data.ask);
                    }
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });
            });
        };

        // Build the dashboard
        $scope.buildDashboard = function() {
            cards();
            ask();
        }

        $scope.activeMenu = 'hba1c';
        $scope.selectedModule = 'monitoring';
        $rootScope.isHiddenGlobal = {
            agp: false,
            meal: false,
            sport: false,
            insulin: false,
            assistant: false,
            steps: false,
            weight: false,
            mobility: false,
            liquid: false,
            bloodpressure: false,
            salt: false,
            symptoms: false,
            heartrate: false,
            hemoglobin: false
        };

        $scope.hideAll = function() {
            for (var widget in $rootScope.isHiddenGlobal) {
                $rootScope.isHiddenGlobal[widget] = true;
            }
        }
        $scope.showWidgets = function(navigation) {
            switch (navigation) {
                case 'Treatment':
                    $scope.selectedModule = 'treatment';
                    $scope.hideAll();
                    $rootScope.isHiddenGlobal.insulin = false;
                    $rootScope.isHiddenGlobal.assistant = false;
                    break;
                case 'Monitoring':
                    $scope.selectedModule = 'monitoring';
                    $scope.hideAll();
                    $rootScope.isHiddenGlobal.agp = false;
                    $rootScope.isHiddenGlobal.hemoglobin = false;
                    $rootScope.isHiddenGlobal.heartrate = false;
                    $rootScope.isHiddenGlobal.weight = false;
                    $rootScope.isHiddenGlobal.bloodpressure = false;
                    $rootScope.isHiddenGlobal.symptoms = false;
                    break;
                case 'Educoaching':
                    $scope.selectedModule = 'educoaching';
                    $scope.hideAll();
                    break;
                case 'Lifestyle':
                    $scope.selectedModule = 'lifestyle';
                    $scope.hideAll();
                    $rootScope.isHiddenGlobal.meal = false;
                    $rootScope.isHiddenGlobal.sport = false;
                    $rootScope.isHiddenGlobal.steps = false;
                    $rootScope.isHiddenGlobal.mobility = false;
                    $rootScope.isHiddenGlobal.liquid = false;
                    $rootScope.isHiddenGlobal.salt = false;
                    break;
                default:
                    $scope.hideAll();
                    break;
            }
        }


        // First build
        $scope.buildDashboard();
        $scope.showWidgets('Monitoring');


        // Skip an ask card
        $scope.skip = function(type) {
            $scope.addEntry({
                type: type,
                skipped: true
            }, function() {
                $scope.buildDashboard();
            });
        };

        //Add an entry
        $scope.addEntry = function(entry, callback) {
            $ocLazyLoad.load('js/services/EntryService.js').then(function() {
                var Entry = $injector.get('Entry');
                if (entry.value > 0 || !entry.value) {
                    var currentEntry = {
                        type: entry.type
                    };

                    if (entry.skipped) {
                        currentEntry.skipped = entry.skipped;
                    }
                    if (entry.datetimeAcquisition) {
                        currentEntry.datetimeAcquisition = entry.datetimeAcquisition;
                    }
                    if (entry.value) {
                        currentEntry.value = entry.value;
                    }
                    if (entry.subType) {
                        currentEntry.subType = entry.subType;
                    }
                    if (entry.values) {
                        currentEntry.values = entry.values;
                    }

                    Entry.create(currentEntry).success(function(data) {
                        callback(null);
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('Failed to add the entry!'),
                            priority: 2
                        });
                    });
                } else {
                    $rootScope.rootAlerts.push({
                        type: 'info',
                        msg: gettextCatalog.getString('Value must be greater than 1.'),
                        priority: 4
                    });
                }
            });
        }

        // Delete an entry
        $scope.deleteEntry = function(entry, callback) {
            ModalService.showModal({
                templateUrl: "templates/modals/deleteEntry.html",
                controller: function($scope, close) {
                    $scope.entry = entry;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if (result) {
                        $ocLazyLoad.load('js/services/EntryService.js').then(function() {
                            var Entry = $injector.get('Entry');
                            Entry.delete({ id: entry._id }).success(function(data) {
                                $rootScope.rootAlerts.push({
                                    type: 'success',
                                    msg: gettextCatalog.getString("The entry has been removed"),
                                    priority: 5
                                });
                                // Rebuild dashboard
                                $scope.buildDashboard();
                            }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type: 'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                            });
                        });
                    }
                });
            });
        }

        $scope.viewMeal = function(entry, callback) {
            //console.log(entry);
            var productIDs = [];
            var language = $window.localStorage.language;
            ModalService.showModal({
                templateUrl: "templates/modals/viewMeal.html",
                controller: function($scope, close) {
                    //$scope.entry = entry;
                    entry.values.forEach(function(value) {
                        productIDs.push(value.name);
                    });
                    //console.log(productIDs, angular.toJson(productIDs));
                    $ocLazyLoad.load('js/services/FoodService.js').then(function() {
                        var Food = $injector.get('Food');
                        Food.getMealProducts(productIDs).success(function (data) {
                            // $rootScope.rootAlerts.push({
                            //     type: 'success',
                            //     msg: gettextCatalog.getString("The entry has been removed"),
                            //     priority: 5
                            // });
                            //console.log('PRODUCT DETAILS', data);
                            data.forEach(function(product) {
                                entry.values.forEach(function(ent) {
                                    if(product.productID == ent.name) {
                                        if(language == "EN") {
                                            ent.productname = product.translation[0].name;
                                        } else if(language == "FR") {
                                            ent.productname = product.translation[1].name;
                                        }                                        
                                        ent.carbs = parseFloat(product.values[2].value.replace(',', '.'))*ent.amount;
                                        //ent.carbs.toFixed(2);
                                        ent.sugar = parseFloat(product.values[4].value.replace(',','.'))*ent.amount;
                                        ent.kcal = parseFloat(product.values[0].value.replace(',','.'))*ent.amount;
                                        ent.fats = parseFloat(product.values[3].value.replace(',','.'))*ent.amount;
                                    }
                                })
                            });
                            //console.log('MERGED DATA', entry);
                            $scope.entry = entry;
                            $scope.amount = { 
                                total_amount: 0,
                                total_carbs: 0,
                                total_sugar: 0,
                                total_fats: 0,
                                total_kcal: 0
                            };
                            //$scope.totalCarbs = { total_carbs: 0 };
                        }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type: 'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                        });
                    });

                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if (result) {

                    }
                });
            });
        }

        $scope.deleteMeal = function(entry, callback) {
            //console.log(entry);
            ModalService.showModal({
                templateUrl: "templates/modals/deleteEntry.html",
                controller: function($scope, close) {
                    $scope.entry = entry;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if (result) {
                        $ocLazyLoad.load('js/services/EntryService.js').then(function() {
                            var Food = $injector.get('Food');
                            Food.delete({ id: entry._id }).success(function (data) {
                                $rootScope.rootAlerts.push({
                                    type: 'success',
                                    msg: gettextCatalog.getString("The entry has been removed"),
                                    priority: 5
                                });
                                // Rebuild dashboard
                                $scope.buildDashboard();
                            }).error(function(status, data) {
                                    $rootScope.rootAlerts.push({
                                        type: 'danger',
                                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                                        priority: 2
                                    });
                            });
                        });
                    }
                });
            });
        }

        //Build a list
        $scope.buildList = function(config, callback) {
            $ocLazyLoad.load('js/services/EntryService.js').then(function() {
                var Entry = $injector.get('Entry');
                Entry.list({
                    type: config.type,
                    subType: config.subType,
                }).success(function(data) {
                    callback(data);
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                    callback(null);
                });
            });
        };
        //--</entries>

        //--<appTips>
        //Got it
        $scope.gotit = function(name) {
            $ocLazyLoad.load('js/services/UIService.js').then(function() {
                var UI = $injector.get('UI');
                UI.gotit({ name: name }).success(function(data) {

                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });
            });
        }

        //Verify the state of an app tip
        $scope.verifyAppTip = function(name, callback) {
            $ocLazyLoad.load('js/services/UIService.js').then(function() {
                var UI = $injector.get('UI');
                UI.verifyAppTip({ name: name }).success(function(data) {
                    callback(data.display);
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });
            });
        };
        //--</appTips>

        //Build a chart
        $scope.buildChart = function(chart, config, callback) {
            $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                var Chart = $injector.get('Chart');
                Chart.build({
                    type: chart.series[0].id,
                    from: config.from,
                    to: config.to
                }).success(function(data) {
                    for (i = 0; i < chart.series.length; i++) {
                        chart.series[i].data = [];
                    }
                    callback(data);
                }).error(function(status, data) {
                    for (i = 0; i < chart.series.length; i++) {
                        chart.series[i].data = [];
                    }
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                    callback(null);
                });
            });
        };


        //A chart
        $scope.aChart = {
            options: {
                chart: {},
                tooltip: {
                    enabled: true
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        stacking: '',
                        dataLabels: {
                            enabled: false
                        }
                    }
                },
            },
            series: [
                { data: [] }
            ],
            title: {
                text: ''
            },
            xAxis: {
                labels: {
                    rotation: 0
                },
                title: {
                    text: ''
                }
            },
            yAxis: {
                plotLines: [{
                    color: '#000',
                    dashStyle: 'Dash',
                    width: 1,
                    zIndex: 99,
                    label: {
                        x: 0
                    }
                }],
                title: {
                    text: ''
                },
                stackLabels: {
                    style: {
                        color: '#D32F2F'
                    },
                    enabled: false,
                    align: 'right'
                },
                formatter: function() {}
            },
            noData: gettextCatalog.getString('No Data'),
            size: {
                height: ''
            }
        };

        //B chart
        $scope.bChart = {
            title: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                opposite: true,
                crosshair: true,
                tickInterval: 3600 * 1000,
                labels: {},
                plotLines: [{ //meal plotlines
                    value: 0, //Date.UTC(2017,10, 3, 7, 0),
                    width: 1,
                    color: '#e6e6e6',
                    dashStyle: 'solid',
                    label: {
                        text: '',
                        align: 'center',
                        y: 12,
                        x: 0
                    }
                }, {
                    value: 0, //Date.UTC(2017,10, 3, 11, 0),
                    width: 1,
                    color: '#e6e6e6',
                    dashStyle: 'solid',
                    label: {
                        text: '',
                        align: 'right',
                        y: 60,
                        x: 0
                    }
                }, {
                    value: 0, //Date.UTC(2017,10, 3, 17, 0),
                    width: 1,
                    color: '#e6e6e6',
                    dashStyle: 'solid',
                    label: {
                        text: '',
                        align: 'right',
                        y: 60,
                        x: 0
                    }
                }, {
                    value: 0, //Date.UTC(2017,10, 3, 20, 0),
                    width: 1,
                    color: '#e6e6e6',
                    dashStyle: 'solid',
                    label: {
                        text: '',
                        align: 'right',
                        y: 60,
                        x: 0
                    }
                }]
            },
            yAxis: {
                title: {
                    text: ''
                },
                gridLineWidth: 0,
                minorGridLineWidth: 0,
                plotLines: [{
                    value: 0,
                    width: 1.3,
                    dashStyle: 'dash',
                    color: '#9e9e9e',
                    zIndex: 5,
                    label: {
                        text: '',
                        align: 'left',
                        x: 5
                    }
                }, {
                    value: 0,
                    color: '#9e9e9e',
                    dashStyle: 'dash',
                    width: 1.3,
                    zIndex: 99,
                    label: {
                        x: -5,
                        y: 15,
                        align: 'right'
                    }
                }, {
                    value: null,
                    width: 2,
                    color: 'blue',
                    zIndex: 1,
                    dashStyle: 'ShortDot',
                    label: {
                        text: null,
                        align: 'center',
                        x: 5,
                        y: 20
                    }
                }],
                min: 0,
                max: 22,
                lineColor: '#e6e6e6',
                lineWidth: 3
            },
            plotOptions: {
                series: {
                    marker: {
                        enabled: false
                    },
                    fillOpacity: 0.2
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'top',
                floating: true,
                x: 0,
                y: 30
            },
            credits: {
                enabled: false
            },

            series: [],

            responsive: {
                rules: []
            },

            options: {
                chart: {},
                tooltip: {
                    shared: true
                }
            },
            noData: gettextCatalog.getString('No Data')
        };

        $scope.matrixChart = {
                    title: {
                        text: ''
                    },
                    options: {
                        legend: {
                            useHTML: true,
                            labelFormatter: function() {
                                if (this.color === 'rgba(255,102,102,0.75)') {
                                    return '<span style="color:' + this.color + '">\u2B24</span> High Risk';
                                }
                                if (this.color === 'rgba(255,153,51,0.75)') {
                                    return '<span style="color:' + this.color + '">\u2B24</span> Moderate Risk';
                                }
                                if (this.color === 'rgba(204,255,204,0.75)') {
                                    return '<span style="color:' + this.color + '">\u2B24</span> Low Risk';
                                }
                            }
                        },
                        chart: {
                            //type: 'polygon',
                            marginTop: 50//,
                            //spacingLeft: 65
                            ,
                            events: {}
                        },
                        tooltip: {
                            useHTML: true
                        }
                    },
                    plotOptions: {
                        series: {
                            marker: {
                                states: {
                                    hover: {
                                        enabled: false
                                    }
                                },
                                enabled: false
                            }
                        }

                    },
                    credits: {
                        enabled: false
                    },
                    xAxis: {
                        type: 'datetime',
                        min: new Date(new Date().setDate(3)).setHours(0, 0, 0, 0),
                        max: new Date(new Date().setDate(3)).setHours(24, 0, 0, 0),
                        tickInterval: 3600 * 1000,
                        plotLines: [],
                        labels: {                            
                            overflow: 'justify',
                            //rotation: 0,
                            step: 2
                        }
                    },
                    yAxis: {
                        title: {
                            text: 'Title',
                            style: {
                                visibility: 'hidden'
                            }
                        },
                        labels: {
                            enabled: true,
                            formatter: function() {
                                return 100;
                            },
                            style: {
                                visibility: 'hidden'
                            }
                        },
                        min: 0,
                        max: 16,
                        lineColor: '#e6e6e6',
                        lineWidth: 3,
                        gridLineWidth: 0,
                        minorGridLineWidth: 0
                    },
                    series: [],
                    noData: gettextCatalog.getString('No Data'),
                    size: {
                        height: '280px'
                    }
                };

    })
    .controller('CardAuditController', function($scope, gettextCatalog, $ocLazyLoad, $injector, $rootScope) {
        $ocLazyLoad.load([
            'bower_components/pickadate/lib/themes/classic.css',
            'bower_components/pickadate/lib/themes/classic.date.css',
        ]);

        $scope.dateRange = {
            from: new Date(new Date().setDate(new Date().getDate() - 1)),
            to: new Date()
        }

        $scope.fetchResults = function() {
            if (($scope.dateRange.from && $scope.dateRange.to) && ($scope.dateRange.from <= $scope.dateRange.to)) {
                $ocLazyLoad.load('js/services/AuditService.js').then(function() {
                    var Audit = $injector.get('Audit');
                    Audit.listByDate({
                        from: $scope.dateRange.from,
                        to: $scope.dateRange.to
                    }).success(function(data) {
                        $scope.logs = data;
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            priority: 2
                        });
                    });
                });
            } else {
                $rootScope.rootAlerts.push({
                    type: 'warning',
                    msg: gettextCatalog.getString('Please choose a valid date range.'),
                    priority: 3
                });
                $scope.logs = [];
            }
        }

        $scope.fetchResults();
    });