angular.module('DashboardCtrl', [
        [
            'bower_components/highcharts/highcharts.js',
            'css/templates/dashboard.css'
        ]
    ])
    .controller('DashboardController', function($scope, gettextCatalog, $ocLazyLoad, $injector, $rootScope, ModalService) {
        $scope.helper = [];
        $scope.asks = [];
        var helper = {
            msg1: gettextCatalog.getString("Empty dashboard"),
            msg2: gettextCatalog.getString("You can add dashboard cards from Settings.")
        };

        // Merge a date and a time and return the new datetime 
        function mergeDatetime(myDate, myTime) {
            return new Date(myDate.getFullYear(), myDate.getMonth(), myDate.getDate(), myTime.getHours(), myTime.getMinutes(), myTime.getSeconds());
        }

        //Build cards
        function cards() {
            $ocLazyLoad.load('js/services/UIService.js').then(function() {
                var UI = $injector.get('UI');
                UI.cards().success(function(data) {
                    $scope.cards = data.cards;
                    if ($scope.cards.length == 0) {
                        $scope.helper = helper;
                    } else {
                        $scope.helper = [];
                    }
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });
            });
        };

        // Build asks
        function ask() {
            $ocLazyLoad.load('js/services/UIService.js').then(function() {
                var UI = $injector.get('UI');
                UI.asks().success(function(data) {
                    $scope.asks = [];
                    if (data.ask !== null) {
                        $scope.asks.push(data.ask);
                    }
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });
            });
        };

        // Build the dashboard
        $scope.buildDashboard = function() {
            cards();
            ask();
        }
        // First build
        $scope.buildDashboard();

        // Skip an ask card
        $scope.skip = function(type) {
            $scope.addEntry({
                type: type,
                skipped: true
            }, function() {
                $scope.buildDashboard();
            });
        };

        //Add an entry
        $scope.addEntry = function(entry, callback) {
            $ocLazyLoad.load('js/services/EntryService.js').then(function() {
                var Entry = $injector.get('Entry');
                if (entry.value > 0 || !entry.value) {
                    var currentEntry = {
                        type: entry.type
                    };

                    if (entry.skipped) {
                        currentEntry.skipped = entry.skipped;
                    }
                    if (entry.datetimeAcquisition) {
                        currentEntry.datetimeAcquisition = entry.datetimeAcquisition;
                    }
                    if (entry.value) {
                        currentEntry.value = entry.value;
                    }
                    if (entry.subType) {
                        currentEntry.subType = entry.subType;
                    }
                    if (entry.values) {
                        currentEntry.values = entry.values;
                    }

                    Entry.create(currentEntry).success(function(data) {
                        callback(null);
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('Failed to add the entry!'),
                            priority: 2
                        });
                    });
                } else {
                    $rootScope.rootAlerts.push({
                        type: 'info',
                        msg: gettextCatalog.getString('Value must be greater than 1.'),
                        priority: 4
                    });
                }
            });
        }

        // Delete an entry
        $scope.deleteEntry = function(entry, callback) {
            ModalService.showModal({
                templateUrl: "templates/modals/deleteEntry.html",
                controller: function($scope, close) {
                    $scope.entry = entry;
                    $scope.close = function(result) {
                        close(result, 500); // close, but give 500ms for bootstrap to animate
                    };
                }
            }).then(function(modal) {
                modal.element.modal();
                modal.close.then(function(result) {
                    if (result) {
                        $ocLazyLoad.load('js/services/EntryService.js').then(function() {
                            var Entry = $injector.get('Entry');
                            Entry.delete({ id: entry._id }).success(function(data) {
                                $rootScope.rootAlerts.push({
                                    type: 'success',
                                    msg: gettextCatalog.getString("The entry has been removed"),
                                    priority: 5
                                });
                                // Rebuild dashboard
                                $scope.buildDashboard();
                            }).error(function(status, data) {
                                $rootScope.rootAlerts.push({
                                    type: 'danger',
                                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                                    priority: 2
                                });
                            });
                        });
                    }
                });
            });
        }

        //Build a list
        $scope.buildList = function(config, callback) {
            $ocLazyLoad.load('js/services/EntryService.js').then(function() {
                var Entry = $injector.get('Entry');
                Entry.list({
                    type: config.type,
                    subType: config.subType,
                }).success(function(data) {
                    callback(data);
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                    callback(null);
                });
            });
        };
        //--</entries>

        //--<appTips>
        //Got it
        $scope.gotit = function(name) {
            $ocLazyLoad.load('js/services/UIService.js').then(function() {
                var UI = $injector.get('UI');
                UI.gotit({ name: name }).success(function(data) {

                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });
            });
        }

        //Verify the state of an app tip
        $scope.verifyAppTip = function(name, callback) {
            $ocLazyLoad.load('js/services/UIService.js').then(function() {
                var UI = $injector.get('UI');
                UI.verifyAppTip({ name: name }).success(function(data) {
                    callback(data.display);
                }).error(function(status, data) {
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                });
            });
        };
        //--</appTips>

        //Build a chart
        $scope.buildChart = function(chart, config, callback) {
            $ocLazyLoad.load('js/services/ChartService.js').then(function() {
                var Chart = $injector.get('Chart');
                Chart.build({
                    type: chart.series[0].id,
                    from: config.from,
                    to: config.to
                }).success(function(data) {
                    for (i = 0; i < chart.series.length; i++) {
                        chart.series[i].data = [];
                    }
                    callback(data);
                }).error(function(status, data) {
                    for (i = 0; i < chart.series.length; i++) {
                        chart.series[i].data = [];
                    }
                    $rootScope.rootAlerts.push({
                        type: 'danger',
                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                        priority: 2
                    });
                    callback(null);
                });
            });
        };


        //A chart
        $scope.aChart = {
            options: {
                chart: {},
                tooltip: {
                    enabled: true
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        stacking: '',
                        dataLabels: {
                            enabled: false
                        }
                    }
                },
            },
            series: [
                { data: [] }
            ],
            title: {
                text: ''
            },
            xAxis: {
                labels: {},
                title: {
                    text: ''
                }
            },
            yAxis: {
                plotLines: [{
                    color: '#000',
                    dashStyle: 'Dash',
                    width: 1,
                    zIndex: 99,
                    label: {
                        x: 0
                    }
                }],
                title: {
                    text: ''
                },
                stackLabels: {
                    style: {
                        color: '#D32F2F'
                    },
                    enabled: false,
                    align: 'right'
                },
                formatter: function() {}
            },
            noData: gettextCatalog.getString('No Data'),
            size: {
                height: ''
            }
        };

        //B chart
        $scope.bChart = {
            title: {
                text: ''
            },
            xAxis: {
                type: 'datetime',
                opposite: true,
                crosshair: true,
                tickInterval: 3600 * 1000,
                labels: {},
<<<<<<< HEAD
                plotLines: [{ //meal plotlines
                    value: 0, //Date.UTC(2017,10, 3, 7, 0),
                    width: 1,
                    color: 'green',
                    dashStyle: 'dash',
                    label: {
                        text: '',
                        align: 'center',
                        y: 12,
                        x: 0
                    }
                }, {
                    value: 0, //Date.UTC(2017,10, 3, 11, 0),
                    width: 1,
                    color: 'green',
                    dashStyle: 'dash',
                    label: {
                        text: '',
                        align: 'right',
                        y: 60,
                        x: 0
                    }
                }, {
                    value: 0, //Date.UTC(2017,10, 3, 17, 0),
                    width: 1,
                    color: 'green',
                    dashStyle: 'dash',
                    label: {
                        text: '',
                        align: 'right',
                        y: 60,
                        x: 0
                    }
                }, {
                    value: 0, //Date.UTC(2017,10, 3, 20, 0),
                    width: 1,
                    color: 'green',
                    dashStyle: 'dash',
                    label: {
                        text: '',
                        align: 'right',
                        y: 60,
                        x: 0
                    }
                }]
=======
                plotLines: []
>>>>>>> 7cdd22fbed7aea7550bda07899bf33f35e599468
            },
            yAxis: {
                title: {
                    text: ''
                },
                gridLineWidth: 0,
                minorGridLineWidth: 0,
                plotLines: [{
                    value: 0,
                    width: 1.5,
                    color: '#000000',
                    zIndex: 5,
                    label: {
                        text: '',
                        align: 'left',
                        x: 5
                    }
                }],
                min: 0,
                max: 22,
                lineColor: '#2E2E1E',
                lineWidth: 2
            },
            plotOptions: {
                series: {
                    marker: {
                        enabled: false
                    },
                    fillOpacity: 0.2
                }
            },
            legend: {
                align: 'center',
                verticalAlign: 'top',
                floating: true,
                x: 0,
                y: 30
            },
            credits: {
                enabled: false
            },

            series: [],

            responsive: {
                rules: []
            },

            options: {
                chart: {},
                tooltip: {
                    shared: true
                }
            },
            noData: gettextCatalog.getString('No Data')
        };

        //Define a matrix chart
        $scope.matrixChart = {
            title: {
                text: ''
            },
            options: {
                legend: {
                    enabled: false
                },
                chart: {
                    type: 'scatter',
                    marginTop: 40,
                    spacingLeft: 65
                },
                tooltip: {}
            },
            plotOptions: {
                series: {
                    lineWidth: 0,
                    tickInterval: 3600 * 1000,
                    pointStart: Date.UTC(2017, 10, 3, 1, 0),
                    pointInterval: 3600 * 1000 // one day
                }
            },
            credits: {
                enabled: false
            },
            xAxis: {
                type: 'datetime',
                min: new Date(new Date().setDate(3)).setHours(0, 0, 0, 0),
                max: new Date(new Date().setDate(3)).setHours(24, 0, 0, 0),
                tickInterval: 3600 * 1000,
                plotLines: [{
                    value: null, //new Date(new Date().setDate(3)).setHours(8, 0, 0, 0),// Date.UTC(2017,10, 3, 7, 0),
                    width: 1,
                    color: 'green',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-apple fa-2x" aria-hidden="true"></i>',
                        align: 'right',
                        rotation: 0,
                        y: -25,
                        x: 8
                    }
                }, {
                    value: null, //Date.UTC(2017,10, 3, 11, 0),
                    width: 1,
                    color: 'green',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-apple fa-2x" aria-hidden="true"></i>',
                        align: 'right',
                        rotation: 0,
                        y: -25,
                        x: 8
                    }
                }, {
                    value: null, //Date.UTC(2017,10, 3, 17, 0),
                    width: 1,
                    color: 'green',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-apple fa-2x" aria-hidden="true"></i>',
                        align: 'right',
                        rotation: 0,
                        y: -25,
                        x: 8
                    }
                }, {
                    value: null, //new Date(new Date().setDate(3)).setHours(21, 0, 0, 0),//Date.UTC(2017,10, 3, 20, 0),
                    width: 1,
                    color: 'green',
                    dashStyle: 'dash',
                    label: {
                        useHTML: true,
                        text: '<i class="fa fa-bed fa-2x" aria-hidden="true"></i>',
                        align: 'right',
                        rotation: 0,
                        y: -25,
                        x: 8
                    }
                }]
            },
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    enabled: false
                },
<<<<<<< HEAD
                // min: 2,
                // max: 5,
                /*,
                                    plotLines: [{
                                        value: 9.5,
                                        width: 1,
                                        color: '#000000',
                                        label: {
                                            text: 'Likelihood of low glucose',
                                            align: 'left'
                                        }
                                    },{
                                        value: 6.5,
                                        width: 1,
                                        color: '#000000',
                                        label: {
                                            text: 'Median glucose',
                                            align: 'left'
                                        }
                                    },{
                                        value: 4,
                                        width: 1,
                                        color: '#000000',
                                        label: {
                                            text: 'Variability below median',
                                            align: 'left'
                                        }
                                    }]*/
                lineColor: '#2E2E1E',
                lineWidth: 2,
                gridLineWidth: 0,
                minorGridLineWidth: 0
            },

            /*series: [{
                name: 'Variability below median',
                data: [{
                    x: Date.UTC(2017, 10, 3, 3, 30),
                    y: 5.5,
                    marker: {
                        //-symbol: 'url(../../../../img/good.png)'
                        enabled: false
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 9, 0),
                    y: 5.5,
                    marker: {
                        //symbol: 'url(../../../../img/bad.png)'
                        enabled: false
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 14, 0),
                    y: 5.5,
                    marker: {
                        //symbol: 'url(../../../../img/notWell.png)'
                        enabled: false
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 18, 0),
                    y: 5.5,
                    marker: {
                        //symbol: 'url(../../../../img/good.png)'
                        enabled: false
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 21, 30),
                    y: 5.5,
                    marker: {
                        //symbol: 'url(../../../../img/good.png)'
                        enabled: false
                    }
                }]
            }, {
                name: 'Median glucose',
                data: [{
                    x: Date.UTC(2017, 10, 3, 3, 30),
                    y: 8,
                    marker: {
                        //symbol: 'url(../../../../img/notWell.png)'
                        enabled: false
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 9, 0),
                    y: 8,
                    marker: {
                        //symbol: 'url(../../../../img/good.png)'
                        enabled: false
=======
                plotOptions: {
                    series: {
                        lineWidth: 0
>>>>>>> 7cdd22fbed7aea7550bda07899bf33f35e599468
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 14, 0),
                    y: 8,
                    marker: {
                        //symbol: 'url(../../../../img/good.png)'
                        enabled: false
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 18, 0),
                    y: 8,
                    marker: {
                        //symbol: 'url(../../../../img/good.png)'
                        enabled: false
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 21, 30),
                    y: 8,
                    marker: {
                        //symbol: 'url(../../../../img/good.png)'
                        enabled: false
                    }
                }]
            }, {
                name: 'Likelihood of low glucose',
                data: [{
                    x: Date.UTC(2017, 10, 3, 3, 30),
                    y: 10.5,
                    marker: {
                        enabled: false,
                        fillColor: 'red',
                        radius: 20,
                        symbol: 'circle'
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 9, 0),
                    y: 10.5,
                    marker: {
                        enabled: false,
                        fillColor: 'orange',
                        radius: 20,
                        symbol: 'circle'
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 14, 0),
                    y: 10.5,
                    marker: {
                        enabled: false,
                        fillColor: 'red',
                        radius: 20,
                        symbol: 'circle'
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 18, 0),
                    y: 10.5,
                    marker: {
                        enabled: false,
                        fillColor: 'green',
                        radius: 20,
                        symbol: 'circle'
                    }
                }, {
                    x: Date.UTC(2017, 10, 3, 21, 30),
                    y: 10.5,
                    marker: {
                        enabled: false,
                        fillColor: 'green',
                        radius: 20,
                        symbol: 'circle'
                    }
                }] 
            }*/
           /*
           series: [{
                    name: 'Hypo1',
                    type: 'polygon',
                    data: [
                        [Date.UTC(2017, 10, 3, -1, 0), 0],
                        [Date.UTC(2017, 10, 3, -1, 0), 10],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 10],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 0]
                    ],
                    color: '#c6ecd9'
                },
                {
                    name: 'Hyper1',
                    type: 'polygon',
                    data: [
                        [Date.UTC(2017, 10, 3, -1, 0), 10],
                        [Date.UTC(2017, 10, 3, -1, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 10]
                    ],
                    color: '#ffd9b3'
                },
<<<<<<< HEAD
                 {
                    name: 'Hypo2',
                    type: 'polygon',
                    data: [
                        [Date.UTC(2017, 10, 3, 6.5, 0), 0],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 10],
                        [Date.UTC(2017, 10, 3, 12.5, 0), 10],
                        [Date.UTC(2017, 10, 3, 12.5, 0), 0]
                    ],
                    color: '#ffcccc'
                },
                 {
                    name: 'Hyper2',
                    type: 'polygon',
                    data: [
                        [Date.UTC(2017, 10, 3, 6.5, 0), 10],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 20],
                        [Date.UTC(2017, 10, 3, 12.5, 0), 20],
                        [Date.UTC(2017, 10, 3, 12.5, 0), 10]
                    ],
                    marker: '92 %',
                },
                 {
                    name: 'Hypo3',
                    type: 'Hyper3',
                    data: [
                        [Date.UTC(2017, 10, 3, -1, 0), 10],
                        [Date.UTC(2017, 10, 3, -1, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 10]
                    ],
                },
                {
                    name: 'Hypo3',
                    type: 'Hyper3',
                    data: [
                        [Date.UTC(2017, 10, 3, -1, 0), 10],
                        [Date.UTC(2017, 10, 3, -1, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 10]
                    ],
                },
                {
                    name: 'Hypo3',
                    type: 'Hyper3',
                    data: [
                        [Date.UTC(2017, 10, 3, -1, 0), 10],
                        [Date.UTC(2017, 10, 3, -1, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 10]
                    ],
                },{
                    name: 'Hypo3',
                    type: 'Hyper3',
                    data: [
                        [Date.UTC(2017, 10, 3, -1, 0), 10],
                        [Date.UTC(2017, 10, 3, -1, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 10]
                    ],
=======
                xAxis: {
                    type: 'datetime',
                    min: new Date(new Date().setDate(3)).setHours(0, 0, 0, 0),
                    max: new Date(new Date().setDate(3)).setHours(24, 0, 0, 0),
                    tickInterval: 3600 * 1000,
                    plotLines: []
                },
                yAxis: {
                    title: {
                        text: ''
                    },
                    labels:{
                        enabled: false
                    },
                    min: 2,
                    max: 5,
                    lineColor: '#2E2E1E',
                    lineWidth: 2,
                    gridLineWidth: 0,
                    minorGridLineWidth: 0
                },
                series: [{
                    name: 'Variability below median',
                    data: [{
                            x: Date.UTC(2017, 10, 3, 3, 30),
                            y: 5.5,
                            marker: {
                                enabled: false
                                }
                            },{
                                x: Date.UTC(2017, 10, 3, 9, 0),
                                y: 5.5,
                                marker: {
                                    enabled: false
                                }
                             },{
                                x: Date.UTC(2017, 10, 3, 14, 0),
                                y: 5.5,
                                marker: {
                                    enabled: false
                                }
                             },{
                                x: Date.UTC(2017, 10, 3, 18, 0),
                                y: 5.5,
                                marker: {
                                    enabled: false
                                }
                             },{
                                x: Date.UTC(2017, 10, 3, 21, 30),
                                y: 5.5,
                                marker: {
                                    enabled: false
                                }
                             }]
                }],
                noData: gettextCatalog.getString('No Data'),
                size: {
                    height: '100px'
>>>>>>> 7cdd22fbed7aea7550bda07899bf33f35e599468
                }
                ,{
                    name: 'Hypo3',
                    type: 'Hyper3',
                    data: [
                        [Date.UTC(2017, 10, 3, -1, 0), 10],
                        [Date.UTC(2017, 10, 3, -1, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 20],
                        [Date.UTC(2017, 10, 3, 6.5, 0), 10]
                    ],
                }
            ],*/

            noData: gettextCatalog.getString('No Data'),
            size: {
                height: '1px'
            }
        };

    })
    .controller('CardAuditController', function($scope, gettextCatalog, $ocLazyLoad, $injector, $rootScope) {
        $ocLazyLoad.load([
            'bower_components/pickadate/lib/themes/classic.css',
            'bower_components/pickadate/lib/themes/classic.date.css',
        ]);

        $scope.dateRange = {
            from: new Date(new Date().setDate(new Date().getDate() - 1)),
            to: new Date()
        }

        $scope.fetchResults = function() {
            if (($scope.dateRange.from && $scope.dateRange.to) && ($scope.dateRange.from <= $scope.dateRange.to)) {
                $ocLazyLoad.load('js/services/AuditService.js').then(function() {
                    var Audit = $injector.get('Audit');
                    Audit.listByDate({
                        from: $scope.dateRange.from,
                        to: $scope.dateRange.to
                    }).success(function(data) {
                        $scope.logs = data;
                    }).error(function(status, data) {
                        $rootScope.rootAlerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            priority: 2
                        });
                    });
                });
            } else {
                $rootScope.rootAlerts.push({
                    type: 'warning',
                    msg: gettextCatalog.getString('Please choose a valid date range.'),
                    priority: 3
                });
                $scope.logs = [];
            }
        }

        $scope.fetchResults();
    });