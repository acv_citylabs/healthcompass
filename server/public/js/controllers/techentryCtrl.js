angular.module('techentryCtrl', []).controller('techentryController', function($scope, $window, $ocLazyLoad, $injector) {
    $ocLazyLoad.load('js/services/techentryService.js').then(function() { 
        var techentry = $injector.get('techentry');
		$scope.labels = [
			'glucose',
			'weight',
			'fruitsVegetables',
			'walking',
			'intensiveSports',
			'blood_pressure',
			'nibbling',
			'water',
			'fishMeat',
			'starchy',
			'hotDrinks',
			'dairyProducts',
			'alcoholicBeverages'
		];
	
		$scope.minDate = "2000-01-01"
		$scope.entries = [];
		$scope.oldentries = [];
		gettechentry();
		$scope.entry = {
			datetime: new Date()
		};


		$scope.sendtechentry = function(){
			//need validation
			$scope.entries.push(angular.copy($scope.entry));
		};
		$scope.delete = function($id){
			$scope.entries.splice($id,1);
		};
		$scope.validate = function(){
			var toAdd = [];
			for	(i = 0; i < $scope.entries.length; i++) {
				var found = false;
				for	(j = 0; j < $scope.oldentries.length; j++) {
					if ($scope.entries[i]._id == $scope.oldentries[j]._id) found = true;
				}
				if (!found) toAdd.push($scope.entries[i]);
			}

			var toDel = [];
			for	(i = 0; i < $scope.oldentries.length; i++) {
				var found = false;
				for	(j = 0; j < $scope.entries.length; j++) {
					if ($scope.entries[j]._id == $scope.oldentries[i]._id) found = true;
				}
				if (!found) toDel.push($scope.oldentries[i]);
			}
				techentry.settechentry({username: $window.localStorage.userID}, toAdd, toDel).then(function(res){gettechentry();});
		};
		function gettechentry(){
			techentry.gettechentry({username: $window.localStorage.username}).then(function(res){
				$scope.oldentries = angular.copy(res.data);
				$scope.entries = angular.copy(res.data);
			});
		}
	});
	
});