angular.module('QuizzCtrl', [
    [
        'css/templates/dashboard.css',
        'css/templates/main.css'
    ]
]).controller('QuizzController', function($scope, $rootScope, gettextCatalog, $log, $ocLazyLoad, $injector,
    $stateParams, $state, ModalService) {
    $ocLazyLoad.load('js/services/QuizzService.js').then(function() {


        $scope.quizzs;
        $scope.listAllQ;
        $scope.listNcQ;
        $scope.QUIZZ_MODE;

        // --------------------------------------------------------------------
        // --- Vars declaration ---
        // --------------------------------------------------------------------
        var Quizz = $injector.get('Quizz');
        var isUserDoingQuizz = undefined;

        $scope.user.anwser = '';
        $scope.user.score = 0;
        $scope.config = {
            quizzmodule: [
                { name: gettextCatalog.getString('Alimentation') },
                { name: gettextCatalog.getString('Medication') },
                { name: gettextCatalog.getString('Quiz 1 : Experimental') },
                { name: gettextCatalog.getString('Physical Activity') }
            ]
        };

        // --------------------------------------------------------------------
        // --- Functions declaration ---
        // --------------------------------------------------------------------

        $scope.bookmark = bookmark;
        $scope.filterQuizzs = filterQuizzs;
        $scope.initQuizz = initQuizz;
        $scope.toggleMode = toggleMode;
        $scope.next = next;
        $scope.showScore = showScore;
        $scope.closeScore = closeScore;
        $scope.verify = verify;
        $scope.popup = popup;

        // --------------------------------------------------------------------
        // --- Controller's activation ---
        // --------------------------------------------------------------------

        activate();

        function activate() {

            $scope.QUIZZ_MODE = Quizz.getQuizzMode();

            // If we are in the quizz menu :
            if (!$stateParams.id) {
                $log.debug("Activating quizz menu");
                isUserDoingQuizz = false;
                $scope.mode = {
                    status: false,
                    title: gettextCatalog.getString("All Questions"),
                    text: gettextCatalog.getString("To complete"),
                }

                $scope.helper = {
                    title: gettextCatalog.getString("Uncompleted questions appear here"),
                    //text: gettextCatalog.getString("Use the heart to add to favorites")
                };
                if (Quizz.getAllQuizzes().length == 0) {
                    // $log.debug("Service hasn't got the quizzes");
                    getAllQuizzs(function() {
                        // $log.debug("All after activation (l : " + $scope.listAllQ.length + ")");
                        for (var i = 0; i < $scope.listAllQ.length; ++i) {
                            // $log.debug("elem id : " + $scope.listAllQ[i].id);
                        }

                    });
                } else {
                
                    // $log.debug("Service already has the quizzes");
                    refreshQuizzes(function callback() {
                        // $log.debug("All after activation (l : " + $scope.listAllQ.length + ")");
                        for (var i = 0; i < $scope.listAllQ.length; ++i) {
                            // $log.debug("elem id : " + $scope.listAllQ[i].id);
                        }
                    });
                }
            }
            // If the user is doing a particular quizz : 
            else if ($stateParams.id && $stateParams.id !== "") {
                // $log.debug("Activating one quizz : " + $stateParams.id);
                isUserDoingQuizz = true;

                if (Quizz.getAllQuizzes().length == 0) {
                    // $log.debug("Service hasn't got the quizzes");
                    getAllQuizzs(function() {
                        // $log.debug("All after activation (l : " + $scope.listAllQ.length + ")");
                        for (var i = 0; i < $scope.listAllQ.length; ++i) {
                            // $log.debug("elem id : " + $scope.listAllQ[i].id);
                        }
                    });
                } else {
                    // $log.debug("Service already has the quizzes");
                    refreshQuizzes(function callback() {
                        // $log.debug("All after activation (l : " + $scope.listAllQ.length + ")");
                        for (var i = 0; i < $scope.listAllQ.length; ++i) {
                           //  $log.debug("elem id : " + $scope.listAllQ[i].id);
                        }
                    });
                }

                getOneQuizz($stateParams.id);
            }
            // If we are lost :
            else {
                $state.go("home.quizzs.main");
            }
        }

        // --------------------------------------------------------------------
        // --- Functions' definitions ---
        // --------------------------------------------------------------------        

        /**
         * Bookmark a Quizz (= mark as done)
         */
        function bookmark(id, done) {
            Quizz.bookmark({ id: id }).success(function(data) {
                refreshQuizzes(function callback() {
                    if (isUserDoingQuizz) {
                        getOneQuizz($stateParams.id);
                    }
                    done();
                });
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type: 'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                    priority: 2
                });
                done();
            });
        }

        /**
         * Retrieve a Quizz
         */
        function getOneQuizz(id) {
            Quizz.read(id).success(function(data) {
                if (data.image) {
                    $scope.image = { 'background': 'url(' + data.image + ') no-repeat center center #fff', 'background-size': 'contain', 'height': '300px' };
                }
                $scope.quizz = data;
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type: 'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                    priority: 2
                });
            });
        }

        /**
         * Retrieve all Quizzs (use only to init on the menu page).
         */
        function getAllQuizzs(callback) {
            Quizz.list().success(function(data) {
                Quizz.setQuizzes(data);
                $scope.quizzs = data;
                $scope.listAllQ = data;
                $scope.listNcQ = [];
                $scope.listSuccess = [];
                var score = 0;

                // Sort achieved and not achieved questions
                for (i = 0; i < data.length; i++) {
                    if (data[i].bookmarked) {
                        score++;
                        $scope.listSuccess.push(data[i]);
                    } else {
                        $scope.listNcQ.push(data[i]);
                    }
                }
                Quizz.setQuizzesBeingDone($scope.listAllQ);
                Quizz.setNonCompletedQuizzes($scope.listNcQ);
                Quizz.setSuccessfulQuizzes($scope.listSuccess);
                // Global score
                $scope.user.score = ((score / data.length) * 100).toFixed(0);
                Quizz.setScore($scope.user.score);

                // $log.debug("Success :", $scope.listSuccess, "Not completed", $scope.listNcQ, "Score", $scope.user.score);
                callback();
            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type: 'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                    priority: 2
                });
                callback();
            });
        }

        /**
         * Refresh the quizz list to get the accurate "bookmarked" values
         * and an updated score.
         */
        function refreshQuizzes(callback) {
            Quizz.list().success(function(data) {
                Quizz.setQuizzes(data);
                $scope.quizzs = data;
                $scope.listAllQ = Quizz.getQuizzesBeingDone();
                $scope.listNcQ = Quizz.getNonCompletedQuizzes();
                $scope.listSuccess = Quizz.getSuccessfulQuizzes();
                var score = 0;

                $log.debug("Refreshing quizzes!");
                $log.debug("All after refresh (l : " + $scope.listAllQ.length + ")");
                for (var i = 0; i < $scope.listAllQ.length; ++i) {
                    $log.debug("elem id : " + $scope.listAllQ[i].id);
                }

                // Sort achieved and not achieved questions
                for (i = 0; i < data.length; i++) {
                    if (data[i].bookmarked) {
                        score++;
                    }
                }
                // Global score
                $scope.user.score = ((score / data.length) * 100).toFixed(0);
                Quizz.setScore($scope.user.score);
                callback();

            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type: 'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                    priority: 2
                });
                callback();
            });
        }

        /**
         * Return quizz according to whether the user want only
         * quizzs that have not been done or all quizz.
         * 
         */
        function filterQuizzs(item) {
            // $log.debug("In filterQuizzs");
            if ($scope.mode.status) {
                return item.bookmarked === false;
            } else {
                return item;
            }
        }

        /**
         * Init the quizzes lists and starts the first quizz.
         */
        function initQuizz(bool) {
            $scope.QUIZZ_MODE = !bool ? 'All' : 'NC';

            Quizz.setQuizzMode($scope.QUIZZ_MODE);
            getAllQuizzs(function() {


                $log.debug("All before init (l : " + $scope.listAllQ.length + ")");
                for (var i = 0; i < $scope.listAllQ.length; ++i) {
                    $log.debug("elem id : " + $scope.listAllQ[i].id);
                }

                var first_id;
                if ($scope.QUIZZ_MODE == 'All') {
                    first_id = $scope.listAllQ.pop().id;
                    Quizz.setQuizzesBeingDone($scope.listAllQ);
                } else {
                    first_id = $scope.listNcQ.pop().id;
                    Quizz.setNonCompletedQuizzes($scope.listNcQ);
                }

                // Initialize progress bar
                var quizzLength = $scope.QUIZZ_MODE == 'All' ? $scope.listAllQ.length : $scope.listNcQ.length;
                Quizz.setQuizzLength(quizzLength+1);

                $log.debug("First id :", first_id);
                $log.debug("All after init: (l : " + $scope.listAllQ.length + ") ");
                for (var i = 0; i < $scope.listAllQ.length; ++i) {
                    $log.debug("elem id : " + $scope.listAllQ[i].id);
                }
                $log.debug("NC :", $scope.listNcQ);
                $state.go("home.quizzs.one", { id: first_id });
            });
        }

     

        var loadProgressBar = function(score) {
             /*$scope.progress =  ($scope.quizzLength - $scope.listAllQ.length) * 100 / $scope.quizzLength;
            var tmp = $scope.progress+'%';*/
            $scope.lenbar= Quizz.getProgressPercent() ;
            $(".progress-bar").animate({
                width: String($scope.lenbar)+"%"
            }, 500)
        }

        loadProgressBar();


        /**
         * Toggle mode (all Quizzs/favorite Quizzs)
         */
        function toggleMode() {
            if (!$scope.mode.status) {
                $scope.mode.title = gettextCatalog.getString("All questions");
            } else {
                $scope.mode.title = gettextCatalog.getString("Completed questions");
            }
        }

        /**
         * Bookmark the current quizz and go to the next quizz
         */
        function next(bool, id) {
            // ASSIGN SCORE TO CURRENT QUESTION
            //    $log.debug("Quizz :", quizz);
            Quizz.read(id).success(function(data) {
                quizz = data;
                // If question was not yet completed or is now false, change tag.
                if (!quizz.bookmarked && bool || quizz.bookmarked && !bool) {
                    bookmark(id, function done() {

                        quizz.bookmarked = true;
                        $scope.popup(bool, id);

                        goToNext();

                    });
                } else {
                    $scope.popup(bool, id);
                    goToNext();
                }

            }).error(function(status, data) {
                $rootScope.rootAlerts.push({
                    type: 'danger',
                    msg: gettextCatalog.getString('An error occurred, please try again later'),
                    priority: 2
                });
            });
        }

        /**
         * Check quizz mode to select the next quizz and check quizz list to
         * see if we've already done all the quizzes and need to see the final
         * score.
         */
        function goToNext() {

            // NAVIGATE TO THE NEXT QUESTION
            $log.debug("Nav Quizz mode :", $scope.QUIZZ_MODE);
            $log.debug("All before next  (l : " + $scope.listAllQ.length + ") ");
            for (var i = 0; i < $scope.listAllQ.length; ++i) {
                $log.debug("elem id : " + $scope.listAllQ[i].id);
            }
            var new_id;
            switch ($scope.QUIZZ_MODE) {
                case 'All': // ALL
                    if (typeof $scope.listAllQ != undefined && $scope.listAllQ.length > 0) {
                        $log.debug("list length : " + $scope.listAllQ.length);
                        // Remove one element (FIFO)
                        new_id = $scope.listAllQ.pop().id;
                        Quizz.setQuizzesBeingDone($scope.listAllQ);
                        $log.debug("NEW ID :", new_id);
                        $log.debug("All after next: (l : " + $scope.listAllQ.length + ") ");
                        for (var i = 0; i < $scope.listAllQ.length; ++i) {
                            $log.debug("elem id : " + $scope.listAllQ[i].id);
                        }
                        $log.debug("NC :", $scope.listNcQ);
                        $state.go("home.quizzs.one", { id: new_id });
                    } else {
                        $log.debug("Showing score!!")
                        var score = Quizz.refreshScore();
                        $scope.user.score = score;
                        // SHOW SCORE
                        $log.debug("All :", $scope.listAllQ);
                        $scope.showScore($scope.user.score, $scope.quizzs);
                    }
                    break;
                case 'NC':
                    if (typeof $scope.listNcQ != undefined && $scope.listNcQ.length > 0) {
                        // Remove one element (FIFO)
                        new_id = $scope.listNcQ.pop().id;
                        Quizz.setNonCompletedQuizzes($scope.listNcQ);
                        $log.debug("NEW ID :", new_id);
                        $state.go("home.quizzs.one", { id: new_id });
                    } else {
                        var score = Quizz.refreshScore();
                        $scope.user.score = score;
                        // SHOW SCORE                        
                        $log.debug("NC :", $scope.listNcQ);
                        // var moder = Quizz.getQuizzMode();
                        //$log.debug("NCCCC QUZI MODE DOIT ETRE FALSE :",moder);

                        $scope.showScore($scope.user.score, $scope.quizzs);
                    }
                    break;
            }

        }

        /**
         * Show the score in a modal.
         */
        function showScore(score, quizzs) {
            $log.debug("show score 1: " + score + " : " + $scope.user.score);
            $('#scoreModal').modal(); // jshint ignore:line
        }


        /**
         * Close the score modal and go to quizz menu
         */
        function closeScore() {
            $("#scoreModal").on('hidden.bs.modal', function() {
                $state.go("home.quizzs.main");
            });
            $('#scoreModal').modal("hide"); // jshint ignore:line


        }

        /**
         * Check if the user answer is valid.
         */
        function verify() {
            var answer = $scope.user.answer;
            //$log.debug("Answer :", answer);
            var ok = (answer != '' && answer != undefined) ? true : false;
            return ok;
        }

        /**
         * Create an alert with a message indicating whether the answer was
         * correct or not.
         */
        function popup(bool, id) {

            var typePop = (bool) ? 'success' : 'danger';
            var msgPop = (bool) ? 'Answer ' + id + ' is correct !' : 'Answer ' + id + ' was wrong :(';


            // $("div.mysuccess").fadeIn(300).delay(1500).fadeOut(400);



            /*ngNotify.config({
                theme: 'pastel',
                type: 'success',
                position: 'top',
                duration: 2000,
                button: false
            });*/


            //Notification('Primary notification');

           /* ngNotify.set(msgPop, {
                target: '#mysuccess'
            }); */


        }
    });
});