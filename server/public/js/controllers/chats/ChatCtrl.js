angular.module('ChatCtrl', []).controller('ChatController', function($log, $scope, $window, gettextCatalog, $stateParams, $state, $ocLazyLoad,
    $injector, $rootScope, ModalService, deviceDetector) { //) {
    $ocLazyLoad.load('js/services/ChatService.js').then(function() {

        var JITSI_APP_STORE = 'https://itunes.apple.com/us/app/jitsi-meet/id1165103905?mt=8';
        var JITSI_PLAY_STORE = 'https://play.google.com/store/apps/details?id=org.jitsi.meet&hl=en';

        var Chat = $injector.get('Chat');
        $scope.hideOptions = true;
        $scope.forbidden = false;
        // keep a copy of the root scope
        var rootScope = $scope.$root;

        // contactUsername is the username of the contact if its account still exists,
        // otherwise it is underscore+old ID ("_23ANID3236234...")
        $scope.contactUsername = $stateParams.username;

        // check whether the contact account has been deleted
        $scope.contactIsDeleted = ($stateParams.username[0] === '_');

        if ($scope.contactUsername !== "") {
            $scope.alerts = [];
            $scope.isWebRTCSupported = $window.mozRTCPeerConnection || $window.webkitRTCPeerConnection;
            $scope.$root.isWebRTCActive = false;
            $scope.$root.callDirection = 'outgoing';
            $scope.isFullscreen = false;
            $scope.messages = [];
            $scope.accountDeleted = gettextCatalog.getString("Deleted account");
            var latestEventDate = null;
            var readChatInterval = null;
            var periodChat = 1000; // 1 second

            if (!$scope.contactIsDeleted) {
                $ocLazyLoad.load('js/services/ContactService.js').then(function() {
                    var Contact = $injector.get('Contact');
                    Contact.readLight({
                        username: $scope.contactUsername
                    }).success(function(profile) {
                        $scope.contactAvatar = profile.avatar;
                        $scope.contactUsername = profile.username;
                    });
                });
            }

            function readChat() {
                if (readChatInterval) {
                    clearInterval(readChatInterval);
                    readChatInterval = null;
                }

                readChatTimer();
                readChatInterval = setInterval(function() {
                    readChatTimer();
                }, periodChat);
            }

            var onWebRTC = function(data) {
                //                console.log("onWebRTC : status : ", data);
                if (rootScope.callStatus !== data.status) rootScope.callStatus = data.status;
                if (rootScope.callDirection !== data.callDirection) rootScope.callDirection = data.callDirection;
            }

            function readChatTimer() {
                var command;
                if ($scope.$root.ringing || $scope.$root.calling) {
                    command = 'token';
                } else {
                    command = 'poll';
                }

                Chat.webrtc({
                    username: $stateParams.username,
                    command: command
                }).success(function(data) {
                    onWebRTC(data);
                });

                Chat.readChat({
                    username: $scope.contactUsername,
                    afterDate: latestEventDate
                }).success(function(data) {
                    if ($scope.alerts.length > 0) {
                        $scope.alerts = [];
                    }
                    if (data.forbidden) {
                        $scope.forbidden = true;
                        $scope.alerts.push({
                            type: 'warning',
                            msg: gettextCatalog.getString("You cannot reply to this conversation"),
                            show: true
                        });
                    }

                    if (data.archived !== null) {
                        $scope.hideOptions = false;
                        $scope.archived = data.archived;
                    }

                    // append to the current array
                    for (var i = 0; i < data.chat.length; i++) {
                        var documentDate = new Date(data.chat[i].datetime);
                        if (!latestEventDate || (documentDate > latestEventDate))  {
                            $scope.messages.push(data.chat[i]);
                            latestEventDate = documentDate;
                        }
                    }
                }).error(function(status, data) {
                    if ($scope.alerts.length === 0) {
                        $scope.alerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            show: true
                        });
                    }
                });
            }
            readChat();

            $scope.sendMessage = function() {
                if ($scope.message && !$scope.contactIsDeleted) {
                    var message = $scope.message;
                    $scope.message = "";
                    Chat.sendMessage({
                        username: $stateParams.username,
                        msg: message
                    }).success(function(data) {
                        if ($scope.alerts.length > 0) {
                            $scope.alerts = [];
                        }
                        readChat();
                    }).error(function(status, data) {
                        $scope.message = message;
                        if ($scope.alerts.length === 0) {
                            $scope.alerts.push({
                                type: 'danger',
                                msg: gettextCatalog.getString('An error occurred, please try again later'),
                                show: true
                            });
                        }
                    });
                }
            };

            $scope.toggleArchiveFlag = function() {
                var newFlag = !$scope.archived;
                Chat.setArchiveFlag({
                    username: $scope.contactUsername,
                    flag: newFlag
                }).success(function(data) {
                    if ($scope.alerts.length > 0) {
                        $scope.alerts = [];
                    }
                }).error(function(status, data) {
                    if ($scope.alerts.length === 0) {
                        $scope.alerts.push({
                            type: 'danger',
                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                            show: true
                        });
                    }
                });
            }

            $scope.deleteChat = function() {
                ModalService.showModal({
                    templateUrl: "templates/modals/deleteChat.html",
                    controller: function($scope, close) {
                        $scope.chat = { username: $stateParams.username };
                        $scope.close = function(result) {
                            close(result, 500); // close, but give 500ms for bootstrap to animate
                        };
                    }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
                        if (result) {
                            Chat.deleteChat({
                                username: $stateParams.username
                            }).success(function(data) {
                                if ($scope.alerts.length > 0) {
                                    $scope.alerts = [];
                                }
                                $state.go("home.chats.main");
                            }).error(function(status, data) {
                                if ($scope.alerts.length === 0) {
                                    $scope.alerts.push({
                                        type: 'danger',
                                        msg: gettextCatalog.getString('An error occurred, please try again later'),
                                        show: true
                                    });
                                }
                            });
                        }
                    });
                });
            }

            $scope.toggleFullScreen = function() {
                $scope.isFullscreen = !$scope.isFullscreen;
            };

            $scope.$on('$stateChangeStart', function(event) {
                if (readChatInterval) {
                    clearInterval(readChatInterval);
                    readChatInterval = null;
                }
            });



            /* ========================= JITSI =========================================================================*/
            var api, name, role, buttons, storelink;

            $scope.showDeviceWarning = function(storelink, room) {
                $log.debug("ismobile :", $scope.isMobile);
                ModalService.showModal({
                    templateUrl: "templates/modals/jitsiMobile.html",
                    controller: function($scope, close) {
                        $scope.room = room;
                        $scope.storelink = storelink;

                        $scope.goToUrl = function(url) {
                            $window.location.href = url;
                        }

                        $scope.close = function(result) {
                            close(result, 500); // close, but give 500ms for bootstrap to animate
                        };
                    }
                }).then(function(modal) {
                    modal.element.modal();
                    modal.close.then(function(result) {
                        $state.go("home.chats.one");
                    });
                });
            }

            $scope.isConnected = false;
            $scope.launchjitsi = function(customDom, storelink) {
                //Get user profile
                // Allows to detect if the OS user is Android or iOS, in order to advise him to download the mobile Jitsi App 

                var myDom, myHosts, myBosh, storelink;
                switch (customDom) {
                    case 'UCL':
                        myDom = 'jitsi.acv.tele.crt1.net';
                        myHosts = {
                            domain: myDom,
                            muc: 'conference.jitsi.acv.tele.crt1.net',
                            focus: 'focus.jitsi.acv.tele.crt1.net' // FIXME: use XEP-0030
                        };
                        myBosh = '//jitsi.acv.tele.crt1.net/http-bind';
                        break;
                    case 'JITSI':
                        myDom = 'meet.jit.si';
                        myHosts = {
                            domain: myDom,
                            muc: 'conference.meet.jit.si', // FIXME: use XEP-0030
                            focus: 'focus.meet.jit.si'
                        };
                        myBosh = '//meet.jit.si/http-bind';
                        break;
                    default:
                        myDom = 'meet.jit.si';
                        myHosts = {
                            domain: myDom,
                            muc: 'conference.meet.jit.si', // FIXME: use XEP-0030
                            focus: 'focus.meet.jit.si',
                        };
                        myBosh = '//meet.jit.si/http-bind';
                        break;
                }

                // $scope.deviceDetector = deviceDetector;
                // DETECT ANDROID OR IOS
                $scope.isMobile = deviceDetector.raw.os.ios || deviceDetector.raw.os.android ? true : false;
                if ($scope.isMobile) {
                    storelink = deviceDetector.raw.os.ios ? JITSI_APP_STORE : JITSI_PLAY_STORE;
                }

                var config = { // eslint-disable-line no-unused-vars
                    //    configLocation: './config.json', // see ./modules/HttpConfigFetch.js
                    /* hosts: {
                        domain: 'jitsi.acv.tele.crt1.net',
                        muc: 'conference.jitsi.acv.tele.crt1.net' // FIXME: use XEP-0030
                    }, */
                    hosts: myHosts,
                    bosh: myBosh, //'//jitsi.acv.tele.crt1.net/http-bind', // FIXME: use xep-0156 for that


                    //  getroomnode: function (path) { return 'someprefixpossiblybasedonpath'; },
                    //  useStunTurn: true, // use XEP-0215 to fetch STUN and TURN server
                    //  useIPv6: true, // ipv6 support. use at your own risk
                    useNicks: false,
                    //clientNode: 'http://jitsi.org/jitsimeet', // The name of client node advertised in XEP-0115 'c' stanza
                    focusUserJid: 'focus@auth.jitsi.acv.tele.crt1.net', // The real JID of focus participant - can be overridden here
                    //defaultSipNumber: '', // Default SIP number

                    // Desktop sharing method. Can be set to 'ext', 'webrtc' or false to disable.
                    desktopSharingChromeMethod: 'ext',
                    // The ID of the jidesha extension for Chrome.
                    desktopSharingChromeExtId: 'diibjkoicjeejcmhdnailmkgecihlobk',
                    // The media sources to use when using screen sharing with the Chrome
                    // extension.
                    desktopSharingChromeSources: ['screen', 'window', 'tab'],
                    // Required version of Chrome extension
                    desktopSharingChromeMinExtVersion: '0.1',

                    // The ID of the jidesha extension for Firefox. If null, we assume that no
                    // extension is required.
                    desktopSharingFirefoxExtId: null,
                    // Whether desktop sharing should be disabled on Firefox.
                    desktopSharingFirefoxDisabled: true,
                    // The maximum version of Firefox which requires a jidesha extension.
                    // Example: if set to 41, we will require the extension for Firefox versions
                    // up to and including 41. On Firefox 42 and higher, we will run without the
                    // extension.
                    // If set to -1, an extension will be required for all versions of Firefox.
                    desktopSharingFirefoxMaxVersionExtRequired: -1,
                    // The URL to the Firefox extension for desktop sharing.
                    desktopSharingFirefoxExtensionURL: null,

                    // Disables ICE/UDP by filtering out local and remote UDP candidates in signalling.
                    webrtcIceUdpDisable: false,
                    // Disables ICE/TCP by filtering out local and remote TCP candidates in signalling.
                    webrtcIceTcpDisable: false,

                    openSctp: true, // Toggle to enable/disable SCTP channels
                    disableStats: false,
                    disableAudioLevels: false,
                    channelLastN: -1, // The default value of the channel attribute last-n.
                    adaptiveLastN: false,
                    //disableAdaptiveSimulcast: false,
                    enableRecording: false,
                    enableWelcomePage: true,
                    //enableClosePage: false, // enabling the close page will ignore the welcome
                    // page redirection when call is hangup
                    disableSimulcast: false,
                    logStats: false, // Enable logging of PeerConnection stats via the focus
                    //    requireDisplayName: true, // Forces the participants that doesn't have display name to enter it when they enter the room.
                    //    startAudioMuted: 10, // every participant after the Nth will start audio muted
                    //    startVideoMuted: 10, // every participant after the Nth will start video muted
                    //    defaultLanguage: "en",
                    // To enable sending statistics to callstats.io you should provide Applicaiton ID and Secret.
                    //    callStatsID: "", // Application ID for callstats.io API
                    //    callStatsSecret: "", // Secret for callstats.io API
                    /*noticeMessage: 'Service update is scheduled for 16th March 2015. ' +
                    'During that time service will not be available. ' +
                    'Apologise for inconvenience.',*/
                    disableThirdPartyRequests: false,
                    minHDHeight: 540,
                    // If true - all users without token will be considered guests and all users
                    // with token will be considered non-guests. Only guests will be allowed to
                    // edit their profile.
                    enableUserRolesBasedOnToken: false,
                    // Suspending video might cause problems with audio playback. Disabling until these are fixed.
                    disableSuspendVideo: true
                };


                buttons = [
                    //main toolbar
                    'microphone', 'camera', 'fullscreen', 'chat'
                    //extended toolbar
                    //'contacts', 'chat', 'raisehand', 'etherpad', 'settings', 'sharedvideo'
                ];

                // settings = ['language', 'devices', 'moderator'];
                settings = ['language', 'devices'];


                var interfaceConfig = {

                    // DEFAULT_REMOTE_DISPLAY_NAME: name,
                    // DEFAULT_LOCAL_DISPLAY_NAME: "me",
                    SHOW_JITSI_WATERMARK: false,
                    // if watermark is disabled by default, it can be shown only for guests
                    SHOW_WATERMARK_FOR_GUESTS: false,
                    // BRAND_WATERMARK_LINK: "",
                    SHOW_POWERED_BY: false,
                    GENERATE_ROOMNAMES_ON_WELCOME_PAGE: false,
                    APP_NAME: "Eglé - City Labs",
                    LANG_DETECTION: true,
                    SHOW_CONTACTLIST_AVATARS: true,
                    //  CLOSE_PAGE_GUEST_HINT: false,
                    ENABLE_FEEDBACK_ANIMATION: false,
                    LANG_DETECTION: true,
                    //  DISABLE_FOCUS_INDICATOR: false,
                    //  DISABLE_DOMINANT_SPEAKER_INDICATOR: false,
                    TOOLBAR_BUTTONS: buttons, // jshint ignore:line
                    /**
                     * Main Toolbar Buttons
                     * All of them should be in TOOLBAR_BUTTONS
                     */
                    MAIN_TOOLBAR_BUTTONS: ['microphone', 'camera', 'fullscreen'],
                    MOBILE_APP_PROMO: false
                    // jshint ignore:line
                };

                // var domain = 'jitsi.acv.tele.crt1.net'; //"meet.jit.si";

                var domain = myDom;
                // compute the roomID (the two usernamespublic/templates/chats/one.html sorted alphabetically)
                var roomID = null;
                if ($rootScope.user.username.localeCompare($scope.contactUsername) < 0) {
                    roomID = ($rootScope.user.username).substring(0, 4) + ($scope.contactUsername).substring(0, 4);
                } else {
                    roomID = ($scope.contactUsername).substring(0, 4) + ($rootScope.user.username).substring(0, 4);
                }

                room = roomID.replace(/\s/g, "");
                var width = "100%";
                var height = 0.75 * ($window.innerHeight);
                // var room_token = $window.sessionStorage.roomToken;
                var htmlElement = document.querySelector('#meet');

                if ($scope.isMobile) {
                    var room_url = 'https://' + myDom + '/' + room;
                    $scope.showDeviceWarning(storelink, room_url);
                } else {
                    api = new JitsiMeetExternalAPI(domain, room, width, height, htmlElement, config, interfaceConfig);
                    api.executeCommand('displayName', $rootScope.user.username);
                    $scope.isConnected = true;
                }

            }


            $scope.closejitsi = function() {
                api.dispose();
                $scope.isConnected = false;
                $state.go("home.main");
                $rootScope.rootAlerts.push({
                    type: 'info',
                    msg: gettextCatalog.getString("You left the session."),
                    priority: 3
                });
            }


            if ($scope.isConnected) {
                $scope.$on('$locationChangeStart', function(event) {
                    $scope.closejitsi();
                });
            }

            // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = //
            //    __      __           _         ___     _____     ___     //
            //    \ \    / /   ___    | |__     | _ \   |_   _|   / __|    //
            //     \ \/\/ /   / -_)   | '_ \    |   /     | |    | (__     //
            //      \_/\_/    \___|   |_.__/    |_|_\    _|_|_    \___|    //
            //    _|"""""|  _|"""""| _|"""""| _|"""""| _|"""""| _|"""""|   //
            //    "`-0-0-'  "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-' "`-0-0-'   //
            // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = //
            //         _       __       __     ____  ______ ______         //
            //        | |     / /___   / /_   / __ \/_  __// ____/         //
            //        | | /| / // _ \ / __ \ / /_/ / / /  / /              //
            //        | |/ |/ //  __// /_/ // _, _/ / /  / /___            //
            //        |__/|__/ \___//_.___//_/ |_| /_/   \____/            //
            // = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = //
            if (!$scope.contactIsDeleted && $scope.isWebRTCSupported) {
                $ocLazyLoad.load('additional_components/socket.io/socket.io.0.9.16.js').then(function() {
                    $ocLazyLoad.load('additional_components/simplewebrtc/simplewebrtc.bundle-2.1.0.js').then(function() {
                        $ocLazyLoad.load('additional_components/webrtc/webrtc.js').then(function() {

                            rootScope.callStatus = 'inactive';

                            // compute the roomID (the two usernames sorted alphabetically and separated by '_')
                            // and keep it as a global variable
                            roomID = null;
                            if ($rootScope.user.username.localeCompare($scope.contactUsername) < 0) {
                                roomID = $rootScope.user.username + '_' + $scope.contactUsername;
                            } else {
                                roomID = $scope.contactUsername + '_' + $rootScope.user.username;
                            }

                            var getSignalingToken = function _getSignalingToken(cb) {
                                Chat.getSignalingToken(roomID).success(function(token) {
                                    return cb(null, token);
                                }).error(function(err) {
                                    console.error('Error: could not get a signaling token for room "' + roomID + '" ', arguments);
                                    if ($scope.alerts.length === 0) {
                                        $scope.alerts.push({
                                            type: 'danger',
                                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                                            show: true
                                        });
                                    }
                                    return cb(null);
                                });
                            }

                            if (!rootScope.webrtc) {
                                w =
                                    rootScope.webrtc = new WebRTC($rootScope.user.username, roomID, "https://beta.egle.be:8888", getSignalingToken);
                                rootScope.webrtc.onStopped = function() { rootScope.isWebRTCActive = false; }
                                rootScope.webrtc.onMediaClosed = function() { rootScope.isWebRTCActive = false; }
                                rootScope.webrtc.onMediaOpened = function() { rootScope.isWebRTCActive = true; }
                                rootScope.webrtc.onReadyToCall = function() { rootScope.isWebRTCActive = true; }
                            } else {
                                // ensure that webrtc is stopped and update the room for this scope
                                rootScope.webrtc.stop();
                                rootScope.webrtc.room = roomID;
                            }


                            $scope.call = function _call() {
                                // if the server responds 'inactive', activate the media before issuing the call
                                rootScope.isWebRTCActive = true;
                                if (rootScope.callStatus === 'inactive') {
                                    // we are calling, activate media
                                    rootScope.webrtc.start(true, false, false);
                                }
                                Chat.webrtc({
                                    username: $stateParams.username,
                                    command: 'call'
                                }).success(function(data) {
                                    //                                    if (data.status === 'calling' && rootScope.callStatus !== 'calling') {
                                    rootScope.webrtc.start(true, true, true);
                                    //                                    }
                                    onWebRTC(data);

                                }).error(function(status, data) {
                                    if ($scope.alerts.length === 0) {
                                        $scope.alerts.push({
                                            type: 'danger',
                                            msg: gettextCatalog.getString('An error occurred, please try again later'),
                                            show: true
                                        });
                                    }
                                    rootScope.webrtc.stop();
                                    rootScope.isWebRTCActive = false;
                                });
                            }

                            $scope.hangup = function _hangup() {
                                rootScope.webrtc.stop();
                                rootScope.isWebRTCActive = false;

                                // send hangup command to the server
                                Chat.webrtc({
                                    username: $stateParams.username,
                                    command: 'hangup'
                                }).success(function(data) {
                                    onWebRTC(data);
                                });
                            }

                            rootScope.$watch('callStatus', function(newVal, oldVal) {
                                //                                console.log("WATCH : ", oldVal, '->', newVal);
                                if (newVal === 'calling') $scope.call();
                                else if (newVal === 'inactive') $scope.hangup();
                            });

                            // Trigger when exit this route
                            $scope.$on('$stateChangeStart', function(event) {
                                if (rootScope.callStatus !== 'inactive') $scope.hangup();
                            });


                            // helper functions (for the console)
                            wr = w.webrtc.webrtc;
                            ls = wr.localStreams;
                            rs = rootScope;
                            stat = function() { console.log(w.status()); }
                            pause = function() { wr.pause(); }
                            mute = function() { wr.mute(); }
                            resume = function() { wr.resume(); }
                            call = function() { $scope.call() }
                            hangup = function() { $scope.hangup() }
                            target = function(media, signaling, peer) {
                                rootScope.webrtc.start(media, signaling, peer);
                            }
                            toggle = $scope.toggle = function() {
                                if (rootScope.webrtc.peer) target(0, 0, 0);
                                else target(1, 1, 1);
                            }
                            tgmedia = function() {
                                if (rootScope.webrtc.media) target(0, rootScope.webrtc.signaling, 0);
                                else target(1, rootScope.webrtc.signaling, rootScope.webrtc.signaling && rootScope.webrtc.peer);
                            }
                            tgsignal = function() {
                                if (rootScope.webrtc.signaling) target(rootScope.webrtc.media, 0, 0);
                                else target(rootScope.webrtc.media, 1, rootScope.webrtc.media && rootScope.webrtc.peer);
                            }
                            tgpeer = function() {
                                if (rootScope.webrtc.peer) target(rootScope.webrtc.media, rootScope.webrtc.signaling, 0);
                                else target(1, 1, 1);
                            }
                            room = function() { rootScope.webrtc.connection.emit('getRoomDescription'); }
                            toggleActive = function() { rootScope.isWebRTCActive = !rootScope.isWebRTCActive; }

                            localStreams = function() { return wr.peers[0].pc.pc.getLocalStreams(); }
                            remoteStreams = function() { return wr.peers[0].pc.pc.getRemoteStreams(); }
                            localStream = function() { return localStreams()[0]; }
                            remoteStream = function() { return remoteStreams()[0]; }
                            localTracks = function() { return localStream().getTracks() }
                            remoteTracks = function() { return remoteStream().getTracks() }

                            countStreams = function() {
                                console.log('localStreams : ' + localStreams().length);
                                console.log('remoteStreams : ' + remoteStreams().length);
                            }

                            p = function() { return wr.getPeers()[0]; }
                            ps = function() { return wr.getPeers(); }
                            pc = function() { return p().pc; }


                            addPeer = function() {
                                return w.addPeer(p().id, {
                                    audio: { receive: false, send: false },
                                    video: { receive: true, send: true }
                                })
                            }

                            showPeerTracks = function(peer) {
                                console.log('remote stream :');
                                peer.pc.getRemoteStreams()[0].getTracks().forEach(function(track) {
                                    console.log(peer.id, track.type, track.readyState ? track.readyState : 'readyState not supported');
                                });
                                console.log('local stream :');
                            }

                            showPeersTracks = function(peer) {
                                wr.getPeers().forEach(function(peer) { showPeerTracks(peer); });
                            }

                            stats = function() {
                                wr.peers.forEach(function(peer) {
                                    peer.pc.getStats(function(error, data) {
                                        var sent = 0,
                                            received = 0;
                                        console.log(data)
                                        for (var attr in data) {
                                            if (typeof data[attr]['ssrc'] !== "undefined") {
                                                console.log('SSRC  ' + ('____________' + data[attr]['ssrc']).slice(-12) + '__ :  ' + data[attr]['id']);
                                                console.log('    sent : ' + ("          " + ((typeof data[attr]['bytesSent'] !== 'undefined') ?
                                                    data[attr]['bytesSent'] : 0)).slice(-10) + ' bytes    ' + attr);
                                                console.log('received : ' + ("          " + ((typeof data[attr]['bytesReceived'] !== 'undefined') ?
                                                    data[attr]['bytesReceived'] : 0)).slice(-10) + ' bytes    ' + attr);

                                                console.log('\n');
                                            }
                                            sent += new Number((typeof data[attr]['bytesSent'] !== 'undefined') ? data[attr]['bytesSent'] : 0);
                                            received += new Number((typeof data[attr]['bytesReceived'] !== 'undefined') ? data[attr]['bytesReceived'] : 0);
                                        }
                                        console.log('total bytes sent : ', sent, '  bytes received : ', received);
                                    });
                                });
                            }


                            listCommands = function() {
                                console.log('wr            =>    w.webrtc.webrtc;');
                                console.log('ls            =>    w.webrtc.webrtc.localStreams;');
                                console.log('stat          =>    console.log(w.status());');
                                console.log('pause         =>    w.webrtc.webrtc.pause();');
                                console.log('mute          =>    w.webrtc.webrtc.mute();');
                                console.log('resume        =>    w.webrtc.webrtc.resume();');
                                console.log();
                                console.log('target        =>    rootScope.webrtc.start(media, signaling, peer);');
                                console.log();
                                console.log('toggle        =>    if (rootScope.webrtc.peer) target(0,0,0); else target(1,1,1);');
                                console.log('tgmedia       =>    switches media');
                                console.log('tgsignal      =>    switches signaling');
                                console.log();
                                console.log('toggleActive  =>    rootScope.isWebRTCActive = !rootScope.isWebRTCActive;');
                                console.log('room          =>    sends "getRoomDescription" to get an updated list of peers');
                                console.log();
                                console.log('localStreams  =>    w.webrtc.webrtc.peers[0].pc.pc.getLocalStreams();');
                                console.log('remoteStreams =>    w.webrtc.webrtc.peers[0].pc.pc.getRemoteStreams();');
                                console.log('localStream   =>    w.webrtc.webrtc.peers[0].pc.pc.getLocalStreams()[0];');
                                console.log('remoteStream  =>    w.webrtc.webrtc.peers[0].pc.pc.getRemoteStreams()[0];');
                                console.log('localTracks   =>    w.webrtc.webrtc.peers[0].pc.pc.getLocalStreams()[0].getTracks();');
                                console.log('remoteTracks  =>    w.webrtc.webrtc.peers[0].pc.pc.getRemoteStreams()[0].getTracks();');
                                console.log();
                                console.log('p             =>    w.webrtc.webrtc.getPeers()[0];');
                                console.log('pc            =>    w.webrtc.webrtc.getPeers()[0].pc;');
                                console.log();
                                console.log('stats         =>    returns a table of sent and received bytes by peer/stream/track/rtp flow');
                            }


                        }); //$ocLazyLoad.load('additional_components/webrtc/webrtc.js')
                    }); //$ocLazyLoad.load('additional_components/simplewebrtc/simplewebrtc.bundle-2.1.0.js')
                }); //$ocLazyLoad.load('additional_components/socket.io/socket.io.0.9.16.js')
            } //if WebRTC Supported
        } else {
            $state.go("home.chats.main");
        }
    });
});