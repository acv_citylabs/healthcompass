angular.module('AssistantCtrl', []).controller('AssistantController', function($scope, $window, $ocLazyLoad, $injector) {
    $ocLazyLoad.load('js/services/AssistantService.js').then(function() {
        var Assistant = $injector.get('Assistant');
        
        Assistant.getAdvice({userID: $window.localStorage.userID},0).then(function(res){
            var parsed = JSON.parse(res.data);
            $scope.question = parsed.shift();
            $scope.answers = parsed;
        });

        $scope.getAdvice = function(){
            $scope.answers = '';
            $scope.question = '';
            if(!$ocLazyLoad.isLoaded('Emergency')){
                Assistant.getAdvice({userID: $window.localStorage.userID}, $scope.selected).then(function(res){
                    $scope.selected = null;
                    var parsed = JSON.parse(res.data);
                    if (parsed.length > 1){
                        $scope.question = parsed.shift();
                        $scope.answers = parsed;
                    } else {
                        $scope.advice = parsed[0];
                    }
                });
            }
        };
    });
});