var moduleB = angular.module('app', [
    'ui.router',
    'routes',
    'oc.lazyLoad',
    'angular-jwt',
    'TokenInterceptorService',
    'rzModule',
    'datetimeFilters',
    'mappingFilters',
    'limitFilters',
    'gettext',
    'AutofocusDirective',
    'pickadate',
    'angularModalService',
    'ng.deviceDetector',
    'ngFileUpload'
]);


/* Turn ON/OFF $log, public side */
moduleB.config(function($logProvider) {
    $logProvider.debugEnabled(true);
});

var underscore = angular.module('underscore', []);
underscore.factory('_', function() {
    return window._;
});